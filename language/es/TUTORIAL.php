<?php

/**
 * pr0game powered by steemnova
 * tutorial
 * (c) 2024 Hyman
 */

$LNG['tutorial_goals_header'] = 'Metas:';
$LNG['tutorial_goals_ignore'] = '(ignorar)';

$LNG['tutorial_rewards_header'] = 'Recompensas:';

$LNG['tutorial_buttons_finalize'] = 'Completa el tutorial';
$LNG['tutorial_buttons_claim'] = 'Reclamar recompensa';
$LNG['tutorial_buttons_continue'] = 'Continuar';
$LNG['tutorial_buttons_skip'] = 'Saltar';
$LNG['tutorial_buttons_skipAll'] = 'Saltar el tutorial';
$LNG['tutorial_buttons_skipAll_confirm'] = '¿De verdad quieres saltarte el resto del tutorial? ¡No puedes reactivarlo!';

$LNG['tutorial_title']['step0'] = '¿Qué tipo de juego es este?';
$LNG['tutorial_text']['step0'] = 
'¡Bienvenido a pr0game!<br><br>
Este tutorial te mostrará los conceptos básicos del juego.<br>
Con frecuencia, se hace referencia a nuestro Wiki para obtener información adicional, ya que no todos los aspectos del juego se explican en el tutorial. 
Puedes acceder al Wiki en cualquier momento a través del menú en el lado izquierdo.<br>
Puedes saltar el tutorial en cualquier momento, pero luego no podrás reiniciarlo.<br><br>
¡Buena suerte, Emperador!';

$LNG['tutorial_title']['step1'] = '¡Empieza a ganar algo!';
$LNG['tutorial_text']['step1'] = 
'El objetivo de este juego es obtener la mayor cantidad de puntos posible. Obtienes un punto por cada 1.000 recursos gastados.
Puedes obtener estos recursos de diversas maneras. La principal es la producción propia de las minas.<br>
Primero, asegúrate de tener un suministro básico. Para ello, necesitas metal y cristal de las minas correspondientes. Para que estas funcionen, también necesitas energía del planta de energía solar.
Si no tienes suficiente energía, la producción no alcanzará el 100%.<br>
Puedes mejorar estos edificios en el menú <b>Edificios</b>. Al hacer clic en el nombre del edificio, verás el beneficio de mejorar la producción de recursos. 
También podrás ver cuánta energía consumirá el siguiente nivel una vez terminado.<br><br>
Para obtener más información, puedes consultar el <b>Wiki</b>:
<a href="https://wiki.pr0game.com/doku.php?id=en:rohstoffproduktion" target="_blank">Raw material production</a>';

$LNG['tutorial_title']['step2'] = '¡Socialízate!';
$LNG['tutorial_text']['step2'] = 
'El juego es mucho más divertido cuando interactúas con otros jugadores.
La mayoría se organiza a través de nuestro Discord. 
En la comunidad, no solo encontrarás respuestas a tus preguntas, sino que también podrás encontrar otros jugadores para jugar juntos.<br>
Si tienes alguna duda, también puedes abrir tickets aquí.';

$LNG['tutorial_title']['step3'] = '¡No te expongas sin defensa!';
$LNG['tutorial_text']['step3'] = 
'Otra forma de obtener recursos es atacando los planetas de otros jugadores.<br>
Sin embargo, esto también puede ocurrirte a ti: ¡construye tu primera defensa!<br>
Es importante tener defensa para proteger al menos tu producción nocturna.<br>
Aquí no solo importa la cantidad, sino también la composición de la flota atacante.<br>
Antes de saquear, el atacante primero debe superar tu defensa. Cada nave que envía reduce sus ganancias y aumenta potencialmente sus pérdidas por naves destruidas.<br>
Las instalaciones de defensa se construyen en el menú <b>Defensa</b> utilizando un astillero, que requiere una fábrica de robots y deuterio.<br><br>
Aquí tienes el artículo correspondiente en el Wiki:
<a href="https://wiki.pr0game.com/doku.php?id=en:verteidigung" target="_blank">Defense facilities</a>';

$LNG['tutorial_title']['step4'] = '¡Piensa a futuro!';
$LNG['tutorial_text']['step4'] = 
'Asegura tu suministro ampliando tus minas.<br>
La construcción de edificios, naves y defensa, así como la investigación, se volverán más costosas con el tiempo.
Por lo tanto, debes aumentar tu producción para satisfacer la demanda de recursos de tu planeta.
Haz clic ahora en Edificios y amplía tus instalaciones cuando tengas suficientes recursos.';

$LNG['tutorial_title']['step5'] = '¡Necesitamos naves!';
$LNG['tutorial_text']['step5'] = 
'Construye una pequeña nave de transporte.<br>
Los pequeños transportes son una forma rápida de transportar recursos en el juego avanzado.<br>
Si vuelan con un motor de impulso, son más rápidos que los grandes transportes durante largos períodos.<br>
Se utilizan para trasladar recursos entre tus planetas, comerciar con otros jugadores o atacarlos.<br><br>
Para obtener más información, consulta el Wiki:
<a href="https://wiki.pr0game.com/doku.php?id=en:expoanleitung#what_do_i_find_with_what" target="_blank">Ships</a>';

$LNG['tutorial_title']['step6'] = '¡Ponle un nombre a tu planeta y encuentra amigos!';
$LNG['tutorial_text']['step6'] = 
'Para una mejor organización, puedes renombrar tus planetas. Haz clic en el nombre del planeta en la vista general de la barra superior de puntos. Prueba ahora.<br><br>
También puedes enviar solicitudes de amigos a otros jugadores. Si aceptan tu solicitud, podrán comunicarse fácilmente y ver el estado en línea de cada uno.
No es necesario estar en la misma alianza.<br>
Otra ventaja es poder estacionar flotas en el planeta de un amigo para defensa conjunta fuera de una alianza.<br><br>
También puedes unirte a una alianza para jugar con más jugadores. 
Dentro de la alianza, puedes enviar correos grupales y ver el estado en línea y los puntos de otros miembros según el rango de la alianza.';

$LNG['tutorial_title']['step7'] = '¡Mira lo que tienen los demás!';
$LNG['tutorial_text']['step7'] = 
'Para ver qué recursos, edificios, investigaciones, naves y defensas tienen otros jugadores, debes espiarlos.
Envía una sonda de espionaje a otro jugador a través del menú de flotas o la galaxia.<br>
¡Otros jugadores también pueden espiarte a ti!';
$LNG['tutorial_goal']['mission'][6] = 'Envía una sonda de espionaje a otro jugador.';

$LNG['tutorial_title']['step8'] = '¡Explora lo desconocido!';
$LNG['tutorial_text']['step8'] = 
'Una expedición puede traer recursos, naves, eventos o pérdidas.
Para probarlo, toma una sonda de espionaje y realiza una expedición haciendo clic en la coordenada 16 y la misión "Expedición" en el menú de flotas.<br>
¿Con una sonda? - Sí. Aunque transporta pocos recursos encontrados, encuentra tantas naves como una flota pequeña de transportes y es más rápida. Esto vale la pena al principio.<br><br>
Para más información, puedes consultar el Wiki sobre cómo maximizar tus ganancias:
<a href="https://wiki.pr0game.com/doku.php?id=en:expoanleitung" target="_blank">Expeditions</a>';

$LNG['tutorial_title']['step9'] = '¡No puedes depender de un solo planeta!';
$LNG['tutorial_text']['step9'] = 
'Para una producción rápida, deberías colonizar otros planetas.
Así podrás producir o construir naves en varios planetas al mismo tiempo.
Ten en cuenta que solo puedes colonizar en el borde interno o externo de un sistema si tienes un alto nivel de astrofísica.
Aunque puedes construir la nave de colonización sin astrofísica, necesitarás astrofísica para la primera colonia.<br><br>
Colonizar en sistemas cercanos mantiene las rutas de transporte cortas, pero también te hace vulnerable en el juego avanzado a jugadores que usan colonias como bases temporales.<br><br>
Para más información, consulta el Wiki:
<a href="https://wiki.pr0game.com/doku.php?id=en:astrophysik" target="_blank">Astrophysics</a>';

$LNG['tutorial_title']['step10'] = '¿Vas a esperar recursos de la producción propia?';
$LNG['tutorial_text']['step10'] = 
'Para obtener recursos más rápido, también puedes atacar a otros jugadores.
Considera la relación costo-beneficio para iniciar un ataque rentable.
Usa los informes de espionaje y el simulador de combate.<br><br>
¡Atención! Muchas misiones de flota pueden ser rastreadas por otros jugadores.<br>
Proteger tu flota es uno de los aspectos más importantes del juego.<br><br>
Lee estos temas en el Wiki:
<a href="https://wiki.pr0game.com/doku.php?id=en:slippys_progame_flottensicherung_leitfaden" target="_blank">Saven</a> - 
<a href="https://wiki.pr0game.com/doku.php?id=en:sensorphalanx" target="_blank">Sensorphalanx</a>';
$LNG['tutorial_goal']['mission'][1] = 'Ataca a otro jugador';

$LNG['tutorial_title']['step11'] = '¡Conviértete en recolector!';
$LNG['tutorial_text']['step11'] = 
'La destrucción de flotas crea un campo de escombros que se puede recolectar.
Para esto, necesitas un reciclador, que puede recoger hasta 20k de recursos del campo de escombros.
Destruir una gran flota puede ser una fuente lucrativa de recursos.<br><br>
En lugar de usar el menú de flotas, los recicladores también pueden enviarse directamente desde el menú de la galaxia haciendo clic en un campo de escombros en el número adecuado.';
$LNG['tutorial_goal']['mission'][8] = 'Recicla un campo de escombros';

$LNG['tutorial_title']['step12'] = 'Lo has logrado';
$LNG['tutorial_text']['step12'] = 
'Felicidades, has completado el tutorial.<br><br>
Este juego ofrece muchas más mecánicas adicionales.<br>
Por ejemplo, existen esencialmente 3 estilos de juego, que también difieren en la cantidad de tiempo requerido:<br>
<ul>
<li>Minero: Expande minas en muchas colonias, utiliza principalmente transportes</li>
<li>Cazador: Alta proporción de puntos en naves de combate, poca producción propia, depende de saquear a otros jugadores</li>
<li>Combinador: Una combinación de ambos estilos</li>
</ul>
Más adelante en el juego, las lunas se vuelven importantes, y ya deberías haber aprendido sobre el sensor Phalanx.<br><br>
Si tienes más preguntas, puedes consultar el Wiki o pedir ayuda a la comunidad en Discord.';
