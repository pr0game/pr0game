<?php

/**
 * pr0game powered by steemnova
 * Accounts
 * (c) 2024 reflexrecon
 */
$LNG["Universes"] = 'Universos';
$LNG["Your_universes"] = 'Sus universos';
$LNG["Other_universes"] = 'Otros universos';
$LNG["Universe_status"] = 'Estado de los universos: ';
$LNG["Playername"] = 'Nombre de juego';
$LNG["Register"] = 'Regístrese en';
$LNG["Universe_start"] = 'Inicio';
$LNG["Universe_end"] = 'Fin';
$LNG["Sitting_end"] = 'Extremo sentado';

$LNG["Sitting_request"] = 'Solicitar Sentarse (no programado)';
$LNG["Sitting_cancel"] = 'Abortar Sentada';

$LNG["Play"] = 'Jugar';
$LNG["Active_players"] = 'Jugadores activos';

$LNG["Settings"] = 'Ajustes';

$LNG['uni_status_regopen_gameopen'] = 'Abrir';
$LNG['uni_status_regclosed_gameclosed'] = 'Cerrado';
$LNG['uni_status_regopen_gameclosed'] = 'Abierto el plazo de inscripción';
$LNG['uni_status_regclosed_gameopen'] = 'Abierto al juego';

$LNG['Back'] = 'Volver';
// Translated into German by Reflexrecon . All rights reversed (C) 2024
