<?php

/**
 * pr0game powered by steemnova
 * Accounts
 * (c) 2024 reflexrecon
 */
$LNG["Universes"] = 'Universen';
$LNG["Your_universes"] = 'Deine Universen';
$LNG["Other_universes"] = 'Andere Universen';
$LNG["Universe_status"] = 'Universums Status: ';
$LNG["Playername"] = 'Spielername';
$LNG["Register"] = 'Registrieren';
$LNG["Universe_start"] = 'Start';
$LNG["Universe_end"] = 'Ende';
$LNG["Sitting_end"] = 'Sitting Ende';

$LNG["Sitting_request"] = 'Sitting anfragen (nicht programmiert)';
$LNG["Sitting_cancel"] = 'Sitting abbrechen';

$LNG["Play"] = 'Spielen';
$LNG["Active_players"] = 'Aktive Spieler';

$LNG["Settings"] = 'Einstellungen';

$LNG['uni_status_regopen_gameopen'] = 'Offen';
$LNG['uni_status_regclosed_gameclosed'] = 'Geschlossen';
$LNG['uni_status_regopen_gameclosed'] = 'Registrierung offen';
$LNG['uni_status_regclosed_gameopen'] = 'Anmeldung offen';

$LNG['Back'] = 'Zurück';
// Translated into German by Reflexrecon . All rights reversed (C) 2024
