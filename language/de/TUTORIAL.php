<?php

/**
 * pr0game powered by steemnova
 * tutorial
 * (c) 2024 Hyman
 */

$LNG['tutorial_goals_header'] = 'Aufgaben:';
$LNG['tutorial_goals_ignore'] = '(ignorieren)';

$LNG['tutorial_rewards_header'] = 'Belohnungen:';

$LNG['tutorial_buttons_finalize'] = 'Tutorial abschließen';
$LNG['tutorial_buttons_claim'] = 'Belohnung abholen';
$LNG['tutorial_buttons_continue'] = 'Weiter';
$LNG['tutorial_buttons_skip'] = 'Überspringen';
$LNG['tutorial_buttons_skipAll'] = 'Tutorial überspringen';
$LNG['tutorial_buttons_skipAll_confirm'] = 'Soll das restliche Tutorial wirklich übersprungen werden? Du kannst es nicht wieder aktivieren!';

$LNG['tutorial_title']['step0'] = 'Was isn das fürn Game?';
$LNG['tutorial_text']['step0'] = 
'Willkommen bei pr0game!<br><br>
Mit diesem Tutorial werden dir die Grundlagen des Spiels gezeigt.<br>
Oft wird auf unser Wiki für Zusatzinformationen verwiesen, da nicht alle Aspekte des Spiels im Tutorial erklärt werden können. 
Auf das Wiki kannst du jederzeit über das Menü auf der linken Seite zugreifen.<br>
Du kannst das Tutorial jederzeit überspringen, aber es lässt sich dann nicht wieder starten.<br><br>
Viel Erfolg, Imperator!';

$LNG['tutorial_title']['step1'] = 'Geh mal was verdienen!';
$LNG['tutorial_text']['step1'] = 
'In diesem Spiel geht es darum, möglichst viele Punkte zu bekommen. Einen Punkt bekommt man für je 1.000 ausgegebenen Ressourcen.
An diese Ressourcen kommt man auf unterschiedlichste Weisen heran. Die Wichtigste ist dabei die eigene Produktion der Minen.<br>
Stelle also zunächst deine Basisversorgung sicher. Hierzu benötigst du Metall und Kristall aus den entsprechenden Minen. Damit diese funktionieren, benötigst du außerdem Energie aus dem Solarkraftwerk.
Sollte nicht genug Energie vorhanden sein, so wird keine 100 %ige Produktion erreicht.<br>
Diese Gebäude kannst du über das Menü <b>Gebäude</b> ausbauen. Mit einem Klick auf den Gebäudenamen wird angezeigt, was der Mehrwert des Ausbaus an Ressourcen ist. 
Hier siehst du auch, wieviel Energie die nächste Stufe bei Fertigstellung verbraucht.<br><br>
Für weitere Infos kannst du es dir im <b>Wiki</b> noch einmal ansehen: 
<a href="https://wiki.pr0game.com/doku.php?id=rohstoffproduktion" target="_blank">Rohstoffproduktion</a>';

$LNG['tutorial_title']['step2'] = 'Komm mal unter Leute!';
$LNG['tutorial_text']['step2'] = 
'Es macht deutlich mehr Spaß, wenn man mit seinem Mitspielern interagiert.
Die Meisten organisieren sich über unseren Discord. 
In der Community erhältst du nicht nur Antworten auf deine Fragen, sondern kannst auch andere Spieler für das gemeinsame Spielen finden.<br>
Bei Unklarheiten kannst du hier auch Tickets aufmachen.';

$LNG['tutorial_title']['step3'] = 'Stell dich nicht nackt auf eine Lichtung!';
$LNG['tutorial_text']['step3'] = 
'Eine weitere Möglichkeit an Ressourcen zu kommen ist das Überfallen der Planeten deiner Mitspieler.<br>
Das kann dir allerdings auch selbst passieren - also bau dir deine erste Verteidigung!<br>
Es ist wichtig eine Verteidigung zu besitzen, um zumindest deine Nachtproduktion zu schützen.<br>
Hier ist aber nicht zwangsweise die Masse ausschlaggebend, sondern es zählt auch die Zusammensetzung der angreifenden Flotte.<br>
Vor dem Plündern muss der Angreifer erst deine Verteidigung überwinden. Jedes Schiff was der Angreifer mit senden muss, verringert seinen Gewinn und erhöht eventuell seinen Verlust durch kaputte Schiffe.<br>
Verteidigungsanlagen werden im <b>Verteidigung</b>-Menü mit einer Schiffswerft gebaut. Diese wiederum benötigt eine Roboterfabrik und Deuterium für den Bau.<br><br>
Hier noch der passende Artikel im Wiki: 
<a href="https://wiki.pr0game.com/doku.php?id=verteidigung#gebrauch_von_verteidigungsanlangen" target="_blank">Verteidigungsanlagen</a>';

$LNG['tutorial_title']['step4'] = 'Du musst schon weiter denken!';
$LNG['tutorial_text']['step4'] = 
'Stell deine Versorgung sicher, indem du deine Minen ausbaust!<br>
Der Bau deiner Gebäude, Schiffe und deiner Verteidigung, sowie das Forschen werden mit der Zeit teurer.
Du solltest also weiter deine Produktion ausbauen, um dem Ressourcenbedarf deines Planeten auszugleichen.
Klicke also jetzt auf Gebäude und bau deine Gebäude entsprechend aus, wenn du genug Ressourcen dafür hast.';

$LNG['tutorial_title']['step5'] = 'Schiffe, wir brauchen Schiffe!';
$LNG['tutorial_text']['step5'] = 
'Bau dir einen kleinen Transporter!<br>
Kleine Transporter sind im späteren Spiel eine schnelle Transportmöglichkeit.<br>
Sollten diese mit Impulstriebwerk fliegen, sind diese über einen langen Zeitraum schneller als der große Transporter.<br>
Sie werden eingesetzt, um Rohstoffe zwischen deinen Planeten zu transportieren, mit anderen Spielern zu Handeln, oder sie zu überfallen.<br><br>
Weitere Infos findest du im Wiki: 
<a href="https://wiki.pr0game.com/doku.php?id=schiffe" target="_blank">Schiffe</a>';

$LNG['tutorial_title']['step6'] = 'Dem Kind einen Namen geben und Freunde suchen!';
$LNG['tutorial_text']['step6'] = 
'Für eine bessere Übersicht können Planeten umbenannt werden. Klicke hierzu auf den Planetennamen in der Übersicht in der Kopfzeile der Punkteanzeige. Probier das jetzt mal aus.<br><br>
Du kannst außerdem Buddyanfragen an andere Spieler stellen. Wenn diese deine Anfrage annehmen, könnt ihr einfacher kommunizieren und seht gegenseitig euren Onlinestatus. 
Hierzu muss man nicht in der gleichen Allianz sein. 
Ein weiterer Vorteil ist das Halten von Flotten beim Buddy, was ein gemeinsames Verteidigen außerhalb einer Allianz ermöglicht.<br><br>
Außerdem kannst du einer Allianz beitreten, um mit weiteren Spielern zusammen zu spielen. 
Hierbei benötigt ihr zum Halten von Flotten keine separate Buddyanfrage mehr. 
Innerhalb der Allianz könnt ihr Rundmails versenden, sowie euren Onlinestatus und Punktestand sehen - in Abhängigkeit vom Allianzrang. 
Ihr seht ebenfalls den Onlinestatus und Punktestand der anderen Allianzmitglieder.';
$LNG['tutorial_goal']['step6'][1] = 'Benenne deinen Planeten um';
$LNG['tutorial_goal']['step6'][2] = 'Stelle eine Buddyanfrage an einen anderen Spieler';
$LNG['tutorial_goal']['step6'][3] = 'Stelle eine Allianzanfrage';

$LNG['tutorial_title']['step7'] = 'Guck mal was die Anderen so haben!';
$LNG['tutorial_text']['step7'] = 
'Um zu sehen, welche Ressourcen, Gebäuden, Forschungen, Schiffen und Verteidigung andere Spieler haben, musst du sie ausspionieren. 
Sende hierzu entweder über das Flottenmenü oder über die Galaxie eine Spionagesonde zu einem anderen Spieler.<br>
Andere Spieler können dich jedoch genauso ausspionieren!';
$LNG['tutorial_goal']['mission'][6] = 'Sende eine Spionagesonde zu einem anderen Spieler.';

$LNG['tutorial_title']['step8'] = 'Der Start in das Unbekannte!';
$LNG['tutorial_text']['step8'] = 
'Eine Expedition kann Rohstoffe, Schiffe, Ereignisse oder Verluste bringen. 
Um dies einmal auszuprobieren, schnapp dir eine Spionagesonde und fliege eine Expedition, indem du auf die Planeten-Koordinate 16 und Mission "Expedition" im Flottenmenü klickst.<br>
Mit einer Sonde? - Ja. Sie transportiert zwar kaum gefundene Ressourcen, findet aber genauso viele Schiffe, wie eine kleine Transporterflotte und ist dabei Schneller. Das Lohnt sich zu Beginn.<br><br>
Für weitere Informationen kannst du im Wiki nachlesen, was du in welcher Form verändern kannst, um eventuell deinen Gewinn zu maximieren: 
<a href="https://wiki.pr0game.com/doku.php?id=expoanleitung" target="_blank">Expeditionen</a>';
$LNG['tutorial_goal']['mission'][15] = 'Sende eine Expoflotte';

$LNG['tutorial_title']['step9'] = 'Du kannst nicht auf Dauer nur einen Planeten haben!';
$LNG['tutorial_text']['step9'] = 
'Um eine schnelle Produktion zu haben, solltest du weitere Planeten besiedeln. 
So hast du die Möglichkeit gleichzeitig auf zwei oder mehr Planeten zu produzieren oder Schiffe zu bauen.
Beachte hierbei, dass du nur mit einer entsprechend hohen Stufe Astrophysik am inneren oder äußeren Rand eines Systems siedeln kannst.
Zudem kann das Kolonieschiff zwar ohne Astrophysik gebaut werden, aber die erste Kolonie erfordert Astrophysik!<br><br>
Planeten in einem System oder direkt in Nachbarsystemen zu besiedeln hält zwar Transportwege kurz, macht dich später im Spiel aber auch anfällig für Spieler, 
die eine Kolonie nicht für die Minen, sondern als kurzzeitigen Flottenstützpunkt - Raidkolo genannt - bei dir ins System setzen.<br><br>
Für weitere Information kannst du im Wiki nachsehen: 
<a href="https://wiki.pr0game.com/doku.php?id=astrophysik" target="_blank">Astrophysik</a>';
$LNG['tutorial_goal']['mission'][7] = 'Besiedel einen weiteren Planeten';

$LNG['tutorial_title']['step10'] = 'Willst du echt auf die Ressourcen durch eigene Produktion warten?';
$LNG['tutorial_text']['step10'] = 
'Um schneller an Ressourcen zu kommen, kannst du auch andere Spieler angreifen. 
Achte hierbei auf den Kosten-Nutzen Faktor, um gewinnbringend einen Angriff zu starten. 
Benutze dazu Spionageberichte und den Kampfsimulator.<br><br>
Achtung! Viele Flottenmissionen sind auf verschiedene Weisen von anderen Spielern nachverfolgbar.<br>
Seine hart erarbeitete Flotte nicht zu verlieren ist einer der wichtigsten Aspekte des Spiels.<br><br>
Lies dir also unbedingt diese Themen im Wiki durch: 
<a href="https://wiki.pr0game.com/doku.php?id=slippys_progame_flottensicherung_leitfaden" target="_blank">Saven</a> - 
<a href="https://wiki.pr0game.com/doku.php?id=sensorphalanx" target="_blank">Sensorphalanx</a>';
$LNG['tutorial_goal']['mission'][1] = 'Greife einen anderen Spieler an';

$LNG['tutorial_title']['step11'] = 'Spiel doch mal Müllmann!';
$LNG['tutorial_text']['step11'] = 
'Bei der Zerstörung von Flotten ensteht ein Trümmerfeld, welches abgebaut werden kann. 
Hierfür benötigst du einen Recycler, der bis zu 20k Ressourcen vom Trümmerfeld abbauen kann. 
Eine große Flotte zu zerstören kann eine lukrative Ressourcenquelle sein.<br><br>
Statt über das Flottenmenü können Recycler auch direkt über das Glaxiemenü per Klick auf ein Trümmerfeld in passender Anzahl gesendet werden.';
$LNG['tutorial_goal']['mission'][8] = 'Recycle ein Trümmerfeld';

$LNG['tutorial_title']['step12'] = 'Du hast es geschafft';
$LNG['tutorial_text']['step12'] = 
'Herzlichen Glückwunsch, du hast das Tutorial beendet.<br><br>
Es gibt noch viele weitere Mechaniken, die deses Spiel zu bieten hat.<br>
Zum Beispiel gibt es im Wesentlichen 3 Spielstile, die sich auch im zeitlichem Aufwand unterscheiden:<br>
<ul>
<li>Miner: Minen auf vielen Kolonien ausbauen, setzt hauptsächlich auf Transporter</li>
<li>Fleeter: Hoher Punkteanteil in Kampfschiffen, kaum Eigenproduktion, lebt vom Überfallen anderer Spieler</li>
<li>Comber: Eine Kombination von beidem</li>
</ul>
Später im Spiel werden Monde wichtig, von denen du bereits die Sensorphalanx kennengelernt haben solltest.<br><br>
Wenn du weitere Fragen hast, kannst du dich im Wiki belesen oder die Community im Discord um Hilfe bitten.';