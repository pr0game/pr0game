<?php

/**
 * pr0game powered by steemnova
 * Accounts
 * (c) 2024 reflexrecon
 */
$LNG["Universes"] = 'Universes';
$LNG["Your_universes"] = 'Your Universes';
$LNG["Other_universes"] = 'Other Universes';
$LNG["Universe_status"] = 'Universes status: ';
$LNG["Playername"] = 'Playername';
$LNG["Register"] = 'Register';
$LNG["Universe_start"] = 'Start';
$LNG["Universe_end"] = 'End';
$LNG["Sitting_end"] = 'Sitting end';

$LNG["Sitting_request"] = 'Request Sitting (not programmed)';
$LNG["Sitting_cancel"] = 'Abort Sitting';

$LNG["Play"] = 'Play';
$LNG["Active_players"] = 'Active Players';

$LNG["Settings"] = 'Settings';

$LNG['uni_status_regopen_gameopen'] = 'Open';
$LNG['uni_status_regclosed_gameclosed'] = 'Closed';
$LNG['uni_status_regopen_gameclosed'] = 'Open to register';
$LNG['uni_status_regclosed_gameopen'] = 'Open to play';

$LNG['Back'] = 'Back';
// Translated into German by Reflexrecon . All rights reversed (C) 2024
