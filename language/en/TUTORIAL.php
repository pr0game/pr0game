<?php

/**
 * pr0game powered by steemnova
 * tutorial
 * (c) 2024 Hyman
 */

$LNG['tutorial_goals_header'] = 'Goals:';
$LNG['tutorial_goals_ignore'] = '(ignore)';

$LNG['tutorial_rewards_header'] = 'Rewards:';

$LNG['tutorial_buttons_finalize'] = 'Complete the tutorial';
$LNG['tutorial_buttons_claim'] = 'Claim reward';
$LNG['tutorial_buttons_continue'] = 'Continue';
$LNG['tutorial_buttons_skip'] = 'Skip';
$LNG['tutorial_buttons_skipAll'] = 'Skip the tutorial';
$LNG['tutorial_buttons_skipAll_confirm'] = 'Do you really want to skip the rest of the tutorial? You can\'t reactivate it!';

$LNG['tutorial_title']['step0'] = 'What kind of game is this?';
$LNG['tutorial_text']['step0'] = 
'Welcome to pr0game!<br><br>
This tutorial will show you the basics of the game.<br>
We often refer to our wiki for additional information, as not all aspects of the game can be explained in the tutorial. You can access the wiki at any time via the menu on the left-hand side.<br>
You can skip the tutorial at any time, but you will not be able to start it again.<br><br>
Good luck, Imperator!';

$LNG['tutorial_title']['step1'] = 'Time to start mining!';
$LNG['tutorial_text']['step1'] = 
'The aim of this game is to score as many points as possible. You get one point for every 1,000 resources you spend. These resources can be obtained in a variety of ways. The most important way is to produce your own mines.<br>
So first ensure your basic supplies. To do this, you need metal and crystal from the corresponding mines. You will also need energy from the solar power plant for these to work. If there is not enough energy available, 100% production will not be achieved.<br>
You can upgrade these buildings via the <b>Buildings</b> menu. Click on the building name to see what the added value of the expansion is in terms of resources. Here you can also see how much energy the next level will consume on completion.<br><br>
For more information, you can take another look at the <b>Wiki</b>: 
<a href="https://wiki.pr0game.com/doku.php?id=en:rohstoffproduktion" target="_blank">Raw material production</a>';

$LNG['tutorial_title']['step2'] = 'Come and meet people!';
$LNG['tutorial_text']['step2'] = 
'It\'s much more fun when you interact with your fellow players. Most of them organize themselves via our Discord. In the community, you not only get answers to your questions, but you can also find other players to play with.<br>
If you are unsure, you can also open tickets here.';

$LNG['tutorial_title']['step3'] = 'Don\'t stand naked in a clearing!';
$LNG['tutorial_text']['step3'] = 
'Another way to get resources is to raid the planets of your fellow players.<br>
However, this can also happen to you - so build your first defense!<br>
It is important to have a defense to at least protect your night production.<br>
However, mass is not necessarily the decisive factor here; the composition of the attacking fleet also counts.<br>
Before plundering, the attacker must first overcome your defenses. Every ship the attacker has to send reduces his profit and possibly increases his loss due to broken ships.<br>
Defenses are built in the <b>Defense</b> menu with a shipyard. This in turn requires a robot factory and deuterium for construction.<br><br>
Here is the matching article in the wiki: 
<a href="https://wiki.pr0game.com/doku.php?id=en:verteidigung" target="_blank">Defense facilities</a>';

$LNG['tutorial_title']['step4'] = 'You have to think ahead!';
$LNG['tutorial_text']['step4'] = 
'Secure your supplies by expanding your mines!<br>
The construction of your buildings, ships and defenses, as well as research, will become more expensive over time. You should therefore continue to expand your production in order to balance out your planet\'s resource requirements. So click on Buildings now and expand your buildings accordingly if you have enough resources.';

$LNG['tutorial_title']['step5'] = 'Ships, we need ships!';
$LNG['tutorial_text']['step5'] = 
'Build yourself a small van!<br>
Small transporters are a quick means of transportation later in the game.<br>
If they fly with impulse engines, they are faster than the large transporter over a long period of time.<br>
They are used to transport resources between your planets, to trade with other players or to raid them.<br><br>
You can find more information in the wiki: 
<a href="https://wiki.pr0game.com/doku.php?id=en:expoanleitung#what_do_i_find_with_what" target="_blank">Ships</a>';

$LNG['tutorial_title']['step6'] = 'Give the child a name and look for friends!';
$LNG['tutorial_text']['step6'] = 
'Planets can be renamed for a better overview. To do this, click on the planet name in the overview in the header of the points display. Try this out now.<br><br>
You can also send buddy requests to other players. If they accept your request, you can communicate more easily and see each other\'s online status. You don\'t have to be in the same alliance to do this. Another advantage is that you can keep fleets with your buddy, which makes it possible to defend together outside of an alliance.<br><br>
You can also join an alliance to play together with other players. You no longer need a separate buddy request to hold fleets. Within the alliance you can send circular mails and see your online status and score - depending on your alliance rank. You can also see the online status and score of other alliance members.';
$LNG['tutorial_goal']['step6'][1] = 'Rename your planet';
$LNG['tutorial_goal']['step6'][2] = 'Make a buddy request to another player';
$LNG['tutorial_goal']['step6'][3] = 'Make an alliance request';

$LNG['tutorial_title']['step7'] = 'Take a look at what the others have!';
$LNG['tutorial_text']['step7'] = 
'To see what resources, buildings, research, ships and defenses other players have, you have to spy on them. To do this, send a spy probe to another player either via the fleet menu or via the galaxy.<br>
However, other players can also spy on you!';
$LNG['tutorial_goal']['mission'][6] = 'Send a spy probe to another player.';

$LNG['tutorial_title']['step8'] = 'The start into the unknown!';
$LNG['tutorial_text']['step8'] = 
'An expedition can bring resources, ships, events or losses. To try this out, grab a spy probe and fly an expedition by clicking on planet coordinate 16 and mission "Expedition" in the fleet menu.<br>
With a probe? - Yes. It transports hardly any resources, but finds just as many ships as a small transporter fleet and is faster. It\'s worth it at the beginning.<br><br>
For more information, you can read in the wiki what you can change and in what form to possibly maximize your profit: 
<a href="https://wiki.pr0game.com/doku.php?id=en:expoanleitung" target="_blank">Expeditions</a>';
$LNG['tutorial_goal']['mission'][15] = 'Send an expo fleet';

$LNG['tutorial_title']['step9'] = 'You can\'t have just one planet for ever!';
$LNG['tutorial_text']['step9'] = 
'To have a fast production, you should colonize more planets. This gives you the opportunity to produce or build ships on two or more planets at the same time. Please note that you can only settle on the inner or outer edge of a system with a correspondingly high astrophysics level. In addition, the colony ship can be built without astrophysics, but the first colony requires astrophysics!<br><br>
Colonizing planets in a system or directly in neighboring systems keeps transport routes short, but also makes you vulnerable to players later in the game who place a colony in your system not for the mines, but as a temporary fleet base - called a raid colony.<br><br>
For more information you can check the wiki: 
<a href="https://wiki.pr0game.com/doku.php?id=en:astrophysik" target="_blank">Astrophysics</a>';
$LNG['tutorial_goal']['mission'][7] = 'Colonize another planet';

$LNG['tutorial_title']['step10'] = 'Do you really want to wait for the resources from your own production?';
$LNG['tutorial_text']['step10'] = 
'To get resources faster, you can also attack other players. Pay attention to the cost-benefit factor in order to launch a profitable attack. Use the combat simulator to do this.<br><br>
Attention! Many fleet missions can be tracked by other players in various ways.<br>
Not losing your hard-earned fleet is one of the most important aspects of the game.<br><br>
So be sure to read through these topics in the wiki: 
<a href="https://wiki.pr0game.com/doku.php?id=en:slippys_progame_flottensicherung_leitfaden" target="_blank">Saven</a> - 
<a href="https://wiki.pr0game.com/doku.php?id=en:sensorphalanx" target="_blank">Sensorphalanx</a>';
$LNG['tutorial_goal']['mission'][1] = 'Attack another player';

$LNG['tutorial_title']['step11'] = 'Why don\'t you play dustman?';
$LNG['tutorial_text']['step11'] = 
'When fleets are destroyed, a debris field is created which can be mined. For this you need a recycler that can mine up to 20k resources from the debris field. Destroying a large fleet can be a lucrative source of resources.<br><br>
Instead of using the fleet menu, recyclers can also be sent in suitable numbers directly via the Glaxie menu by clicking on a debris field.';
$LNG['tutorial_goal']['mission'][8] = 'Recycle a debris field';

$LNG['tutorial_title']['step12'] = 'You have made it';
$LNG['tutorial_text']['step12'] = 
'Congratulations, you have completed the tutorial.<br><br>
There are many more mechanics that this game has to offer.<br>
For example, there are essentially 3 styles of play, which also differ in terms of the time required:<br>
<ul>
<li>Miner: Expand mines on many colonies, mainly uses transporters</li>
<li>Fleeter: High percentage of points in battleships, hardly any own production, lives from raiding other players</li>
<li>Comber: A combination of both</li>
</ul>
Moons become important later in the game, and you should have already become familiar with the sensor phalanx.<br><br>
If you have further questions, you can read the wiki or ask the community in the Discord for help.';
