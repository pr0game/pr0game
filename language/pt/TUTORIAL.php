<?php

/**
 * pr0game powered by steemnova
 * tutorial
 * (c) 2024 Hyman
 */

$LNG['cabeçalho_dos_objectivos_do_tutorial'] = 'Objectivos:';
$LNG['tutorial_goals_ignore'] = '(ignorar)';

$LNG['cabeçalho_recompensas_tutorial'] = 'Recompensas:';

$LNG['botões_tutorial_finalizar'] = 'Completar o tutorial';
$LNG['botões_tutorial_reclamação'] = 'Reivindicar recompensa';
$LNG['botões_tutorial_continuar'] = 'Continuar';
$LNG['botões_tutorial_pular'] = 'Pular';
$LNG['botões_tutorial_pularTudo'] = 'Pular o tutorial'; 

$LNG['tutorial_title']['step0'] = 'Bem-vindo ao Tutorial!';
$LNG['tutorial_text']['step0'] = 
'Bem-vindo ao jogo! Este tutorial irá guiá-lo pelos conceitos básicos para que você possa começar a jogar e crescer rapidamente.
Você verá metas e dicas que o ajudarão a progredir em vários aspectos do jogo, desde a construção de minas até a expansão do seu império.<br><br>
Vamos começar com o básico para que você possa configurar sua produção de recursos e se preparar para futuras aventuras.
Boa sorte e divirta-se!';

$LNG['tutorial_title']['step1'] = 'Comece a ganhar recursos!';
$LNG['tutorial_text']['step1'] = 
'O objetivo deste jogo é acumular o máximo de pontos possível. Você ganha um ponto para cada 1.000 recursos gastos.
Existem várias maneiras de obter recursos, sendo a produção de suas próprias minas a mais importante.<br>
Para começar, certifique-se de estabelecer uma produção básica. Você precisará de metal e cristal, que são produzidos em minas específicas. Para fazer as minas funcionarem, é necessário ter energia proveniente da usina solar.
Se a energia não for suficiente, a produção não será de 100%.<br>
Você pode construir esses edifícios através do menu <b>Construções</b>. Clicando no nome do edifício, você pode ver o que a próxima expansão oferece em termos de recursos e quanta energia será consumida na próxima fase.<br><br>
Para mais informações, consulte o <b>Wiki</b>: 
<a href="https://wiki.pr0game.com/doku.php?id=en:rohstoffproduktion" target="_blank">Raw material production</a>';

$LNG['tutorial_title']['step2'] = 'Interaja com outros jogadores!';
$LNG['tutorial_text']['step2'] = 
'Jogar é muito mais divertido quando você interage com outros jogadores.
A maioria organiza-se no nosso Discord, onde você pode obter respostas para suas dúvidas e encontrar outros jogadores para formar parcerias.<br>
Se tiver dúvidas, você também pode abrir tickets lá.';

$LNG['tutorial_title']['step3'] = 'Defenda-se!';
$LNG['tutorial_text']['step3'] = 
'Outra maneira de obter recursos é atacando planetas de outros jogadores.<br>
Mas isso também pode acontecer com você, então é melhor construir uma defesa inicial!<br>
Ter uma defesa é importante para proteger ao menos a produção durante a noite.<br>
A quantidade não é o mais importante; a composição da frota de ataque também conta.<br>
Antes de saquear, o atacante precisa superar sua defesa. Cada nave que o atacante precisa enviar reduz seu lucro e aumenta suas perdas em caso de destruição.<br>
Você pode construir defesas no menu <b>Defesa</b> usando um estaleiro, que requer uma fábrica de robôs e deutério para construção.<br><br>
Para mais informações, consulte o Wiki: 
<a href="https://wiki.pr0game.com/doku.php?id=en:verteidigung" target="_blank">Defense facilities</a>';

$LNG['tutorial_title']['step4'] = 'Planeje seu crescimento!';
$LNG['tutorial_text']['step4'] = 
'Garanta sua produção de recursos expandindo suas minas!<br>
Com o tempo, a construção de edifícios, naves e defesas, além de pesquisas, ficam mais caras.
Então, aumente sua produção para acompanhar a demanda de recursos no seu planeta.
Clique em Construções e expanda os edifícios conforme tiver recursos suficientes.';

$LNG['tutorial_title']['step5'] = 'Construa naves!';
$LNG['tutorial_text']['step5'] = 
'Construa um pequeno transportador!<br>
Transportadores pequenos são uma opção rápida para transporte no jogo mais avançado.<br>
Com um motor de impulso, são mais rápidos que transportadores grandes em viagens longas.<br>
Eles são usados para transportar recursos entre planetas, negociar com outros jogadores, ou atacar.<br><br>
Mais informações no Wiki: 
<a href="https://wiki.pr0game.com/doku.php?id=en:expoanleitung#what_do_i_find_with_what" target="_blank">Ships</a>';

$LNG['tutorial_title']['step6'] = 'Nomeie e faça amigos!';
$LNG['tutorial_text']['step6'] = 
'Para organizar melhor seus planetas, você pode renomeá-los. Clique no nome do planeta na barra superior e tente renomeá-lo agora.<br><br>
Você também pode enviar pedidos de amizade para outros jogadores. Quando aceitos, fica mais fácil se comunicar e ver o status online um do outro.
Esse recurso funciona mesmo fora de alianças.<br><br>
Juntar-se a uma aliança permite que você jogue com mais pessoas e envie mensagens coletivas, além de ver o status de atividade e pontuação dos membros, dependendo do cargo.<br><br>
Objetivos:
<ul>
<li>Renomeie seu planeta</li>
<li>Envie um pedido de amizade</li>
<li>Envie um pedido para uma aliança</li>
</ul>';

$LNG['tutorial_title']['step7'] = 'Observe os outros!';
$LNG['tutorial_text']['step7'] = 
'Para ver quais recursos, edifícios, pesquisas, naves e defesas outros jogadores têm, você pode espioná-los. 
Envie uma sonda de espionagem pelo menu da frota ou pela galáxia.<br>
Lembre-se de que outros jogadores também podem espionar você!';
$LNG['tutorial_goal']['mission'][6] = 'Envie uma sonda de espionagem a outro jogador.';

$LNG['tutorial_title']['step8'] = 'Explore o desconhecido!';
$LNG['tutorial_text']['step8'] = 
'Uma expedição pode trazer recursos, naves, eventos, ou até perdas.
Para experimentar, use uma sonda de espionagem para iniciar uma expedição na coordenada 16 e escolha a missão "Expedição" no menu da frota.<br>
Uma sonda não transporta muitos recursos, mas encontra naves e é rápida, sendo útil no começo.<br><br>
Mais informações no Wiki: 
<a href="https://wiki.pr0game.com/doku.php?id=en:expoanleitung" target="_blank">Expeditions</a>';
$LNG['tutorial_goal']['mission'][15] = 'Envie uma frota para uma expedição';

$LNG['tutorial_title']['step9'] = 'Expanda com novos planetas!';
$LNG['tutorial_text']['step9'] = 
'Para aumentar a produção, colonize novos planetas e produza ou construa naves simultaneamente.
Observe que para colonizar nas bordas do sistema, você precisa de um nível avançado de Astrofísica.
Um transporte curto economiza tempo, mas o deixa vulnerável a jogadores com colônias de ataque temporárias.<br><br>
Para mais informações, consulte o Wiki: 
<a href="https://wiki.pr0game.com/doku.php?id=en:astrophysik" target="_blank">Astrophysics</a>';
$LNG['tutorial_goal']['mission'][7] = 'Colonize um novo planeta';

$LNG['tutorial_title']['step10'] = 'Não dependa apenas de produção própria!';
$LNG['tutorial_text']['step10'] = 
'Para obter recursos mais rapidamente, você pode atacar outros jogadores.
Avalie o custo-benefício para garantir que o ataque compense e utilize os relatórios de espionagem e simulador de combate.<br><br>
Atenção! Muitas missões de frota podem ser rastreadas por outros jogadores.<br>
Proteger sua frota é um dos aspectos mais importantes do jogo.<br><br>
Leia sobre esses tópicos no Wiki: 
<a href="https://wiki.pr0game.com/doku.php?id=en:slippys_progame_flottensicherung_leitfaden" target="_blank">Saven</a> - 
<a href="https://wiki.pr0game.com/doku.php?id=en:sensorphalanx" target="_blank">Sensorphalanx</a>';
$LNG['tutorial_goal']['mission'][1] = 'Ataque outro jogador';


$LNG['tutorial_title']['step11'] = 'Experimente ser um reciclador!';
$LNG['tutorial_text']['step11'] = 
'Quando frotas são destruídas, um campo de destroços é formado e pode ser reciclado.
Para isso, você precisa de um reciclador, que é capaz de coletar até 20.000 recursos do campo de destroços.
Destruir uma frota grande pode ser uma fonte lucrativa de recursos.<br><br>
Além de enviá-los pelo menu de frotas, recicladores também podem ser enviados diretamente através do menu da galáxia, clicando em um campo de destroços e selecionando a quantidade apropriada.';
$LNG['tutorial_goal']['mission'][8] = 'Recicle um campo de destroços';

$LNG['tutorial_title']['step12'] = 'Você conseguiu';
$LNG['tutorial_text']['step12'] = 
'Parabéns, você concluiu o tutorial!<br><br>
Existem ainda muitas outras mecânicas que este jogo oferece.<br>
Por exemplo, existem essencialmente três estilos de jogo, que diferem em esforço e estratégia:<br>
<ul>
<li>Minerador: foca em expandir as minas em várias colônias, usando principalmente transportadores</li>
<li>Frotista: investe fortemente em naves de combate, com pouca produção própria, sobrevivendo ao saquear outros jogadores</li>
<li>Combinado: uma combinação de ambos os estilos</li>
</ul>
Mais tarde, as luas se tornam importantes no jogo, e você já deve ter conhecido a falange sensorial.<br><br>
Se tiver mais perguntas, consulte o Wiki ou peça ajuda à comunidade no Discord.';
