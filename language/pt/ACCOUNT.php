<?php

/**
 * pr0game powered by steemnova
 * Accounts
 * (c) 2024 reflexrecon
 */
$LNG["Universes"] = 'Universos';
$LNG["Your_universes"] = 'Os vossos universos';
$LNG["Other_universes"] = 'Outros Universos';
$LNG["Universe_status"] = 'Estado dos universos: ';
$LNG["Playername"] = 'Nome de jogador';
$LNG["Register"] = 'Registo';
$LNG["Universe_start"] = 'Início';
$LNG["Universe_end"] = 'Fim';
$LNG["Sitting_end"] = 'Final de sessão';

$LNG["Sitting_request"] = 'Solicitação de sessão (não programada)';
$LNG["Sitting_cancel"] = 'Abortar a sessão';

$LNG["Play"] = 'Jogar';
$LNG["Active_players"] = 'Jogadores activos';

$LNG["Settings"] = 'Definições';

$LNG['uni_status_regopen_gameopen'] = 'Aberto';
$LNG['uni_status_regclosed_gameclosed'] = 'Fechado';
$LNG['uni_status_regopen_gameclosed'] = 'Aberto ao registo';
$LNG['uni_status_regclosed_gameopen'] = 'Aberto para jogar';

$LNG['Back'] = 'Voltar';
// Translated into German by Reflexrecon . All rights reversed (C) 2024
