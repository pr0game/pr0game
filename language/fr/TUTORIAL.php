<?php

/**
 * pr0game powered by steemnova
 * tutorial
 * (c) 2024 Hyman
 */

$LNG['tutorial_goals_header'] = 'Objectifs:';
$LNG['tutorial_goals_ignore'] = '(ignorer)';

$LNG['tutorial_rewards_header'] = 'Récompenses:';

$LNG['tutorial_buttons_finalize'] = 'Terminer le tutoriel';
$LNG['tutorial_buttons_claim'] = 'Demander une récompense';
$LNG['tutorial_buttons_continue'] = 'Continuer';
$LNG['tutorial_buttons_skip'] = 'Passer';
$LNG['tutorial_buttons_skipAll'] = 'Sauter le tutoriel';
$LNG['tutorial_buttons_skipAll_confirm'] = 'Voulez-vous vraiment sauter le reste du tutoriel ? Vous ne pouvez pas le réactiver !';

$LNG['tutorial_title']['step0'] = 'C’est quoi ce jeu ?';
$LNG['tutorial_text']['step0'] = 
'Bienvenue sur pr0game !<br><br>
Ce didacticiel va te montrer les bases du jeu.<br>
Souvent, des informations complémentaires sont disponibles sur notre Wiki, car tous les aspects du jeu ne peuvent pas être expliqués dans le didacticiel. 
Tu peux accéder au Wiki à tout moment via le menu de gauche.<br>
Tu peux sauter le didacticiel à tout moment, mais il ne sera alors plus accessible.<br><br>
Bonne chance, Empereur !';

$LNG['tutorial_title']['step1'] = 'Va gagner ta vie !';
$LNG['tutorial_text']['step1'] = 
'Dans ce jeu, l’objectif est d’obtenir le plus de points possible. On obtient un point pour chaque 1 000 ressources dépensées.
Ces ressources peuvent être obtenues de différentes manières. La principale est la production de tes mines.<br>
Assure-toi d’abord de bien établir ta base de ressources. Pour cela, tu as besoin de métal et de cristal, produits par les mines correspondantes. Pour que celles-ci fonctionnent, il te faudra également de l’énergie produite par la centrale solaire.
Si l’énergie est insuffisante, la production ne fonctionnera pas à 100 %.<br>
Ces bâtiments peuvent être améliorés via le menu <b>Bâtiments</b>. En cliquant sur le nom d’un bâtiment, tu verras en quoi l’amélioration augmentera ta production de ressources. 
Tu pourras aussi voir la quantité d’énergie consommée par le niveau suivant.<br><br>
Pour plus d’infos, consulte le <b>Wiki</b> : 
<a href="https://wiki.pr0game.com/doku.php?id=en:rohstoffproduktion" target="_blank">Raw material production</a>';

$LNG['tutorial_title']['step2'] = 'Va sociabiliser !';
$LNG['tutorial_text']['step2'] = 
'Le jeu est beaucoup plus amusant quand on interagit avec les autres joueurs.
La plupart d’entre eux se retrouvent sur notre serveur Discord. 
Dans la communauté, tu peux poser des questions et trouver d’autres joueurs pour jouer ensemble.<br>
En cas de problème, tu peux aussi y ouvrir des tickets.';

$LNG['tutorial_title']['step3'] = 'Ne reste pas à découvert !';
$LNG['tutorial_text']['step3'] = 
'Une autre façon d’obtenir des ressources est de piller les planètes d’autres joueurs.<br>
Cependant, cela peut aussi t’arriver – alors construis ta première défense !<br>
Il est important d’avoir une défense pour protéger au moins ta production nocturne.<br>
Ce n’est pas tant la quantité qui compte, mais la composition de la flotte attaquante.<br>
Avant de pouvoir piller, l’attaquant doit d’abord surmonter ta défense. Plus l’attaquant doit envoyer de vaisseaux, moins son attaque sera rentable.<br>
Les installations de défense se construisent dans le menu <b>Défense</b> et nécessitent un chantier spatial. Celui-ci nécessite également une usine de robots et du deutérium.<br><br>
Voici l’article correspondant dans le Wiki : 
<a href="https://wiki.pr0game.com/doku.php?id=en:verteidigung" target="_blank">Defense facilities</a>';

$LNG['tutorial_title']['step4'] = 'Pense à long terme !';
$LNG['tutorial_text']['step4'] = 
'Assure ta production de base en améliorant tes mines !<br>
La construction de bâtiments, de vaisseaux et de défenses, ainsi que la recherche, devient de plus en plus coûteuse.
Tu devrais donc continuer à développer ta production pour répondre aux besoins de ton empire.
Clique maintenant sur Bâtiments et améliore-les dès que tu as suffisamment de ressources.';

$LNG['tutorial_title']['step5'] = 'On a besoin de vaisseaux !';
$LNG['tutorial_text']['step5'] = 
'Construis un petit transporteur !<br>
Les petits transporteurs sont une méthode de transport rapide plus tard dans le jeu.<br>
Avec le moteur à impulsion, ils sont souvent plus rapides que les grands transporteurs sur de longues distances.<br>
Ils sont utilisés pour transporter des ressources entre tes planètes, commercer avec d’autres joueurs ou les piller.<br><br>
Pour plus d’infos, consulte le Wiki : 
<a href="https://wiki.pr0game.com/doku.php?id=en:expoanleitung#what_do_i_find_with_what" target="_blank">Ships</a>';

$LNG['tutorial_title']['step6'] = 'Donne un nom à ta planète et cherche des amis !';
$LNG['tutorial_text']['step6'] = 
'Pour une meilleure organisation, tu peux renommer tes planètes. Clique sur le nom de la planète dans l’onglet de pointage pour l’essayer.<br><br>
Tu peux aussi envoyer des demandes d’amitié à d’autres joueurs. Une fois acceptées, elles facilitent la communication et permettent de voir le statut en ligne.<br>
Cela ne nécessite pas d’être dans la même alliance. Un autre avantage est de pouvoir stationner des flottes chez un ami, permettant une défense commune.<br><br>
Tu peux également rejoindre une alliance pour jouer en groupe. 
Au sein de l’alliance, tu peux envoyer des messages de groupe et voir le statut en ligne et les points des membres en fonction de leur rang.<br>';
$LNG['tutorial_goal']['step6'][1] = 'Renomme ta planète';
$LNG['tutorial_goal']['step6'][2] = 'Envoie une demande d’amitié';
$LNG['tutorial_goal']['step6'][3] = 'Envoie une demande d’adhésion à une alliance';

$LNG['tutorial_title']['step7'] = 'Regarde ce que les autres ont !';
$LNG['tutorial_text']['step7'] = 
'Pour voir les ressources, bâtiments, recherches, vaisseaux et défenses des autres joueurs, espionne-les en envoyant une sonde d’espionnage.<br>
Cependant, d’autres joueurs peuvent aussi t’espionner !';
$LNG['tutorial_goal']['mission'][6] = 'Envoie une sonde d’espionnage.';

$LNG['tutorial_title']['step8'] = 'Part à l’aventure !';
$LNG['tutorial_text']['step8'] = 
'Une expédition peut apporter des ressources, des vaisseaux, des événements ou des pertes. 
Pour essayer, envoie une sonde d’espionnage en cliquant sur la coordonnée 16 de la planète en mission "Expédition".<br>
Avec une sonde ? Oui. Elle transporte peu de ressources trouvées mais est aussi efficace qu’une flotte de petits transporteurs et plus rapide. Cela vaut le coup au début.<br><br>
Pour en savoir plus, consulte le Wiki : 
<a href="https://wiki.pr0game.com/doku.php?id=en:expoanleitung" target="_blank">Expeditions</a>';
$LNG['tutorial_goal']['mission'][15] = 'Envoie une flotte d’expédition';

$LNG['tutorial_title']['step9'] = 'Ne te contente pas d’une seule planète !';
$LNG['tutorial_text']['step9'] = 
'Pour une production efficace, colonise d’autres planètes. 
Ainsi, tu pourras produire ou construire des vaisseaux en simultané.<br>
Note que pour coloniser sur les bords internes ou externes d’un système, un certain niveau en astrophysique est requis.
Bien que le vaisseau de colonisation puisse être construit sans astrophysique, la première colonie l’exige.<br><br>
Coloniser près d’un même système réduit les trajets mais te rend vulnérable aux "Raider Colos".<br><br>
Pour plus d’informations, consulte le Wiki : 
<a href="https://wiki.pr0game.com/doku.php?id=en:astrophysik" target="_blank">Astrophysics</a>';
$LNG['tutorial_goal']['mission'][7] = 'Colonise une nouvelle planète';

$LNG['tutorial_title']['step10'] = 'Tu vas attendre longtemps pour produire tes ressources ?';
$LNG['tutorial_text']['step10'] = 
'Pour obtenir des ressources rapidement, tu peux attaquer d’autres joueurs. 
Pense au rapport coût-bénéfice pour que l’attaque soit rentable. Utilise les rapports d’espionnage et un simulateur de combat.<br><br>
Attention ! De nombreuses missions de flotte peuvent être suivies par d’autres joueurs.<br>
Protéger ta flotte est un aspect important du jeu.<br><br>
Lis ces articles sur le Wiki : 
<a href="https://wiki.pr0game.com/doku.php?id=en:slippys_progame_flottensicherung_leitfaden" target="_blank">Saven</a> - 
<a href="https://wiki.pr0game.com/doku.php?id=en:sensorphalanx" target="_blank">Sensorphalanx</a>';
$LNG['tutorial_goal']['mission'][1] = 'Attaque un autre joueur';

$LNG['tutorial_title']['step11'] = 'Joue au recycleur !';
$LNG['tutorial_text']['step11'] = 
'La destruction de flottes crée des champs de débris, qui peuvent être collectés avec un recycleur.
Un recycleur peut récupérer jusqu’à 20k de ressources dans un champ de débris. 
Détruire une grande flotte peut donc devenir une source précieuse de ressources.<br><br>
Au lieu de passer par le menu Flotte, les recycleurs peuvent aussi être envoyés directement depuis le menu Galaxie en cliquant sur un champ de débris et en sélectionnant le nombre requis.';
$LNG['tutorial_goal']['mission'][8] = 'Recycle un champ de débris';

$LNG['tutorial_title']['step12'] = 'Tu as terminé';
$LNG['tutorial_text']['step12'] = 
'Félicitations, tu as terminé le didacticiel !<br><br>
Il existe encore de nombreuses autres mécaniques dans ce jeu.<br>
Par exemple, il y a essentiellement 3 styles de jeu, chacun avec un investissement en temps différent :<br>
<ul>
<li>Mineur : développe les mines sur de nombreuses colonies et utilise principalement des transporteurs</li>
<li>Flotteur : investit principalement dans des vaisseaux de combat, produit peu lui-même, dépend des raids sur d’autres joueurs</li>
<li>Mixte : une combinaison des deux</li>
</ul>
Plus tard dans le jeu, les lunes deviennent importantes. Tu as déjà découvert la Phalange de capteurs.<br><br>
Si tu as d’autres questions, tu peux consulter le Wiki ou demander de l’aide à la communauté sur Discord.';
