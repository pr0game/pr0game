<?php

/**
 * pr0game powered by steemnova
 * Accounts
 * (c) 2024 reflexrecon
 */
$LNG["Universes"] = 'Univers';
$LNG["Your_universes"] = 'Vos univers';
$LNG["Other_universes"] = 'Autres univers';
$LNG["Universe_status"] = 'Statut des univers: ';
$LNG["Playername"] = 'Nom de joueur';
$LNG["Register"] = 'Registre';
$LNG["Universe_start"] = 'Démarrage';
$LNG["Universe_end"] = 'Fin';
$LNG["Sitting_end"] = 'Fin de séance';

$LNG["Sitting_request"] = 'Demande de sitting (non programmé)';
$LNG["Sitting_cancel"] = 'Abandonner la sitting';

$LNG["Play"] = 'Jouer';
$LNG["Active_players"] = 'Joueurs actifs';

$LNG["Settings"] = 'Paramètres';

$LNG['uni_status_regopen_gameopen'] = 'Ouvrir';
$LNG['uni_status_regclosed_gameclosed'] = 'Fermé';
$LNG['uni_status_regopen_gameclosed'] = 'Ouverture à l\'inscription';
$LNG['uni_status_regclosed_gameopen'] = 'Ouvert au jeu';

$LNG['Back'] = 'Retour';
// Translated into German by Reflexrecon . All rights reversed (C) 2024
