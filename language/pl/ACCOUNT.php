<?php

/**
 * pr0game powered by steemnova
 * Accounts
 * (c) 2024 reflexrecon
 */
$LNG["Universes"] = 'Wszechświaty';
$LNG["Your_universes"] = 'Twoje wszechświaty';
$LNG["Other_universes"] = 'Inne wszechświaty';
$LNG["Universe_status"] = 'Status wszechświatów: ';
$LNG["Playername"] = 'Nazwa gracza';
$LNG["Register"] = 'Rejestr';
$LNG["Universe_start"] = 'Start';
$LNG["Universe_end"] = 'Koniec';
$LNG["Sitting_end"] = 'Koniec siedzenia';

$LNG["Sitting_request"] = 'Żądanie siedzenia (nie zaprogramowane)';
$LNG["Sitting_cancel"] = 'Przerwij siedzenie';

$LNG["Play"] = 'Grać';
$LNG["Active_players"] = 'Aktywni gracze';

$LNG["Settings"] = 'Ustawienia';

$LNG['uni_status_regopen_gameopen'] = 'Otwarty';
$LNG['uni_status_regclosed_gameclosed'] = 'Zamknięte';
$LNG['uni_status_regopen_gameclosed'] = 'Rejestracja otwarta';
$LNG['uni_status_regclosed_gameopen'] = 'Otwarty do gry';

$LNG['Back'] = 'Powrót';
// Translated into German by Reflexrecon . All rights reversed (C) 2024
