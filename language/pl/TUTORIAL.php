<?php

/**
 * pr0game powered by steemnova
 * tutorial
 * (c) 2024 Hyman
 */

$LNG['tutorial_goals_header'] = 'Cele:';
$LNG['tutorial_goals_ignore'] = '(ignoruj)';

$LNG['tutorial_rewards_header'] = 'Nagrody:';

$LNG['tutorial_buttons_finalize'] = 'Ukończ samouczek';
$LNG['tutorial_buttons_claim'] = 'Odbierz nagrodę';
$LNG['tutorial_buttons_continue'] = 'Kontynuuj';
$LNG['tutorial_buttons_skip'] = 'Pomiń';
$LNG['tutorial_buttons_skipAll'] = 'Pomiń samouczek';
$LNG['tutorial_buttons_skipAll_confirm'] = 'Czy naprawdę chcesz pominąć resztę samouczka? Nie można go ponownie aktywować!';

$LNG['tutorial_title']['step0'] = 'O co chodzi w tej grze?';
$LNG['tutorial_text']['step0'] = 
'Witamy w pr0game!<br><br>
Ten samouczek wprowadzi cię w podstawy gry.<br>
Często pojawiają się odnośniki do naszego Wiki, aby dostarczyć więcej informacji, ponieważ nie wszystkie aspekty gry mogą być wyjaśnione w samouczku.
Wiki można zawsze znaleźć w menu po lewej stronie.<br>
Możesz pominąć samouczek w dowolnym momencie, ale nie będziesz mógł go ponownie uruchomić.<br><br>
Powodzenia, Imperatorze!';

$LNG['tutorial_title']['step1'] = 'Zacznij zarabiać!';
$LNG['tutorial_text']['step1'] = 
'W tej grze chodzi o zdobywanie jak największej liczby punktów. Jeden punkt zdobywa się za każde wydane 1 000 surowców.
Surowce można zdobyć na różne sposoby. Najważniejszym z nich jest produkcja z własnych kopalni.<br>
Zapewnij więc najpierw podstawowe zaopatrzenie, budując kopalnie metalu i kryształu oraz zapewniając energię z elektrowni słonecznej.
Brak wystarczającej energii oznacza niższą produkcję.<br>
Te budynki możesz rozbudować w menu <b>Budynki</b>. Klikając na nazwę budynku, zobaczysz, ile zasobów przyniesie jego rozbudowa oraz ile energii będzie zużywać na wyższym poziomie.<br><br>
Więcej informacji znajdziesz w <b>Wiki</b>:
<a href="https://wiki.pr0game.com/doku.php?id=en:rohstoffproduktion" target="_blank">Raw material production</a>';

$LNG['tutorial_title']['step2'] = 'Poznaj innych graczy!';
$LNG['tutorial_text']['step2'] = 
'Gra jest bardziej interesująca, gdy możesz komunikować się z innymi graczami.
Większość graczy korzysta z naszego Discorda.
W społeczności znajdziesz odpowiedzi na swoje pytania oraz innych graczy, z którymi możesz grać razem.<br>
W razie potrzeby możesz również tworzyć zgłoszenia pomocy.';

$LNG['tutorial_title']['step3'] = 'Nie stój bezbronny na otwartej przestrzeni!';
$LNG['tutorial_text']['step3'] = 
'Innym sposobem na zdobywanie surowców jest atakowanie planet innych graczy.<br>
Jednak to samo może spotkać ciebie - zbuduj swoją pierwszą obronę!<br>
Obrona jest ważna, by chronić przynajmniej zasoby wyprodukowane w nocy.<br>
Tutaj liczy się nie tylko ilość, ale również kompozycja floty atakującej.<br>
Zanim cię ograbią, napastnik musi przełamać twoją obronę. Im więcej statków musi wysłać, tym mniej zarobi i bardziej ryzykuje straty.<br>
Instalacje obronne można budować w menu <b>Obrona</b> za pomocą stoczni. Aby ją zbudować, potrzebujesz fabryki robotów i deuteru.<br><br>
Więcej informacji w Wiki:
<a href="https://wiki.pr0game.com/doku.php?id=en:verteidigung" target="_blank">Defense facilities</a>';

$LNG['tutorial_title']['step4'] = 'Myśl strategicznie!';
$LNG['tutorial_text']['step4'] = 
'Zapewnij ciągłe zaopatrzenie, rozbudowując swoje kopalnie!<br>
Budowa budynków, statków i obrony, a także badania, stają się coraz droższe z czasem.
Aby sprostać potrzebom zasobowym swojej planety, musisz stale zwiększać produkcję.
Teraz wejdź do menu Budynki i rozbuduj swoje budynki, gdy masz odpowiednie zasoby.';

$LNG['tutorial_title']['step5'] = 'Potrzebujemy statków!';
$LNG['tutorial_text']['step5'] = 
'Zbuduj mały transporter!<br>
Małe transportery są szybką metodą transportu w późniejszej grze.<br>
Gdy lecą z napędem impulsowym, są szybsze niż duże transportery na długich dystansach.<br>
Służą do transportu surowców między twoimi planetami, handlu z innymi graczami lub napadania na nich.<br><br>
Więcej informacji w Wiki:
<a href="https://wiki.pr0game.com/doku.php?id=en:expoanleitung#what_do_i_find_with_what" target="_blank">Ships</a>';

$LNG['tutorial_title']['step6'] = 'Nazwij swoją planetę i szukaj sojuszników!';
$LNG['tutorial_text']['step6'] = 
'Aby lepiej zarządzać planetami, można je nazwać. Kliknij na nazwę planety w menu głównym, aby nadać jej nazwę.<br><br>
Możesz także wysyłać prośby o znajomych do innych graczy. Po zaakceptowaniu łatwiej jest komunikować się i widzieć status online.
Nie trzeba być w tej samej alianсji.
Możesz również wysłać swoje floty do sojusznika w celu wspólnej obrony.<br><br>
Możesz także dołączyć do aliansu, aby grać z innymi graczami. W aliansie nie potrzebujesz osobnych próśb o znajomych do trzymania floty na ich planetach.
Członkowie aliansu mogą wysyłać wiadomości grupowe i widzieć status online oraz punkty innych członków, w zależności od ich rangi.';

$LNG['tutorial_goal']['step6'][1] = 'Zmień nazwę swojej planety';
$LNG['tutorial_goal']['step6'][2] = 'Wyślij prośbę o znajomego do innego gracza';
$LNG['tutorial_goal']['step6'][3] = 'Wyślij prośbę o dołączenie do aliansu';

$LNG['tutorial_title']['step7'] = 'Zobacz, co mają inni!';
$LNG['tutorial_text']['step7'] = 
'Aby zobaczyć zasoby, budynki, badania, statki i obronę innych graczy, musisz ich szpiegować.
Wysyłaj sondy szpiegowskie przez menu Floty lub z Galaktyki.<br>
Pamiętaj, że inni gracze mogą cię również szpiegować!';
$LNG['tutorial_goal']['mission'][6] = 'Wyślij sondę szpiegowską do innego gracza.';

$LNG['tutorial_title']['step8'] = 'Eksploruj nieznane!';
$LNG['tutorial_text']['step8'] = 
'Eksploracje mogą przynieść zasoby, statki, zdarzenia lub straty.
Wypróbuj to, wysyłając sondę na ekspedycję - wybierz współrzędne planety 16 i misję "Ekspedycja" w menu Floty.<br>
Tylko sonda? - Tak, choć nie zabiera wielu zasobów, znajduje tyle samo statków co mała flota i jest szybsza. To opłacalne na początku.<br><br>
Więcej informacji znajdziesz w Wiki:
<a href="https://wiki.pr0game.com/doku.php?id=en:expoanleitung" target="_blank">Expeditions</a>';
$LNG['tutorial_goal']['mission'][15] = 'Wyślij flotę na ekspedycję';

$LNG['tutorial_title']['step9'] = 'Nie zostaniesz przy jednej planecie!';
$LNG['tutorial_text']['step9'] = 
'Aby przyspieszyć produkcję, zasiedlaj kolejne planety.
Produkcja na wielu planetach pozwala na jednoczesną budowę i produkcję.<br>
Kolonizacja wymaga odpowiedniego poziomu Astrofizyki, a pierwszy statek kolonizacyjny możesz zbudować bez niej, ale kolonizacja wymaga już Astrofizyki!<br>
Bliskość planet skraca czas transportu, ale może uczynić cię celem dla graczy szukających szybkich ataków.<br><br>
Więcej informacji znajdziesz w Wiki:
<a href="https://wiki.pr0game.com/doku.php?id=en:astrophysik" target="_blank">Astrophysics</a>';
$LNG['tutorial_goal']['mission'][7] = 'Zasiedlaj kolejną planetę';

$LNG['tutorial_title']['step10'] = 'Nie czekaj na zasoby z własnej produkcji!';
$LNG['tutorial_text']['step10'] = 
'Aby szybciej zwiększać swoją potęgę, możesz handlować z innymi graczami.<br>
W ten sposób możesz zdobyć zasoby, które są ci potrzebne, lub pozbyć się nadmiaru, jeśli masz ich za dużo.<br>
Oferty handlowe można publikować na naszym serwerze Discord. Możesz również wyszukać inne oferty i zawrzeć transakcję.<br>
Jako kurs wymiany przyjęto <b>2.5:1.5:1 dla Metalu:Kryształu:Deuteru</b>, ale można ustalać inne kursy. Przestrzegaj zasad gry i handluj uczciwie!<br><br>
Więcej informacji znajdziesz w Wiki:
<a href="https://wiki.pr0game.com/doku.php?id=en:slippys_progame_flottensicherung_leitfaden" target="_blank">Saven</a> - 
<a href="https://wiki.pr0game.com/doku.php?id=en:sensorphalanx" target="_blank">Sensorphalanx</a>';
$LNG['tutorial_goal']['mission'][9] = 'Dokonaj wymiany surowców z innym graczem';

$LNG['tutorial_title']['step11'] = 'Zagraj w recyklera!';
$LNG['tutorial_text']['step11'] = 
'Podczas niszczenia flot powstaje pole szczątków, które można zbierać. 
Do tego potrzebujesz recyklera, który może zebrać do 20k zasobów z pola szczątków. 
Zniszczenie dużej floty może być zatem lukratywnym źródłem zasobów.<br><br>
Zamiast korzystać z menu floty, recyklery mogą być również wysyłane bezpośrednio z menu Galaktyki, klikając na pole szczątków i wybierając odpowiednią ilość.';
$LNG['tutorial_goal']['mission'][8] = 'Zrecyklinguj pole szczątków';

$LNG['tutorial_title']['step12'] = 'Udało Ci się';
$LNG['tutorial_text']['step12'] = 
'Gratulacje, ukończyłeś samouczek!<br><br>
Gra oferuje wiele innych mechanik.<br>
Na przykład istnieją zasadniczo 3 style gry, różniące się czasem zaangażowania:<br>
<ul>
<li>Górnik: rozwija kopalnie na wielu koloniach, głównie używa transporterów</li>
<li>Flociarz: większość punktów zdobywa za flotę bojową, produkuje niewiele zasobów samodzielnie, polega na najazdach na innych graczy</li>
<li>Mieszany: połączenie obu stylów</li>
</ul>
W późniejszym etapie gry ważne stają się księżyce, a Ty już poznałeś Phalankę Sensorów.<br><br>
Jeśli masz dalsze pytania, możesz zajrzeć do Wiki lub poprosić o pomoc społeczność na Discordzie.';
