<?php

/**
 * pr0game powered by steemnova
 * Accounts
 * (c) 2024 reflexrecon
 */
$LNG["Universes"] = 'Вселенные';
$LNG["Your_universes"] = 'Ваши вселенные';
$LNG["Other_universes"] = 'Другие вселенные';
$LNG["Universe_status"] = 'Состояние вселенных: ';
$LNG["Playername"] = 'Игровое имя';
$LNG["Register"] = 'Регистрация';
$LNG["Universe_start"] = 'Начало';
$LNG["Universe_end"] = 'Конец';
$LNG["Sitting_end"] = 'Сидячий конец';

$LNG["Sitting_request"] = 'Запрос Сидеть (не запрограммирован)';
$LNG["Sitting_cancel"] = 'Прервать сидение';

$LNG["Play"] = 'Играть';
$LNG["Active_players"] = 'Активные игроки';

$LNG["Settings"] = 'Настройки';

$LNG['uni_status_regopen_gameopen'] = 'Открыть';
$LNG['uni_status_regclosed_gameclosed'] = 'Закрытый';
$LNG['uni_status_regopen_gameclosed'] = 'Открыт для регистрации';
$LNG['uni_status_regclosed_gameopen'] = 'Открыто для игры';

$LNG['Back'] = 'Назад';
// Translated into German by Reflexrecon . All rights reversed (C) 2024
