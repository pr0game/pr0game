<?php

/**
 * pr0game powered by steemnova
 * Accounts
 * (c) 2024 reflexrecon
 */
$LNG["Universes"] = 'Evrenler';
$LNG["Your_universes"] = 'Evrenleriniz';
$LNG["Other_universes"] = 'Diğer Evrenler';
$LNG["Universe_status"] = 'Evrenlerin durumu: ';
$LNG["Playername"] = 'Oyuncu Adı';
$LNG["Register"] = 'Kayıt Olun';
$LNG["Universe_start"] = 'Başlangıç';
$LNG["Universe_end"] = 'Bitiş';
$LNG["Sitting_end"] = 'Oturma ucu';

$LNG["Sitting_request"] = 'Oturma Talebi (programlanmamış)';
$LNG["Sitting_cancel"] = 'Oturmayı İptal Et';

$LNG["Play"] = 'Oyun';
$LNG["Active_players"] = 'Aktif Oyuncular';

$LNG["Settings"] = 'Ayarlar';

$LNG['uni_status_regopen_gameopen'] = 'Açık';
$LNG['uni_status_regclosed_gameclosed'] = 'Kapalı';
$LNG['uni_status_regopen_gameclosed'] = 'Kayıt için açık';
$LNG['uni_status_regclosed_gameopen'] = 'Oynamaya açık';

$LNG['Back'] = 'Geri';
// Translated into German by Reflexrecon . All rights reversed (C) 2024
