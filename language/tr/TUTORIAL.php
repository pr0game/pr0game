<?php

/**
 * pr0game powered by steemnova
 * tutorial
 * (c) 2024 Hyman
 */

$LNG['tutorial_goals_header'] = 'Hedefler:';
$LNG['tutorial_goals_ignore'] = '(görmezden gel)';

$LNG['tutorial_rewards_header'] = 'Ödüller:';

$LNG['tutorial_buttons_finalize'] = 'Öğreticiyi tamamla';
$LNG['tutorial_buttons_claim'] = 'Ödül talep et';
$LNG['tutorial_buttons_continue'] = 'Devam et';
$LNG['tutorial_buttons_skip'] = 'Atla';
$LNG['tutorial_buttons_skipAll'] = 'Öğreticiyi atla';
$LNG['tutorial_buttons_skipAll_confirm'] = 'Gerçekten eğitimin geri kalanını atlamak istiyor musunuz? Yeniden etkinleştiremezsiniz!';

$LNG['tutorial_title']['step0'] = 'Bu ne tür bir oyun?';
$LNG['tutorial_text']['step0'] = 
'Pr0game\'e hoş geldin!<br><br>
Bu eğitimde, oyunun temel bilgileri gösterilecektir.<br>
Ek bilgi için sık sık wiki\'ye başvurulacak, çünkü oyunun tüm yönleri eğitimde açıklanamaz. 
Wiki’ye sol menüden istediğin zaman erişebilirsin.<br>
Eğitimi istediğin zaman atlayabilirsin ancak tekrar başlatılamaz.<br><br>
Bol şanslar, İmparator!';

$LNG['tutorial_title']['step1'] = 'Bir şeyler kazan!';
$LNG['tutorial_text']['step1'] = 
'Bu oyunun amacı, mümkün olduğunca çok puan kazanmaktır. Harcadığın her 1.000 kaynak için bir puan kazanırsın.
Bu kaynaklara farklı yollarla ulaşılabilir. En önemlisi ise madenlerin kendi üretimidir.<br>
Öncelikle temel kaynak üretimini sağla. Bunun için, Metal ve Kristal madenlerinden kaynak elde etmelisin. Madenlerin çalışması için de Güneş Enerji Santrali’nden enerji gereklidir.
Yeterli enerji sağlanmadığında %100 üretim sağlanamaz.<br>
Bu binaları <b>Binalar</b> menüsünden genişletebilirsin. Binanın adının üzerine tıklayarak, yükseltmenin kaynak değerini görebilirsin. 
Ayrıca, bir sonraki seviyenin ne kadar enerji tükettiğini de göreceksin.<br><br>
Daha fazla bilgi için wiki\'ye bakabilirsin: 
<a href="https://wiki.pr0game.com/doku.php?id=en:rohstoffproduktion" target="_blank">Raw material production</a>';

$LNG['tutorial_title']['step2'] = 'İnsanlarla kaynaş!';
$LNG['tutorial_text']['step2'] = 
'Diğer oyuncularla etkileşimde bulunmak oyunu daha eğlenceli hale getirir.
Çoğu oyuncu Discord sunucumuzda organize olur. 
Toplulukta sorularına yanıt alabilir ve birlikte oynayacak diğer oyuncuları bulabilirsin.<br>
Belirsizlik durumlarında burada destek bileti açabilirsin.';

$LNG['tutorial_title']['step3'] = 'Savunmasız kalma!';
$LNG['tutorial_text']['step3'] = 
'Kaynak elde etmenin bir diğer yolu, diğer oyuncuların gezegenlerini yağmalamaktır.<br>
Ancak bu sana da yapılabilir - bu yüzden ilk savunmanı inşa et!<br>
En azından gece üretimini korumak için bir savunmaya sahip olmak önemlidir.<br>
Ancak yalnızca savunma miktarı değil, saldırı filolarının kompozisyonu da önemlidir.<br>
Yağmalamadan önce saldırganın savunmanı aşması gerekir. Saldırganın göndermek zorunda kaldığı her gemi, kazancını azaltır ve gemilerinin zarar görme riskini artırır.<br>
Savunma yapıları <b>Savunma</b> menüsünde Gemi Tersanesi ile inşa edilir. Bu da bir Robot Fabrikası ve Deuterium gerektirir.<br><br>
Wiki\'de savunma hakkında daha fazla bilgi bulabilirsin: 
<a href="https://wiki.pr0game.com/doku.php?id=en:verteidigung" target="_blank">Defense facilities</a>';

$LNG['tutorial_title']['step4'] = 'Uzun vadeli düşün!';
$LNG['tutorial_text']['step4'] = 
'Kaynak üretimini artırarak sağlama al!<br>
Binaların, gemilerin, savunman ve araştırmalar zamanla daha pahalı hale gelir.
Bu nedenle, üretimini artırarak gezegeninin kaynak ihtiyaçlarını karşılamaya çalışmalısın.
Yeterli kaynak olduğunda binalarını genişletmek için <b>Binalar</b> menüsüne tıkla ve gerekli yükseltmeleri yap.';

$LNG['tutorial_title']['step5'] = 'Gemiye ihtiyacımız var!';
$LNG['tutorial_text']['step5'] = 
'Küçük bir taşıyıcı inşa et!<br>
Küçük taşıyıcılar, oyunun ilerleyen safhalarında hızlı bir taşıma aracıdır.<br>
Eğer itme motoruyla donatılırsa, büyük taşıyıcılardan uzun süre daha hızlı olabilirler.<br>
Bu taşıyıcılar, gezegenler arasında kaynak taşımak, diğer oyuncularla ticaret yapmak veya yağmalamak için kullanılır.<br><br>
Wiki\'de daha fazla bilgi bulabilirsin: 
<a href="https://wiki.pr0game.com/doku.php?id=en:expoanleitung#what_do_i_find_with_what" target="_blank">Ships</a>';

$LNG['tutorial_title']['step6'] = 'Gezegenine isim ver ve dostlar bul!';
$LNG['tutorial_text']['step6'] = 
'Gezegenleri daha iyi yönetmek için yeniden adlandırabilirsin. Üst menüdeki puan gösterge alanında gezegen adını tıklayarak isim değiştirebilirsin.<br><br>
Ayrıca diğer oyunculara dostluk isteği gönderebilirsin. İstek kabul edilirse daha kolay iletişim kurabilir ve birbirinizin çevrim içi durumunu görebilirsiniz. 
Aynı ittifakta olmanıza gerek yoktur. 
Dost oyunculara filo desteği verebilir, birlikte savunma yapabilirsiniz.<br><br>
Ayrıca bir ittifaka katılarak daha fazla oyuncu ile birlikte oynayabilirsin. 
İttifakta dostluk isteğine gerek olmadan filo desteği sağlanabilir. 
İttifak içinde grup mesajları gönderilebilir, çevrim içi durumu ve puan durumu görebilirsiniz - ittifak rütbesine bağlı olarak.';

$LNG['tutorial_goal']['step6'][1] = 'Gezegenini yeniden adlandır';
$LNG['tutorial_goal']['step6'][2] = 'Bir oyuncuya dostluk isteği gönder';
$LNG['tutorial_goal']['step6'][3] = 'Bir ittifaka katıl';

$LNG['tutorial_title']['step7'] = 'Diğerlerinin neye sahip olduğunu öğren!';
$LNG['tutorial_text']['step7'] = 
'Diğer oyuncuların kaynaklarını, binalarını, araştırmalarını, gemilerini ve savunmalarını görmek için onları casuslayabilirsin. 
Bir casus probunu diğer bir oyuncuya göndermek için Filo menüsünden veya Galaksi menüsünden yola çıkabilirsin.<br>
Ancak, diğer oyuncular da seni casuslayabilir!';
$LNG['tutorial_goal']['mission'][6] = 'Bir oyuncuya casus sondası gönder.';

$LNG['tutorial_title']['step8'] = 'Bilinmeze yolculuk!';
$LNG['tutorial_text']['step8'] = 
'Bir keşif gezisi, kaynaklar, gemiler, olaylar veya kayıplar getirebilir. 
Bunu denemek için bir casus probu al ve Filo menüsünde Koordinat 16 ve "Keşif" görevini seçerek bir keşif gezisine çık.<br>
Bir prob mu? - Evet. Çok az kaynak taşısa da, küçük bir taşıyıcı filosu kadar gemi bulabilir ve daha hızlıdır. Başlangıç için faydalıdır.<br><br>
Wiki\'de daha fazla bilgiye erişebilirsin: 
<a href="https://wiki.pr0game.com/doku.php?id=en:expoanleitung" target="_blank">Expeditions</a>';
$LNG['tutorial_goal']['mission'][15] = 'Bir keşif filosu gönder';

$LNG['tutorial_title']['step9'] = 'Tek bir gezegenle yetinemezsin!';
$LNG['tutorial_text']['step9'] = 
'Hızlı bir üretim için daha fazla gezegen kolonileştir. 
Bu şekilde, aynı anda iki veya daha fazla gezegende üretim yapabilir ya da gemi inşa edebilirsin.
Sistemin iç kısmında veya dış kısmında koloni kurmak için yeterince yüksek bir Astrofizik seviyesi gereklidir.
Koloni gemisi Astrofizik olmadan inşa edilebilir, ancak ilk koloni Astrofizik gerektirir!<br><br>
Bir sistemde ya da komşu sistemlerde kolonileşmek, nakliye yollarını kısa tutar ancak daha sonra oyunda, saldırgan oyuncular için kolay bir hedef olabilirsin.<br><br>
Daha fazla bilgi için Wiki\'ye göz atabilirsin: 
<a href="https://wiki.pr0game.com/doku.php?id=en:astrophysik" target="_blank">Astrophysics</a>';
$LNG['tutorial_goal']['mission'][7] = 'Yeni bir gezegen kolonileştir';

$LNG['tutorial_title']['step10'] = 'Gerçekten sadece kendi üretiminle mi yetineceksin?';
$LNG['tutorial_text']['step10'] = 
'Kaynaklara daha hızlı ulaşmak istiyorsan başka oyunculara saldırabilir ya da kaynak transfer edebilirsin.<br>
Ancak dikkat et: diğer oyuncuların kaynaklarını almak ya da onların seni yağmalamasını engellemek için iyi bir stratejiye ihtiyacın var! 
Başlangıç için küçük taşıyıcıları diğer oyuncuların gezegenlerine gönderip kaynaklarını yağmalayabilirsin.
Yağmalama esnasında dikkat edilmesi gereken temel şeyler için Wiki\'yi inceleyebilirsin: 
<a href="https://wiki.pr0game.com/doku.php?id=en:slippys_progame_flottensicherung_leitfaden" target="_blank">Saven</a> - 
<a href="https://wiki.pr0game.com/doku.php?id=en:sensorphalanx" target="_blank">Sensorphalanx</a>';
$LNG['tutorial_goal']['mission'][7] = 'Başka bir oyuncunun gezegenini yağmala';

$LNG['tutorial_title']['step11'] = 'Çöpçü modunu dene!';
$LNG['tutorial_text']['step11'] = 
'Filolar yok edildiğinde, bir enkaz alanı oluşur ve bu alan kaynaklarla doludur.
Bu enkazı toplamak için bir geri dönüştürücüye ihtiyacın var. Bu gemiler, enkaz alanından 20.000’e kadar kaynak toplayabilirler.
Büyük bir filoyu yok etmek, yüksek miktarda kaynak kazancı sağlayabilir.<br><br>
Geri dönüştürücüleri, filo menüsünden göndermek yerine galaksi menüsünden doğrudan enkaz alanına tıklayarak da yollayabilirsin.';
$LNG['tutorial_goal']['mission'][8] = 'Bir enkaz alanını geri dönüştür';

$LNG['tutorial_title']['step12'] = 'Başarıya ulaştın';
$LNG['tutorial_text']['step12'] = 
'Tebrikler, eğitimi tamamladın!<br><br>
Bu oyun daha pek çok mekanik sunuyor. Örneğin, zaman ve çaba gereksinimleri açısından farklılık gösteren üç ana oyun tarzı var:<br>
<ul>
<li>Madenci: Kolonilerde madenleri geliştirir, çoğunlukla taşıyıcı kullanır</li>
<li>Filocu: Puanlarının çoğunu savaş gemilerinden elde eder, kendi üretimi azdır, diğer oyuncuları yağmalar</li>
<li>Komber: Her iki tarzın bir kombinasyonudur</li>
</ul>
Oyunun ilerleyen aşamalarında, aylar önemli hale gelir ve sensor filoları gibi özellikleri keşfetmiş olmalısın.<br><br>
Daha fazla soruların varsa, wiki\'yi okuyabilir veya Discord’da topluluktan yardım isteyebilirsin.';
