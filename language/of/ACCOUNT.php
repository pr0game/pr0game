<?php

/**
 * pr0game powered by steemnova
 * Accounts
 * (c) 2024 reflexrecon
 */
$LNG["Universes"] = 'Unifersne';
$LNG["Your_universes"] = 'Deine Unifersne';
$LNG["Other_universes"] = 'Andere Unifersum';
$LNG["Universe_status"] = 'Unifersums Schdadus: ';
$LNG["Playername"] = 'Schbielername';
$LNG["Register"] = 'Regisdrierne';
$LNG["Universe_start"] = 'Schdard';
$LNG["Universe_end"] = 'Ende';
$LNG["Sitting_end"] = 'Sidding Ende';

$LNG["Sitting_request"] = 'Sidding anfragne (ned brogrammierd)';
$LNG["Sitting_cancel"] = 'Sidding abbrechne';

$LNG["Play"] = 'Schbielne';
$LNG["Active_players"] = 'Agdife Schbieler';

$LNG["Settings"] = 'Einsdellungne';

$LNG['uni_status_regopen_gameopen'] = 'Offne';
$LNG['uni_status_regclosed_gameclosed'] = 'Geschlossne';
$LNG['uni_status_regopen_gameclosed'] = 'Regisdrierung offne';
$LNG['uni_status_regclosed_gameopen'] = 'Anmeldung offne';

$LNG['Back'] = 'Dsurügg';
// Translated into German by Reflexrecon . All rights reversed (C) 2024
