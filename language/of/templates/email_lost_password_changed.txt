Hallo {USERNAME},

dein neues Kennwort für die Website "{GAMENAME}" lautet:
{PASSWORD}

Solltest du immer noch Probleme mit dem Login haben, erstelle bitte ein Ticket auf unserem Discord:
{DISCORD}

Vielen Dank,

Dein {GAMENAME} Team
