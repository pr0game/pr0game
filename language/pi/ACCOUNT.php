<?php

/**
 * pr0game powered by steemnova
 * Accounts
 * (c) 2024 reflexrecon
 */
$LNG["Universes"] = 'Weltmeere';
$LNG["Your_universes"] = 'Deine Weltmeere';
$LNG["Other_universes"] = 'Andere Weltmeere';
$LNG["Universe_status"] = 'Weltmeers Status: ';
$LNG["Playername"] = 'Spielername';
$LNG["Register"] = 'Registrieren';
$LNG["Universe_start"] = 'Start';
$LNG["Universe_end"] = 'Ende';
$LNG["Sitting_end"] = 'Sitting Ende';

$LNG["Sitting_request"] = 'Sitting anfragen (nicht programmiert)';
$LNG["Sitting_cancel"] = 'Sitting abbrechen';

$LNG["Play"] = 'Spielen';
$LNG["Active_players"] = 'Aktive Piraten';

$LNG["Settings"] = 'Einstellungen';

$LNG['uni_status_regopen_gameopen'] = 'Offen';
$LNG['uni_status_regclosed_gameclosed'] = 'Geschlossen';
$LNG['uni_status_regopen_gameclosed'] = 'Registrierung offen';
$LNG['uni_status_regclosed_gameopen'] = 'Anmeldung offen';

$LNG['Back'] = 'Zurück';
// Translated into German by Reflexrecon . All rights reversed (C) 2024
