const atackids = new Set([202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 213, 214, 215])
const defids = new Set([202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 401, 402, 403, 404, 405, 406, 407, 408])

function get_exportstring() {
    console.log("Exporting....")
    let currentwave = Sim.waves[0]
    var mainstring = currentwave.resources.metal + " " + currentwave.resources.crystal + " " + currentwave.resources.deuterium + " " + currentwave.plunder + "\n"


    for (let i = 0; i < Sim.waves.length; i++) {
        currentwave = Sim.waves[i]


        for (let fleet of currentwave.fleets['atk']) {
            console.log(fleet.origin.galaxy)
            let farray = [0,
                fleet.id,
                fleet.player.techs[109],
                fleet.player.techs[110],
                fleet.player.techs[111],
                fleet.player.techs[115],
                fleet.player.techs[117],
                fleet.player.techs[118],
                fleet.origin.galaxy,
                fleet.origin.system,
                fleet.origin.position,
                fleet.shipsSpeed,
                fleet.recyclers,
                fleet.recyclersOrigin.galaxy,
                fleet.recyclersOrigin.system,
                fleet.recyclersOrigin.position,
                fleet.recyclersSpeed,
                fleet.recyclersDistance,
                fleet.recyclersFuel,
                fleet.recyclersCargo,
                fleet.recyclersDuration
            ]
            for (let shipid of atackids) {
                if (shipid in fleet.ships) {
                    farray.push(fleet.ships[shipid])
                } else {
                    farray.push(0)
                }
            }
            mainstring = mainstring + (farray.map(item => String(item).trim()).join(" ") + "\n")
        }
        for (let fleet of currentwave.fleets['def']) {
            console.log(fleet.origin.galaxy)
            let farray = [1,
                fleet.id,
                fleet.player.techs[109],
                fleet.player.techs[110],
                fleet.player.techs[111],
                fleet.player.techs[115],
                fleet.player.techs[117],
                fleet.player.techs[118],
                fleet.origin.galaxy,
                fleet.origin.system,
                fleet.origin.position,
                fleet.shipsSpeed,
                fleet.recyclers,
                fleet.recyclersOrigin.galaxy,
                fleet.recyclersOrigin.system,
                fleet.recyclersOrigin.position,
                fleet.recyclersSpeed,
                fleet.recyclersDistance,
                fleet.recyclersFuel,
                fleet.recyclersCargo,
                fleet.recyclersDuration
            ]
            if (fleet.spaceDock) {
                farray.push(fleet.spaceDock)
            } else {
                farray.push(0)
            }

            for (let shipid of defids) {
                if (shipid in fleet.ships) {
                    farray.push(fleet.ships[shipid])
                } else {
                    farray.push(0)
                }
            }
            mainstring = mainstring + (farray.map(item => String(item).trim()).join(" ") + "\n")
        }
        if (currentwave.simulations != 0) {
            console.log("multimple waves!" + i)
            console.log(currentwave.simulations)
            mainstring = mainstring + "# " + currentwave.simulations + "\n"
        }

    }

    fade_text("Exported!", 500)
    return LZString.compressToEncodedURIComponent(mainstring)

}

async function waitForButtonEnable(button) {
    return new Promise((resolve) => {
        // Create a MutationObserver to watch for changes in the button's disabled attribute
        const observer = new MutationObserver((mutations) => {
            mutations.forEach((mutation) => {
                if (mutation.attributeName === 'disabled') {
                    // Check if the button is now enabled
                    if (!button.disabled) {
                        // Stop observing and resolve the promise
                        observer.disconnect();
                        resolve();
                    }
                }
            });
        });

        // Start observing the button for attribute changes
        observer.observe(button, {attributes: true});

        // Also, resolve immediately if the button is already enabled
        if (!button.disabled) {
            observer.disconnect();
            resolve();
        }
    });
}

function delay(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function import_settings(importstring) {
    fade_text("importing...", 500)
    reset_sim()
    let lines = LZString.decompressFromEncodedURIComponent(importstring).split("\n")
    const atackids = [202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 213, 214, 215]
    const atackids_def = [202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215]
    const defids = [401, 402, 403, 404, 405, 406, 407, 408]
    //set_res
    let l1 = lines[0].split(" ")
    let met = l1[0]
    let cryst = l1[1]
    let deut = l1[2]
    let plunder = l1[3]
    let cwave = Sim.waves[0]
    let cwave_idx = 0
    console.log(met, cryst, deut, lines[0].split(" "))
    let obj = document.querySelector('input[data-sim="resource"][data-entity="metal"]')
    obj.value = met
    obj.dispatchEvent(new Event('change'));
    obj = document.querySelector('input[data-sim="resource"][data-entity="crystal"]')
    obj.value = cryst
    obj.dispatchEvent(new Event('change'));
    obj = document.querySelector('input[data-sim="resource"][data-entity="deuterium"]')
    obj.value = deut
    obj.dispatchEvent(new Event('change'));
    document.getElementById('plunder').value = parseInt(plunder)
    document.getElementById('plunder').dispatchEvent(new Event('change'));

    //cwave.plunderPossible.metal = parseInt(met)
    //cwave.plunderPossible.crystal = parseInt(cryst)
    //cwave.plunderPossible.deuterium = parseInt(deut)
    //cwave.plunder = parseInt(plunder)
    let attackerumber = 0
    let defendernumber = 0
    for (let line of lines) {
        let spt = line.split(" ")
        if (spt.length === 4 || spt.length === 1) {
            continue
        }
        if (spt[0] === "#") { //do next wave!
            let obj = document.getElementById('simSimulations')
            console.log("handling new wave...")
            obj.value = parseInt(spt[1])
            obj.dispatchEvent(new Event('change'))
            document.querySelector('a.nav-link[href="#"][data-player="0"][data-side="def"]').click()
            document.querySelector('a.nav-link[href="#"][data-player="0"][data-side="atk"]').click()
            await delay(500);
            //TODO: remove the users which don't exist in the current wave
            //TODO: adapt the export to be working with the import
            document.getElementById('simulate').click()
            let nextwavebutton = document.querySelector('button[data-wave="next"]')
            await waitForButtonEnable(nextwavebutton);
            nextwavebutton.click()
            cwave_idx += 1
            cwave = Sim.waves[cwave_idx]
            attackerumber = 0
            defendernumber = 0
            console.log("done")
            await delay(500);
            continue
        }
        if (spt[0] == 0) { //attacker

            let obj = document.querySelector('a.nav-link[data-player="' + attackerumber + '"][data-side="atk"]');
            if (obj) {
                obj.click()
            } else {
                document.querySelector('a.nav-link.addPlayerBtn[data-side="atk"]').click()
            }
            attackerumber += 1
            let fleet = cwave.fleets['atk'][attackerumber - 1]
            fleet.id = parseInt(spt[1])
            fleet.player.techs[109] = parseInt(spt[2])
            fleet.player.techs[110] = parseInt(spt[3])
            fleet.player.techs[111] = parseInt(spt[4])
            fleet.player.techs[115] = parseInt(spt[5])
            fleet.player.techs[117] = parseInt(spt[6])
            fleet.player.techs[118] = parseInt(spt[7])
            fleet.origin.galaxy = parseInt(spt[8])
            fleet.origin.system = parseInt(spt[9])
            fleet.origin.position = parseInt(spt[10])
            fleet.shipsSpeed = parseInt(spt[11])
            fleet.recyclers = parseInt(spt[12])
            fleet.recyclersOrigin.galaxy = parseInt(spt[13])
            fleet.recyclersOrigin.system = parseInt(spt[14])
            fleet.recyclersOrigin.position = parseInt(spt[15])
            fleet.recyclersSpeed = parseInt(spt[16])

            fleet.recyclersDistance = parseInt(spt[17])
            fleet.recyclersFuel = parseInt(spt[18])
            fleet.recyclersCargo = parseInt(spt[19])
            fleet.recyclersDuration = parseInt(spt[20])
            let offsetnr = 21
            for (let shipidx = 0; shipidx < atackids.length; shipidx++) {
                let nr = parseInt(spt[offsetnr + shipidx])
                if (nr === 0) {
                    continue
                }
                //fleet.ships[atackids[shipidx]]=nr
                let obj = document.querySelector('input[type="number"][data-side="atk"][data-sim="ship"][data-shipid="' + atackids[shipidx] + '"]')
                obj.value = nr
                //Sim.updateData(obj)
                obj.dispatchEvent(new Event('change'));
                console.log(atackids[shipidx], nr, obj)
            }
            let galaob = document.querySelector('input[data-sim="coordinate"][data-entity="galaxy"][data-side="atk"]')
            galaob.value = parseInt(spt[8])
            galaob.dispatchEvent(new Event('change'));

        } else {//defender

            let obj2 = document.querySelector('a.nav-link[data-player="' + defendernumber + '"][data-side="def"]');
            if (obj2) {
                obj2.click()
            } else {
                document.querySelector('a.nav-link.addPlayerBtn[data-side="def"]').click()
            }
            defendernumber += 1
            let fleet = cwave.fleets['def'][defendernumber - 1]
            fleet.id = parseInt(spt[1])
            fleet.player.techs[109] = parseInt(spt[2])
            fleet.player.techs[110] = parseInt(spt[3])
            fleet.player.techs[111] = parseInt(spt[4])
            fleet.player.techs[115] = parseInt(spt[5])
            fleet.player.techs[117] = parseInt(spt[6])
            fleet.player.techs[118] = parseInt(spt[7])
            fleet.origin.galaxy = parseInt(spt[8])
            fleet.origin.system = parseInt(spt[9])
            fleet.origin.position = parseInt(spt[10])
            fleet.shipsSpeed = parseInt(spt[11])
            fleet.recyclers = parseInt(spt[12])
            fleet.recyclersOrigin.galaxy = parseInt(spt[13])
            fleet.recyclersOrigin.system = parseInt(spt[14])
            fleet.recyclersOrigin.position = parseInt(spt[15])
            fleet.recyclersSpeed = parseInt(spt[16])
            fleet.recyclersDistance = parseInt(spt[17])
            fleet.recyclersFuel = parseInt(spt[18])
            fleet.recyclersCargo = parseInt(spt[19])
            fleet.recyclersDuration = parseInt(spt[20])
            let offsetnr = 22
            for (let shipidx = 0; shipidx < atackids_def.length; shipidx++) {
                let nr = parseInt(spt[offsetnr + shipidx])
                let obj = document.querySelector('input[type="number"][data-side="def"][data-sim="ship"][data-shipid="' + atackids_def[shipidx] + '"]')
                if (!obj) {
                    if (defendernumber !== 1) {
                        continue
                    }
                    obj = document.querySelector('input[data-sim="defence"][data-shipid="' + atackids_def[shipidx] + '"]')
                }
                obj.value = nr
                obj.dispatchEvent(new Event('change'));
            }
            offsetnr += atackids_def.length

            let galaob = document.querySelector('input[data-sim="coordinate"][data-entity="galaxy"][data-side="def"]')
            console.log("gala:", galaob.value, spt[8])
            galaob.value = parseInt(spt[8])
            galaob.dispatchEvent(new Event('change'));


            if (defendernumber !== 1) {
                continue
            }

            fleet.spaceDock = parseInt(spt[22])
            let obj = document.querySelector('input[data-buildid="35"]')
            obj.value = parseInt(spt[17])

            for (let shipidx = 0; shipidx < defids.length; shipidx++) {
                let nr = parseInt(spt[offsetnr + shipidx])
                let obj = document.querySelector('input[data-sim="defence"][data-shipid="' + defids[shipidx] + '"]')

                obj.value = nr
                if (obj.type === "checkbox") {
                    obj.checked = nr === 1
                }
                obj.dispatchEvent(new Event('change'));
            }


        }
        Sim.uiEvents()

    }
    document.querySelector('a.nav-link[href="#"][data-player="0"][data-side="def"]').click()
    document.querySelector('a.nav-link[href="#"][data-player="0"][data-side="atk"]').click()
}

function reset_sim() {
    Sim.wave = 0;
    Sim.waves = [];
    Sim.atk = 0;
    Sim.def = 0;
    Sim.lifeformsModal = null;
    Sim.recyclersModal = null;
    Sim.infosModal = null;
    Sim.wfModal = null;
    Sim.simModal = null;
    Sim.shareModal = null;
    Sim.init()
}


async function test_sim() {
    let original = get_exportstring()
    await import_settings(original)
    let newstr = get_exportstring()
    alert(original === newstr)

}

const universe_settings = {
    galaxies: 4,
    systems: 422,
    debrisFactor: 30,
//"globalDeuteriumSaveFactor":1,

}

function save_current() {
    let save_attackers = true
    let save_defenders = false
    let wave_obj = Sim.activeWave()
    let save_string = ""

    for (let fleet of currentwave.fleets['atk']) {
        let farray = [0,
            fleet.id,
            fleet.player.techs[109],
            fleet.player.techs[110],
            fleet.player.techs[111],
            fleet.player.techs[115],
            fleet.player.techs[117],
            fleet.player.techs[118],
            fleet.origin.galaxy,
            fleet.origin.system,
            fleet.origin.position,
            fleet.shipsSpeed,
            fleet.recyclers,
            fleet.recyclersOrigin.galaxy,
            fleet.recyclersOrigin.system,
            fleet.recyclersOrigin.position,
            fleet.recyclersSpeed]
        for (let shipid of atackids) {
            if (shipid in fleet.ships) {
                farray.push(fleet.ships[shipid])
            } else {
                farray.push(0)
            }
        }
        mainstring = mainstring + (farray.map(item => String(item).trim()).join(" ") + "\n")
    }
    for (let fleet of currentwave.fleets['def']) {
        let farray = [1,
            fleet.id,
            fleet.player.techs[109],
            fleet.player.techs[110],
            fleet.player.techs[111],
            fleet.player.techs[115],
            fleet.player.techs[117],
            fleet.player.techs[118],
            fleet.origin.galaxy,
            fleet.origin.system,
            fleet.origin.position,
            fleet.shipsSpeed,
            fleet.recyclers,
            fleet.recyclersOrigin.galaxy,
            fleet.recyclersOrigin.system,
            fleet.recyclersOrigin.position,
            fleet.recyclersSpeed,
        ]
        if (fleet.spaceDock) {
            farray.push(fleet.spaceDock)
        } else {
            farray.push(0)
        }

        for (let shipid of defids) {
            if (shipid in fleet.ships) {
                farray.push(fleet.ships[shipid])
            } else {
                farray.push(0)
            }
        }
        mainstring = mainstring + (farray.map(item => String(item).trim()).join(" ") + "\n")
    }


}


function fade_text(text, time) {
    document.getElementById('importfade').innerHTML = text;
    document.getElementById('importfade').hidden = false
    setTimeout(function () {
        document.getElementById('importfade').hidden = true
    }, time)


}


let save_modal = null
let saved_fleets = []
document.addEventListener("DOMContentLoaded", function () {

    const urlParams = new URLSearchParams(window.location.search);

// Check if "sim" parameter exists and retrieve its value
    if (urlParams.has('sim')) {
        const simValue = urlParams.get('sim');
        console.log(`sim parameter is present with value: ${simValue}`);
        setTimeout(() => {
            import_settings(simValue)
        }, 500)
    } else {
setTimeout(load_urls,500)


    }


    save_modal = new bootstrap.Modal(document.getElementById("save_fleet_modal"))
    document.getElementById("load_saved_fleets").addEventListener("click", load_saved_fleets)
    console.log("sim parameter is noasdsadt present in the URL.");
    load_storage_fleets()
    for (let i = 0; i < saved_fleets.length; i++) {
        create_dom_fleet(saved_fleets[i].name, i)
    }


});

function load_storage_fleets() {
    let fleets = localStorage.getItem("stored_fleets")
    if (fleets == null) {
        return
    }
    saved_fleets = JSON.parse(fleets)
}

function store_database() {
    saved_fleets
    localStorage.setItem("stored_fleets", JSON.stringify(saved_fleets))
}


function load_saved_fleets() {
    save_modal.show()
}


function load(elem) {
    const fleets = elem.dataset.fname;
    console.log('Load function called for:', elem, fleets);
    console.log(saved_fleets[parseInt(fleets)])
    loading_saved(saved_fleets[parseInt(fleets)])
    // Implement loading logic here
}

function loading_saved(fleet_obj) {
    let currentwave = Sim.waves[Sim.wave]
    let attackerumber = 0
    let defendernumber = 0

    if ("atk" in fleet_obj) {
        if (currentwave.fleets.atk.length > 1) {
            Sim["atk"] = currentwave.fleets.atk.length - 1
            while (Sim["atk"] !== 0) {
                console.log(Sim["atk"])
                document.querySelector('a[class="removePlayer"][data-side="atk"]').click()
            }
        }

        for (let idx = 0; idx < fleet_obj['atk'].length; idx++) {
            console.log("doing player", idx)
            let spt = fleet_obj['atk'][idx].info


            let obj = document.querySelector('a.nav-link[data-player="' + attackerumber + '"][data-side="atk"]');
            if (obj) {
                obj.click()
            } else {
                document.querySelector('a.nav-link.addPlayerBtn[data-side="atk"]').click()
            }
            attackerumber += 1
            let fleet = currentwave.fleets['atk'][attackerumber - 1]
            fleet.id = parseInt(spt[0])
            fleet.player.techs[109] = parseInt(spt[1])
            fleet.player.techs[110] = parseInt(spt[2])
            fleet.player.techs[111] = parseInt(spt[3])
            fleet.player.techs[115] = parseInt(spt[4])
            fleet.player.techs[117] = parseInt(spt[5])
            fleet.player.techs[118] = parseInt(spt[6])
            fleet.origin.galaxy = parseInt(spt[7])
            fleet.origin.system = parseInt(spt[8])
            fleet.origin.position = parseInt(spt[9])
            fleet.shipsSpeed = parseInt(spt[10])
            fleet.recyclers = parseInt(spt[11])
            fleet.recyclersOrigin.galaxy = parseInt(spt[12])
            fleet.recyclersOrigin.system = parseInt(spt[13])
            fleet.recyclersOrigin.position = parseInt(spt[14])
            fleet.recyclersSpeed = parseInt(spt[15])
            fleet.recyclersDistance = parseInt(spt[16])
            fleet.recyclersFuel = parseInt(spt[17])
            fleet.recyclersCargo = parseInt(spt[18])
            fleet.recyclersDuration = parseInt(spt[19])
            let ships = fleet_obj['atk'][idx].ships
            console.log(ships)
            for (let shipid in ships) {
                let obj = document.querySelector('input[type="number"][data-side="atk"][data-sim="ship"][data-shipid="' + shipid + '"]')
                obj.value = ships[shipid]
                obj.dispatchEvent(new Event('change'));
            }


            let galaob = document.querySelector('input[data-sim="coordinate"][data-entity="galaxy"][data-side="atk"]')
            galaob.value = parseInt(spt[7])
            galaob.dispatchEvent(new Event('change'));
        }

        document.querySelector('a.nav-link[href="#"][data-player="0"][data-side="atk"]').click()
    }


    if ("def" in fleet_obj) {
        if (currentwave.fleets.def.length > 1) {
            Sim["def"] = currentwave.fleets.atk.length - 1
            while (Sim["def"] !== 0) {
                console.log(Sim["def"])
                document.querySelector('a[class="removePlayer"][data-side="def"]').click()
            }
        }
        for (let idx = 0; idx < fleet_obj['def'].length; idx++) {
            console.log("doing player", idx)
            let spt = fleet_obj['def'][idx].info

            let obj2 = document.querySelector('a.nav-link[data-player="' + defendernumber + '"][data-side="def"]');
            if (obj2) {
                obj2.click()
            } else {
                document.querySelector('a.nav-link.addPlayerBtn[data-side="def"]').click()
            }
            defendernumber += 1
            let fleet = currentwave.fleets['def'][defendernumber - 1]
            fleet.id = parseInt(spt[0])
            fleet.player.techs[109] = parseInt(spt[1])
            fleet.player.techs[110] = parseInt(spt[2])
            fleet.player.techs[111] = parseInt(spt[3])
            fleet.player.techs[115] = parseInt(spt[4])
            fleet.player.techs[117] = parseInt(spt[5])
            fleet.player.techs[118] = parseInt(spt[6])
            fleet.origin.galaxy = parseInt(spt[7])
            fleet.origin.system = parseInt(spt[8])
            fleet.origin.position = parseInt(spt[9])
            fleet.shipsSpeed = parseInt(spt[10])
            fleet.recyclers = parseInt(spt[11])
            fleet.recyclersOrigin.galaxy = parseInt(spt[12])
            fleet.recyclersOrigin.system = parseInt(spt[13])
            fleet.recyclersOrigin.position = parseInt(spt[14])
            fleet.recyclersSpeed = parseInt(spt[15])
            fleet.recyclersDistance = parseInt(spt[16])
            fleet.recyclersFuel = parseInt(spt[17])
            fleet.recyclersCargo = parseInt(spt[18])
            fleet.recyclersDuration = parseInt(spt[19])
            let ships = fleet_obj['def'][idx].ships
            console.log(ships)
            for (let shipid in ships) {
                if (shipid in defids && defendernumber !== 1) {
                    continue
                }
                console.log(shipid)

                let obj = document.querySelector('input[type="number"][data-side="def"][data-shipid="' + shipid + '"]')
                if (obj == null) {

                    console.log(shipid)
                    continue
                }

                obj.value = ships[shipid]
                obj.dispatchEvent(new Event('change'));
            }
            let galaob = document.querySelector('input[data-sim="coordinate"][data-entity="galaxy"][data-side="def"]')
            galaob.value = parseInt(spt[7])
            galaob.dispatchEvent(new Event('change'));

            if (defendernumber !== 1) {
                continue
            }

            fleet.spaceDock = parseInt(spt[16])
            let obj = document.querySelector('input[data-buildid="35"]')
            obj.value = parseInt(spt[16])
            obj.dispatchEvent(new Event('change'));

        }


        document.querySelector('a.nav-link[href="#"][data-player="0"][data-side="def"]').click()


    }
}

function add_entry() {
    let name = document.getElementById("save_fleet_newname").value;
    if (name.trim().length === 0) {
        alert("Name is required");
        return
    }
    let currentwave = Sim.waves[Sim.wave]
    let saving = {name: name}


    if (document.getElementById("fleet_save_atk").checked) {
        let atkary = []


        for (let fleet of currentwave.fleets['atk']) {
            let data = [fleet.id,
                fleet.player.techs[109],
                fleet.player.techs[110],
                fleet.player.techs[111],
                fleet.player.techs[115],
                fleet.player.techs[117],
                fleet.player.techs[118],
                fleet.origin.galaxy,
                fleet.origin.system,
                fleet.origin.position,
                fleet.shipsSpeed,
                fleet.recyclers,
                fleet.recyclersOrigin.galaxy,
                fleet.recyclersOrigin.system,
                fleet.recyclersOrigin.position,
                fleet.recyclersSpeed,
                fleet.recyclersDistance,
                fleet.recyclersFuel,
                fleet.recyclersCargo,
                fleet.recyclersDuration]

            let ships = {}
            for (let shipid of atackids) {
                if (shipid in fleet.ships) {
                    ships[shipid] = fleet.ships[shipid]
                } else {
                    ships[shipid] = 0
                }
            }
            atkary.push({info: data, ships: ships})
        }
        saving["atk"] = atkary
    }
    if (document.getElementById("fleet_save_def").checked) {
        let defary = []
        for (let fleet of currentwave.fleets['def']) {
            let data = [fleet.id,
                fleet.player.techs[109],
                fleet.player.techs[110],
                fleet.player.techs[111],
                fleet.player.techs[115],
                fleet.player.techs[117],
                fleet.player.techs[118],
                fleet.origin.galaxy,
                fleet.origin.system,
                fleet.origin.position,
                fleet.shipsSpeed,
                fleet.recyclers,
                fleet.recyclersOrigin.galaxy,
                fleet.recyclersOrigin.system,
                fleet.recyclersOrigin.position,
                fleet.recyclersSpeed,
                fleet.recyclersDistance,
                fleet.recyclersFuel,
                fleet.recyclersCargo,
                fleet.recyclersDuration]
            if (fleet.spaceDock) {
                data.push(fleet.spaceDock)
            } else {
                data.push(0)
            }
            let ships = {}
            for (let shipid of defids) {
                if (shipid in fleet.ships) {
                    ships[shipid] = fleet.ships[shipid]
                } else {
                    ships[shipid] = 0
                }
            }
            defary.push({info: data, ships: ships})
        }
        saving["def"] = defary
    }

    create_dom_fleet(name, saved_fleets.length)
    saved_fleets.push(saving)
    store_database()
}


function deleteElem(elem) {
    const fleets = parseInt(elem.dataset.fname);


    for (let i = fleets + 1; i < saved_fleets.length; i++) {
        let todo = document.querySelector('div[class="data-box"][data-fname="' + i + '"]')
        console.log("to change", todo)
        todo.dataset.fname = parseInt(todo.dataset.fname) - 1

    }

    saved_fleets.splice(fleets, 1);
    console.log('Delete function called for:', elem, fleets);
    // Implement delete logic here
    elem.remove(); // Remove the parent element
    store_database()
}

function create_dom_fleet(name, idx) {
    // Create elements
    const dataBox = document.createElement('div');
    dataBox.className = 'data-box';
    dataBox.innerText = name
    dataBox.dataset.fname = idx;
    const deleteButton = document.createElement('button');
    deleteButton.style.background = '#a52834';
    deleteButton.style.float = 'right';
    deleteButton.style.marginTop = '-3px';
    deleteButton.className = 'btn btn-xs';
    deleteButton.innerText = 'X';

    const loadButton = document.createElement('button');
    loadButton.style.float = 'right';
    loadButton.style.marginTop = '-3px';
    loadButton.style.marginRight = '5px';
    loadButton.className = 'btn btn-xs btn-secondary';
    loadButton.innerText = 'Load';

    // Attach event listeners
    deleteButton.onclick = function () {
        deleteElem(dataBox);
    };

    loadButton.onclick = function () {
        load(dataBox);
    };

    // Append buttons to the data box
    dataBox.appendChild(deleteButton);
    dataBox.appendChild(loadButton);

    // Append the data box to the container
    document.getElementById('fleets_current').appendChild(dataBox);
}


function load_urls(){
    const urlParams = new URLSearchParams(window.location.search);
    const pattern = /^im\[(\d+)\]$/;
    const numberToStringMap = {
        901: "metal",
        902: "crystal",
        903: "deuterium",
    };
    urlParams.forEach((value, key) => {
        const match = key.match(pattern);
        if (match) {
            const number = match[1]; // Extracted number part from the key
            console.log(`Number: ${number}, Value: ${value}`);
            let obj = null
            if (numberToStringMap[number]) {
                console.log("inside", numberToStringMap[number], number)
                obj = document.querySelector('input[data-entity="' + numberToStringMap[number] + '"]')
            } else {
                obj = document.querySelector('input[data-entity="' + number + '"][data-side="def"]')
            }
            console.log(obj)
            if (obj) {
                console.log("load!")
                obj.value = value
                console.log(value, obj.value)
                obj.dispatchEvent(new Event('change'));
            }
        }
    });

}

