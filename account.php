<?php

/**
 * pr0game powered by steemnova
 * Accounts
 * (c) 2024 reflexrecon
 */

require_once __DIR__.'/vendor/autoload.php';

define('MODE', 'ACCOUNT');
define('ROOT_PATH', dirname(__FILE__) . '/');

require 'includes/pages/account/AbstractAccountPage.class.php';
require 'includes/pages/account/ShowErrorPage.class.php';
require 'includes/common.php';

$page = HTTP::_GP('page', 'account');
$mode = HTTP::_GP('mode', 'show');
$page = str_replace(['_', '\\', '/', '.', "\0"], '', $page);
$pageClass = 'Show' . ucwords($page) . 'Page';
$ACCOUNT =& Singleton()->ACCOUNT;
$path = 'includes/pages/account/' . $pageClass . '.class.php';

if (!file_exists($path)) {
    ShowErrorPage::printError($LNG['page_doesnt_exist']);
}

// Added Autoload in feature Versions
require $path;

$pageObj = new $pageClass();
// PHP 5.2 FIX
// can't use $pageObj::$requireModule
$pageProps = get_class_vars(get_class($pageObj));

if (
    isset($pageProps['requireModule'])
    && $pageProps['requireModule'] !== 0
    && !isModuleAvailable($pageProps['requireModule'])
) {
    ShowErrorPage::printError($LNG['sys_module_inactive']);
}

if (!is_callable([$pageObj, $mode])) {
    if (!isset($pageProps['defaultController']) || !is_callable([$pageObj, $pageProps['defaultController']])) {
        ShowErrorPage::printError($LNG['page_doesnt_exist']);
    }
    $mode = $pageProps['defaultController'];
}

$pageObj->{$mode}();
