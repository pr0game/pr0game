<?php

/**
 *  2Moons
 *   by Jan-Otto Kröpke 2009-2016
 *
 * For the full copyright and license information, please view the LICENSE
 *
 * @package 2Moons
 * @author Jan-Otto Kröpke <slaver7@gmail.com>
 * @copyright 2009 Lucky
 * @copyright 2016 Jan-Otto Kröpke <slaver7@gmail.com>
 * @licence MIT
 * @version 1.8.0
 * @link https://github.com/jkroepke/2Moons
 */

if (!allowedTo(str_replace([dirname(__FILE__), '\\', '/', '.php'], '', __FILE__))) {
    throw new Exception("Permission error!");
}

function ShowBanPage()
{
    $db = Database::get();
    $LNG =& Singleton()->LNG;
    $USER =& Singleton()->USER;
    
    if (empty($_GET['mode'])) {
        $_GET['mode'] = $_GET['page'];
    }
    if (!isset($_GET['order'])) {
        $_GET['order'] = '';
    }
    
    $WHEREBANA = "";
    if (!isset($_GET['view'])) {
        $_GET['view'] = '';
    }
    if ($_GET['view'] == 'bana') {
        $WHEREBANA	= "AND `bana` = '1'";
    }
    $template	= new template();
    $template->assign_vars([
        'signalColors'      => $USER['signalColors'],
    ]);
    $template->loadscript('filterlist.js');
    switch ($_GET['mode']) {
        case 'user':
            $ORDER = $_GET['order'] == 'id' ? "id" : "username";
            $sql = "SELECT `username`, `id`, `bana` FROM %%USERS%% WHERE `id` != 1 AND `authlevel` <= :authlevel AND `universe` = :universe ".$WHEREBANA." ORDER BY ".$ORDER." ASC;";
            $userList   = $db->select($sql, [
                ':authlevel'    => $USER['authlevel'],
                ':universe'     => Universe::getEmulated(),
            ]);
            $userSelect	= ['List' => '', 'ListBan' => ''];
    
            $users	=	0;
            foreach ($userList as $user) {
                $userSelect['List']	.=	'<option value="'.$user['id'].'">'.$user['username'].'&nbsp;&nbsp;(ID:&nbsp;'.$user['id'].')'.(($user['bana']	==	'1') ? $LNG['bo_characters_suus'] : '').'</option>';
                $users++;
            }
    
            if (!isset($_GET['order2'])) {
                $_GET['order2'] = '';
            }
            $ORDER2 = $_GET['order2'] == 'id' ? "id" : "username";
    
            $banned = 0;
            $sql = "SELECT `username`, `id`
                FROM %%USERS%%
                WHERE `bana` = '1' AND `universe` = :universe
                ORDER BY ".$ORDER2." ASC;";
            $userListBan    = $db->select($sql, [
                ':universe'     => Universe::getEmulated(),
            ]);
            foreach ($userListBan as $user) {
                $userSelect['ListBan']	.=	'<option value="'.$user['id'].'">'.$user['username'].'&nbsp;&nbsp;(ID:&nbsp;'.$user['id'].')</option>';
                $banned++;
            }

            $id = HTTP::_GP('ban_id', 0);
            if (isset($_POST['panel_change']) || isset($_POST['unban_id'])) {
                $id = HTTP::_GP('unban_id', 0);
            }
            $sql = "SELECT b.`theme`, b.`longer`, u.`id`, u.`urlaubs_modus`, u.`banaday`, u.`username`
                FROM %%USERS%% as u 
                LEFT JOIN %%USERS_BANNED%% as b ON b.`user_id` = u.`id`
                WHERE u.`id` = :user_id AND u.`universe` = :universe;";
            $bannedUser = $db->SelectSingle($sql, [
                ':user_id'     => $id,
                ':universe' => Universe::getEmulated(),
            ]);

            if (isset($_POST['panel']) || isset($_POST['panel_change'])) {
                if ($bannedUser['banaday'] <= TIMESTAMP) {
                    $title              = $LNG['bo_bbb_title_1'];
                    $changedate         = $LNG['bo_bbb_title_2'];
                    $changedate_advert  = '';
                    $reas               = '';
                    $timesus            = '';
                } else {
                    $title              = $LNG['bo_bbb_title_3'];
                    $changedate         = $LNG['bo_bbb_title_6'];
                    $changedate_advert  = '<td class="c" width="18px"><img src="./styles/resource/images/admin/i.gif" class="tooltip" data-tooltip-content="'.$LNG['bo_bbb_title_4'].'"></td>';
                    $reas               = $bannedUser['theme'];
                    $timesus            =
                        "<tr>
                            <th>".$LNG['bo_bbb_title_5']."</th>
                            <th height=25 colspan=2>".date($LNG['php_tdformat'], $bannedUser['longer'])."</th>
                        </tr>";
                }
        
                if (isset($_POST['panel_change'])) {
                    $vacation = ($bannedUser['urlaubs_modus'] == 1) ? true : false;
                    $perma = ($bannedUser['banaday'] == 2147483647) ? true : false;
                } else {
                    $vacation = true;
                    $perma = false;
                }
        
                $template->assign_vars([
                    'user_id'           => $id,
                    'name'              => $bannedUser['username'],
                    'bantitle'          => $title,
                    'changedate'        => $changedate,
                    'reas'              => $reas,
                    'changedate_advert' => $changedate_advert,
                    'timesus'           => $timesus,
                    'perma'             => $perma,
                    'vacation'          => $vacation,
                ]);
            } elseif (isset($_POST['bannow']) && $bannedUser['id'] != 1) {
                $id                = HTTP::_GP('ban_id', 0);
                $Name              = HTTP::_GP('ban_name', '', true);
                $reas              = HTTP::_GP('why', '', true);
                $days              = HTTP::_GP('days', 0);
                $hour              = HTTP::_GP('hour', 0);
                $mins              = HTTP::_GP('mins', 0);
                $secs              = HTTP::_GP('secs', 0);
                $admin             = $USER['username'];
                $mail              = ""; //$ACCOUNT['email'];
                $BanTime           = $days * 86400 + $hour * 3600 + $mins * 60 + $secs;
        
                if ($bannedUser['longer'] > TIMESTAMP && $bannedUser['banaday'] != 2147483647) {
                    $BanTime          += ($bannedUser['longer'] - TIMESTAMP);
                }
        
                if (isset($_POST['permanent'])) {
                    $BannedUntil = 2147483647;
                } else {
                    $BannedUntil = ($BanTime + TIMESTAMP) < TIMESTAMP ? TIMESTAMP : TIMESTAMP + $BanTime;
                }

                if ($bannedUser['banaday'] > TIMESTAMP) {
                    $sql = "UPDATE %%USERS_BANNED%% SET `theme` = :reas, `time` = :time, 
                        `longer` = :longer, `author` = :author, `email` = :mail
                        WHERE `user_id` = :user_id AND `universe` = :universe;";
                    $db->update($sql, [
                        ':reas'     => $reas,
                        ':time'     => TIMESTAMP,
                        ':longer'   => $BannedUntil,
                        ':author'   => $admin,
                        ':mail'     => $mail,
                        ':user_id'  => $id,
                        ':universe' => Universe::getEmulated(),
                    ]);
                } else {
                    $sql = "INSERT INTO %%USERS_BANNED%% SET `user_id` = :user_id, `who` = :name, `theme` = :reas, `time` = :time, 
                        `longer` = :longer, `author` = :author, `universe` = :universe, `email` = :mail;";
                    $db->insert($sql, [
                        ':user_id'  => $id,
                        ':name'     => $Name,
                        ':reas'     => $reas,
                        ':time'     => TIMESTAMP,
                        ':longer'   => $BannedUntil,
                        ':author'   => $admin,
                        ':universe' => Universe::getEmulated(),
                        ':mail'     => $mail,
                    ]);
                }

                $sql = "UPDATE %%USERS%% SET `bana` = 1, `banaday` = :bannedUntil 
                    WHERE `id` = :user_id AND `universe` = :universe;";
                $db->update($sql, [
                    ':bannedUntil'  => $BannedUntil,
                    ':user_id'      => $id,
                    ':universe'    	=> Universe::getEmulated(),
                ]);
                if (isset($_POST['vacat'])) {
                    PlayerUtil::enable_vmode($bannedUser);
                }
                
                $template->message($LNG['bo_the_player'] . $Name . $LNG['bo_banned'], '?page=bans');
                exit;
            } elseif (isset($_POST['unban_id'])) {
                $id	= HTTP::_GP('unban_id', 0);

                $sql = "UPDATE %%USERS%% SET `bana` = 0, `banaday` = 0 
                    WHERE `id` = :user_id AND `universe` = :universe;";
                $db->update($sql, [
                    ':user_id'  => $id,
                    ':universe' => Universe::getEmulated(),
                ]);
                $template->message($LNG['bo_the_player2'] . $bannedUser['username'] . $LNG['bo_unbanned'], '?page=bans');
                exit;
            }
            $template->assign_vars([
                'UserSelect'        => $userSelect,
                'usercount'         => $users,
                'bancount'          => $banned,
                'bo_select_title'   => $LNG['bo_select_title'],
            ]);
            $template->show('BanPageUser.tpl');
            break;
        case 'account':
            $ORDER = $_GET['order'] == 'id' ? "id" : "accountname";
            $sql = "SELECT `accountname`, `id`, `bana` 
                FROM %%ACCOUNTS%% 
                WHERE `id` != 1 AND `authlevel` <= :authlevel ".$WHEREBANA." 
                ORDER BY ".$ORDER." ASC;";
            $accountList   = $db->select($sql, [
                ':authlevel'    => $USER['authlevel'],
            ]);
            $accountSelect	= ['List' => '', 'ListBan' => ''];

            $accounts	=	0;
            foreach ($accountList as $account) {
                $accountSelect['List']	.=	'<option value="'.$account['id'].'">'.$account['accountname'].'&nbsp;&nbsp;(ID:&nbsp;'.$account['id'].')'.(($account['bana']	==	'1') ? $LNG['bo_characters_suus'] : '').'</option>';
                $accounts++;
            }

            if (!isset($_GET['order2'])) {
                $_GET['order2'] = '';
            }
            $ORDER2 = $_GET['order2'] == 'id' ? "id" : "accountname";

            $banned = 0;
            $sql = "SELECT `accountname`, `id` 
                FROM %%ACCOUNTS%% 
                WHERE `bana` = '1' 
                ORDER BY ".$ORDER2." ASC;";
            $accountListBan    = $db->select($sql, []);
            foreach ($accountListBan as $account) {
                $accountSelect['ListBan']	.=	'<option value="'.$account['id'].'">'.$account['accountname'].'&nbsp;&nbsp;(ID:&nbsp;'.$account['id'].')</option>';
                $banned++;
            }

            $id = HTTP::_GP('ban_id', 0);
            if (isset($_POST['panel_change']) || isset($_POST['unban_id'])) {
                $id = HTTP::_GP('unban_id', 0);
            }
            $sql = "SELECT b.`theme`, b.`longer`, a.`id`, a.`banaday`, a.`accountname` 
                FROM %%ACCOUNTS%% as a 
                LEFT JOIN %%ACCOUNTS_BANNED%% as b ON b.`account_id` = a.`id`
                WHERE a.`id` = :account_id;";
            $bannedAccount = $db->SelectSingle($sql, [
                ':account_id'   => $id,
            ]);
            
            if (isset($_POST['panel']) || isset($_POST['panel_change'])) {
                if ($bannedAccount['banaday'] <= TIMESTAMP) {
                    $title              = $LNG['bo_bbb_title_1'];
                    $changedate         = $LNG['bo_bbb_title_2'];
                    $changedate_advert  = '';
                    $reas               = '';
                    $timesus            = '';
                } else {
                    $title              = $LNG['bo_bbb_title_3'];
                    $changedate         = $LNG['bo_bbb_title_6'];
                    $changedate_advert  = '<td class="c" width="18px"><img src="./styles/resource/images/admin/i.gif" class="tooltip" data-tooltip-content="'.$LNG['bo_bbb_title_4'].'"></td>';
                    $reas               = $bannedAccount['theme'];
                    $timesus            =
                        "<tr>
                            <th>".$LNG['bo_bbb_title_5']."</th>
                            <th height=25 colspan=2>".date($LNG['php_tdformat'], $bannedAccount['longer'])."</th>
                        </tr>";
                }
        
                if (isset($_POST['panel_change'])) {
                    $vacation = true;
                    $perma = ($bannedAccount['banaday'] == 2147483647) ? true : false;
                } else {
                    $vacation = true;
                    $perma = false;
                }

                $template->assign_vars([
                    'account_id'        => $id,
                    'name'              => $bannedAccount['accountname'],
                    'bantitle'          => $title,
                    'changedate'        => $changedate,
                    'reas'              => $reas,
                    'changedate_advert' => $changedate_advert,
                    'timesus'           => $timesus,
                    'perma'             => $perma,
                    'vacation'          => $vacation,
                ]);
            } elseif (isset($_POST['bannow']) && $bannedAccount['id'] != 1) {
                $id                = HTTP::_GP('ban_id', 0);
                $Name              = HTTP::_GP('ban_name', '', true);
                $reas              = HTTP::_GP('why', '', true);
                $days              = HTTP::_GP('days', 0);
                $hour              = HTTP::_GP('hour', 0);
                $mins              = HTTP::_GP('mins', 0);
                $secs              = HTTP::_GP('secs', 0);
                $admin             = $USER['username'];
                $mail              = "";//$ACCOUNT['email'];
                $BanTime           = $days * 86400 + $hour * 3600 + $mins * 60 + $secs;
        
                if ($bannedAccount['longer'] > TIMESTAMP && $bannedAccount['banaday'] != 2147483647) {
                    $BanTime += ($bannedAccount['longer'] - TIMESTAMP);
                }
        
                if (isset($_POST['permanent'])) {
                    $BannedUntil = 2147483647;
                } else {
                    $BannedUntil = ($BanTime + TIMESTAMP) < TIMESTAMP ? TIMESTAMP : TIMESTAMP + $BanTime;
                }

                $sql = "SELECT au.user_id, u.username FROM %%ACCOUNTS_TO_USERS%% au JOIN %%USERS%% u ON u.`id` = au.`user_id` WHERE au.`account_id` = :account_id;";
                $users = $db->select($sql, [':account_id' => $id]);

                if ($bannedAccount['banaday'] > TIMESTAMP) {
                    $sql = "UPDATE %%ACCOUNTS_BANNED%% SET `theme` = :reas, `time` = :time, 
                        `longer` = :longer, `author` = :author, `email` = :mail
                        WHERE account_id = :account_id;";
                    $db->update($sql, [
                        ':reas'         => $reas,
                        ':time'         => TIMESTAMP,
                        ':longer'       => $BannedUntil,
                        ':author'       => $admin,
                        ':mail'         => $mail,
                        ':account_id'   => $id,
                    ]);

                    foreach ($users as $user) {
                        $sql = "UPDATE %%USERS_BANNED%% SET `theme` = :reas, `time` = :time, 
                            `longer` = :longer, `author` = :author, `email` = :mail
                            WHERE `user_id` = :user_id AND `universe` = :universe;";
                        $db->update($sql, [
                            ':reas'     => $reas,
                            ':time'     => TIMESTAMP,
                            ':longer'   => $BannedUntil,
                            ':author'   => $admin,
                            ':mail'     => $mail,
                            ':user_id'  => $user['user_id'],
                            ':universe' => Universe::getEmulated(),
                        ]);
                    }
                } else {
                    $sql = "INSERT INTO %%ACCOUNTS_BANNED%% SET `account_id` = :account_id, `who` = :name, `theme` = :reas, `time` = :time, 
                        `longer` = :longer, `author` = :author, `email` = :mail;";
                    $db->insert($sql, [
                        ':account_id'   => $id,
                        ':name'  	    => $Name,
                        ':reas'         => $reas,
                        ':time'         => TIMESTAMP,
                        ':longer'       => $BannedUntil,
                        ':author'       => $admin,
                        ':mail'         => $mail,
                    ]);

                    foreach ($users as $user) {
                        $sql = "INSERT INTO %%USERS_BANNED%% SET `user_id` = :user_id, `who` = :name, `theme` = :reas, `time` = :time, 
                            `longer` = :longer, `author` = :author, `universe` = :universe, `email` = :mail;";
                        $db->insert($sql, [
                            ':user_id'  => $user['user_id'],
                            ':name'  	=> $user['username'],
                            ':reas'     => $reas,
                            ':time'     => TIMESTAMP,
                            ':longer'   => $BannedUntil,
                            ':author'   => $admin,
                            ':universe' => Universe::getEmulated(),
                            ':mail'     => $mail,
                        ]);
                    }
                }

                $sql = "UPDATE %%ACCOUNTS%% SET `bana` = 1, `banaday` = :bannedUntil 
                    WHERE `id` = :account_id;";
                $db->update($sql, [
                    ':bannedUntil'  => $BannedUntil,
                    ':account_id'   => $id,
                ]);

                foreach ($users as $user) {
                    $sql = "UPDATE %%USERS%% SET `bana` = 1, `banaday` = :bannedUntil 
                        WHERE `id` = :user_id AND `universe` = :universe;";
                    $db->update($sql, [
                        ':bannedUntil'  => $BannedUntil,
                        ':user_id'      => $user['user_id'],
                        ':universe'    	=> Universe::getEmulated(),
                    ]);
                    if (isset($_POST['vacat'])) {
                        PlayerUtil::enable_vmode($user);
                    }
                }
                
                $template->message($LNG['bo_the_player'] . $Name . $LNG['bo_banned'], '?page=bans');
                exit;
            } elseif (isset($_POST['unban_id'])) {
                $id	= HTTP::_GP('unban_id', 0);

                $sql = "UPDATE %%ACCOUNTS%% SET `bana` = 0, `banaday` = 0 
                    WHERE `id` = :account_id;";
                $db->update($sql, [
                    ':account_id'   => $id,
                ]);
                $template->message($LNG['bo_the_player2'] . $bannedAccount['accountname'] . $LNG['bo_unbanned'], '?page=bans');
                exit;
            }
            $template->assign_vars([
                'accountSelect'     => $accountSelect,
                'accountCount'      => $accounts,
                'bancount'          => $banned,
                'bo_select_title'   => $LNG['bo_select_title'],
            ]);
            $template->show('BanPageAccount.tpl');
            break;
        default:
            $template->assign_vars([
                'ban_title'		=> "Bannpanel",
                'ban_title_a'	=> "Account Bannpanel",
                'ban_title_u'	=> "User Bannpanel",
            ]);
            $template->show('BanPage.tpl');
            break;
    }
}
