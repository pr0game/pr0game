<?php

/**
 *  2Moons
 *   by Jan-Otto Kröpke 2009-2016
 *
 * For the full copyright and license information, please view the LICENSE
 *
 * @package 2Moons
 * @author Jan-Otto Kröpke <slaver7@gmail.com>
 * @copyright 2009 Lucky
 * @copyright 2016 Jan-Otto Kröpke <slaver7@gmail.com>
 * @licence MIT
 * @version 1.8.0
 * @link https://github.com/jkroepke/2Moons
 */

if (!allowedTo(str_replace([dirname(__FILE__), '\\', '/', '.php'], '', __FILE__))) {
    throw new Exception("Permission error!");
}

function ShowCreatorPage()
{
    $LNG =& Singleton()->LNG;
    $USER =& Singleton()->USER;
    $template   = new template();

    if (empty($_GET['mode'])) {
        $_GET['mode'] = $_GET['page'];
    }
    switch ($_GET['mode']) {
        case 'account':
            $LNG->includeData(['PUBLIC']);
            if ($_POST) {
                $accountName   = HTTP::_GP('name', '', UTF8_SUPPORT);
                $accountPass   = HTTP::_GP('password', '');
                $accountPass2  = HTTP::_GP('password2', '');
                $accountMail   = HTTP::_GP('email', '');
                $accountMail2  = HTTP::_GP('email2', '');
                $authlevel   = HTTP::_GP('authlevel', 0);
                $Language   = HTTP::_GP('lang', '');
                $accountID  = HTTP::_GP('accountID', 0);

                $db = Database::get();
                $sql = "SELECT COUNT(*) as count FROM %%ACCOUNTS%% WHERE accountname = :accountname;";
                $ExistsUser = $db->selectSingle($sql, [':accountname' => $accountName], 'count');

                $sql = "SELECT COUNT(*) as count FROM %%ACCOUNTS%% WHERE email = :email OR email_2 = :email;";
                $ExistsMails = $db->selectSingle($sql, [':email' => $accountMail], 'count');

                $errors = "";

                $config = Config::get(Universe::getEmulated());
                
                if ($ExistsMails != 0) {
                    $errors .= $LNG['mail_already_exists'];
                }
                if (empty($accountName)) {
                    $errors .= $LNG['registerErrorUsernameEmpty'];
                    $errors .= "<br>";
                }

                if (strlen($accountPass) < 6) {
                    $errors .= sprintf($LNG['registerErrorPasswordLength'], 6);
                    $errors .= "<br>";
                }

                if ($accountPass != $accountPass2) {
                    $errors .= $LNG['registerErrorPasswordSame'];
                    $errors .= "<br>";
                }

                if ($accountMail != $accountMail2) {
                    $errors .= $LNG['registerErrorMailSame'];
                    $errors .= "<br>";
                }

                if (!AccountUtil::isNameValid($accountName)) {
                    $errors .= $LNG['registerErrorUsernameChar'];
                    $errors .= "<br>";
                }

                if ($ExistsUser != 0) {
                    $errors .= $LNG['registerErrorUsernameExist'];
                    $errors .= "<br>";
                }
            
                if (!empty($errors)) {
                    $template->message($errors, '?page=create&mode=account', 10, true);
                    exit;
                }

                $Language   = array_key_exists($Language, $LNG->getAllowedLangs(false)) ? $Language : $config->lang;
                AccountUtil::createAccount(
                    $accountName, 
                    AccountUtil::cryptPassword($accountPass), 
                    $accountMail, 
                    $Language,
                    $authlevel
                );

                $template->message($LNG['new_user_success'], '?page=create&mode=account', 5, true);
                exit;
            }

            $AUTH           = [];
            $AUTH[AUTH_USR] = $LNG['user_level_' . AUTH_USR];

            if ($USER['authlevel'] >= AUTH_OPS) {
                $AUTH[AUTH_OPS] = $LNG['user_level_' . AUTH_OPS];
            }

            if ($USER['authlevel'] >= AUTH_MOD) {
                $AUTH[AUTH_MOD] = $LNG['user_level_' . AUTH_MOD];
            }

            if ($USER['authlevel'] >= AUTH_ADM) {
                $AUTH[AUTH_ADM] = $LNG['user_level_' . AUTH_ADM];
            }

            $template->assign_vars([
                'admin_auth'            => $USER['authlevel'],
                'accountID'             => $LNG['accountID'],
                'new_add_account'       => $LNG['new_add_account'],
                'new_creator_refresh'   => $LNG['new_creator_refresh'],
                'new_creator_go_back'   => $LNG['new_creator_go_back'],
                'universe'              => $LNG['mu_universe'],
                'user_reg'              => $LNG['user_reg'],
                'pass_reg'              => $LNG['pass_reg'],
                'pass2_reg'             => $LNG['pass2_reg'],
                'email_reg'             => $LNG['email_reg'],
                'email2_reg'            => $LNG['email2_reg'],
                'new_coord'             => $LNG['new_coord'],
                'new_range'             => $LNG['new_range'],
                'lang_reg'              => $LNG['lang_reg'],
                'new_title_acc'         => $LNG['new_title_acc'],
                'Selector'              => ['auth' => $AUTH, 'lang' => $LNG->getAllowedLangs(false)],
                'signalColors'          => $USER['signalColors']
            ]);
            $template->show('CreatePageAccount.tpl');
            break;
        case 'user':
            $LNG->includeData(['PUBLIC']);
            if ($_POST) {
                $UserName   = HTTP::_GP('name', '', UTF8_SUPPORT);
                $UserAuth   = HTTP::_GP('authlevel', 0);
                $Galaxy     = HTTP::_GP('galaxy', 0);
                $System     = HTTP::_GP('system', 0);
                $Planet     = HTTP::_GP('planet', 0);
                $Language   = HTTP::_GP('lang', '');
                $accountID  = HTTP::_GP('accountID', 0);

                $db = Database::get();
                $sql = "SELECT COUNT(*) as count FROM %%USERS%% WHERE universe = :universe AND username = :username;";
                $ExistsUser = $db->selectSingle($sql, [':universe' => Universe::getEmulated(), ':username' => $UserName], 'count');

                $sql = "SELECT COUNT(*) as count FROM %%USERS%% u INNER JOIN %%ACCOUNTS_TO_USERS%% atu on atu.user_id = u.id WHERE u.universe = :universe AND atu.account_id = :accountId;";
                $AccountHasUser = $db->selectSingle($sql, [':universe' => Universe::getEmulated(), ':accountId' => $accountID], 'count');
                
                $errors = "";

                $config = Config::get(Universe::getEmulated());
                
                if (empty($accountID)) {
                    $errors .= $LNG['empty_account_field'];
                    $errors .= "<br>";
                }

                if (empty($UserName)) {
                    $errors .= $LNG['registerErrorUsernameEmpty'];
                    $errors .= "<br>";
                }

                if (!PlayerUtil::isNameValid($UserName)) {
                    $errors .= $LNG['registerErrorUsernameChar'];
                    $errors .= "<br>";
                }

                if ($ExistsUser != 0) {
                    $errors .= $LNG['registerErrorUsernameExist'];
                    $errors .= "<br>";
                }

                if ($AccountHasUser != 0) {
                    $errors .= $LNG['account_already_has_user'];
                    $errors .= "<br>";
                }
                

                if (!PlayerUtil::isPositionFree(Universe::getEmulated(), $Galaxy, $System, $Planet)) {
                    $errors .= $LNG['po_complete_all'];
                    $errors .= "<br>";
                }

                if ($Galaxy > $config->max_galaxy || $System > $config->max_system || $Planet > $config->max_planets) {
                    $errors .= $LNG['po_complete_all2'];
                    $errors .= "<br>";
                }

                if (!empty($errors)) {
                    $template->message($errors, '?page=create&mode=user', 10, true);
                    exit;
                }

                $Language   = array_key_exists($Language, $LNG->getAllowedLangs(false)) ? $Language : $config->lang;

                PlayerUtil::createPlayer(
                    $accountID,
                    Universe::getEmulated(),
                    $UserName,
                    $Language,
                    $Galaxy,
                    $System,
                    $Planet,
                    $LNG['fcm_planet'],
                    $UserAuth
                );

                $template->message($LNG['new_user_success'], '?page=create&mode=user', 5, true);
                exit;
            }

            $AUTH           = [];
            $AUTH[AUTH_USR] = $LNG['user_level_' . AUTH_USR];

            if ($USER['authlevel'] >= AUTH_OPS) {
                $AUTH[AUTH_OPS] = $LNG['user_level_' . AUTH_OPS];
            }

            if ($USER['authlevel'] >= AUTH_MOD) {
                $AUTH[AUTH_MOD] = $LNG['user_level_' . AUTH_MOD];
            }

            if ($USER['authlevel'] >= AUTH_ADM) {
                $AUTH[AUTH_ADM] = $LNG['user_level_' . AUTH_ADM];
            }

            $template->assign_vars([
                'admin_auth'            => $USER['authlevel'],
                'accountID'             => $LNG['accountID'],
                'new_add_user'          => $LNG['new_add_user'],
                'new_creator_refresh'   => $LNG['new_creator_refresh'],
                'new_creator_go_back'   => $LNG['new_creator_go_back'],
                'universe'              => $LNG['mu_universe'],
                'user_reg'              => $LNG['user_reg'],
                'new_coord'             => $LNG['new_coord'],
                'new_range'             => $LNG['new_range'],
                'lang_reg'              => $LNG['lang_reg'],
                'new_title'             => $LNG['new_title'],
                'Selector'              => ['auth' => $AUTH, 'lang' => $LNG->getAllowedLangs(false)],
                'signalColors'          => $USER['signalColors']
            ]);
            $template->show('CreatePageUser.tpl');
            break;
        case 'moon':
            if ($_POST) {
                $PlanetID   = HTTP::_GP('add_moon', 0);
                $MoonName   = HTTP::_GP('name', '', UTF8_SUPPORT);
                $Diameter   = HTTP::_GP('diameter', 0);

                $MoonPlanet = $GLOBALS['DATABASE']->getFirstRow("SELECT temp_max, temp_min, id_luna, galaxy, `system`,"
                    . " planet, planet_type, destruyed, id_owner FROM " . PLANETS . " WHERE id = '" . $PlanetID
                    . "' AND universe = '" . Universe::getEmulated() . "' AND planet_type = '1' AND destruyed = '0';");

                if (!isset($MoonPlanet)) {
                    $template->message($LNG['mo_planet_doesnt_exist'], '?page=create&mode=moon', 3, true);
                    exit;
                }

                $moonId = PlayerUtil::createMoon(
                    Universe::getEmulated(),
                    $MoonPlanet['galaxy'],
                    $MoonPlanet['system'],
                    $MoonPlanet['planet'],
                    $MoonPlanet['id_owner'],
                    20,
                    (($_POST['diameter_check'] == 'on') ? null : $Diameter),
                    $MoonName
                );



                if ($moonId !== false) {
                    $template->message($LNG['mo_moon_added'], '?page=create&mode=moon', 3, true);
                } else {
                    $template->message($LNG['mo_moon_unavaible'], '?page=create&mode=moon', 3, true);
                }
                exit;
            }

            $template->assign_vars([
                'admin_auth'            => $USER['authlevel'],
                'universum'             => $LNG['mu_universe'],
                'po_add_moon'           => $LNG['po_add_moon'],
                'input_id_planet'       => $LNG['input_id_planet'],
                'mo_moon_name'          => $LNG['mo_moon_name'],
                'mo_diameter'           => $LNG['mo_diameter'],
                'mo_temperature'        => $LNG['mo_temperature'],
                'mo_fields_avaibles'    => $LNG['mo_fields_avaibles'],
                'button_add'            => $LNG['button_add'],
                'new_creator_refresh'   => $LNG['new_creator_refresh'],
                'mo_moon'               => $LNG['fcm_moon'],
                'new_creator_go_back'   => $LNG['new_creator_go_back'],
                'signalColors'          => $USER['signalColors']
            ]);

            $template->show('CreatePageMoon.tpl');
            break;
        case 'planet':
            if ($_POST) {
                $id          = HTTP::_GP('id', 0);
                $Galaxy      = HTTP::_GP('galaxy', 0);
                $System      = HTTP::_GP('system', 0);
                $Planet      = HTTP::_GP('planet', 0);
                $name        = HTTP::_GP('name', '', UTF8_SUPPORT);
                $field_max   = HTTP::_GP('field_max', 0);

                $config         = Config::get(Universe::getEmulated());

                if ($Galaxy > $config->max_galaxy || $System > $config->max_system || $Planet > $config->max_planets) {
                    $template->message($LNG['po_complete_all2'], '?page=create&mode=planet', 3, true);
                    exit;
                }

                $ISUser     = $GLOBALS['DATABASE']->getFirstRow("SELECT id, authlevel FROM " . USERS . " WHERE id = '"
                    . $id . "' AND universe = '" . Universe::getEmulated() . "';");
                if (!PlayerUtil::checkPosition(Universe::getEmulated(), $Galaxy, $System, $Planet) || !isset($ISUser)) {
                    $template->message($LNG['po_complete_all'], '?page=create&mode=planet', 3, true);
                    exit;
                }

                $planetId = PlayerUtil::createPlanet(
                    $Galaxy,
                    $System,
                    $Planet,
                    Universe::getEmulated(),
                    $id,
                    null,
                    false,
                    $ISUser['authlevel']
                );

                $SQL  = "UPDATE " . PLANETS . " SET ";

                if ($field_max > 0) {
                    $SQL .= "field_max = '" . $field_max . "' ";
                }

                if (!empty($name)) {
                    $SQL .= ", name = '" . $GLOBALS['DATABASE']->sql_escape($name) . "' ";
                }

                $SQL .= "WHERE ";
                $SQL .= "id = '" . $planetId . "'";
                $GLOBALS['DATABASE']->query($SQL);

                $template->message($LNG['po_complete_succes'], '?page=create&mode=planet', 3, true);
                exit;
            }

            $template->assign_vars([
                'admin_auth'            => $USER['authlevel'],
                'po_add_planet'         => $LNG['po_add_planet'],
                'po_galaxy'             => $LNG['po_galaxy'],
                'po_system'             => $LNG['po_system'],
                'po_planet'             => $LNG['po_planet'],
                'input_id_user'         => $LNG['input_id_user'],
                'new_creator_coor'      => $LNG['new_creator_coor'],
                'po_name_planet'        => $LNG['po_name_planet'],
                'po_fields_max'         => $LNG['po_fields_max'],
                'button_add'            => $LNG['button_add'],
                'po_colony'             => $LNG['fcp_colony'],
                'new_creator_refresh'   => $LNG['new_creator_refresh'],
                'new_creator_go_back'   => $LNG['new_creator_go_back'],
                'signalColors'          => $USER['signalColors'],
            ]);

            $template->show('CreatePagePlanet.tpl');
            break;
        default:
            $template->assign_vars([
                'new_creator_title_a'   => $LNG['new_creator_title_a'],
                'new_creator_title_u'   => $LNG['new_creator_title_u'],
                'new_creator_title_p'   => $LNG['new_creator_title_p'],
                'new_creator_title_l'   => $LNG['new_creator_title_l'],
                'new_creator_title'     => $LNG['new_creator_title'],
                'signalColors'          => $USER['signalColors'],
            ]);

            $template->show('CreatePage.tpl');
            break;
    }
}
