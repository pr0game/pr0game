<?php

if ($USER['authlevel'] != AUTH_ADM) {
    throw new Exception("Permission error!");
}

function ShowUniverseMailReminderPage()
{
    $LNG =& Singleton()->LNG;
    $USER =& Singleton()->USER;

    $template   = new template();
    $universe = UNIVERSE::current();
    $config = Config::get($universe);

    if (!$config->mail_active) {
        $template->assign_vars([
            'error'         => $LNG['ow_smtp_errors'],
        ]);
    }
    else {
        $ACTION	= HTTP::_GP('action', '');
        if ($ACTION == 'send') {
            $open_time	= HTTP::_GP('opentime', TIMESTAMP);

            require_once('includes/classes/Database.class.php');
            require_once('includes/classes/Mail.class.php');
            $db = Database::get();

            $sql = "SELECT a.`email`, u.`username`, us.`lang`, a.`timezone` FROM %%ACCOUNTS%% a
                JOIN %%ACCOUNTS_TO_USERS%% au ON au.`account_id` = a.`id`
                JOIN %%USERS%% u ON u.`id` = au.`user_id`
                JOIN %%USERS_SETTINGS%% us ON us.`user_id` = u.`id`
                WHERE u.`universe` = :universe AND onlinetime < :openTime;";
            $receivers = $db->select($sql, [':universe' => $universe, ':openTime' => $open_time]);

            $sql = "SELECT `email`, `accountname` AS username, `language` AS lang, `timezone`
                FROM %%ACCOUNTS_VALID%%;";
            $receivers += $db->select($sql);

            $langObjects = [];
            
            foreach ($receivers as $user) {
                if (!isset($langObjects[$user['lang']])) {
                    $langObjects[$user['lang']]	= new Language($user['lang']);
                    $langObjects[$user['lang']]->includeData(['L18N', 'ADMIN', 'CUSTOM']);
                }

                $ULNG = $langObjects[$user['lang']];
    
                $MailSubject = sprintf($ULNG['mr_mail_title'], $config->uni_name);
                $MailRAW = $ULNG->getTemplate('email_universe_open');
                
                $MailContent	= str_replace([
                    '{USERNAME}',
                    '{GAMENAME}',
                    '{UNINAME}',
                    '{HTTPPATH}',
                    '{DISCORD}',
                    '{OPENTIME}',
                ], [
                    $user['username'],
                    $config->game_name,
                    $config->uni_name,
                    HTTP_PATH,
                    DISCORD_URL,
                    _date($LNG['php_tdformat'], $open_time, $user['timezone']),
                ], $MailRAW);

                Mail::send($user['email'], $user['username'], $MailSubject, $MailContent);
            }

            exit($LNG['ga_success']);
        }

        // if ($config->uni_status != STATUS_CLOSED && $config->uni_status != STATUS_REG_ONLY) {
        //     $template->assign_vars([
        //         'error'         => $LNG['mr_not_needed'],
        //     ]);
        // }
        // else {
            $time = TIMESTAMP + TIME_24_HOURS;
            $template->assign_vars([
                'error'         => false,
                'opentime'      => $time,
                'readableTime'  => _date($LNG['php_tdformat'], $time, $USER['timezone']),
            ]);
        // }
    }

    $template->assign_vars([
        'signalColors' 	=> $USER['signalColors'],
    ]);
    
    $template->show('UniverseMailReminderPage.tpl');
}