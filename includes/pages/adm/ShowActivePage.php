<?php

/**
 *  2Moons
 *   by Jan-Otto Kröpke 2009-2016
 *
 * For the full copyright and license information, please view the LICENSE
 *
 * @package 2Moons
 * @author Jan-Otto Kröpke <slaver7@gmail.com>
 * @copyright 2009 Lucky
 * @copyright 2016 Jan-Otto Kröpke <slaver7@gmail.com>
 * @licence MIT
 * @version 1.8.0
 * @link https://github.com/jkroepke/2Moons
 */

if (!allowedTo(str_replace([dirname(__FILE__), '\\', '/', '.php'], '', __FILE__))) {
    throw new Exception("Permission error!");
}

function ShowActivePage()
{
    $LNG =& Singleton()->LNG;
    $USER =& Singleton()->USER;
    $db = Database::get();

    $id = HTTP::_GP('id', 0);
    $action = HTTP::_GP('action', '');
    if ($action == 'delete' && !empty($id)) {
        $sql = "DELETE FROM %%ACCOUNTS_VALID%% WHERE `validationID` = :validationID;";
        $db->delete($sql, [':validationID' => $id]);
    }

    $sql = "SELECT * FROM %%ACCOUNTS_VALID%% ORDER BY validationID ASC;";
    $accounts = $db->select($sql);
    foreach ($accounts as $account) {
        $account['date'] = _date($LNG['php_tdformat'], $account['date'], $USER['timezone']);
    }

    $template	= new template();
    $template->assign_vars([
        'accounts'      => $accounts,
        'uni'           => Universe::getEmulated(),
        'signalColors'  => $USER['signalColors'],
    ]);
    $template->show('ActivePage.tpl');
}
