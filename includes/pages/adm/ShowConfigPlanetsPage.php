<?php

/**
 *  2Moons
 *   by Timo_Ka 2023
 *
 * For the full copyright and license information, please view the LICENSE
 *
 * @package pr0game
 * @copyright 2023 Timo_Ka
 * @licence MIT
 * @version 1.8.0
 * @link https://codeberg.org/pr0game/pr0game
 */

function ShowConfigPlanetsPage (){
    send();
    show();
}


if (!allowedTo('ShowMultiIPPage')) {
    throw new Exception("Permission error!");
}

function send()
{
    $action = HTTP::_GP('mode', '');
    
    if ($action != "send" ){
        return;
    }
    
    $planetData = [];
    $db = Database::get();
    $sql = "SELECT * FROM %%PLANET_DATA%% WHERE universe = :universe ORDER BY planet_position ASC";
    $planetDataDefault = $db->select($sql, [
        ':universe' => 0,
    ]);

    for ($i=0; $i <= 14; $i++) { 
        $planetData[$i] = [
            'universe' => 0,
            'planet_position' => $i+1,
            'temp_min' => HTTP::_GP("temp_min_" . $i+1, $planetDataDefault[$i]['temp_min']),
            'temp_max' => HTTP::_GP("temp_max_" . $i+1, $planetDataDefault[$i]['temp_max']),
            'fields_min' => HTTP::_GP("fields_min_" . $i+1, $planetDataDefault[$i]['fields_min']),
            'fields_max' => HTTP::_GP("fields_max_" . $i+1, $planetDataDefault[$i]['fields_max']),
            'metalBonusPercent' => HTTP::_GP("metalBonusPercent_" . $i+1, $planetDataDefault[$i]['metalBonusPercent']),
            'crystalBonusPercent' => HTTP::_GP("crystalBonusPercent_" . $i+1, $planetDataDefault[$i]['crystalBonusPercent']),
            'deuteriumBonusPercent' => HTTP::_GP("deuteriumBonusPercent_" . $i+1, $planetDataDefault[$i]['deuteriumBonusPercent']),
        ];
    }

    for ($i=0; $i <= 14; $i++) { 
        
        if (!array_diff_assoc($planetDataDefault[$i], $planetData[$i]) ){
            continue;
        }

        $sql = "SELECT COUNT(*) as count 
                FROM %%PLANET_DATA%%  
                WHERE universe = :universe AND planet_position = :planet_position
                ORDER BY planet_position ASC;";

        $count = $db->selectSingle($sql,[
            ':universe'             => Universe::getEmulated(),
            ':planet_position'      => $i+1,
        ], 'count');

        if ( $count == 0 ){
            $sql = "INSERT INTO %%PLANET_DATA%% SET      
            universe                = :universe,
            planet_position         = :planet_position,
            temp_min                = :temp_min,
            temp_max                = :temp_max,
            fields_min              = :fields_min,
            fields_max              = :fields_max,
            metalBonusPercent       = :metalBonusPercent,
            crystalBonusPercent     = :crystalBonusPercent,
            deuteriumBonusPercent   = :deuteriumBonusPercent;"; 
            
            $db->insert($sql, [
                ':universe'             => Universe::getEmulated(),
                ':planet_position'      => $i+1,
                ':temp_min'             => $planetData[$i]['temp_min'],
                ':temp_max'             => $planetData[$i]['temp_max'],
                ':fields_min'           => $planetData[$i]['fields_min'],
                ':fields_max'           => $planetData[$i]['fields_max'],
                ':metalBonusPercent'    => $planetData[$i]['metalBonusPercent'],
                ':crystalBonusPercent'  => $planetData[$i]['crystalBonusPercent'],
                ':deuteriumBonusPercent'=> $planetData[$i]['deuteriumBonusPercent'],
            ]);
        } else {
            $sql = "UPDATE %%PLANET_DATA%% SET      
            temp_min                = :temp_min,
            temp_max                = :temp_max,
            fields_min              = :fields_min,
            fields_max              = :fields_max,
            metalBonusPercent       = :metalBonusPercent,
            crystalBonusPercent     = :crystalBonusPercent,
            deuteriumBonusPercent   = :deuteriumBonusPercent
            WHERE universe = :universe AND planet_position = :planet_position;"; 
            
            $db->update($sql, [
                ':universe'             => Universe::getEmulated(),
                ':planet_position'      => $i+1,
                ':temp_min'             => $planetData[$i]['temp_min'],
                ':temp_max'             => $planetData[$i]['temp_max'],
                ':fields_min'           => $planetData[$i]['fields_min'],
                ':fields_max'           => $planetData[$i]['fields_max'],
                ':metalBonusPercent'    => $planetData[$i]['metalBonusPercent'],
                ':crystalBonusPercent'  => $planetData[$i]['crystalBonusPercent'],
                ':deuteriumBonusPercent'=> $planetData[$i]['deuteriumBonusPercent'],
            ]);
        }
    }
}

function show()
{

    $USER =& Singleton()->USER;

    $mergedArray = PlayerUtil::getPlanetDataPlanetConfig();

    $template	= new template();
    $template->assign_vars([
        'signalColors'      => $USER['signalColors'],
        'planet_data'       => $mergedArray,
    ]);

    $template->show('ConfigBodyPlanets.tpl');
}

