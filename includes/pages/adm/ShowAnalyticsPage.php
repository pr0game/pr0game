<?php

if (!allowedTo('ShowMultiIPPage')) {
    throw new Exception("Permission error!");
}

function ShowAnalyticsPage()
{
    $LNG =& Singleton()->LNG;
    $USER =& Singleton()->USER;
    $db = Database::get();

    if (!isset($_GET['order'])) {
        $_GET['order'] = '';
    }
    $ORDER = $_GET['order'] == 'id' ? "id" : "username";
    if (!isset($_GET['order2'])) {
        $_GET['order2'] = '';
    }
    $ORDER2 = $_GET['order2'] == 'id' ? "id" : "username";

    if (isset($_POST['user_name']) && isset($_POST['userListSubmit'])) {
        $sql = 'UPDATE %%USERS%% SET `enable_analytics` = 1 WHERE `id` = :userID;';
        $db->update($sql, [
            ':userID' => $_POST['user_name'],
        ]);
    } else if (isset($_POST['analyse_name']) && isset($_POST['analysedUsersSubmit'])) {
        $sql = 'UPDATE %%USERS%% SET `enable_analytics` = 0 WHERE `id` = :userID;';
        $db->update($sql, [
            ':userID' => $_POST['analyse_name'],
        ]);
        $sql = 'DELETE FROM %%USERS_ANALYTICS%% WHERE `userID` = :userID;';
        $db->delete($sql, [
            ':userID' => $_POST['analyse_name'],
        ]);
    }

    $userSelect	= ['List' => '', 'ListBan' => ''];
    $usercount = 0;
    $bancount = 0;

    $sql = 'SELECT `username`, `id`, `enable_analytics` FROM %%USERS%%
        WHERE `authlevel` = 0 AND `universe` = :uni
        ORDER BY ' . $ORDER . ' ASC;';
    $userList = $db->select($sql, [
        ':uni' => Universe::getEmulated(),
    ]);
    foreach ($userList as $a) {
        $userSelect['List'] .= '<option value="'.$a['id'].'">'.$a['username'].'&nbsp;&nbsp;(ID:&nbsp;'.$a['id'].')'.(($a['enable_analytics']	==	'1') ? '&nbsp;(tracked)' : '').'</option>';
        $usercount++;
    }

    $sql = 'SELECT `username`, `id`, `enable_analytics` FROM %%USERS%%
        WHERE `enable_analytics` = 1 AND `universe` = :uni
        ORDER BY ' . $ORDER2 . ' ASC;';
    $userListBan = $db->select($sql, [
        ':uni' => Universe::getEmulated(),
    ]);
    foreach ($userListBan as $b) {
        $userSelect['ListBan']	.=	'<option value="'.$b['id'].'">'.$b['username'].'&nbsp;&nbsp;(ID:&nbsp;'.$b['id'].')</option>';
        $bancount++;
    }

    $sql = 'SELECT ac.`userID`, u.`username`, a.`ally_name`, ac.`onlinetime`, ac.`url`
        FROM %%USERS_ANALYTICS%% AS ac
        LEFT JOIN %%USERS%% AS u ON u.`id` = ac.`userID`
        LEFT JOIN %%ALLIANCE%% AS a ON a.`id` = u.`ally_id`
        WHERE u.`universe` = :uni ORDER BY `onlinetime` DESC;';

    $analytics = $db->select($sql, [
        ':uni' => Universe::getEmulated(),
    ]);

    foreach ($analytics as &$Data) {
        $Data['onlinetime']	= _date($LNG['php_tdformat'], $Data['onlinetime']);
    }

    $template	= new template();
    $template->loadscript('filterlist.js');
    $template->assign_vars([
        'UserSelect'        => $userSelect,
        'usercount'         => $usercount,
        'bancount'          => $bancount,
        'analytics'	        => $analytics,
        'signalColors'      => $USER['signalColors'],
        'bo_select_title'   => $LNG['bo_select_title'],
    ]);
    $template->show('Analytics.tpl');
}
