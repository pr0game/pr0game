<?php

/**
 * pr0game powered by steemnova
 * Accounts
 * (c) 2024 reflexrecon
 */

abstract class AbstractAccountPage
{
    /**
     * reference of the template object
     * @var template
     */
    protected $tplObj;

    /**
     * reference of the template object
     * @var ResourceUpdate
     */
    protected $ecoObj;
    protected $window;
    protected $disableEcoSystem = false;

    protected function __construct()
    {
        $this->setWindow('full');
        $this->initTemplate();
    }

    protected function initTemplate()
    {
        if (isset($this->tplObj)) {
            return true;
        }

        $this->tplObj = new template();
        [$tplDir] = $this->tplObj->getTemplateDir();
        $this->tplObj->setTemplateDir($tplDir . 'account/');
        return true;
    }

    protected function setWindow($window)
    {
        $this->window = $window;
    }

    protected function getWindow()
    {
        return $this->window;
    }

    protected function getQueryString()
    {
        $queryString = [];
        $page = HTTP::_GP('page', '');

        if (!empty($page)) {
            $queryString['page'] = $page;
        }

        $mode = HTTP::_GP('mode', '');
        if (!empty($mode)) {
            $queryString['mode'] = $mode;
        }

        $id = HTTP::_GP('id', '');
        if (!empty($id)) {
            $queryString['id'] = $id;
        }

        return http_build_query($queryString);
    }

    protected function getNavigationData()
    {
        $LNG =& Singleton()->LNG;
        $ACCOUNT =& Singleton()->ACCOUNT;
        $THEME =& Singleton()->THEME;
        $config = Config::get();

        if ($ACCOUNT['bana'] == 1) {
            echo $LNG['banned_message'];
            die();
        }

        $themeSettings = $THEME->getStyleSettings();

        $this->assign([
            'delete' => $ACCOUNT['db_deaktjava'] ? sprintf($LNG['tn_delete_mode'], _date($LNG['php_tdformat'], $ACCOUNT['db_deaktjava'] + ($config->del_user_manually * 86400)), $ACCOUNT['timezone']) : false,
            'accountname' => $ACCOUNT['accountname'],
            'shortlyNumber' => $themeSettings['TOPNAV_SHORTLY_NUMBER'],
            'hasBoard' => filter_var($config->forum_url, FILTER_VALIDATE_URL),
            #'hasAdminAccess' => !empty(Session::load()->adminAccess),
            'discordUrl' => DISCORD_URL,
        ]);
    }

    protected function getPageData()
    {
        $ACCOUNT =& Singleton()->ACCOUNT;
        $THEME =& Singleton()->THEME;
        $LNG =& Singleton()->LNG;
        $LNG->includeData(['INGAME', 'ACCOUNT']);
        $dateTimeServer = new DateTime("now");
        if (isset($ACCOUNT['timezone'])) {
            try {
                $dateTimeUser = new DateTime("now", new DateTimeZone($ACCOUNT['timezone']));
            } catch (Exception $e) {
                $dateTimeUser = $dateTimeServer;
            }
        } else {
            $dateTimeUser = $dateTimeServer;
        }

        $config = Config::get();
        $deletion = false;
        if ($ACCOUNT['db_deaktjava']) {
            $deletion = sprintf($LNG['tn_delete_mode'], _date($LNG['php_tdformat'], AccountUtil::getDeletionDate($ACCOUNT['db_deaktjava'], $config)), $ACCOUNT['timezone']);
        }
        $this->assign([
            'delete' => $deletion,
            'authlevel' => $ACCOUNT['authlevel'],
            'userID' => $ACCOUNT['id'],
            'bodyclass' => $this->getWindow(),
            'game_name' => $config->game_name,
            'uni_name' => $config->uni_name,
            'use_google_analytics' => $config->use_google_analytics,
            'google_analytics_key' => $config->google_analytics_key,
            'debug' => $config->debug,
            'version' => $config->version,
            'date' => explode("|", date('Y\|n\|j\|G\|i\|s\|Z', TIMESTAMP)),
            'REV' => substr($config->version, -4),
            'Offset' => $dateTimeUser->getOffset() - $dateTimeServer->getOffset(),
            'queryString' => $this->getQueryString(),
            'themeSettings' => $THEME->getStyleSettings(),
        ]);
    }

    protected function printMessage($message, $redirectButtons = null, $redirect = null, $fullSide = true)
    {
        $this->assign([
            'message' => $message,
            'redirectButtons' => $redirectButtons,
        ]);

        if (isset($redirect)) {
            $this->tplObj->gotoside($redirect[0], $redirect[1]);
        }

        if (!$fullSide) {
            $this->setWindow('popup');
        }

        $this->display('error.default.tpl');
    }

    protected function assign($array, $nocache = true)
    {
        $this->tplObj->assign_vars($array, $nocache);
    }

    protected function display($file)
    {
        $THEME =& Singleton()->THEME;
        $LNG =& Singleton()->LNG;
        $ACCOUNT =& Singleton()->ACCOUNT;
        // $this->updatePlanetTime();
  
        if ($this->getWindow() !== 'ajax') {
            $this->getPageData();
        }
        $config = Config::get();
        $captchakey = $config->recaptcha_pub_key;
        $this->assign([
            'lang'              => $ACCOUNT['lang'],
            'accountname'       => $ACCOUNT['accountname'],
            'dpath'             => $THEME->getTheme(),
            'scripts'           => $this->tplObj->jsscript,
            'execscript'        => implode("\n", $this->tplObj->script),
            'basepath'          => PROTOCOL . HTTP_HOST . HTTP_BASE,
            'TIMEZONESTRING'    => $ACCOUNT['timezone'],
            'signalColors'      => $ACCOUNT['signalColors'],
            'captchakey'        => $captchakey,
        ]);
        $this->assign([
            'LNG' => $LNG
        ], false);

        $this->tplObj->display('extends:layout.' . $this->getWindow() . '.tpl|' . $file);
        exit;
    }

    protected function sendJSON($data)
    {
        // $this->updatePlanetTime();
        echo json_encode($data);
        exit;
    }

    protected function redirectTo($url)
    {
        // $this->updatePlanetTime();
        HTTP::redirectTo($url);
        exit;
    }
}
