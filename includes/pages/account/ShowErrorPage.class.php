<?php

/**
 * pr0game powered by steemnova
 * Accounts
 * (c) 2024 reflexrecon
 */


class ShowErrorPage extends AbstractAccountPage
{
    public static $requireModule = 0;

    protected $disableEcoSystem = true;

    public function __construct()
    {
        parent::__construct();
        $this->initTemplate();
        $LNG		= Singleton()->LNG;
        $ACCOUNT	= Singleton()->ACCOUNT;
        $this->assign([
            'id'                => $ACCOUNT['id'],
            'username'          => $ACCOUNT['accountname'],
            'authlevel'         => $ACCOUNT['authlevel'],
            'toplink'           => "account.php?page=account",
            'topname'           => sprintf($LNG['Back']),
        ]);
    }

    public static function printError($Message, $fullSide = true, $redirect = null)
    {
        $pageObj	= new self();
        $pageObj->printMessage($Message, null, $redirect, $fullSide);
    }

    public function show()
    {
    }
}
