<?php

/**
 * pr0game powered by steemnova
 * Accounts
 * (c) 2024 reflexrecon
 */


class ShowAccountSettingsPage extends AbstractAccountPage
{
    public function __construct()
    {
        parent::__construct();
    }

    public function show()
    {
        $LNG =& Singleton()->LNG;
        $LNG->includeData(['INGAME', 'ACCOUNT']);
        $ACCOUNT =& Singleton()->ACCOUNT;

        $this->assign([
            'Selectors'         => [
                'timezones' => get_timezone_selector(),
                'Skins' => Theme::getAvalibleSkins(),
                'lang' => $LNG->getAllowedLangs(false),
            ],
            'id'            => $ACCOUNT['id'],
            'delete'        => $ACCOUNT['db_deaktjava'],
            'accountname'   => $ACCOUNT['accountname'],
            'userLang'      => $ACCOUNT['lang'],
            'theme'         => $ACCOUNT['dpath'],
            'timezone'      => $ACCOUNT['timezone'],
            'toplink'       => "account.php?page=account",
            'topname'       => sprintf($LNG['Back']),
        ]);

        $this->display('page.accountSettings.default.tpl');
    }

    public function send()
    {
        $ACCOUNT =& Singleton()->ACCOUNT;
        $LNG =& Singleton()->LNG;
        $LNG->includeData(['INGAME', 'ACCOUNT']);
        $THEME =& Singleton()->THEME;
        $password           = HTTP::_GP('password', '');
        $newpassword        = HTTP::_GP('newpassword', '');
        $newpassword2       = HTTP::_GP('newpassword2', '');
        $email              = HTTP::_GP('email', $ACCOUNT['email']);
        $language           = HTTP::_GP('language', '');
        $theme              = HTTP::_GP('theme', $THEME->getThemeName());
        $timezone           = HTTP::_GP('timezone', '');

        $language = array_key_exists($language, $LNG->getAllowedLangs(false)) ? $language : $LNG->getLanguage();
        $theme = array_key_exists($theme, Theme::getAvalibleSkins()) ? $theme : $THEME->getThemeName();

        $db = Database::get();
        $db->startTransaction();
        $sql = "SELECT * FROM %%ACCOUNTS%% a WHERE a.id = :id FOR UPDATE;";
        $db->select($sql, [
            ':id' => $ACCOUNT['id'],
        ]);
        $this->assign([
            'Selectors'         => [
                'timezones' => get_timezone_selector(),
                'Skins' => Theme::getAvalibleSkins(),
                'lang' => $LNG->getAllowedLangs(false),
            ],
            'id'            => $ACCOUNT['id'],
            'delete'        => $ACCOUNT['db_deaktjava'],
            'accountname'   => $ACCOUNT['accountname'],
            'userLang'      => $ACCOUNT['lang'],
            'theme'         => $ACCOUNT['dpath'],
            'timezone'      => $ACCOUNT['timezone'],
            'toplink'       => "account.php?page=account",
            'topname'       => sprintf($LNG['Back']),
        ]);
        if (
            !empty($newpassword)
            && !empty($password)
            && password_verify($password, $ACCOUNT['password'])
            && $newpassword == $newpassword2
        ) {
            $newpass     = AccountUtil::cryptPassword($newpassword);
            $sql = "UPDATE %%ACCOUNTS%% SET password = :newpass WHERE id = :accountID;";
            $db->update($sql, [
                ':newpass'  => $newpass,
                ':accountID'   => $ACCOUNT['id'],
            ]);
            Session::load()->delete();
        }

        if (!empty($email) && $email != $ACCOUNT['email']) {
            if (!password_verify($password, $ACCOUNT['password'])) {
                $this->printMessage($LNG['op_need_pass_mail'], [
                    [
                        'label' => $LNG['sys_back'],
                        'url'   => 'game.php?page=settings',
                    ],
                ]);
            } elseif (!ValidateAddress($email)) {
                $this->printMessage($LNG['op_not_vaild_mail'], [
                    [
                        'label' => $LNG['sys_back'],
                        'url'   => 'account.php?page=accountSettings',
                    ],
                ]);
            } else {
                $sql = "SELECT"
                    . " (SELECT COUNT(*) FROM %%ACCOUNTS%% WHERE id != :accountID AND (email ="
                    . " :email OR email_2 = :email)) + (SELECT COUNT(*) FROM %%ACCOUNTS_VALID%% WHERE email = :email) as count";
                $Count = $db->selectSingle($sql, [
                    ':accountID'   => $ACCOUNT['id'],
                    ':email'    => $email,
                ], 'count');

                if (!empty($Count)) {
                    $this->printMessage(sprintf($LNG['op_change_mail_exist'], $email), [
                        [
                            'label' => $LNG['sys_back'],
                            'url'   => 'account.php?page=accountSettings',
                        ],
                    ]);
                } else {
                    $sql = "UPDATE %%ACCOUNTS%% SET email = :email, setmail = :time WHERE id = :userID;";
                    $db->update($sql, [
                        ':email'    => $email,
                        ':time'     => (TIMESTAMP + 604800),
                        ':userID'   => $ACCOUNT['id'],
                    ]);
                }
            }
        }

        $sql =  "UPDATE %%ACCOUNTS%% SET
		lang				        = :lang,
		dpath				        = :theme,
		timezone				    = :timezone
		WHERE id = :userID;";
        $db->update($sql, [
            ':lang'              => $language,
            ':theme'             => $theme,
            ':timezone'          => $timezone,
            ':userID'            => $ACCOUNT['id'],
        ]);
        $this->assign([
            'Selectors'         => [
                'timezones' => get_timezone_selector(),
                'Skins' => Theme::getAvalibleSkins(),
                'lang' => $LNG->getAllowedLangs(false),
            ],
            'id'            => $ACCOUNT['id'],
            'accountname'   => $ACCOUNT['accountname'],
            'userLang'      => $ACCOUNT['lang'],
            'theme'         => $ACCOUNT['dpath'],
            'timezone'      => $ACCOUNT['timezone'],
            'toplink'       => "account.php?page=account",
            'topname'       => sprintf($LNG['Back']),
        ]);
        $db->commit();

        $this->printMessage($LNG['op_options_changed'], [
            [
                'label' => $LNG['sys_forward'],
                'url'   => 'account.php?page=accountSettings',
            ]
        ]);
    }

    public function toggleDelete()
    {
        $LNG =& Singleton()->LNG;
        $ACCOUNT =& Singleton()->ACCOUNT;

        $db = Database::get();
        $db->startTransaction();

        $sql = "SELECT id FROM %%ACCOUNTS%% WHERE id = :accountID FOR UPDATE;";
        $db->selectSingle($sql, [
            ':accountID'   => $ACCOUNT['id'],
        ]);
        $this->assign([
            'toplink'       => "account.php?page=account",
            'topname'       => sprintf($LNG['Back']),
        ]);
        if ($ACCOUNT['db_deaktjava'] === 0) {
            $sql = "UPDATE %%ACCOUNTS%% SET db_deaktjava = :timestamp WHERE id = :accountID;";
            $db->update($sql, [
                ':accountID'       => $ACCOUNT['id'],
                ':timestamp'    => TIMESTAMP,
            ]);
            $db->commit();

            $text = $LNG['op_options_deletion_activated'];
            $text .= '<br>' . $LNG['op_options_no_other_settings_changed'];
            $this->printMessage($text, [
                [
                    'label' => $LNG['sys_forward'],
                    'url'   => 'account.php?page=accountSettings',
                ]
            ]);
        } else {
            $sql = "UPDATE %%ACCOUNTS%% SET db_deaktjava = 0 WHERE id = :accountID;";
            $db->update($sql, [':accountID'   => $ACCOUNT['id']]);
            $db->commit();

            $text = $LNG['op_options_deletion_deactivated'];
            $text .= '<br>' . $LNG['op_options_no_other_settings_changed'];
            $this->printMessage($text, [
                [
                    'label' => $LNG['sys_forward'],
                    'url'   => 'account.php?page=accountSettings',
                ]
            ]);
        }
    }
}
