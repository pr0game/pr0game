<?php

/**
 * pr0game powered by steemnova
 * Accounts
 * (c) 2024 reflexrecon
 */


class ShowAccountPage extends AbstractAccountPage
{
    public function __construct()
    {
        parent::__construct();
    }

    public function show()
    {
        $LNG =& Singleton()->LNG;
        $LNG->includeData(['INGAME', 'ACCOUNT']);
        $ACCOUNT =& Singleton()->ACCOUNT;
        $newUniverses = AccountUtil::get_non_player_universes($ACCOUNT['id']);
        $playerUniverses = AccountUtil::get_player_universes($ACCOUNT['id']);
        $this->assign([
            'id'                => $ACCOUNT['id'],
            'username'          => $ACCOUNT['accountname'],
            'authlevel'         => $ACCOUNT['authlevel'],
            'newUniverses'      => $newUniverses,
            'playerUniverses'   => $playerUniverses,
            'toplink'           => "account.php?page=accountSettings",
            'topname'           => sprintf($LNG['Settings']),
        ]);

        $this->display('page.account.default.tpl');
    }

    public function send()
    {
        $ACCOUNT =& Singleton()->ACCOUNT;
        $userID = HTTP::_GP('userID', 0);
        $universe = HTTP::_GP('universe', 0);

        $db = Database::get();
        $sql = "SELECT count(*) as count FROM %%ACCOUNTS_TO_USERS%% WHERE `account_id` = :accountId && user_id = :userId;";
        $count = $db->selectSingle($sql, [':accountId' => $ACCOUNT['id'], ':userId' => $userID], 'count');

        if ($count == 0) {
            HTTP::redirectTo('account.php?page=account');
        }
        
        $session	= Session::create();
        $session->userId		= $userID;
        $session->universe      = $universe;
        $session->planetId      = 0;
        $session->adminAccess	= 0;
        $session->save();

        HTTP::redirectTo('game.php');
    }
}
