<?php

/**
 * pr0game powered by steemnova
 * Accounts
 * (c) 2024 reflexrecon
 */

class ShowRegisterPage extends AbstractAccountPage
{
    public function __construct()
    {
        parent::__construct();
    }

    public function show()
    {
        require_once 'includes/pages/account/ShowErrorPage.class.php';
        $uniId = HTTP::_GP('uni', '0');
        $LNG =& Singleton()->LNG;
        $ACCOUNT =& Singleton()->ACCOUNT;
        $LNG->includeData(['INGAME', 'ACCOUNT']);
        $accountName = "";
        $status ="";
        $config = Config::get($uniId);
        if ($config->uni_status == STATUS_CLOSED && $ACCOUNT['authlevel'] < 3) {
            $status = sprintf($LNG['uni_status_regclosed_gameclosed']);
            ShowErrorPage::printError($LNG['uni_status_regclosed_gameclosed']);
        } elseif ($config->uni_status == STATUS_REG_ONLY && $ACCOUNT['authlevel'] < 3) {
            $status = sprintf($LNG['uni_status_regopen_gameclosed']);
        } elseif ($config->uni_status == STATUS_LOGIN_ONLY && $ACCOUNT['authlevel'] < 3) {
            $status = sprintf($LNG['uni_status_regclosed_gameopen']);
            ShowErrorPage::printError($LNG['uni_status_regclosed_gameopen']);
        } else if ($config->uni_status == STATUS_OPEN && $ACCOUNT['authlevel'] < 3) {
            $status = sprintf($LNG['uni_status_regopen_gameopen']);
        } else {
            $status = sprintf($LNG['uni_status_regopen_gameopen']);
        }
        $db = Database::get();
        $sql = "SELECT `user_id` FROM %%ACCOUNTS_TO_USERS%% WHERE `account_id` = :accountId;";
        $defaultUnis["default"] = "default";
        $user_ids = $db->select($sql, [':accountId' => $ACCOUNT['id']]);
        foreach ($user_ids as $user_id) {
            $sql = "SELECT u.`uni_name`, u.`uni` FROM %%CONFIG_UNIVERSE%% u INNER JOIN %%USERS%% us on u.uni = us.universe WHERE us.`id` = :userId;";
            $universeName = $db->selectSingle($sql, [':userId' => $user_id['user_id']]);
            $defaultUnis[$universeName['uni']] = $universeName['uni_name'];
        }

        $config = Config::get();
        $this->assign([
            'Selectors'         => [
                'timezones' => get_timezone_selector(),
                'Skins' => Theme::getAvalibleSkins(),
                'lang' => $LNG->getAllowedLangs(false),
            ],
            'accountName' => $accountName,
            'universe' => $config->uni_name,
            'uniId' => $uniId,
            'status' => $status,
            'registerPasswordDesc' => sprintf($LNG['registerPasswordDesc'], 6),
            'toplink'       => "account.php?page=account",
            'topname'       => sprintf($LNG['Back']),
            'userLang' => $ACCOUNT['lang'],
            'timezone' => $ACCOUNT['timezone'],
            'theme' => $ACCOUNT['dpath'],
            'defaultUnis' => $defaultUnis,
            'default' => $defaultUnis['default']
        ]);

        $this->display('page.register.default.tpl');
    }

    public function send()
    {
        $LNG =& Singleton()->LNG;
        $ACCOUNT =& Singleton()->ACCOUNT;
        $universe = HTTP::_GP('uni', 0);
        $config = Config::get($universe);
        $LNG->includeData(['INGAME', 'ACCOUNT']);
        $this->assign([
            'accountName' => $ACCOUNT['accountname'],
            'universe' => $config->uni_name,
            'toplink'       => "account.php?page=account",
            'topname'       => sprintf($LNG['Back'])
        ]);
        if ($config->uni_status == STATUS_CLOSED || $config->uni_status == STATUS_LOGIN_ONLY || $universe == 0) {
            $this->printMessage($LNG['registerErrorUniClosed'], [[
                'label' => $LNG['registerBack'],
                'url' => 'javascript:window.history.back()',
            ]]);
        }

        $userName = HTTP::_GP('username', '', UTF8_SUPPORT);
        $language = HTTP::_GP('language', 'en');
        $theme = HTTP::_GP('theme', '');
        $timezone = HTTP::_GP('timezone', '');
        $colors = HTTP::_GP('colors', 'default');
        $mission = HTTP::_GP('mission', 'default');
        $stb = HTTP::_GP('stb', 'default');

        $errors = [];

        if (empty($userName)) {
            $errors[] = $LNG['registerErrorUsernameEmpty'];
        }

        if (!AccountUtil::isNameValid($userName)) {
            $errors[] = $LNG['registerErrorUsernameChar'];
        }

        

        $db = Database::get();
        $colorSettingsUni = 'default';
        $missionSettingsUni = 'default';
        $stbSettingsUni = 'default';
        // dropdown key value damit man die bezeichnung des unis nicht durcs select ziehen sollen
        if ($colors != 'default' || $mission != 'default' || $stb != 'default') {
            $unisql = "SELECT u.uni FROM %%CONFIG_UNIVERSE%% u WHERE u.uni_name = :uniName;";
            if ($colors != 'default') {
                //verify for acc im uni
                $colorSettingsUni = $db->selectSingle($unisql, [':uniName' => $colors], 'id');
            }
            if ($mission != 'default') {
                $missionSettingsUni = $db->selectSingle($unisql, [':uniName' => $mission], 'id');
            }
            if ($stb != 'default') {
                $stbSettingsUni = $db->selectSingle($unisql, [':uniName' => $stb], 'id');
            }
        }
        $sql = "SELECT COUNT(*) as count
				FROM %%USERS%%
				WHERE universe = :universe
				AND username = :userName;";

        $countUsername = $db->selectSingle($sql, [
            ':universe' => $universe,
            ':userName' => $userName,
        ], 'count');
 
        if ($countUsername != 0) {
            $errors[] = $LNG['registerErrorUsernameExist'];
        }

        if (!empty($errors)) {
            $this->assign([
                'accountName' => $ACCOUNT['accountname'],
                'toplink'       => "account.php?page=account",
                'topname'       => sprintf($LNG['Back']),
            ]);
            $this->printMessage(implode("<br>\r\n", $errors), [[
                'label' => $LNG['registerBack'],
                'url' => 'javascript:window.history.back()',
            ]]);
        }

        if ($config->ref_active == 1 && !empty($referralID)) {
            $sql = "SELECT COUNT(*) as state FROM %%USERS%% WHERE id = :referralID AND universe = :universe;";
            $Count = $db->selectSingle($sql, [
                ':referralID' => $referralID,
                ':universe' => $universe
            ], 'state');

            if ($Count == 0) {
                $referralID = 0;
            }
        } else {
            $referralID = 0;
        }

        $validationKey = md5(uniqid('2m'));

        require_once 'includes/classes/PlayerUtil.class.php';

        list($userID, $planetID) = PlayerUtil::createPlayer(
            $ACCOUNT['id'], $universe, $userName, $language, null, null, null, null, 
            $ACCOUNT['authlevel'], Session::getClientIp(), $stbSettingsUni, $colorSettingsUni, $missionSettingsUni, $timezone, $theme
        );
        $LNG->includeData(['PUBLIC']);
        $senderName = $LNG['registerWelcomePMSenderName'];
        $subject 	= $LNG['registerWelcomePMSubject'];
        $message 	= sprintf($LNG['registerWelcomePMText'], $config->game_name, $universe);

        PlayerUtil::sendMessage($userID, 1, $senderName, 1, $subject, $message, TIMESTAMP);
        $session	= Session::create();
        $session->userId		= $userID;
        $session->universe      = $universe;
        $session->adminAccess	= 0;
        $session->save();

        HTTP::redirectTo('game.php');
    }
}
