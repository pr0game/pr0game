<?php

/**
 * pr0game powered by steemnova
 * Accounts
 * (c) 2024 reflexrecon
 */


class ShowLogoutPage extends AbstractAccountPage
{
    public static $requireModule = 0;

    public function __construct()
    {
        parent::__construct();
        $this->setWindow('popup');
    }

    public function show()
    {
        Session::load()->delete();
        $this->display('page.logout.default.tpl');
    }
}
