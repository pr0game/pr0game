<?php

/**
 *  2Moons
 *   by Timo_Ka 2023
 *
 * For the full copyright and license information, please view the LICENSE
 *
 * @package pr0game
 * @copyright 2023 Timo_Ka
 * @licence MIT
 * @version 1.8.0
 * @link https://codeberg.org/pr0game/pr0game
 */

class ShowTeamPage extends AbstractLoginPage
{

    public function __construct()
    {
        parent::__construct();

    }

    public function show()
    {

        $universe = Universe::current();
        $db = Database::get();

        $sql = "SELECT `id`, `username` FROM %%USERS%% WHERE authlevel >= 1 and `universe` = :universe";
        $admins = $db->select($sql,[
            ':universe' => $universe,
        ]);

        $idSchmopfi         = "0";
        $idCornag           = "0";
        $idQualle           = "0";
        $idReflexrecon      = "0";
        $idAdman            = "0";
        $idTimoKa           = "0";
        $idHackbrett        = "0";
        $idMasterspiel      = "0";
        $idPara             = "0";
        $idAtain            = "0";
        $idMrgl             = "0";

        foreach ($admins as $user) {
            switch (strtolower($user["username"])){
                case strtolower("Schmopfi"):
                    $idSchmopfi = $user["id"];
                    break;
                case strtolower("Cornag"):
                    $idCornag = $user["id"];
                    break;
                case strtolower("Qualle"):
                    $idQualle = $user["id"];
                    break;
                case strtolower("reflexrecon"):
                    $idReflexrecon = $user["id"];
                    break;
                case strtolower("Adman"):
                    $idAdman = $user["id"];
                    break;
                case strtolower("timo_ka"):
                    $idTimoKa = $user["id"];
                    break;
                case strtolower("Hackmin"):
                    $idHackbrett = $user["id"];
                    break;
                case strtolower("Masterspiel"):
                    $idMasterspiel = $user["id"];
                    break;
                case strtolower("para Admin"):
                    $idPara = $user["id"];
                    break;
                case strtolower("Atain"):
                    $idAtain = $user["id"];
                    break;
                case strtolower("Captain Mrgl"):
                    $idMrgl = $user["id"];
                    break;
                default:
                    break;
            }
        }

        $this->assign([
            'idSchmopfi'		    => $idSchmopfi,
            'idCornag'		        => $idCornag,
            'idQualle'		        => $idQualle,
            'idReflexrecon'		    => $idReflexrecon,
            'idAdman'		        => $idAdman,
            'idTimoKa'		        => $idTimoKa,
            'idHackbrett'		    => $idHackbrett,
            'idMasterspiel'		    => $idMasterspiel,
            'idPara'		        => $idPara,
            'idAtain'		        => $idAtain,
            'idMrgl'		        => $idMrgl,
        ]);
        $this->display('page.team.default.tpl');

    }

}