<?php

/**
 *  2Moons
 *   by Jan-Otto Kröpke 2009-2016
 * For the full copyright and license information, please view the LICENSE
 * @package 2Moons
 * @author Jan-Otto Kröpke <slaver7@gmail.com>
 * @copyright 2009 Lucky
 * @copyright 2016 Jan-Otto Kröpke <slaver7@gmail.com>
 * @licence MIT
 * @version 1.8.0
 * @link https://github.com/jkroepke/2Moons
 */

class ShowRegisterPage extends AbstractLoginPage
{
    public function __construct()
    {
        parent::__construct();
    }

    public function show()
    {
        $LNG =& Singleton()->LNG;
        $referralData = ['id' => 0, 'name' => ''];
        $accountName = "";

        $config = Config::get();
        if ($config->ref_active == 1 && !empty($referralID)) {
            $db = Database::get();

            $sql = "SELECT username FROM %%USERS%% WHERE id = :referralID AND universe = :universe;";
            $referralAccountName = $db->selectSingle($sql, [
                ':referralID' => $referralID,
                ':universe' => Universe::current()
            ], 'username');

            if (!empty($referralAccountName)) {
                $referralData = ['id' => $referralID, 'name' => $referralAccountName];
            }
        }

        $this->assign([
            'referralData' => $referralData,
            'accountName' => $accountName,
            'timezones' => get_timezone_selector(),
            'registerPasswordDesc' => sprintf($LNG['registerPasswordDesc'], 6),
            'registerRulesDesc' => sprintf($LNG['registerRulesDesc'], '<a href="index.php?page=rules&lang=' . ($GLOBALS['_COOKIE']['lang'] ?? 'de') . '">' . $LNG['menu_rules'] . '</a>'),
            'registerDataPrivacyDesc' => sprintf($LNG['registerDataPrivacyDesc'], '<a href="index.php?page=dataPrivacy&lang=' . ($GLOBALS['_COOKIE']['lang'] ?? 'de') . '">' . $LNG['siteTitleDataPrivacy'] . '</a>')
        ]);

        $this->display('page.register.default.tpl');
    }

    public function send()
    {
        $LNG =& Singleton()->LNG;
        $config = Config::get();

        $userName       = HTTP::_GP('username', '', UTF8_SUPPORT);
        $password       = HTTP::_GP('password', '', true);
        $password2      = HTTP::_GP('passwordReplay', '', true);
        $mailAddress    = HTTP::_GP('email', '');
        $mailAddress2   = HTTP::_GP('emailReplay', '');
        $rulesChecked   = HTTP::_GP('rules', 0);
        $privacyChecked = HTTP::_GP('privacy', 0);
        $language       = HTTP::_GP('lang', '');
        $timezone       = HTTP::_GP('time', '');

        $referralID = HTTP::_GP('referralID', 0);

        $errors = [];

        if (empty($userName)) {
            $errors[] = $LNG['registerErrorUsernameEmpty'];
        }

        if (!AccountUtil::isNameValid($userName)) {
            $errors[] = $LNG['registerErrorUsernameChar'];
        }

        if (strlen($password) < 6) {
            $errors[] = sprintf($LNG['registerErrorPasswordLength'], 6);
        }

        if ($password != $password2) {
            $errors[] = $LNG['registerErrorPasswordSame'];
        }

        if (!AccountUtil::isMailValid($mailAddress)) {
            $errors[] = $LNG['registerErrorMailInvalid'];
        }

        if (empty($mailAddress)) {
            $errors[] = $LNG['registerErrorMailEmpty'];
        }

        if ($mailAddress != $mailAddress2) {
            $errors[] = $LNG['registerErrorMailSame'];
        }

        if ($rulesChecked != 1) {
            $errors[] = $LNG['registerErrorRules'];
        }

        if ($privacyChecked != 1) {
            $errors[] = $LNG['registerErrorDataPrivacy'];
        }

        $db = Database::get();

        $sql = "SELECT (
				SELECT COUNT(*)
				FROM %%ACCOUNTS%%
				WHERE accountname = :userName
			) + (
				SELECT COUNT(*)
				FROM %%ACCOUNTS_VALID%%
				WHERE accountname = :userName
			) as count;";

        $countUsername = $db->selectSingle($sql, [
            ':userName' => $userName,
        ], 'count');

        $sql = "SELECT (
			SELECT COUNT(*)
			FROM %%ACCOUNTS%%
			WHERE (
				email = :mailAddress
				OR email_2 = :mailAddress
			)
		) + (
			SELECT COUNT(*)
			FROM %%ACCOUNTS_VALID%%
			WHERE email = :mailAddress
		) as count;";

        $countMail = $db->selectSingle($sql, [
            ':mailAddress' => $mailAddress,
        ], 'count');

        if ($countUsername != 0) {
            $errors[] = $LNG['registerErrorUsernameExist'];
        }

        if ($countMail != 0) {
            $errors[] = $LNG['registerErrorMailExist'];
        }

        if (!empty($errors)) {
            $this->printMessage(implode("<br>\r\n", $errors), [[
                'label' => $LNG['registerBack'],
                'url' => 'javascript:window.history.back()',
            ]]);
        }

        if ($config->ref_active == 1 && !empty($referralID)) {
            $sql = "SELECT COUNT(*) as state FROM %%USERS%% WHERE id = :referralID;";
            $Count = $db->selectSingle($sql, [
                ':referralID' => $referralID,
            ], 'state');

            if ($Count == 0) {
                $referralID = 0;
            }
        } else {
            $referralID = 0;
        }

        $validationKey = md5(uniqid('2m'));

        $sql = "INSERT INTO %%ACCOUNTS_VALID%% SET
				`accountname` = :userName,
				`validationKey` = :validationKey,
				`password` = :password,
				`email` = :mailAddress,
				`date` = :timestamp,
				`ip` = :remoteAddr,
				`language` = :language,
				`referralID` = :referralID,
                `timezone` = :timezone;";


        $db->insert($sql, [
            ':userName' => $userName,
            ':validationKey' => $validationKey,
            ':password' => AccountUtil::cryptPassword($password),
            ':mailAddress' => $mailAddress,
            ':timestamp' => TIMESTAMP,
            ':remoteAddr' => Session::getClientIp(),
            ':language' => $language,
            ':referralID' => $referralID,
            ':timezone' => $timezone,
        ]);

        $sql = "SELECT validationID AS id FROM %%ACCOUNTS_VALID%% WHERE validationKey = :validationKey;";
        $validationID = $db->selectSingle($sql, [
            ':validationKey' => $validationKey,
        ], 'id');
        $verifyURL = 'index.php?page=vertify&i=' . $validationID . '&k=' . $validationKey;

        if ($config->user_valid == 0) {
            $this->redirectTo($verifyURL);
        } else {
            require 'includes/classes/Mail.class.php';
            $MailRAW = $LNG->getTemplate('email_vaild_reg');
            $MailContent = str_replace([
                '{USERNAME}',
                '{EMAIL}',
                '{GAMENAME}',
                '{VERTIFYURL}',
                '{DISCORD}',
            ], [
                $userName,
                $mailAddress,
                $config->game_name,
                HTTP_PATH . $verifyURL,
                DISCORD_URL,
            ], $MailRAW);

            $subject = sprintf($LNG['registerMailVertifyTitle'], $config->game_name);
            Mail::send($mailAddress, $userName, $subject, $MailContent);

            $this->printMessage($LNG['registerSendComplete']);
        }
    }
}
