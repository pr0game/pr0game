<?php

/**
 *  2Moons
 *   by Jan-Otto Kröpke 2009-2016
 *
 * For the full copyright and license information, please view the LICENSE
 *
 * @package 2Moons
 * @author Jan-Otto Kröpke <slaver7@gmail.com>
 * @copyright 2009 Lucky
 * @copyright 2016 Jan-Otto Kröpke <slaver7@gmail.com>
 * @licence MIT
 * @version 1.8.0
 * @link https://github.com/jkroepke/2Moons
 */


class ShowLostPasswordPage extends AbstractLoginPage
{
    public static $requireModule = 0;

    public function __construct()
    {
        parent::__construct();
    }

    public function show()
    {
        if (!Config::get()->mail_active) {
            $this->redirectTo('index.php');
        }

        $universeSelect	= $this->getUniverseSelector();

        $this->assign([
            'universeSelect'	=> $universeSelect
        ]);

        $this->display('page.lostPassword.default.tpl');
    }

    public function newPassword()
    {
        $LNG =& Singleton()->LNG;
        $accountID		= HTTP::_GP('u', 0);
        $validationKey	= HTTP::_GP('k', '');

        $db = Database::get();

        $sql = "SELECT COUNT(*) as state FROM %%LOSTPASSWORD%% WHERE accountID = :accountID AND `key` = :validationKey AND `time` > :time AND hasChanged = 0;";
        $isValid = $db->selectSingle($sql, [
            ':accountID'		=> $accountID,
            ':validationKey'	=> $validationKey,
            ':time'				=> (TIMESTAMP - 1800)
        ], 'state');

        if (empty($isValid)) {
            $this->printMessage($LNG['passwordValidInValid'], [[
                'label'	=> $LNG['passwordBack'],
                'url'	=> 'index.php',
            ]]);
        }

        $newPassword	= uniqid();

        $sql = "UPDATE %%ACCOUNTS%% SET password = :newPassword WHERE id = :accountID;";
        $db->update($sql, [
            ':accountID'		=> $accountID,
            ':newPassword'	=> AccountUtil::cryptPassword($newPassword)
        ]);

        $sql = "UPDATE %%LOSTPASSWORD%% SET hasChanged = 1 WHERE accountID = :accountID AND `key` = :validationKey;";
        $db->update($sql, [
            ':accountID'			=> $accountID,
            ':validationKey'	=> $validationKey
        ]);

        $sql = "SELECT accountname, email_2 as mail FROM %%ACCOUNTS%% WHERE id = :accountID;";
        $accountData = $db->selectSingle($sql, [
            ':accountID'	=> $accountID,
        ]);

        $config			= Config::get();

        $MailRAW		= $LNG->getTemplate('email_lost_password_changed');
        $MailContent	= str_replace([
            '{USERNAME}',
            '{GAMENAME}',
            '{DISCORD}',
            '{PASSWORD}',
        ], [
            $accountData['accountname'],
            $config->game_name,
            DISCORD_URL,
            $newPassword,
        ], $MailRAW);

        if ($config->mail_active && !empty($config->smtp_host)) {
            require 'includes/classes/Mail.class.php';
            $subject	= sprintf($LNG['passwordChangedMailTitle'], $config->game_name);
            Mail::send($accountData['mail'], $accountData['accountname'], $subject, $MailContent);

            $this->printMessage($LNG['passwordChangedMailSend'], [[
                'label'	=> $LNG['passwordNext'],
                'url'	=> 'index.php',
            ]]);
        } else {
            $this->printMessage(nl2br($MailContent), [[
                'label'	=> $LNG['passwordNext'],
                'url'	=> 'index.php',
            ]]);
        }
    }

    public function send()
    {
        $LNG =& Singleton()->LNG;
        $accountname	= HTTP::_GP('username', '', UTF8_SUPPORT);
        $mail		= HTTP::_GP('mail', '', true);

        $errorMessages	= [];

        if (empty($accountname)) {
            $errorMessages[]	= $LNG['passwordUsernameEmpty'];
        }

        if (empty($mail)) {
            $errorMessages[]	= $LNG['passwordErrorMailEmpty'];
        }

        $config	= Config::get();

        if (!empty($errorMessages)) {
            $message	= implode("<br>\r\n", $errorMessages);
            $this->printMessage($message, [[
                'label'	=> $LNG['passwordBack'],
                'url'	=> 'index.php?page=lostPassword',
            ]]);
        }

        $db = Database::get();

        $sql = "SELECT id FROM %%ACCOUNTS%% WHERE accountname = :accountname AND email_2 = :mail;";
        $accountID = $db->selectSingle($sql, [
            ':accountname'	=> $accountname,
            ':mail'		=> $mail
        ], 'id');

        if (empty($accountID)) {
            $this->printMessage($LNG['passwordErrorUnknown'], [[
                'label'	=> $LNG['passwordBack'],
                'url'	=> 'index.php?page=lostPassword',
            ]]);
        }

        $sql = "SELECT COUNT(*) as state FROM %%LOSTPASSWORD%% WHERE accountID = :accountID AND time > :time AND hasChanged = 0;";
        $hasChanged = $db->selectSingle($sql, [
            ':accountID'	=> $accountID,
            ':time'		=> (TIMESTAMP - 86400)
        ], 'state');

        if (!empty($hasChanged)) {
            $this->printMessage($LNG['passwordErrorOnePerDay'], [[
                'label'	=> $LNG['passwordBack'],
                'url'	=> 'index.php?page=lostPassword',
            ]]);
        }

        $validationKey	= md5(uniqid());

        $sql = "INSERT INTO %%LOSTPASSWORD%% SET accountID = :accountID, `key` = :validationKey, `time` = :timestamp, fromIP = :remoteAddr;";
        $db->insert($sql, [
            ':accountID'	=> $accountID,
            ':timestamp'	=> TIMESTAMP,
            ':validationKey'=> $validationKey,
            ':remoteAddr'	=> Session::getClientIp()
        ]);

        $MailRAW		= $LNG->getTemplate('email_lost_password_validation');

        $MailContent	= str_replace([
            '{USERNAME}',
            '{GAMENAME}',
            '{VALIDURL}',
        ], [
            $accountname,
            $config->game_name,
            HTTP_PATH.'index.php?page=lostPassword&mode=newPassword&u='.$accountID.'&k='.$validationKey,
        ], $MailRAW);

        if ($config->mail_active && !empty($config->smtp_host)) {
            require 'includes/classes/Mail.class.php';
            $subject	= sprintf($LNG['passwordValidMailTitle'], $config->game_name);
            Mail::send($mail, $accountname, $subject, $MailContent);

            $this->printMessage($LNG['passwordValidMailSend'], [[
                'label'	=> $LNG['passwordNext'],
                'url'	=> 'index.php',
            ]]);
        } else {
            $verifyURL = 'index.php?page=lostPassword&mode=newPassword&u='.$accountID.'&k='.$validationKey;
            $this->redirectTo($verifyURL);
            // $validurl = HTTP_PATH.'index.php?page=lostPassword&mode=newPassword&u='.$userID.'&k='.$validationKey;
            // echo '<meta http-equiv="refresh" content="0; url='.$validurl.'"/>';
        }
    }
}
