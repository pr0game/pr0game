<?php

/**
 *  2Moons
 *   by Jan-Otto Kröpke 2009-2016
 *
 * For the full copyright and license information, please view the LICENSE
 *
 * @package 2Moons
 * @author Jan-Otto Kröpke <slaver7@gmail.com>
 * @copyright 2009 Lucky
 * @copyright 2016 Jan-Otto Kröpke <slaver7@gmail.com>
 * @licence MIT
 * @version 1.8.0
 * @link https://github.com/jkroepke/2Moons
 */

class ShowVertifyPage extends AbstractLoginPage
{
    public static $requireModule = 0;

    public function __construct()
    {
        parent::__construct();
    }

    private function _activeUser()
    {
        $LNG =& Singleton()->LNG;

        $validationID	= HTTP::_GP('i', 0);
        $validationKey	= HTTP::_GP('k', '');

        $db = Database::get();

        $sql = "SELECT * FROM %%ACCOUNTS_VALID%%
		WHERE validationID	= :validationID
		AND validationKey	= :validationKey;";

        $userData = $db->selectSingle($sql, [
            ':validationKey'	=> $validationKey,
            ':validationID'		=> $validationID
        ]);

        if (empty($userData)) {
            $this->printMessage($LNG['vertifyNoUserFound']);
        }

        $config	= Config::get();

        $sql = "DELETE FROM %%ACCOUNTS_VALID%% WHERE validationID = :validationID;";
        $db->delete($sql, [
            ':validationID'	=> $validationID
        ]);
        $accountID = AccountUtil::createAccount($userData['accountname'], $userData['password'], $userData['email'], $userData['language'], 0, $userData['ip'], $userData['timezone']);

        if ($config->mail_active == 1) {
            require('includes/classes/Mail.class.php');
            $MailSubject	= sprintf($LNG['registerMailCompleteTitle'], $config->game_name);
            $MailRAW		= $LNG->getTemplate('email_reg_done');
            $MailContent	= str_replace([
                '{USERNAME}',
                '{GAMENAME}',
                '{DISCORD}',
            ], [
                $userData['accountname'],
                $config->game_name,
                DISCORD_URL,
            ], $MailRAW);

            try {
                Mail::send($userData['email'], $userData['accountname'], $MailSubject, $MailContent);
            } catch (Exception $e) {
                // This mail is wayne.
            }
        }

        if (!empty($userData['referralID'])) {
            $sql = "UPDATE %%USERS%% SET
			`ref_id`	= :referralId,
			`ref_bonus`	= 1
			WHERE
			`id`		= :userID;";

            $db->update($sql, [
                ':referralId'	=> $userData['referralID'],
                ':userID'		=> $userID
            ]);
        }

        return [
            'accountID'	=> $accountID,
            'accountname'	=> $userData['accountname'],
        ];
    }

    public function show()
    {
        $userData	= $this->_activeUser();
        $db = Database::get();
        $sql = "SELECT * FROM %%ACCOUNTS%% WHERE `id` = :accountID;";
        $ACCOUNT = $db->selectSingle($sql, [
            ':accountID'	=> $userData['accountID']
        ]);
        Singleton()->ACCOUNT = $ACCOUNT;
        $ACCOUNT =& Singleton()->ACCOUNT;
        $session	= Session::create();
        $session->accountId		= (int) $userData['accountID'];
        $session->adminAccess	= 0;
        $session->save();

        HTTP::redirectTo('account.php');
    }

    public function json()
    {
        $LNG =& Singleton()->LNG;
        $userData	= $this->_activeUser();
        $this->sendJSON(sprintf($LNG['vertifyAdminMessage'], $userData['accountname']));
    }
}
