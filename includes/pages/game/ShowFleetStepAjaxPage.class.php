<?php

/**
 * pr0game powered by steemnova
 * Fleets
 * (c) 2024 Hyman
 */

class ShowFleetStepAjaxPage extends AbstractGamePage
{
    public $returnData = [];

    public static $requireModule = 0;

    public function __construct()
    {
        parent::__construct();
        $this->setWindow('ajax');
    }

    /**
     * This function will delete the prepared fleet in session via its token.
     * Called by ajax in flotten.js on browser page change.
     * Function addBeforeUnload() adds the listener to the window object. It is called in FleetStep1 and 2 pages.
     * CheckTarget() and CheckResources() are called by forms of Sttep1 and 2 to proceed, which is also a page change.
     * To avoid deleting the fleet token, removeBeforeUnload() is called within them. 
     * On failed checks addBeforeUnload() is called again.
     *
     * @return void
     */
    public function show()
    {
        $token = HTTP::_GP('token', '');
        $session = Session::load();
        $fleet = $session->fleet;
        if (isset($fleet[$token])) {
            unset($fleet[$token]);
            $session->fleet = $fleet;
            $session->save();
        }
    }
}
