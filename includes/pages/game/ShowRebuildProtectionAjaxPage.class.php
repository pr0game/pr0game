<?php

/**
 * pr0game powered by steemnova
 * RebuildProtection
 * (c) 2024 Hyman
 */

class ShowRebuildProtectionAjaxPage extends AbstractGamePage
{
    public static $requireModule = 0;

    public function __construct()
    {
        parent::__construct();
        $this->setWindow('ajax');
    }

    public function show()
    {
        $USER =& Singleton()->USER;
        $db = Database::get();
        $sql = "UPDATE %%USERS%% SET `rebuild_protection` = 0, `rebuild_protection_attackers` = ''
            WHERE `id` = :userID;";
        $db->update($sql, [
            ':userID' => $USER['id'],
        ]);
    }
}