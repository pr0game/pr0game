<?php

/**
 *  2Moons
 *   by Jan-Otto Kröpke 2009-2016
 *
 * For the full copyright and license information, please view the LICENSE
 *
 * @package 2Moons
 * @author Jan-Otto Kröpke <slaver7@gmail.com>
 * @copyright 2009 Lucky
 * @copyright 2016 Jan-Otto Kröpke <slaver7@gmail.com>
 * @licence MIT
 * @version 1.8.0
 * @link https://github.com/jkroepke/2Moons
 */


class ShowEmpirePage extends AbstractGamePage
{
    public static $requireModule = MODULE_IMPERIUM;

    public function __construct()
    {
        parent::__construct();
    }

    public function show()
    {
        $USER =& Singleton()->USER;
        $resource =& Singleton()->resource;
        $reslist =& Singleton()->reslist;
        $db = Database::get();
        $db->startTransaction();
        $db->selectSingle("SELECT * FROM %%USERS%% u INNER JOIN %%USERS_SETTINGS%% us ON us.user_id = u.id WHERE u.id = :userID FOR UPDATE;", [':userID' => $USER['id']]);
        $order = $USER['planet_sort_order'] == 1 ? 'DESC' : 'ASC';

        $sql = "SELECT * FROM %%PLANETS%% WHERE id_owner = :userID AND destruyed = '0' ORDER BY ";
        switch ($USER['planet_sort']) {
            case 2:
                $sql .= 'name ' . $order;
                break;
            case 1:
                $sql .= 'galaxy ' . $order . ', system ' . $order . ', planet ' . $order . ', planet_type ' . $order;
                break;
            default:
                $sql .= 'id ' . $order;
                break;
        }
        $sql .= " FOR UPDATE";
        $PlanetsRAW = $db->select($sql, [
            ':userID' => $USER['id'],
        ]);

        $PLANETS = [];

        $PlanetRess = new ResourceUpdate();

        foreach ($PlanetsRAW as $CPLANET) {
            list($USER, $CPLANET) = $PlanetRess->CalcResource($USER, $CPLANET, true);

            $PLANETS[] = $CPLANET;
            unset($CPLANET);
        }

        $db->commit();

        $planetList = [];
        $config = Config::get($USER['universe']);

        if (!empty($USER['b_tech_queue'])) {
            $researchque=unserialize($USER['b_tech_queue']);

            $researchid= $researchque[0][0];
            $researchlvl= $researchque[0][1];
            $planetid=$researchque[0][4];
        } else {
            $researchid= -1;
            $researchlvl= -1;
            $planetid= -1;
        }
        $this->assign([
            'researchq_id'      => $researchid,
            'researchq_lvl'     => $researchlvl,
            'researchq_planet'  => $planetid,
        ]);
        
        $sumBuildings = [];
        foreach ($reslist['build'] as $elementID) {
            if (!BuildFunctions::isEnabled($elementID)) {
                continue;
            }
            $sumBuildings[1][$elementID]['sum'] = 0;
            $sumBuildings[1][$elementID]['cnt'] = 0;
            $sumBuildings[3][$elementID]['sum'] = 0;
            $sumBuildings[3][$elementID]['cnt'] = 0;
        }
        
        foreach ($PLANETS as $Planet) {
            $planetList['image'][$Planet['id']]['id'] = $Planet['id'];
            $planetList['name'][$Planet['id']] = $Planet['name'];
            $planetList['image'][$Planet['id']]['picture'] = $Planet['image'];
            $planetList['image'][$Planet['id']]['planet_style'] = $Planet['planet_style'];

            $planetList['coords'][$Planet['id']]['galaxy'] = $Planet['galaxy'];
            $planetList['coords'][$Planet['id']]['system'] = $Planet['system'];
            $planetList['coords'][$Planet['id']]['planet'] = $Planet['planet'];

            $planetList['field'][$Planet['id']]['current'] = $Planet['field_current'];
            $planetList['field'][$Planet['id']]['max'] = CalculateMaxPlanetFields($Planet);

            $planetList['energy_used'][$Planet['id']] = $Planet['energy'] + $Planet['energy_used'];

            $planetList['resource'][RESOURCE_METAL][$Planet['id']] = $Planet['metal'];
            $planetList['resource'][RESOURCE_CRYSTAL][$Planet['id']] = $Planet['crystal'];
            $planetList['resource'][RESOURCE_DEUT][$Planet['id']] = $Planet['deuterium'];
            $planetList['resource'][RESOURCE_ENERGY][$Planet['id']] = $Planet['energy'] + $Planet['energy_used'];

            if ($Planet['planet_type'] == 1) {
                $basic[RESOURCE_METAL][$Planet['id']] = $config->metal_basic_income * ($config->resource_multiplier / 2500);
                $basic[RESOURCE_CRYSTAL][$Planet['id']] = $config->crystal_basic_income * ($config->resource_multiplier / 2500);
                $basic[RESOURCE_DEUT][$Planet['id']] = $config->deuterium_basic_income * ($config->resource_multiplier / 2500);
                $planetList['resourceFull'][RESOURCE_METAL][$Planet['id']] = floor(max($Planet['metal_max'] - $Planet['metal'], 0) / max(1, $Planet['metal_perhour'] + $basic[901][$Planet['id']]));
                $planetList['resourceFull'][RESOURCE_CRYSTAL][$Planet['id']] = floor(max($Planet['crystal_max'] - $Planet['crystal'], 0) / max(1, $Planet['crystal_perhour'] + $basic[902][$Planet['id']]));
                $planetList['resourceFull'][RESOURCE_DEUT][$Planet['id']] = floor(max($Planet['deuterium_max'] - $Planet['deuterium'], 0) / max($basic[903][$Planet['id']] + $Planet['deuterium_perhour'], 1));
            } else {
                $basic[RESOURCE_METAL][$Planet['id']] = 0;
                $basic[RESOURCE_CRYSTAL][$Planet['id']] = 0;
                $basic[RESOURCE_DEUT][$Planet['id']] = 0;
            }
            if (!empty($Planet['b_building_id'])) {
                $Queue = unserialize($Planet['b_building_id']);
                $planetList['currentBuilding'][$Planet['id']] = $Queue[0][0];
                $planetList['currentBuildinglvl'][$Planet['id']] = $Queue[0][1];
            } else {
                $planetList['currentBuilding'][$Planet['id']] = -1;
                $planetList['currentBuildinglvl'][$Planet['id']] = -1;
            }

            $planetList['resourcePerHour'][RESOURCE_METAL][$Planet['id']] = $basic[RESOURCE_METAL][$Planet['id']] + $Planet['metal_perhour'];
            $planetList['resourcePerHour'][RESOURCE_CRYSTAL][$Planet['id']] = $basic[RESOURCE_CRYSTAL][$Planet['id']] + $Planet['crystal_perhour'];
            $planetList['resourcePerHour'][RESOURCE_DEUT][$Planet['id']] = $basic[RESOURCE_DEUT][$Planet['id']] + $Planet['deuterium_perhour'];

            $planetList['resourceProductionPercent'][RESOURCE_METAL][$Planet['id']] = $Planet['metal_mine_porcent'];
            $planetList['resourceProductionPercent'][RESOURCE_CRYSTAL][$Planet['id']] = $Planet['crystal_mine_porcent'];
            $planetList['resourceProductionPercent'][RESOURCE_DEUT][$Planet['id']] = $Planet['deuterium_sintetizer_porcent'];

            $planetList['planet_type'][$Planet['id']] = $Planet['planet_type'];

            foreach ($reslist['build'] as $elementID) {
                if (!BuildFunctions::isEnabled($elementID)) {
                    continue;
                }
                $planetList['build'][$elementID][$Planet['id']] = $Planet[$resource[$elementID]];

                if ($Planet[$resource[$elementID]] > 0 && in_array($elementID, $reslist['allow'][$Planet['planet_type']])) {
                    $sumBuildings[$Planet['planet_type']][$elementID]['sum'] += $Planet[$resource[$elementID]];
                    $sumBuildings[$Planet['planet_type']][$elementID]['cnt'] += 1;
                }
                
                $planetList['buildable'][$elementID][$Planet['id']] = false;
                if (BuildFunctions::isTechnologieAccessible($USER, $Planet, $elementID)) {
                    $levelToBuild = $Planet[$resource[$elementID]] + 1;
                    $costResources = BuildFunctions::getElementPrice($USER, $Planet, $elementID, false, $levelToBuild);
                    $planetList['buildable'][$elementID][$Planet['id']] = BuildFunctions::isElementBuyable($USER, $Planet, $elementID, $costResources);
                }
            }

            foreach ($reslist['fleet'] as $elementID) {
                if (!BuildFunctions::isEnabled($elementID)) {
                    continue;
                }
                $planetList['fleet'][$elementID][$Planet['id']] = $Planet[$resource[$elementID]];
                $planetList['currentShipyard'][$elementID][$Planet['id']] = 0;
                $planetList['currentFlying'][$elementID][$Planet['id']] = 0;

                $planetList['buildable'][$elementID][$Planet['id']] = false;
                if (BuildFunctions::isTechnologieAccessible($USER, $Planet, $elementID)) {
                    $costResources = BuildFunctions::getElementPrice($USER, $Planet, $elementID, false, 1);
                    $planetList['buildable'][$elementID][$Planet['id']] = BuildFunctions::isElementBuyable($USER, $Planet, $elementID, $costResources);
                }
            }

            foreach ($reslist['defense'] as $elementID) {
                if (!BuildFunctions::isEnabled($elementID)) {
                    continue;
                }
                $planetList['defense'][$elementID][$Planet['id']] = $Planet[$resource[$elementID]];
                $planetList['currentShipyard'][$elementID][$Planet['id']] = 0;

                $planetList['buildable'][$elementID][$Planet['id']] = false;
                if (BuildFunctions::isTechnologieAccessible($USER, $Planet, $elementID)) {
                    $costResources = BuildFunctions::getElementPrice($USER, $Planet, $elementID, false, 1);
                    $planetList['buildable'][$elementID][$Planet['id']] = BuildFunctions::isElementBuyable($USER, $Planet, $elementID, $costResources);
                }
            }

            foreach ($reslist['missile'] as $elementID) {
                if (!BuildFunctions::isEnabled($elementID)) {
                    continue;
                }
                $planetList['missiles'][$elementID][$Planet['id']] = $Planet[$resource[$elementID]];
                $planetList['currentShipyard'][$elementID][$Planet['id']] = 0;
                $planetList['currentFlying'][$elementID][$Planet['id']] = 0;

                $planetList['buildable'][$elementID][$Planet['id']] = false;
                if (BuildFunctions::isTechnologieAccessible($USER, $Planet, $elementID)) {
                    $costResources = BuildFunctions::getElementPrice($USER, $Planet, $elementID, false, 1);
                    $planetList['buildable'][$elementID][$Planet['id']] = BuildFunctions::isElementBuyable($USER, $Planet, $elementID, $costResources);
                }
            }

            // Special case 'MISSION_STATION': will be selected for correct 'fleet_start_id'/'fleet_end_id' only depending on canceld status
            // Special case 'MISSION_TRANSPORT': ships return only to 'fleet_start_id', ress return to 'fleet_end_id' except if canceled
            // Others: ships and ress will return to 'fleet_start_id'
            $sql = "SELECT `fleet_array`, `fleet_resource_metal`, `fleet_resource_crystal`, `fleet_resource_deuterium`, `fleet_start_id`, `fleet_end_id`, `fleet_mission`, `hasCanceled` FROM %%FLEETS%% 
                WHERE `fleet_owner` = :userID
                AND (
                    (`fleet_start_id` = :planetID AND (`fleet_mission` != :missionStay OR `hasCanceled` = 1))
                    OR (`fleet_end_id` = :planetID AND (`fleet_mission` = :missionStay OR `fleet_mission` = :missionTransport) AND `hasCanceled` = 0)
                );";
            $fleets = $db->select($sql, [
                ':userID' => $USER['id'],
                ':planetID' => $Planet['id'],
                ':missionStay' => MISSION_STATION,
                ':missionTransport' => MISSION_TRANSPORT,
            ]);

            $planetList['currentTransported'][RESOURCE_METAL][$Planet['id']] = 0;
            $planetList['currentTransported'][RESOURCE_CRYSTAL][$Planet['id']] = 0;
            $planetList['currentTransported'][RESOURCE_DEUT][$Planet['id']] = 0;

            foreach ($fleets as $fleetID => $fleet) {
                // Special case 'MISSION_TRANSPORT': ships return only to 'fleet_start_id'
                if ($fleet['fleet_mission'] != MISSION_TRANSPORT || $fleet['fleet_start_id'] == $Planet['id']) {
                    $fleetArray = FleetFunctions::unserialize($fleet['fleet_array']);
                    foreach ($fleetArray as $elementID => $amount) {
                        $planetList['currentFlying'][$elementID][$Planet['id']] += $amount;
                    }
                }
                
                // Special case 'MISSION_TRANSPORT': ress return to 'fleet_end_id' except if canceled
                if ($fleet['fleet_mission'] != MISSION_TRANSPORT || 
                    ($fleet['fleet_start_id'] == $Planet['id'] && $fleet['hasCanceled']) || 
                    ($fleet['fleet_end_id'] == $Planet['id'] && !$fleet['hasCanceled'])
                    ) {
                    $planetList['currentTransported'][RESOURCE_METAL][$Planet['id']] += $fleet['fleet_resource_metal'];
                    $planetList['currentTransported'][RESOURCE_CRYSTAL][$Planet['id']] += $fleet['fleet_resource_crystal'];
                    $planetList['currentTransported'][RESOURCE_DEUT][$Planet['id']] += $fleet['fleet_resource_deuterium'];
                }
            }

            if (!empty($Planet['b_hangar_id'])) {
                foreach (unserialize($Planet['b_hangar_id']) as $que) {
                    $planetList['currentShipyard'][$que[0]][$Planet['id']] += $que[1];
                }
            }

            foreach ($reslist['tech'] as $elementID) {
                if (!BuildFunctions::isEnabled($elementID)) {
                    continue;
                }

                $planetList['tech'][$elementID][$Planet['id']] = -1;
                $planetList['buildable'][$elementID][$Planet['id']] = false;
                if (BuildFunctions::isTechnologieAccessible($USER, $Planet, $elementID)) {
                    $planetList['tech'][$elementID][$Planet['id']] = $USER[$resource[$elementID]];
                    $levelToBuild = $USER[$resource[$elementID]] + 1;
                    $costResources = BuildFunctions::getElementPrice($USER, $Planet, $elementID, false, $levelToBuild);
                    $planetList['buildable'][$elementID][$Planet['id']] = BuildFunctions::isElementBuyable($USER, $Planet, $elementID, $costResources);
                } else if ($Planet[$resource[RESEARCH_LABORATORY]] > 0) {
                    $planetList['tech'][$elementID][$Planet['id']] = 0;
                }
            }
        }

        foreach ($reslist['build'] as $elementID) {
            if (!BuildFunctions::isEnabled($elementID)) {
                continue;
            }
            if (in_array($elementID, $reslist['allow'][1]) && in_array($elementID, $reslist['allow'][3]) && 
                    ($sumBuildings[1][$elementID]['cnt'] + $sumBuildings[3][$elementID]['cnt']) > 0) {
                $avgBuildings[$elementID] = ($sumBuildings[1][$elementID]['sum'] + $sumBuildings[3][$elementID]['sum']) / 
                    ($sumBuildings[1][$elementID]['cnt'] + $sumBuildings[3][$elementID]['cnt']);
            } else if (in_array($elementID, $reslist['allow'][1]) && $sumBuildings[1][$elementID]['cnt'] > 0) {
                $avgBuildings[$elementID] = $sumBuildings[1][$elementID]['sum'] / $sumBuildings[1][$elementID]['cnt'];
            } else if ($sumBuildings[3][$elementID]['cnt'] > 0) {
                $avgBuildings[$elementID] = $sumBuildings[3][$elementID]['sum'] / $sumBuildings[3][$elementID]['cnt'];
            } else {
                $avgBuildings[$elementID] = 0;
            }
        }

        $currentPlanetCount = PlayerUtil::currentPlanetCount($USER);
        $maxPlanetCount = PlayerUtil::maxPlanetCount($USER);      
        $enablePlanetStyles = $USER['enable_planet_styles'];

        $this->assign([
            'colspan'                       => count($PLANETS) + 2,
            'planetList'                    => $planetList,
            'avgBuildings'                  => $avgBuildings,
            'currentPlanetCount'            => $currentPlanetCount,
            'maxPlanetCount'                => $maxPlanetCount,
            'enablePlanetStyles'            => $enablePlanetStyles,
            'enableShowProdPerHour'         => $_COOKIE['empire_showProdPerHour'] ?? true,
            'enableShowStorageFilled'       => $_COOKIE['empire_showStorageFilled'] ?? true,
            'enableHiglightBuildable'       => $_COOKIE['empire_highlightBuildable'] ?? false,
            'enableHiglightNonBuildable'    => $_COOKIE['empire_highlightNonBuildable'] ?? false,
        ]);

        $this->display('page.empire.default.tpl');
    }
}
