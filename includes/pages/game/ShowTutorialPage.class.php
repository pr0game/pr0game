<?php

/**
 * pr0game powered by steemnova
 * tutorial
 * (c) 2024 Hyman
 */

class ShowTutorialPage extends AbstractGamePage
{
    public static $requireModule = MODULE_TUTORIAL;

    public function __construct()
    {
        parent::__construct();
        require_once 'includes/classes/tutorials/TutorialUtil.class.php';
    }

    /**
     * Attempts to skip the current tutorial step goal if given by request, or the whole step, if able
     * 
     * Called by button/link in shared.tutorial.tpl
     *
     * @return void
     */
    public function skip()
    {
        $goal = HTTP::_GP('goal', 0);
        $tutorialUtil = new TutorialUtil();

        if ($goal > 0) {
            $tutorialUtil->skipGoal($goal);
        }
        else {
            $tutorialUtil->skip();
        }

        $this->redirectTo('game.php?page=Overview');
    }

    /**
     * Skips the entire tutorial
     * 
     * Called by button/link in shared.tutorial.tpl
     *
     * @return void
     */
    public function skipAll()
    {
        $tutorialUtil = new TutorialUtil();
        $tutorialUtil->skipAll();
        $this->redirectTo('game.php?page=Overview');
    }

    /**
     * Claims all rewards for the tutorial step, if any, and tags it accordingly
     * 
     * Called by button/link in shared.tutorial.tpl
     *
     * @return void
     */
    public function claim()
    {
        $tutorialUtil = new TutorialUtil();
        $tutorialUtil->claim();
        $this->redirectTo('game.php?page=Overview');
    }

    /**
     * Attempts to tag the given goal as fullfilled
     * 
     * Called by button/link in shared.tutorial.tpl
     *
     * @param integer $goal
     * @return void
     */
    public function fullfill()
    {
        $goal = HTTP::_GP('goal', 0);
        $tutorialUtil = new TutorialUtil();

        if ($goal > 0) {
            $tutorialUtil->fullfillGoal($goal);
        }

        $this->redirectTo('game.php?page=Overview');
    }

    /**
     * Nothing to show, redirect to overview
     *
     * @return void
     */
    public function show()
    {
        $this->redirectTo('game.php?page=Overview');
    }
}