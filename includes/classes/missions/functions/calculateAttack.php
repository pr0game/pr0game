<?php

/*
 * Battle engines
 *
 * Use SteemNova_Julia if programming runtime Julia is available
 * Use SteemNova if php-ds is installed, otherwise use SteamNova_Array
 * SteemNova's Battle Engine based on Arrays; Very slow, not recommended
 */

/*
 * DON'T MODIFY ↓
 */
function loadBattleEngine($sim = false)
{
    require_once 'includes/JuliaFunctions.php';
    if (isJuliaRunning($sim)) {
        include_once("SteemNova_Julia.php");
    } else if (extension_loaded('ds')) {
        include_once("SteemNova.php");
    } else {
        include_once("SteemNova_Array.php");
    }
}
