<?php

function CheckRebuildProtection(array $currentTotalFleetState, array $combatResult, int $combatTime, int $noobProtectionPoints)
{
    $db = Database::get();

    $attacker = [];
    foreach ($combatResult['unitLostPerSideAndUser']['attacker'] as $userID => $value) {
        $attacker[] = $userID;
    }

    $noobProtection = $noobProtectionPoints * 1000;
    foreach ($currentTotalFleetState as $userID => $state) {
        $sql = "SELECT `rebuild_protection_locked_until` FROM %%USERS%% WHERE `id` = :userID;";
        $until = $db->selectSingle($sql, [':userID' => $userID], 'rebuild_protection_locked_until');
        if ($until > $combatTime) {
            continue;
        }

        $lostUnits = 0;
        if (isset($combatResult['unitLostPerSideAndUser']['attacker'][$userID])) {
            $lostUnits += $combatResult['unitLostPerSideAndUser']['attacker'][$userID];
        }
        if (isset($combatResult['unitLostPerSideAndUser']['defender'][$userID])) {
            $lostUnits += $combatResult['unitLostPerSideAndUser']['defender'][$userID];
        }

        $lostCapacity = 0;
        if (isset($combatResult['transportCapacityLostPerSideAndUser']['attacker'][$userID])) {
            $lostCapacity += $combatResult['transportCapacityLostPerSideAndUser']['attacker'][$userID];
        }
        if (isset($combatResult['transportCapacityLostPerSideAndUser']['defender'][$userID])) {
            $lostCapacity += $combatResult['transportCapacityLostPerSideAndUser']['defender'][$userID];
        }

        if (($lostUnits > 0 && $lostUnits > $noobProtection && ($lostUnits / $state['totalUnits']) > 0.7) ||
            ($lostCapacity > 0 && $lostCapacity > $noobProtection && ($lostCapacity / $state['totalCapacity']) > 0.8)) {
            $sql = "UPDATE %%USERS%% SET `rebuild_protection` = :someTime, `rebuild_protection_attackers` = :attackers, `rebuild_protection_locked_until` = :someTimeLater
                WHERE `id` = :userID;";
            $db->update($sql, [
                ':someTime' => $combatTime,
                ':attackers' => json_encode($attacker),
                ':someTimeLater' => $combatTime + TIME_1_MONTH,
                ':userID' => $userID,
            ]);
        }
    }
}