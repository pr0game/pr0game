<?php

/**
 *  2Moons
 *   by Jan-Otto Kröpke 2009-2016
 *
 * For the full copyright and license information, please view the LICENSE
 *
 * @package 2Moons
 * @author Jan-Otto Kröpke <slaver7@gmail.com>
 * @copyright 2009 Lucky
 * @copyright 2016 Jan-Otto Kröpke <slaver7@gmail.com>
 * @licence MIT
 * @version 1.8.0
 * @link https://github.com/jkroepke/2Moons
 */

require_once 'includes/classes/cronjob/CronjobTask.interface.php';

class CleanerCronjob implements CronjobTask
{
    public function run()
    {
        $config	= Config::get(ROOT_UNI);

        $unis	= Universe::availableUniverses();

        //Delete old messages
        $del_before 	= TIMESTAMP - ($config->del_oldstuff * 86400);
        $del_inactive 	= TIMESTAMP - ($config->del_user_automatic * 86400);
        $del_deleted 	= TIMESTAMP - ($config->del_user_manually * 86400);
        $del_messages 	= TIMESTAMP - ($config->message_delete_days * 86400);

        if ($del_inactive === TIMESTAMP) {
            $del_inactive = 2147483647; // Tue Jan 19 2038 04:14:07 GMT+0100
        }

        $sql	= 'DELETE FROM %%MESSAGES%% WHERE `message_time` < :time;';
        Database::get()->delete($sql, [
            ':time'	=> $del_before
        ]);

        $sql	= 'DELETE FROM %%ALLIANCE%% WHERE `ally_members` = 0;';
        Database::get()->delete($sql);

        $sql	= 'DELETE FROM %%PLANETS%% WHERE `destruyed` < :time AND `destruyed` != 0;';
        Database::get()->delete($sql, [
            ':time'	=> TIMESTAMP
        ]);

        $sql	= 'DELETE FROM %%SESSION%% WHERE `lastonline` < :time;';
        Database::get()->delete($sql, [
            ':time'	=> TIMESTAMP - SESSION_LIFETIME
        ]);

        $sql	= 'DELETE FROM %%FLEETS_EVENT%% WHERE fleetID NOT IN (SELECT fleet_id FROM %%FLEETS%%);';
        Database::get()->delete($sql);

        $sql	= 'DELETE FROM %%LOG_FLEETS%% WHERE `start_time` < :time;';
        Database::get()->delete($sql, [
            ':time'	=> $del_before
        ]);

        $sql	= 'UPDATE %%PLANETS%% SET `tf_active` = 0 WHERE `tf_active` = 1;';
        Database::get()->update($sql);

        $sql	= 'DELETE FROM %%PHALANX_LOG%% WHERE `phalanx_time` < :time;';
        Database::get()->delete($sql, [
            ':time'	=> $del_before
        ]);

        $sql	= 'DELETE FROM %%PHALANX_FLEETS%% WHERE `phalanx_log_id` NOT IN (SELECT `id` FROM %%PHALANX_LOG%%)';
        Database::get()->delete($sql);

        foreach($unis as $uni)
        {
            $config	= Config::get($uni);
            $status = $config->uni_status;
            if ($status == STATUS_REG_ONLY) {
                continue;
            }

            $del_inactive 	= TIMESTAMP - ($config->del_user_automatic * 86400);
            $del_deleted 	= TIMESTAMP - ($config->del_user_manually * 86400);
            $sql	= 'SELECT `id` FROM %%USERS%% WHERE `authlevel` = :authlevel AND universe = :uni
		    AND ((`db_deaktjava` != 0 AND `db_deaktjava` < :timeDeleted) OR `onlinetime` < :timeInactive);';

            $deleteUserIds = Database::get()->select($sql, [
                ':authlevel'	=> AUTH_USR,
                ':timeDeleted'	=> $del_deleted,
                ':timeInactive'	=> $del_inactive,
                ':uni'			=> $uni
            ]);

            if (!empty($deleteUserIds)) {
                foreach ($deleteUserIds as $dataRow) {
                    PlayerUtil::deletePlayer($dataRow['id']);
                }
            }
        }

         //delete accounts set to delete
        $config	= Config::get();
        $del_deleted 	= TIMESTAMP - ($config->del_user_manually * 86400);
        $sql	= 'SELECT `id` FROM %%ACCOUNTS%% WHERE `authlevel` = :authlevel AND 
        (`db_deaktjava` != 0 AND `db_deaktjava` < :timeDeleted);';

        $deleteAccountIds = Database::get()->select($sql, [
            ':authlevel'	=> AUTH_USR,
            ':timeDeleted'	=> $del_deleted
        ]);

        if (!empty($deleteAccountIds)) {
            foreach ($deleteAccountIds as $dataRow) {
                AccountUtil::deleteAccount($dataRow['id']);
            }
        }

        //delete accounts without users without activity for 28 days
        $sql = "SELECT * FROM %%ACCOUNTS%% WHERE `id` NOT IN (SELECT DISTINCT(account_id) FROM uni1_accounts_to_users) AND `onlinetime` < :inactive AND `authlevel` = :authlevel;";
        $deleteAccountIds = Database::get()->select($sql, [
            ':authlevel'	=> AUTH_USR,
            ':inactive'	    => TIMESTAMP - TIME_1_MONTH
        ]);

        if (!empty($deleteAccountIds)) {
            foreach ($deleteAccountIds as $dataRow) {
                AccountUtil::deleteAccount($dataRow['id']);
            }
        }

        $sql	= 'DELETE FROM %%MESSAGES%% WHERE `message_deleted` < :time;';
        Database::get()->delete($sql, [
            ':time'	=> $del_messages
        ]);
    }
}
