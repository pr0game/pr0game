<?php

/**
 *  2Moons
 *   by Jan-Otto Kröpke 2009-2016
 *
 * For the full copyright and license information, please view the LICENSE
 *
 * @package 2Moons
 * @author Jan-Otto Kröpke <slaver7@gmail.com>
 * @copyright 2009 Lucky
 * @copyright 2016 Jan-Otto Kröpke <slaver7@gmail.com>
 * @licence MIT
 * @version 1.8.0
 * @link https://github.com/jkroepke/2Moons
 */

class PlayerUtil
{
    public static function isPositionFree($universe, $galaxy, $system, $position, $type = 1)
    {
        $db = Database::get();
        $sql = 'SELECT COUNT(*) as record
            FROM %%PLANETS%%
            WHERE `universe` = :universe
            AND `galaxy` = :galaxy
            AND `system` = :system
            AND `planet` = :position
            AND `planet_type` = :type;';

        $count = $db->selectSingle($sql, [
            ':universe' => $universe,
            ':galaxy'   => $galaxy,
            ':system'   => $system,
            ':position' => $position,
            ':type'     => $type,
        ], 'record');

        return $count == 0 && $position > 0 && $system > 0 && $galaxy > 0;
    }

    public static function isNameValid($name, $isPlanetName = false)
    {
        if (UTF8_SUPPORT && $isPlanetName) {
            return preg_match('/^[\p{L}\p{N}_\-. ]*$/u', $name);
        } elseif (UTF8_SUPPORT) {
            return preg_match('/^[0-9a-zA-ZäöüÄÖÜß _.-]*$/u', $name);
        } else {
            return preg_match('/^[A-z0-9_\-. ]*$/', $name);
        }
    }

    public static function checkPosition($universe, $galaxy, $system, $position)
    {
        $config = Config::get($universe);

        return !(1 > $galaxy
            || 1 > $system
            || 1 > $position
            || $config->max_galaxy < $galaxy
            || $config->max_system < $system
            || $config->max_planets < $position);
    }

    public static function getPlanetDataPlanetConfig() {
        $db = Database::get();
        $planetDataBackend = [];

        $sql = "SELECT * FROM %%PLANET_DATA%% WHERE universe = :universe";
        $planetDataBackend = $db->select($sql, [
            ':universe' => 0,
        ]);

        $sql = "SELECT * FROM %%PLANET_DATA%% WHERE universe = :universe";
        $planetDataBackend_universe = $db->select($sql, [
            ':universe' => Universe::current(),
        ]);

        // Step 1: Re-index generic array by `planet_position`
        $genericByPosition = [];
        foreach ($planetDataBackend as $entry) {
            $genericByPosition[$entry['planet_position']] = $entry;
        }

        // Step 2: Replace entries in the indexed array with specific entries based on `planet_position`
        foreach ($planetDataBackend_universe as $specificEntry) {
            $positionSorted = $specificEntry['planet_position'];
            // If the position exists in the generic array, replace it
            $genericByPosition[$positionSorted] = $specificEntry;
        }

        // Step 3: Re-index the array by resetting the keys to a numeric index if needed
        $planetDataBackend = array_values($genericByPosition);

        return $planetDataBackend;
    }

    public static function randomHP($universe)
    {
        $config = Config::get($universe);
        $db = Database::get();
        //get avg planets per system per galaxy
        $sql = 'SELECT tab.`galaxy`, (sum(tab.anz)/:maxSys) as AvgPlanetsPerSys FROM (
            SELECT p.`galaxy`, p.`system`, COUNT(p.`planet`) as anz
            FROM %%PLANETS%% p
            JOIN %%USERS%% u on u.`id` = p.`id_owner`
            WHERE `planet_type` = 1 AND p.`galaxy` <= :maxGala AND u.`onlinetime` >= ( :ts - :inactive ) AND p.`universe` = :universe
            GROUP BY p.`galaxy`, p.`system`
        ) as tab GROUP BY `galaxy` ORDER BY tab.`galaxy` ASC;';
        $result = $db->select($sql, [
            ':universe' => $universe,
            ':maxGala' => $config->max_galaxy,
            ':maxSys' => $config->max_system,
            ':ts' => TIMESTAMP,
            ':inactive' => INACTIVE,
        ]);

        $avgPlanetsPerGala = [];
        if (empty($result)) {
            for ($i = 1; $i <= $config->max_galaxy; $i++) {
                $avgPlanetsPerGala[] = ['galaxy' => $i, 'AvgPlanetsPerSys' => 0];
            }
        } else {
            $i = 1;
            foreach ($result as $resultArray) {
                while ($i < $resultArray['galaxy']) {
                    $avgPlanetsPerGala[] = ['galaxy' => $i, 'AvgPlanetsPerSys' => 0];
                    $i ++;
                }
                $avgPlanetsPerGala[] = $resultArray;
                $i ++;
            }
            for (; $i <= $config->max_galaxy; $i++) {
                $avgPlanetsPerGala[] = ['galaxy' => $i, 'AvgPlanetsPerSys' => 0];
            }
        }

        // get gala with min avg systems
        $minAvg = $config->max_planets;
        $galaxy = 0;
        $galaArray = [];
        foreach ($avgPlanetsPerGala as $data) {
            if ($data['AvgPlanetsPerSys'] < $minAvg) {
                $minAvg = $data['AvgPlanetsPerSys'];
            }
        }
        foreach ($avgPlanetsPerGala as $data) {
            if ($data['AvgPlanetsPerSys'] == $minAvg) {
                array_push($galaArray, $data['galaxy']);
            }
        }
        $galaxy = $galaArray[rand(0, count($galaArray) - 1)];

        // get system with planet count for selected gala
        $sql = 'SELECT `system`, count(`planet`) as anz FROM %%PLANETS%%
            WHERE `planet_type` = 1 AND `galaxy` = :gala AND `universe` = :universe GROUP BY `system`;';
        $result = $db->select($sql, [
            ':gala' => $galaxy,
            ':universe' => $universe,
        ]);

        $systems = [];
        if (empty($result)) {
            for ($i = 1; $i <= $config->max_system; $i++) {
                $systems[] = ['system' => $i, 'anz' => 0];
            }
        } else {
            $i = 1;
            foreach ($result as $resultArray) {
                while ($i < $resultArray['system']) {
                    $systems[] = ['system' => $i, 'anz' => 0];
                    $i ++;
                }
                $systems[] = $resultArray;
                $i ++;
            }
            for (; $i <= $config->max_system; $i++) {
                $systems[] = ['system' => $i, 'anz' => 0];
            }
        }

        // get empty systems in selected gala
        for ($planetamount = 0; $planetamount <= $config->max_planets; $planetamount++) {
            $usableSystems = [];
            foreach ($systems as $sysArray) {
                if ($sysArray['anz'] == $planetamount) {
                    $usableSystems[] = $sysArray['system'];
                }
            }

            $position = 0;
            $system = 0;

            // find random system and random planet inside
            if (!empty($usableSystems)) {
                do {
                    $system = $usableSystems[array_rand($usableSystems)];
                    do {
                        $position = random_int(round($config->max_planets * 0.2), round($config->max_planets * 0.8));
                    } while (!PlayerUtil::allowPlanetPosition($position, null, $universe));
                } while (self::isPositionFree($universe, $galaxy, $system, $position) === false);
            }
            if (PlayerUtil::allowPlanetPosition($position, null, $universe) && self::isPositionFree($universe, $galaxy, $system, $position)) {
                break;
            }
        }
        $pos = [
            'galaxy'   => $galaxy,
            'system'   => $system,
            'position' => $position,
        ];
        return $pos;
    }

    public static function blockHP($universe)
    {
        $config = Config::get($universe);
        $db = Database::get();

        //if less than 50 planets, place randomly
        $sql = 'SELECT Count(`id`) AS planetcount FROM %%PLANETS%% WHERE `universe` = :universe;';
        $planetcount = $db->selectSingle($sql, [
            ':universe' => $universe,
        ], 'planetcount');
        if ($planetcount < 50) {
            $pos = self::randomHP($universe);
            return $pos;
        }

        $minPos = round($config->max_planets * 0.2);
        $maxPos = round($config->max_planets * 0.8);
        $maxPlanets = $maxPos - $minPos;

        //Search for systems with one planet, if none, search for 2 planets
        for ($planetamount = 1; $planetamount < 3; $planetamount++) {
            $sql = 'SELECT p.`galaxy`, p.`system` FROM
                %%PLANETS%% p
                JOIN (
                        SELECT `galaxy`, `system`
                        FROM %%PLANETS%%
                        WHERE `planet_type` = 1
                        AND `universe` = :universe
                        AND `galaxy` <= :maxGala
                        AND `planet` >= :minPos
                        AND `planet` <= :maxPos
                        GROUP BY `galaxy`, `system`
                        HAVING COUNT(`planet`) < :maxPlanets
                    ) as tab ON tab.`galaxy` = p.`galaxy` AND tab.`system` = p.`system`
                JOIN %%USERS%% u on u.`id` = p.`id_owner`
                WHERE p.`planet_type` = 1 AND u.`onlinetime` >= ( :ts - :inactive ) AND p.`universe` = :universe
                GROUP BY p.`galaxy`, p.`system`
                HAVING COUNT(p.`planet`) < :planetamount;';
            $systems = $db->select($sql, [
                ':universe' => $universe,
                ':maxGala' => $config->max_galaxy,
                ':minPos' => $minPos,
                ':maxPos' => $maxPos,
                ':maxPlanets' => $maxPlanets,
                ':planetamount' => $planetamount,
                ':ts' => TIMESTAMP,
                ':inactive' => INACTIVE,
            ]);

            if (!empty($systems)) {
                break;
            }
        }

        // if systems found, place planet
        if (!empty($systems)) {
            do {
                $galasys = $systems[array_rand($systems)];
                $galaxy = $galasys['galaxy'];
                $system = $galasys['system'];
                do {
                    $position = random_int($minPos, $maxPos);
                } while (!PlayerUtil::allowPlanetPosition($position, null, $universe));
            } while (self::isPositionFree($universe, $galaxy, $system, $position) === false);
            $pos = [
                'galaxy'   => $galaxy,
                'system'   => $system,
                'position'  => $position,
            ];
            return $pos;
        }

        // else place random planet
        $pos = self::randomHP($universe);
        return $pos;
    }

    public static function linearHP($universe, $config){
        $galaxy = $config->last_galaxy_pos;
            $system = $config->last_system_pos;
            $position = $config->last_planet_pos;

            if ($galaxy > $config->max_galaxy) {
                $galaxy = 1;
            }

            if ($system > $config->max_system) {
                $system = 1;
            }

            do {
                $position = random_int(round($config->max_planets * 0.2), round($config->max_planets * 0.8));
                if ($position < 3) {
                    $position += 1;
                } else {
                    if ($system >= $config->max_system) {
                        $system = 1;
                        if ($galaxy >= $config->max_galaxy) {
                            $galaxy = 1;
                        } else {
                            $galaxy += 1;
                        }
                    } else {
                        $system += 1;
                    }
                }
            } while (self::isPositionFree($universe, $galaxy, $system, $position) === false);

            // Update last coordinates to config table
            $config->last_galaxy_pos = $galaxy;
            $config->last_system_pos = $system;
            $config->last_planet_pos = $position;

            $pos = [
                'galaxy'   => $galaxy,
                'system'   => $system,
                'position' => $position,
            ];
            return $pos;
    }

    public static function createPlayer(
        $accountId,
        $universe,
        $userName,
        $userLanguage = null,
        $galaxy = null,
        $system = null,
        $position = null,
        $name = null,
        $authlevel = 0,
        $userIpAddress = null,
        $stb = null,
        $colors = null,
        $mission = null,
        $timezone = null,
        $theme = null
    ) {
        $config = Config::get($universe);
        $db = Database::get();

        if (isset($universe, $galaxy, $system, $position)) {
            $pos = [
                'galaxy'   => $galaxy,
                'system'   => $system,
                'position' => $position,
            ];
        } elseif ($config->planet_creation == 1) {
            $pos = PlayerUtil::randomHP($universe);
        } elseif ($config->planet_creation == 2) {
            $rand_vs_block = rand(0, 1);
            switch ($rand_vs_block) {
                case 0:
                    $pos = PlayerUtil::randomHP($universe);
                    break;
                case 1:
                    $pos = PlayerUtil::blockHP($universe);
                    break;
            }
        } else {
            $pos = PlayerUtil::linearHP($universe, $config);
        }

        if (isset($universe, $galaxy, $system, $position)) {
            if (self::checkPosition($universe, $galaxy, $system, $position) === false) {
                throw new Exception(
                    sprintf("Try to create a planet at position: %s:%s:%s!", $galaxy, $system, $position)
                );
            }

            if (self::isPositionFree($universe, $galaxy, $system, $position) === false) {
                throw new Exception(
                    sprintf("Position is not empty: %s:%s:%s!", $galaxy, $system, $position)
                );
            }
        }

        $params = [
            ':username'          => $userName,
            ':authlevel'         => $authlevel,
            ':universe'          => $universe,
            ':registerAddress'   => !empty($userIpAddress) ? $userIpAddress : Session::getClientIp(),
            ':onlinetime'        => TIMESTAMP,
            ':registerTimestamp' => TIMESTAMP,
            ':nameLastChanged'   => 0,
        ];

        $sql = 'INSERT INTO %%USERS%% SET
            `username`		= :username,
            `authlevel`		= :authlevel,
            `universe`		= :universe,
            `ip_at_reg`		= :registerAddress,
            `onlinetime`	= :onlinetime,
            `register_time` = :registerTimestamp,
            `uctime`		= :nameLastChanged;';

        $db->insert($sql, $params);

        $userId = $db->lastInsertId();
        $timezone = $timezone ?? $config->timezone;
        $theme = $theme ?? DEFAULT_THEME;
        $params = [
            ':userId'            => $userId,
            ':language'          => $userLanguage,
            ':dpath'             => $theme,
            ':timezone'          => $timezone,
        ];

        $sql = 'INSERT INTO %%USERS_SETTINGS%% SET
            `user_id`		= :userId,
            `lang`			= :language,
            `dpath`			= :dpath,
            `timezone`		= :timezone;';

        $db->insert($sql, $params);

        if ($stb != null && $stb != "default") {
            $sql = 'UPDATE %%USERS_SETTINGS%% dest SET
                dest.`stb_small_ress`	= new.`stb_small_ress`,
                dest.`stb_med_ress`		= new.`stb_med_ress`,
                dest.`stb_big_ress`		= new.`stb_big_ress`,
                dest.`stb_small_time`	= new.`stb_small_time`,
                dest.`stb_med_time`		= new.`stb_med_time`,
                dest.`stb_big_time`		= new.`stb_big_time`,
                dest.`stb_enabled`      = new.`stb_enabled`
                FROM (
                    SELECT us.*
                    FROM %%USERS_SETTINGS%% us
                    INNER JOIN %%ACCOUNTS_TO_USERS%% atu ON us.`user_id` = atu.`user_id`
                    INNER JOIN %%USERS%% u ON us.`user_id` = u.`user_id`
                    WHERE atu.`account_id` = :accId and u.universe = :universe
                ) as new
                WHERE dest.user_id = :userId;';

            $db->update($sql, [
                ':accId' => $accountId,
                ':universe' => $stb,
            ]);
        }
        if ($colors != null && $colors != "default") {
            $sql = 'UPDATE %%USERS_SETTINGS%% dest SET
                dest.`colorMission2friend`	        = new.`colorMission2friend`,
                dest.`colorMission1Own`	            = new.`colorMission1Own`,
                dest.`colorMission2Own`	            = new.`colorMission2Own`,
                dest.`colorMission3Own`	            = new.`colorMission3Own`,
                dest.`colorMission4Own`	            = new.`colorMission4Own`,
                dest.`colorMission5Own`	            = new.`colorMission5Own`,
                dest.`colorMission6Own`	            = new.`colorMission6Own`,
                dest.`colorMission7Own`	            = new.`colorMission7Own`,
                dest.`colorMission7OwnReturn`	    = new.`colorMission7OwnReturn`,
                dest.`colorMission8Own`	            = new.`colorMission8Own`,
                dest.`colorMission9Own`	            = new.`colorMission9Own`,
                dest.`colorMission10Own`	        = new.`colorMission10Own`,
                dest.`colorMission15Own`	        = new.`colorMission15Own`,
                dest.`colorMission16Own`	        = new.`colorMission16Own`,
                dest.`colorMission17Own`	        = new.`colorMission17Own`,
                dest.`colorMissionReturnOwn`	    = new.`colorMissionReturnOwn`,
                dest.`colorMission1Foreign`	        = new.`colorMission1Foreign`,
                dest.`colorMission2Foreign`	        = new.`colorMission2Foreign`,
                dest.`colorMission3Foreign`	        = new.`colorMission3Foreign`,
                dest.`colorMission4Foreign`	        = new.`colorMission4Foreign`,
                dest.`colorMission5Foreign`	        = new.`colorMission5Foreign`,
                dest.`colorMission6Foreign`	        = new.`colorMission6Foreign`,
                dest.`colorMission7Foreign`	        = new.`colorMission7Foreign`,
                dest.`colorMission8Foreign`	        = new.`colorMission8Foreign`,
                dest.`colorMission9Foreign`	        = new.`colorMission9Foreign`,
                dest.`colorMission10Foreign`	    = new.`colorMission10Foreign`,
                dest.`colorMission15Foreign`	    = new.`colorMission15Foreign`,
                dest.`colorMission16Foreign`	    = new.`colorMission16Foreign`,
                dest.`colorMission17Foreign`	    = new.`colorMission17Foreign`,
                dest.`colorMissionReturnForeign`	= new.`colorMissionReturnForeign`,
                dest.`colorStaticTimer`	            = new.`colorStaticTimer`,
                dest.`colorPositive`	            = new.`colorPositive`,
                dest.`colorNegative`	            = new.`colorNegative`,
                dest.`colorNeutral`	                = new.`colorNeutral`,
                FROM (
                    SELECT us.*
                    FROM %%USERS_SETTINGS%% us
                    INNER JOIN %%ACCOUNTS_TO_USERS%% atu ON us.`user_id` = atu.`user_id`
                    INNER JOIN %%USERS%% u ON us.`user_id` = u.`user_id`
                    WHERE atu.`account_id` = :accId and u.universe = :universe
                ) as new
                WHERE dest.user_id = :userId;';

            $db->update($sql, [
                ':accId' => $accountId,
                ':universe' => $colors,
            ]);
        }
        if ($mission != null && $mission != "default") {
            $sql = 'UPDATE %%USERS_SETTINGS%% dest SET
                dest.`mission`	= new.`mission`
                FROM (
                    SELECT us.*
                    FROM %%USERS_SETTINGS%% us
                    INNER JOIN %%ACCOUNTS_TO_USERS%% atu ON us.`user_id` = atu.`user_id`
                    INNER JOIN %%USERS%% u ON us.`user_id` = u.`user_id`
                    WHERE atu.`account_id` = :accId and u.universe = :universe
                ) as new
                WHERE dest.user_id = :userId;';
            $db->update($sql, [
                ':accId' => $accountId,
                ':universe' => $mission,
            ]);
        }

        $planetId = self::createPlanet(
            $pos['galaxy'],
            $pos['system'],
            $pos['position'],
            $universe,
            $userId,
            $name,
            true,
            $authlevel
        );

        $currentUserAmount = $config->users_amount + 1;
        $config->users_amount = $currentUserAmount;

        $sql = 'UPDATE %%USERS%% SET
            `galaxy` = :galaxy,
            `system` = :system,
            `planet` = :position,
            `id_planet` = :planetId
            WHERE `id` = :userId;';

        $db->update($sql, [
            ':galaxy'   => $pos['galaxy'],
            ':system'   => $pos['system'],
            ':position' => $pos['position'],
            ':planetId' => $planetId,
            ':userId'   => $userId,
        ]);

        $sql = 'SELECT MAX(`total_rank`) as `rank` FROM %%STATPOINTS%% WHERE `universe` = :universe AND `stat_type` = :type;';
        $rank = $db->selectSingle($sql, [
            ':universe' => $universe,
            ':type'     => 1,
        ], 'rank');

        $sql = "INSERT INTO %%STATPOINTS%% SET
            `id_owner`   = :userId,
            `universe`   = :universe,
            `stat_type`  = :type,
            `tech_rank`  = :rank,
            `build_rank` = :rank,
            `defs_rank`	 = :rank,
            `fleet_rank` = :rank,
            `total_rank` = :rank;";

        $db->insert($sql, [
           ':universe'  => $universe,
           ':userId'    => $userId,
           ':type'      => 1,
           ':rank'      => $rank + 1,
        ]);

        $sql = 'INSERT INTO %%ADVANCED_STATS%% SET `userId` = :userId;';
        $db->insert($sql, [
            ':userId'    => $userId,
        ]);
        $sql = 'INSERT INTO %%ACCOUNTS_TO_USERS%% SET
            `account_id`    = :accId,
            user_id         = :userId,
            end_time        = 0;';
        $db->insert($sql, [
            ':userId'    => $userId,
            ':accId'    => $accountId,
        ]);
        $config->save();

        return [
            $userId,
            $planetId
        ];
    }

    public static function createPlanet(
        $galaxy,
        $system,
        $position,
        $universe,
        $userId,
        $name = null,
        $isHome = false,
        $authlevel = 0
    ) {
        $LNG =& Singleton()->LNG;

        if (self::checkPosition($universe, $galaxy, $system, $position) === false) {
            throw new Exception(
                sprintf("Try to create a planet at position: %s:%s:%s!", $galaxy, $system, $position)
            );
        }

        if (self::isPositionFree($universe, $galaxy, $system, $position) === false) {
            throw new Exception(
                sprintf("Position is not empty: %s:%s:%s!", $galaxy, $system, $position)
            );
        }
        $db = Database::get();

        $sql = 'SELECT count(`id`) as anz FROM %%PLANETS%% WHERE `id_owner` = :userId AND `planet_type` = 1';
        $planetArray = $db->selectSingle($sql, [
            ':userId' => $userId,
        ]);

        $planetData = [];
        require 'includes/PlanetData.php';

        $planetDataBackend = PlayerUtil::getPlanetDataPlanetConfig();

        $config = Config::get($universe);
        $positionBasedRessourceBonus = $config->planet_ressource_bonus;

        $metalBonusPercent = 0;
        $crystalBonusPercent = 0;
        $deuteriumBonusPercent = 0;

        $dataIndex = (int) ceil($position / ($config->max_planets / count($planetDataBackend)));

        if ($isHome) {
            $maxFields = $config->initial_fields;
            $maxTemperature = $config->initial_temp;
            if ($positionBasedRessourceBonus){
                $metalBonusPercent = 0;
                $crystalBonusPercent = 0;
                $deuteriumBonusPercent = 0;
            }
        } elseif (!empty($planetArray) && $planetArray['anz'] == 1) {
            if ($positionBasedRessourceBonus){
                $metalBonusPercent = $planetDataBackend[$dataIndex-1]['metalBonusPercent'];
                $crystalBonusPercent = $planetDataBackend[$dataIndex-1]['crystalBonusPercent'];
                $deuteriumBonusPercent = $planetDataBackend[$dataIndex-1]['deuteriumBonusPercent'];
            }
            $addToAvgSize = (int) floor(($planetDataBackend[$dataIndex-1]['fields_max'] - $planetDataBackend[$dataIndex-1]['fields_min']) / 2);
            $avgSize = $planetDataBackend[$dataIndex-1]['fields_min'] + $addToAvgSize;
            $maxFields = (int) floor($avgSize * $config->planet_size_factor);
            $addToAvgTemperature = (int) floor(($planetDataBackend[$dataIndex-1]['temp_max'] - $planetDataBackend[$dataIndex-1]['temp_min']) / 2);
            $maxTemperature = $planetDataBackend[$dataIndex-1]['temp_min'] + $addToAvgTemperature;
        } else {
            if ($positionBasedRessourceBonus){
                $metalBonusPercent = $planetDataBackend[$dataIndex-1]['metalBonusPercent'];
                $crystalBonusPercent = $planetDataBackend[$dataIndex-1]['crystalBonusPercent'];
                $deuteriumBonusPercent = $planetDataBackend[$dataIndex-1]['deuteriumBonusPercent'];
            }
            $maxFields = (int) floor(random_int($planetDataBackend[$dataIndex-1]['fields_min'], $planetDataBackend[$dataIndex-1]['fields_max']) * $config->planet_size_factor);
            $maxTemperature = random_int($planetDataBackend[$dataIndex-1]['temp_min'], $planetDataBackend[$dataIndex-1]['temp_max']);
        }

        require_once 'includes/classes/achievements/PlayerUtilAchievement.class.php';
        $sql = 'SELECT `galaxy` FROM %%USERS%% WHERE `id` = :userId;';
        $hpGala = $db->selectSingle($sql, [
            ':userId' => $userId,
        ], 'galaxy');
        PlayerUtilAchievement::checkPlayerUtilAchievements40($isHome, $hpGala, $userId, $galaxy, $config);
        $minTemperature = $maxTemperature - 40;

        $diameter = (int) floor(1000 * sqrt($maxFields));

        $sql = 'SELECT `image` FROM %%PLANETS%% WHERE `id_owner` = :userId; and `destruyed` = 0';
        $planetPicturesAchieved = $db->select($sql, [
            ':userId' => $userId,
        ]);

        $planetPictures = [];
        foreach ($planetPicturesAchieved as $image => $value) {
            array_push( $planetPictures, $value["image"]);
        }

        $enabledAllPlanetPictures = $config->all_planet_pictures;
        $maxImages = 0;
        do {
            if ($enabledAllPlanetPictures){
                $planetData[$dataIndex]['image'] = getAllPlanetPictures();
            }
            $imageNames = array_keys($planetData[$dataIndex]['image']);
            $imageNameType = $imageNames[array_rand($imageNames)];
            $imageName = $imageNameType;
            $imageName .= 'planet';
            $imageName .= $planetData[$dataIndex]['image'][$imageNameType] < 10 ? '0' : '';
            $imageName .= $planetData[$dataIndex]['image'][$imageNameType];
            $maxImages++;
        } while (in_array($imageName, $planetPictures) && ($maxImages < MAXPLANETPICTURECOUNT) && ($enabledAllPlanetPictures));

        if (empty($name)) {
            $name = $isHome ? $LNG['fcm_mainplanet'] : $LNG['fcp_colony'];
        }

        $params = [
            ':name'                     => $name,
            ':universe'                 => $universe,
            ':userId'                   => $userId,
            ':galaxy'                   => $galaxy,
            ':system'                   => $system,
            ':position'                 => $position,
            ':updateTimestamp'          => TIMESTAMP,
            ':type'                     => 1,
            ':imageName'                => $imageName,
            ':diameter'                 => $diameter,
            ':maxFields'                => $maxFields,
            ':minTemperature'           => $minTemperature,
            ':maxTemperature'           => $maxTemperature,
            ':metal_start'              => $config->metal_start,
            ':metalBonusPercent'        => $metalBonusPercent,
            ':crystal_start'            => $config->crystal_start,
            ':crystalBonusPercent'      => $crystalBonusPercent,
            ':deuterium_start'          => $config->deuterium_start,
            ':deuteriumBonusPercent'    => $deuteriumBonusPercent,
            ':creation_time'            => TIMESTAMP,
        ];

        $sql = 'INSERT INTO %%PLANETS%% SET
            `name`		                = :name,
            `universe`                  = :universe,
            `id_owner`                  = :userId,
            `galaxy`		            = :galaxy,
            `system`		            = :system,
            `planet`		            = :position,
            `last_update`               = :updateTimestamp,
            `planet_type`               = :type,
            `image`		                = :imageName,
            `diameter`                  = :diameter,
            `field_max`                 = :maxFields,
            `temp_min` 	                = :minTemperature,
            `temp_max` 	                = :maxTemperature,
            `metal`		                = :metal_start,
            `metal_bonus_percent`       = :metalBonusPercent,
            `crystal`		            = :crystal_start,
            `crystal_bonus_percent`     = :crystalBonusPercent,
            `deuterium`                 = :deuterium_start,
            `deuterium_bonus_percent`   = :deuteriumBonusPercent,
            `creation_time`             = :creation_time;';

        $db->insert($sql, $params);

        $planetId = $db->lastInsertId();

        return $planetId;
    }

    public static function createMoon(
        $universe,
        $galaxy,
        $system,
        $position,
        $userId,
        $chance,
        $diameter = null,
        $moonName = null
    ) {
        $LNG =& Singleton()->LNG;

        $db = Database::get();

        $sql = 'SELECT `id_luna`, `planet_type`, `id`, `name`, `temp_max`, `temp_min`
            FROM %%PLANETS%%
            WHERE `universe` = :universe
            AND `galaxy` = :galaxy
            AND `system` = :system
            AND `planet` = :position
            AND `planet_type` = :type;';

        $parentPlanet = $db->selectSingle($sql, [
            ':universe' => $universe,
            ':galaxy'   => $galaxy,
            ':system'   => $system,
            ':position' => $position,
            ':type'     => 1,
        ]);

        if ($parentPlanet['id_luna'] != 0) {
            return false;
        }

        $config = Config::get($universe);
        $moon_size_factor = $config->moon_size_factor;

        if (is_null($diameter)) {
            $diameter = floor(pow(random_int(10, 20) + 3 * $chance, 0.5) * 1000 * $moon_size_factor);
        }

        $maxTemperature = $parentPlanet['temp_max'] - random_int(10, 45);
        $minTemperature = $parentPlanet['temp_min'] - random_int(10, 45);

        if (empty($moonName)) {
            $moonName = $LNG['type_planet_3'] ?? 'Mond';
        }

        $sql = 'INSERT INTO %%PLANETS%% SET
            `name`				= :name,
            `id_owner`			= :owner,
            `universe`			= :universe,
            `galaxy`			= :galaxy,
            `system`			= :system,
            `planet`			= :planet,
            `last_update`		= :updateTimestamp,
            `planet_type`		= :type,
            `image`				= :image,
            `diameter`			= :diameter,
            `field_max`			= :fields,
            `temp_min`			= :minTemperature,
            `temp_max`			= :maxTemperature,
            `metal`				= :metal,
            `metal_perhour`		= :metPerHour,
            `crystal`			= :crystal,
            `crystal_perhour`	= :cryPerHour,
            `deuterium`			= :deuterium,
            `deuterium_perhour` = :deuPerHour;';

        $db->insert($sql, [
            ':name'             => $moonName,
            ':owner'            => $userId,
            ':universe'         => $universe,
            ':galaxy'           => $galaxy,
            ':system'           => $system,
            ':planet'           => $position,
            ':updateTimestamp'  => TIMESTAMP,
            ':type'             => 3,
            ':image'            => 'mond0'.rand(1,9),
            ':diameter'         => $diameter,
            ':fields'           => 1,
            ':minTemperature'   => $minTemperature,
            ':maxTemperature'   => $maxTemperature,
            ':metal'            => 0,
            ':metPerHour'       => 0,
            ':crystal'          => 0,
            ':cryPerHour'       => 0,
            ':deuterium'        => 0,
            ':deuPerHour'       => 0,
        ]);

        $moonId = $db->lastInsertId();

        $sql = 'UPDATE %%PLANETS%% SET `id_luna` = :moonId WHERE `id` = :planetId;';

        $db->update($sql, [
            ':moonId'   => $moonId,
            ':planetId' => $parentPlanet['id'],
        ]);

        return $moonId;
    }



    public static function removeFromAlliance($userData)
    {
        $db = Database::get();
        if (!empty($userData['ally_id'])) {
            $sql = 'SELECT `ally_owner`, `ally_members` FROM %%ALLIANCE%% WHERE `id` = :allianceId;';
            $allyData = $db->selectSingle($sql, [
                ':allianceId'   => $userData['ally_id']
            ]);

            if ($allyData['ally_members'] > 1) {
                if ($allyData['ally_owner'] == $userData['id']) { // Removed player is ally leader
                    // Find new leader by respecting rank settings
                    $sql = 'SELECT u.`id` FROM %%ALLIANCE_RANK%% r
                        JOIN %%USERS%% u ON u.`ally_rank_id` = r.`rankID`
                        WHERE r.`allianceID` = :allianceId
                          AND u.`onlinetime` < :onlinetime
                          AND ( r.`transfer` = 1 OR r.`admin` = 1 OR r.`kick` = 1 OR r.`diplomatic` = 1 OR r.`manageusers` = 1 OR r.`ranks` = 1 )
                          ORDER BY r.`transfer` DESC, r.`admin` DESC, r.`kick` DESC, r.`diplomatic` DESC, r.`manageusers` DESC, r.`ranks` DESC, u.`onlinetime` DESC
                          LIMIT 1;';
                    $newLeaders = $db->select($sql, [
                        ':allianceId'   => $userData['ally_id'],
                        ':onlinetime'   => TIMESTAMP - TIME_72_HOURS,
                    ]);

                    $newLeader = 0;
                    if (!empty($newLeaders)) { // New leader by rank found?
                        $newLeader = $newLeaders[0]['id'];
                    } else {
                        // Find new leader by onlinetime
                        $sql = 'SELECT u.`id` FROM %%ALLIANCE_RANK%% r
                            JOIN %%USERS%% u ON u.`ally_rank_id` = r.`rankID`
                            WHERE r.`allianceID` = :allianceId
                            ORDER BY u.`onlinetime` DESC;';
                        $newLeader = $db->selectSingle($sql, [
                            ':allianceId'   => $userData['ally_id'],
                        ], 'id');
                    }

                    if (!empty($newLeader)) { // New leader found?
                        // Update rank of new leader
                        $sql = 'UPDATE %%USERS%% SET `ally_rank_id` = 0 WHERE `id` = :UserID;';
                        $db->update($sql, [
                            ':UserID' => $newLeader,
                        ]);

                        // Update ally_owner to new leader and reduce member amount
                        $sql = 'UPDATE %%ALLIANCE%% SET `ally_owner` = :UserID, `ally_members` = `ally_members` - 1 WHERE `id` = :allianceId;';
                        $db->update($sql, [
                            ':allianceId'   => $userData['ally_id'],
                            ':UserID' => $newLeader,
                        ]);
                    }
                    else {
                        // No new leader found, just reduce member amount
                        $sql = 'UPDATE %%ALLIANCE%% SET `ally_members` = `ally_members` - 1 WHERE `id` = :allianceId;';
                        $db->update($sql, [
                            ':allianceId'   => $userData['ally_id']
                        ]);
                    }
                } else {
                    // Removed player is not leader, just reduce member amount
                    $sql = 'UPDATE %%ALLIANCE%% SET `ally_members` = `ally_members` - 1 WHERE `id` = :allianceId;';
                    $db->update($sql, [
                        ':allianceId'   => $userData['ally_id']
                    ]);
                }
            } else {
                // Delete alliance, since there are no members
                $sql = 'DELETE FROM %%ALLIANCE%% WHERE `id` = :allianceId;';
                $db->delete($sql, [
                    ':allianceId'   => $userData['ally_id']
                ]);

                $sql = 'DELETE FROM %%ALLIANCE_RANK%% WHERE `allianceID` = :allianceId;';
                $db->delete($sql, [
                    ':allianceId'   => $userData['ally_id']
                ]);

                $sql = 'DELETE FROM %%ALLIANCE_REQUEST%% WHERE `allianceID` = :allianceId;';
                $db->delete($sql, [
                    ':allianceId'   => $userData['ally_id']
                ]);

                $sql = 'DELETE FROM %%STATPOINTS%% WHERE `stat_type` = :type AND `id_owner` = :allianceId;';
                $db->delete($sql, [
                    ':allianceId'   => $userData['ally_id'],
                    ':type'         => 2
                ]);

                $sql = 'DELETE FROM %%STATPOINTS_HISTORY%% WHERE `stat_type` = :type AND `id_owner` = :allianceId;';
                $db->delete($sql, [
                    ':allianceId'   => $userData['ally_id'],
                    ':type'         => 2
                ]);

                $sql = 'UPDATE %%STATPOINTS%% SET `id_ally` = :resetId WHERE `id_ally` = :allianceId;';
                $db->update($sql, [
                    ':allianceId'   => $userData['ally_id'],
                    ':resetId'      => 0
                ]);
            }

            $sql = 'UPDATE %%USERS%% SET `ally_id` = 0, `ally_register_time` = 0, `ally_rank_id` = 0 WHERE `id` = :UserID;';
            $db->update($sql, [
                ':UserID' => $userData['id']
            ]);

            $sql = 'UPDATE %%STATPOINTS%% SET `id_ally` = :resetId WHERE `stat_type` = :type AND `id_owner` = :UserID;';
            $db->update($sql, [
                ':UserID'   => $userData['id'],
                ':resetId'  => 0,
                ':type'     => 1,
            ]);
        }

        $sql = 'DELETE FROM %%ALLIANCE_REQUEST%% WHERE `userID` = :userId;';
        $db->delete($sql, [
            ':userId'   => $userData['id']
        ]);
    }

    public static function removeFromBuddy($userId)
    {
        $db = Database::get();
        $sql = 'DELETE FROM %%BUDDY%% WHERE `owner` = :userId OR `sender` = :userId;';
        $db->delete($sql, [
            ':userId'   => $userId
        ]);
    }

    public static function deletePlayer($userId)
    {
        if (ROOT_USER == $userId) {
            // superuser can not be deleted.
            throw new Exception("Superuser #" . ROOT_USER . " can't be deleted!");
        }

        $db = Database::get();
        $sql = 'SELECT `id`, `universe`, `ally_id` FROM %%USERS%% WHERE `id` = :userId;';
        $userData = $db->selectSingle($sql, [
            ':userId'   => $userId
        ]);

        if (empty($userData)) {
            return false;
        }

        PlayerUtil::removeFromAlliance($userData);

        PlayerUtil::removeFromBuddy($userId);

        $sql = 'DELETE %%FLEETS%%, %%FLEETS_EVENT%%
            FROM %%FLEETS%% LEFT JOIN %%FLEETS_EVENT%% on `fleet_id` = fleetId
            WHERE `fleet_owner` = :userId;';
        $db->delete($sql, [
            ':userId'   => $userId
        ]);

        $sql = 'DELETE FROM %%MESSAGES%% WHERE `message_owner` = :userId;';
        $db->delete($sql, [
            ':userId'   => $userId
        ]);

        $sql = 'DELETE FROM %%NOTES%% WHERE `owner` = :userId;';
        $db->delete($sql, [
            ':userId'   => $userId
        ]);

        $sql = 'DELETE %%PLANETS%%, %%PLANET_WRECKFIELD%%
            FROM %%PLANETS%% LEFT JOIN %%PLANET_WRECKFIELD%% on `planetID` = id
            WHERE `id_owner` = :userId;';
        $db->delete($sql, [
            ':userId'   => $userId
        ]);

        $sql = 'DELETE FROM %%USERS_SETTINGS%% WHERE `user_id` = :userId;';
        $db->delete($sql, [
            ':userId'   => $userId
        ]);

        $sql = 'DELETE FROM %%USERS%% WHERE `id` = :userId;';
        $db->delete($sql, [
            ':userId'   => $userId
        ]);

        $sql = 'DELETE FROM %%STATPOINTS%% WHERE `stat_type` = :type AND `id_owner` = :userId;';
        $db->delete($sql, [
            ':userId'   => $userId,
            ':type'     => 1
        ]);

        $sql = 'DELETE FROM %%STATPOINTS_HISTORY%% WHERE `stat_type` = :type AND `id_owner` = :userId;';
        $db->delete($sql, [
            ':userId'   => $userId,
            ':type'     => 1
        ]);

        $sql = 'DELETE FROM %%ADVANCED_STATS%% WHERE `userId` = :userId;';
        $db->delete($sql, [
            ':userId'    => $userId,
        ]);

        $sql = 'DELETE FROM %%ADVANCED_STATS_HISTORY%% WHERE `userId` = :userId;';
        $db->delete($sql, [
            ':userId'    => $userId,
        ]);

        $fleetIds = $db->select('SELECT `fleet_id` FROM %%FLEETS%% WHERE `fleet_target_owner` = :userId;', [
            ':userId'   => $userId
        ]);

        foreach ($fleetIds as $fleetId) {
            FleetFunctions::sendFleetBack(['id' => $userId], $fleetId['fleet_id']);
        }

        $sql = 'DELETE FROM %%ACCOUNTS_TO_USERS%% WHERE `user_id` = :userId;';
        $db->delete($sql, [
            ':userId'   => $userId
        ]);

        /*
        $sql = 'UPDATE %%UNIVERSE%% SET `userAmount` = `userAmount` - 1 WHERE `universe` = :universe;';
        $db->update($sql, array(
            ':universe' => $userData['universe']
        ));

        Cache::get()->flush('universe');
        */

        return true;
    }

    /**
	 * Delete all Planets of players and recreate them according to the algorithm, currently selected.
	 *
	 * @param int	 		$universe       The id of the Universe
	 */
    public static function reshuffleAllPlanets(
        $universe
    ){
        $USER =& Singleton()->USER;
        $db = Database::get();
        $config = Config::get($universe);
        $sql = 'SELECT MAX(count) AS max FROM(SELECT count(*) AS count, `id_owner` FROM %%PLANETS%% WHERE `universe` = :uni GROUP BY `id_owner`) AS plani;';
        $maxPlanetAmount = $db->selectSingle($sql, [
            ':uni' => $universe
        ], 'max');
        $uniStatus = $config->uni_status;

        if ($maxPlanetAmount > 1 || $uniStatus != STATUS_CLOSED || $USER['authlevel'] != AUTH_ADM) {
            $result = "";
            $LNG =& Singleton()->LNG;
            if ($maxPlanetAmount > 1) {
                $result = $LNG['rp_planet_error'];
            } elseif ($uniStatus != STATUS_CLOSED) {
                $result = $LNG['rp_universe_error'];
            } elseif ($USER['authlevel'] != AUTH_ADM) {
                $result = $LNG['rp_rights_error'];
            }
            return $result;
        }
        $sql = 'UPDATE %%USERS%% SET `onlinetime` = :onlinetime WHERE `universe` = :universe;';
        $db->update($sql, [
            ':onlinetime' => TIMESTAMP,
            ':universe' => $universe
        ]);
        $sql = 'SELECT `id` FROM %%USERS%% WHERE `authlevel` = :auth AND `universe` = :universe ORDER BY `id`;';
        $userIds = $db->select($sql, [
            ':auth'      => AUTH_USR,
            ':universe' => $universe
        ], 'id');
        foreach ($userIds as $userId) {
            $sql = 'DELETE FROM %%PLANETS%% WHERE `id_owner` = :userId;';
            $db->delete($sql, [
                ':userId'   => $userId['id'],
            ]);
        }
        $config->last_galaxy_pos = 1;
        $config->last_system_pos = 1;
        foreach ($userIds as $userId) {
            if ($config->planet_creation == 1) {
                $pos = PlayerUtil::randomHP($universe);
            } elseif ($config->planet_creation == 2) {
                $rand_vs_block = rand(0, 1);
                switch ($rand_vs_block) {
                    case 0:
                        $pos = PlayerUtil::randomHP($universe);
                        break;
                    case 1:
                        $pos = PlayerUtil::blockHP($universe);
                        break;
                }
            } else {
                $pos = PlayerUtil::linearHP($universe, $config);
            }
            if (isset($universe, $pos['galaxy'], $pos['system'], $pos['position'])) {
                if (self::checkPosition($universe, $pos['galaxy'], $pos['system'], $pos['position']) === false) {
                    throw new Exception(
                        sprintf("Try to create a planet at position: %s:%s:%s!", $pos['galaxy'], $pos['system'], $pos['position'])
                    );
                }

                if (self::isPositionFree($universe, $pos['galaxy'], $pos['system'], $pos['position']) === false) {
                    throw new Exception(
                        sprintf("Position is not empty: %s:%s:%s!", $pos['galaxy'], $pos['system'], $pos['position'])
                    );
                }
            }
            $sql = 'SELECT `lang` FROM %%USERS_SETTINGS%% WHERE `user_id` = :userId;';
            $lang = $db->selectSingle($sql, [
                ':userId' => $userId['id'],
            ], 'lang');
            $LNG    = new Language($lang);
            Singleton()->LNG = $LNG;
            $LNG =& Singleton()->LNG;
            $LNG->includeData(['L18N', 'INGAME', 'TECH', 'CUSTOM']);
            $name = $LNG['fcm_mainplanet'];
            $planetId = self::createPlanet(
                $pos['galaxy'],
                $pos['system'],
                $pos['position'],
                $universe,
                $userId['id'],
                $name,
                true
            );
            $sql = 'UPDATE %%USERS%% SET
                `galaxy` = :galaxy,
                `system` = :system,
                `planet` = :position,
                `id_planet` = :planetId
                WHERE `id` = :userId;';

            $db->update($sql, [
                ':galaxy'   => $pos['galaxy'],
                ':system'   => $pos['system'],
                ':position' => $pos['position'],
                ':planetId' => $planetId,
                ':userId'   => $userId['id'],
            ]);
        }
        return true;
    }
    public static function deletePlanet($planetId)
    {
        $db = Database::get();
        $sql = 'SELECT `id_owner`, `planet_type`, `id_luna` FROM %%PLANETS%%
                    WHERE `id` = :planetId AND `id` NOT IN (SELECT `id_planet` FROM %%USERS%%);';
        $planetData = $db->selectSingle($sql, [
            ':planetId' => $planetId
        ]);

        if (empty($planetData)) {
            throw new Exception("Can not found planet #" . $planetId . "!");
        }

        $sql = 'SELECT `fleet_id` FROM %%FLEETS%% WHERE `fleet_end_id` = :planetId
                    OR (`fleet_end_type` = 3 AND `fleet_end_id` = :moondId);';
        $fleetIds = $db->select($sql, [
            ':planetId' => $planetId,
            ':moondId'  => $planetData['id_luna']
        ]);

        foreach ($fleetIds as $fleetId) {
            FleetFunctions::sendFleetBack(['id' => $planetData['id_owner']], $fleetId['fleet_id']);
        }

        if ($planetData['planet_type'] == 3) {
            $sql = 'DELETE FROM %%PLANETS%% WHERE id = :planetId;';
            $db->delete($sql, [
                ':planetId' => $planetId
            ]);

            $sql = 'UPDATE %%PLANETS%% SET `id_luna` = :resetId WHERE `id_luna` = :planetId;';
            $db->update($sql, [
                ':resetId'  => 0,
                ':planetId' => $planetId
            ]);
        } else {
            require_once 'includes/classes/achievements/PlayerUtilAchievement.class.php';
            PlayerUtilAchievement::checkPlayerUtilAchievementsDeletion($planetData, $planetId);
            $sql = 'UPDATE %%PLANETS%% SET `destruyed` = :time WHERE `id` = :planetID;';
            $db->update($sql, [
                ':time'   => TIMESTAMP + 86400,
                ':planetID' => $planetId,
            ]);
            $sql = 'DELETE FROM %%PLANETS%% WHERE `id` = :lunaID;';
            $db->delete($sql, [
                ':lunaID' => $planetData['id_luna']
            ]);
            $sql = 'DELETE FROM %%PLANET_WRECKFIELD%% WHERE `planetID` = :planetID;';
            $db->delete($sql, [
                ':planetID' => $planetId,
            ]);
        }
        $session	= Session::load();
        $session->planetId      = 0;
        return true;
    }

    private static function getAstroTech($USER)
    {
        $resource =& Singleton()->resource;

        $astroTech = $USER[$resource[124]];

        $CurrentQueue = !empty($USER['b_tech_queue']) ? unserialize($USER['b_tech_queue']) : [];
        if (!empty($CurrentQueue) && count($CurrentQueue) > 0) {
            foreach ($CurrentQueue as $ListIDArray) {
                if ($ListIDArray[0] == 124 && $ListIDArray[3] <= TIMESTAMP) {
                    $astroTech++;
                }
            }
        }

        return $astroTech;
    }

    public static function maxPlanetCount($USER)
    {
        $resource =& Singleton()->resource;
        $config = Config::get($USER['universe']);

        $planetPerTech = $config->max_additional_planets;

        if ($config->max_initial_planets == 0) {
            $planetPerTech = 999;
        }

        if ($config->max_initial_planets == 0) {
            $planetPerBonus = 999;
        }

        // http://owiki.de/index.php/Astrophysik#.C3.9Cbersicht
        return (int) ceil(
            $config->max_initial_planets + min(
                $planetPerTech,
                PlayerUtil::getAstroTech($USER) * $config->planets_per_tech
            )
        );
    }

    public static function currentPlanetCount($USER)
    {
        $db = Database::get();
        $sql = 'SELECT COUNT(*) as state
            FROM %%PLANETS%%
            WHERE `id_owner`	= :userId
            AND `planet_type`	= :type
            AND `destruyed`		= :destroyed;';

        $currentPlanetCount = $db->selectSingle($sql, [
            ':userId'       => $USER['id'],
            ':type'         => 1,
            ':destroyed'    => 0,
        ], 'state');

        return $currentPlanetCount;
    }

    public static function allowPlanetPosition($position, $USER = null, $universe = ROOT_UNI)
    {
        // http://owiki.de/index.php/Astrophysik#.C3.9Cbersicht

        if (isset($USER)) {
            $config = Config::get($USER['universe']);
            $astroTech = PlayerUtil::getAstroTech($USER);
        } else {
            $config = Config::get($universe);
            $astroTech = 1;
        }

        switch ($position) {
            case 0:
                return false;
                break;
            case 1:
            case ($config->max_planets):
                return $astroTech >= 8;
                break;
            case 2:
            case ($config->max_planets - 1):
                return $astroTech >= 6;
                break;
            case 3:
            case ($config->max_planets - 2):
                return $astroTech >= 4;
                break;
            default:
                return $astroTech >= 1 && $config->max_planets >= $position;
                break;
        }
    }

    public static function sendMessage(
        $userId,
        $senderId,
        $senderName,
        $messageType,
        $subject,
        $text,
        $time,
        $parentID = null,
        $unread = 1,
        $universe = null
    ) {
        if (is_null($universe)) {
            $universe = Universe::current();
        }

        $db = Database::get();

        $sql = 'INSERT INTO %%MESSAGES%% SET
            `message_owner`     = :userId,
            `message_sender`    = :sender,
            `message_time`      = :time,
            `message_type`      = :type,
            `message_from`      = :from,
            `message_subject`   = :subject,
            `message_text`      = :text,
            `message_unread`    = :unread,
            `message_universe`  = :universe;';

        $db->insert($sql, [
            ':userId'   => $userId,
            ':sender'   => $senderId,
            ':time'     => $time,
            ':type'     => $messageType,
            ':from'     => $senderName,
            ':subject'  => $subject,
            ':text'     => $text,
            ':unread'   => $unread,
            ':universe' => $universe,
        ]);

        if ($messageType === 1) {
            $sql = 'SELECT `username`, `discord_id`, `discord_hook` FROM %%USERS%%
                WHERE `universe` = :universe AND `id` = :userID AND `authlevel` > 0;';
            $admin = $db->selectSingle($sql, [
                ':universe' => $universe,
                ':userID' => $userId,
            ]);

            if (!empty($admin['discord_hook'])) {
                $title = '';
                if (!empty($admin['discord_id'])) {
                    $title = '<@' . $admin['discord_id'] . '> hat eine neue Ingame-Nachricht erhalten.';
                } else {
                    $title = '<@' . $admin['username'] . '> hat eine neue Ingame-Nachricht erhalten.';
                }

                $content = [
                    'Universum' => $universe,
                    'Absender' => $senderId . ' - ' . $senderName,
                    'Betreff' => $subject,
                ];
                require_once 'includes/classes/class.Discord.php';
                Discord::sendMessage($admin['discord_hook'], $title, $content);
            }
        }

        require_once 'includes/classes/achievements/PlayerUtilAchievement.class.php';
        PlayerUtilAchievement::checkPlayerUtilAchievements36($userId, $text);
    }

    public static function clearPlanets($USER)
    {
        $reslist =& Singleton()->reslist;
        $resource =& Singleton()->resource;

        $db = Database::get();

        $params = [];
        $params[] = $resource[RESOURCE_METAL] . ' = 0';
        $params[] = $resource[RESOURCE_CRYSTAL] . ' = 0';
        $params[] = $resource[RESOURCE_DEUT] . ' = 0';

        foreach ($reslist['fleet'] as $element) {
            $params[] = $resource[$element] . ' = 0';
        }

        foreach ($reslist['defense'] as $element) {
            $params[] = $resource[$element] . ' = 0';
        }

        $sql = 'UPDATE %%PLANETS%% SET `b_hangar` = 0, `b_hangar_id` = "", ' . implode(", ", $params) . ' WHERE `id_owner` = :owner;';
        $db->update($sql, [':owner' => $USER['id']]);
    }

    public static function disable_vmode(&$USER, &$PLANET = null)
    {
        $db = Database::get();

        $sql = 'SELECT `urlaubs_start` FROM %%USERS%% WHERE `id` = :userID;';
        $umode_start = $db->selectSingle($sql, [
            ':userID'   => $USER['id'],
        ], 'urlaubs_start');
        $umode_delta = TIMESTAMP - $umode_start;
        if ($USER['urlaubs_until'] <= TIMESTAMP) {
            if (!empty($USER['b_tech']) && !empty($USER['b_tech_queue'])) {
                $CurrentQueue       = unserialize($USER['b_tech_queue']);
                foreach ($CurrentQueue as &$TechArray) {
                    $TechArray[3] += $umode_delta;
                }
                $USER['b_tech_queue'] = serialize($CurrentQueue);
                unset($CurrentQueue);
                $sql = 'UPDATE %%USERS%% SET
                    `b_tech` = `b_tech` + :umode_delta,
                    `b_tech_queue` = :b_tech_queue
                    WHERE `id` = :userID;';
                $db->update($sql, [
                    ':umode_delta' => $umode_delta,
                    ':userID'   => $USER['id'],
                    ':b_tech_queue' => $USER['b_tech_queue'],
                ]);
                $USER['b_tech'] = $USER['b_tech'] + $umode_delta;
            }
            if (isset($PLANET) && !empty($PLANET['b_building']) && !empty($PLANET['b_building_id'])) {
                $PLANET['b_building'] = $PLANET['b_building'] + $umode_delta;
                $CurrentQueue       = unserialize($PLANET['b_building_id']);
                foreach ($CurrentQueue as &$BuildArray) {
                    $BuildArray[3] += $umode_delta;
                }
                $PLANET['b_building_id'] = serialize($CurrentQueue);
                unset($CurrentQueue);
            }

            $sql = 'SELECT `id`, `b_building`, `b_building_id` FROM %%PLANETS%% WHERE `id_owner` = :userID AND `destruyed` = 0;';
            $planets = $db->select($sql, [
                ':userID'   => $USER['id'],
            ]);

            foreach ($planets as $CPLANET) {
                if (!empty($CPLANET['b_building']) && !empty($CPLANET['b_building_id'])) {
                    $CPLANET['b_building'] = $CPLANET['b_building'] + $umode_delta;
                    $CurrentQueue = unserialize($CPLANET['b_building_id']);
                    foreach ($CurrentQueue as &$BuildArray) {
                        $BuildArray[3] += $umode_delta;
                    }
                    $CPLANET['b_building_id'] = serialize($CurrentQueue);
                    unset($CurrentQueue);
                }

                $sql = 'UPDATE %%PLANETS%% SET
                    `b_building` = :building,
                    `b_building_id` = :current_queue,
                    `last_update` = :timestamp,
                    `metal_mine_porcent` = "10",
                    `crystal_mine_porcent` = "10",
                    `deuterium_sintetizer_porcent` = "10",
                    `solar_plant_porcent` = "10",
                    `fusion_plant_porcent` = "10",
                    `solar_satelit_porcent` = "10"
                    WHERE `id` = :planetID;';
                $db->update($sql, [
                    ':planetID' => $CPLANET['id'],
                    ':building' => $CPLANET['b_building'],
                    ':current_queue' => $CPLANET['b_building_id'],
                    ':timestamp' => TIMESTAMP,
                ]);

                $sql = 'UPDATE %%PLANET_WRECKFIELD%% SET
                    `created` = `created` + :umode_delta,
                    `repair_order_start` =`repair_order_start` + :umode_delta,
                    `repair_order_end` = `repair_order_end` + :umode_delta
                    WHERE `planetID` = :planetID;';
                $db->update($sql, [
                    ':umode_delta' => $umode_delta,
                    ':planetID' => $CPLANET['id'],
                ]);

                unset($CPLANET);
            }

            $sql = 'UPDATE %%USERS%% SET
                `urlaubs_modus` = 0,
                `urlaubs_until` = 0,
                `urlaubs_start` = 0
                WHERE `id` = :userID;';
            $db->update($sql, [':userID'   => $USER['id']]);

            $USER['urlaubs_modus'] = 0;
            $USER['urlaubs_until'] = 0;
            $USER['urlaubs_start'] = 0;

            $sql = "SELECT * FROM %%PLANETS%% WHERE id_owner = :id;";
            $PlanetsRAW = $db->select($sql, [
                ':id' => $USER['id'],
            ]);

            $PlanetRess = new ResourceUpdate();
            if (isset($PLANET)) {
                $PLANET['last_update'] = TIMESTAMP;
                $PLANET['eco_hash'] = '';
                list($USER, $PLANET) = $PlanetRess->CalcResource($USER, $PLANET, true);
                require_once 'includes/classes/achievements/PlayerUtilAchievement.class.php';
                PlayerUtilAchievement::checkPlayerUtilAchievements33($USER['id']);
            }

            foreach ($PlanetsRAW as $CPLANET) {
                $CPLANET['eco_hash'] = '';
                list($USER, $CPLANET) = $PlanetRess->CalcResource($USER, $CPLANET, true);
            }
        }
    }

    public static function enable_vmode(&$USER, &$PLANET = null)
    {
        $db = Database::get();

        $sql = 'UPDATE %%USERS%% SET `urlaubs_modus` = 1, `urlaubs_until` = :time, `urlaubs_start` = :startTime
            WHERE `id` = :userID';
        $db->update($sql, [
            ':userID'       => $USER['id'],
            ':time'         => (TIMESTAMP + Config::get()->vmode_min_time),
            ':startTime'    => TIMESTAMP,
        ]);

        $sql = 'UPDATE %%PLANETS%% SET `energy_used` = 0, `energy` = 0, `metal_mine_porcent` = "0",
            `crystal_mine_porcent` = "0", `deuterium_sintetizer_porcent` = "0", `solar_plant_porcent` = "0",
            `fusion_plant_porcent` = "0", `solar_satelit_porcent` = "0", `metal_perhour` = 0,
            `crystal_perhour` = 0, `deuterium_perhour` = 0 WHERE `id_owner` = :userID;';
        $db->update($sql, [
            ':userID'   => $USER['id'],
        ]);

        $sql = 'SELECT `fleet_id`, `fleet_owner` FROM %%FLEETS%%
            WHERE `fleet_universe` = :universe
            AND `fleet_mission` = :fleet_mision
            AND `fleet_target_owner` = :id;';
        $FleetsRAW = $db->select($sql, [
            ':universe'     => Universe::current(),
            ':fleet_mision' => MISSION_HOLD,
            ':id'           => $USER['id'],
        ]);
        foreach ($FleetsRAW as $Fleet) {
            $attacker['id'] = $Fleet['fleet_owner'];
            FleetFunctions::SendFleetBack($attacker, $Fleet['fleet_id']);
        }
        if (isset($PLANET)) {
            $PLANET['energy_used'] = '0';
            $PLANET['energy'] = '0';
            $PLANET['metal_mine_porcent'] = '0';
            $PLANET['crystal_mine_porcent'] = '0';
            $PLANET['deuterium_sintetizer_porcent'] = '0';
            $PLANET['solar_plant_porcent'] = '0';
            $PLANET['fusion_plant_porcent'] = '0';
            $PLANET['solar_satelit_porcent'] = '0';
            $PLANET['metal_perhour'] = '0';
            $PLANET['crystal_perhour'] = '0';
            $PLANET['deuterium_perhour'] = '0';
            require_once 'includes/classes/achievements/PlayerUtilAchievement.class.php';
            PlayerUtilAchievement::checkPlayerUtilAchievements31($USER['id']);
        }
    }

    public static function player_colors($USER = null)
    {
        if (!isset($USER)) {
            return [
                'colorMission2friend' => '#ff00ff',

                'colorMission1Own' => '#66cc33',
                'colorMission2Own' => '#339966',
                'colorMission3Own' => '#5bf1c2',
                'colorMission4Own' => '#cf79de',
                'colorMission5Own' => '#80a0c0',
                'colorMission6Own' => '#ffcc66',
                'colorMission7Own' => '#c1c1c1',
                'colorMission7OwnReturn' => '#cf79de',
                'colorMission8Own' => '#ceff68',
                'colorMission9Own' => '#ffff99',
                'colorMission10Own' => '#ffcc66',
                'colorMission15Own' => '#5bf1c2',
                'colorMission16Own' => '#5bf1c2',
                'colorMission17Own' => '#5bf1c2',

                'colorMissionReturnOwn' => '#6e8eea',

                'colorMission1Foreign' => '#ff0000',
                'colorMission2Foreign' => '#aa0000',
                'colorMission3Foreign' => '#00ff00',
                'colorMission4Foreign' => '#ad57bc',
                'colorMission5Foreign' => '#3399cc',
                'colorMission6Foreign' => '#ff6600',
                'colorMission7Foreign' => '#00ff00',
                'colorMission8Foreign' => '#acdd46',
                'colorMission9Foreign' => '#dddd77',
                'colorMission10Foreign' => '#ff6600',
                'colorMission15Foreign' => '#39d0a0',
                'colorMission16Foreign' => '#39d0a0',
                'colorMission17Foreign' => '#39d0a0',

                'colorMissionReturnForeign' => '#6e8eea',

                'colorStaticTimer' => '#ffff00',
            ];
        }

        if (isset($USER['colors'])) {
            return $USER['colors'];
        }

        return [
            'colorMission2friend' => $USER['colorMission2friend'],

            'colorMission1Own' => $USER['colorMission1Own'],
            'colorMission2Own' => $USER['colorMission2Own'],
            'colorMission3Own' => $USER['colorMission3Own'],
            'colorMission4Own' => $USER['colorMission4Own'],
            'colorMission5Own' => $USER['colorMission5Own'],
            'colorMission6Own' => $USER['colorMission6Own'],
            'colorMission7Own' => $USER['colorMission7Own'],
            'colorMission7OwnReturn' => $USER['colorMission7OwnReturn'],
            'colorMission8Own' => $USER['colorMission8Own'],
            'colorMission9Own' => $USER['colorMission9Own'],
            'colorMission10Own' => $USER['colorMission10Own'],
            'colorMission15Own' => $USER['colorMission15Own'],
            'colorMission16Own' => $USER['colorMission16Own'],
            'colorMission17Own' => $USER['colorMission17Own'],

            'colorMissionReturnOwn' => $USER['colorMissionReturnOwn'],

            'colorMission1Foreign' => $USER['colorMission1Foreign'],
            'colorMission2Foreign' => $USER['colorMission2Foreign'],
            'colorMission3Foreign' => $USER['colorMission3Foreign'],
            'colorMission4Foreign' => $USER['colorMission4Foreign'],
            'colorMission5Foreign' => $USER['colorMission5Foreign'],
            'colorMission6Foreign' => $USER['colorMission6Foreign'],
            'colorMission7Foreign' => $USER['colorMission7Foreign'],
            'colorMission8Foreign' => $USER['colorMission8Foreign'],
            'colorMission9Foreign' => $USER['colorMission9Foreign'],
            'colorMission10Foreign' => $USER['colorMission10Foreign'],
            'colorMission15Foreign' => $USER['colorMission15Foreign'],
            'colorMission16Foreign' => $USER['colorMission16Foreign'],
            'colorMission17Foreign' => $USER['colorMission17Foreign'],

            'colorMissionReturnForeign' => $USER['colorMissionReturnForeign'],

            'colorStaticTimer' => $USER['colorStaticTimer'],
        ];
    }

    public static function player_stb_settings($USER = null)
    {
        if (!isset($USER)) {
            return [
                'stb_small_ress'    => 500,
                'stb_med_ress'      => 3000,
                'stb_big_ress'      => 7000,
                'stb_small_time'    => 1,
                'stb_med_time'      => 2,
                'stb_big_time'      => 3,
                'stb_enabled'       => 0,
            ];
        }

        if (isset($USER['stb_settings'])) {
            return $USER['stb_settings'];
        }

        return [
            'stb_small_ress'    => $USER['stb_small_ress'],
            'stb_med_ress'      => $USER['stb_med_ress'],
            'stb_big_ress'      => $USER['stb_big_ress'],
            'stb_small_time'    => $USER['stb_small_time'],
            'stb_med_time'      => $USER['stb_med_time'],
            'stb_big_time'      => $USER['stb_big_time'],
            'stb_enabled'       => $USER['stb_enabled'],
        ];
    }

    public static function player_signal_colors($USER = null)
    {
        if (!isset($USER)) {
            return [
                'colorPositive' => '#00ff00',
                'colorNegative' => '#ff0000',
                'colorNeutral' => '#ffd600',
            ];
        }

        if (isset($USER['signalColors'])) {
            return $USER['signalColors'];
        }

        return [
            'colorPositive' => $USER['colorPositive'],
            'colorNegative' => $USER['colorNegative'],
            'colorNeutral' => $USER['colorNeutral'],
        ];
    }

    public static function get_buddy_status($userId, $targetUserId)
    {
        $db = Database::get();

        $sql = 'SELECT id FROM %%BUDDY%% WHERE ((`sender` = :userID AND `owner` = :targetID) OR (`sender` = :targetID AND `owner` = :userID)) AND id NOT IN (SELECT id FROM %%BUDDY_REQUEST%%);';
        $buddy = $db->selectSingle($sql, [
            ':userID'       => $userId,
            ':targetID'     => $targetUserId,
        ]);
        if (empty($buddy)) {
            return false;
        }
        return true;
    }

    public static function get_block_status($userId, $targetUserId)
    {
        $db = Database::get();

        $sql = 'SELECT `block_dm`, `block_trade` FROM %%USERS_BLOCKLIST%% WHERE `blocked_user` = :userID AND `blocking_user` = :targetID;';
        $blocked = $db->selectSingle($sql, [
            ':userID'       => $userId,
            ':targetID'     => $targetUserId,
        ]);
        if (empty($blocked)) {
            return [
                'block_dm' => 0,
                'block_trade' => 0,
            ];
        }
        return $blocked;
    }

    public static function get_authlevel($userId)
    {
        $db = Database::get();

        $sql = 'SELECT `authlevel` FROM %%USERS%% WHERE `id` = :userID;';
        $authlevel = $db->selectSingle($sql, [
            ':userID'       => $userId,
        ], 'authlevel');

        return $authlevel;
    }

    public static function get_account_id($userId)
    {
        $db = Database::get();

        $sql = 'SELECT `account_id` FROM %%ACCOUNTS_TO_USERS%% WHERE `user_id` = :userID;';
        $accountId = $db->selectSingle($sql, [
            ':userID'       => $userId,
        ], 'account_id');
        if ($accountId === false) {
            $accountId = 0;
        }
        return $accountId;
    }

    /**
	 * Calculate and update honorpoint changes for 1 vs 1 fights
	 *
     * honorable if:
     * Attacker points <= Target points AND (50% Attacker military points <= Target military points OR Attacker military points < Target military points OR Target is Bandit(Low 30%))
     * Attacker points > Target points AND (50% Attacker military points < Target military points OR < 10 military points difference)
     *
     * dishonorable if:
     * 50% Attacker military points > Target military points AND > 10 military points difference AND Target is better than Banditlord(Low 20%)
     *
     * neutral if:
     * Attacker is in the same alliance as the target
     * Attacker is a buddy of the target
     * Defender is inactive
     * any case not covered by the above
     *
	 * @param int           $attacker               Id of attacker
	 * @param int           $defender               ID of defender
	 * @param int           $attackerUnitsLost      Units destroyed by the defender
     * @param int           $defenderUnitsLost      Units destroyed by the attacker
	 */
    public static function calculate_honorpoints_single($attacker, $defender, $attackerUnitsLost, $defenderUnitsLost)
    {
        $honorable = false;
        $dishonorable = false;
        $db = Database::get();
        $sql = 'SELECT u.`id`, s.`total_points`, s.`fleet_points`, s.`defs_points`, u.`honorpoints`, u.`ally_id`, u.`onlinetime`
        FROM %%USERS%% u
        INNER JOIN %%STATPOINTS%% s ON s.id_owner = u.id
        WHERE id = :id AND s.stat_type = 1;';

        $attackerUser = $db->selectSingle($sql, [
            ':id' => $attacker,
        ]);
        $defenderUser = $db->selectSingle($sql, [
            ':id' => $defender,
        ]);
        $sql = "SELECT count(*) as anz FROM %%BUDDY%% WHERE (sender = :id AND owner = :targetPlayerId) OR (sender = :targetPlayerId AND owner = :id);";
        $buddy = $db->selectSingle($sql, [
            ':id' => $attackerUser['id'],
            ':targetPlayerId' => $defenderUser['id'],
        ]);

        $honorDataTop = PlayerUtil::get_honor_percent("low");

        if (
            !(TIMESTAMP - INACTIVE > $defenderUser['onlinetime'])
            && !($attackerUser['ally_id'] == $defenderUser['ally_id'] && $attackerUser['ally_id'] != 0)
            && !($buddy['anz'] > 0)
        ) {
            if (
                $attackerUser['total_points'] <= $defenderUser['total_points']
                && (($attackerUser['fleet_points'] + $attackerUser['defs_points']) <= (($defenderUser['fleet_points'] + $defenderUser['defs_points']) * 2)
                || ($defenderUser['honorpoints'] <= $honorDataTop['top30']['honorpoints'] && $defenderUser['honorpoints'] <= -500))
            ) {
                $honorable = true;
            } else if (
                $attackerUser['total_points'] > $defenderUser['total_points']
                && (($attackerUser['fleet_points'] + $attackerUser['defs_points']) <= (($defenderUser['fleet_points'] + $defenderUser['defs_points']) * 2) || ($attackerUser['fleet_points'] + $attackerUser['defs_points']) - ($defenderUser['fleet_points'] + $defenderUser['defs_points']) <= 10)) {
                $honorable = true;
            } else if (
                ($attackerUser['fleet_points'] + $attackerUser['defs_points']) > (($defenderUser['fleet_points'] + $defenderUser['defs_points']) * 2)
                && ($attackerUser['fleet_points'] + $attackerUser['defs_points']) - ($defenderUser['fleet_points'] + $defenderUser['defs_points']) > 10
                && $defenderUser['honorpoints'] >= $honorDataTop['top30']['honorpoints']
            ) {
                $dishonorable = true;
            }
        }

        $sql = 'UPDATE %%USERS%% SET `honorpoints` = `honorpoints` + :honorpoints WHERE `id` = :userID;';
        if($honorable) {
            $attackerHonorpointsChange = floor(pow($defenderUnitsLost, 0.9) / 1000);
            $defenderHonorpointsChange = floor(pow($attackerUnitsLost, 0.9) / 1000);
            $db->update($sql, [
                ':honorpoints'  => $defenderHonorpointsChange,
                ':userID'       => $defenderUser['id'],
            ]);

            $sqlfights = 'UPDATE %%ADVANCED_STATS%% SET `honorable_fights` = `honorable_fights` + 1 WHERE `userId` = :userID;';
            $db->update($sqlfights, [':userID' => $attacker]);
            $db->update($sqlfights, [':userID' => $defender]);
        } else if($dishonorable) {
            $attackerHonorpointsChange = 0 - floor(pow($defenderUnitsLost, 0.9) / 1000);
            $sqlfights = 'UPDATE %%ADVANCED_STATS%% SET `dishonorable_fights` = `dishonorable_fights` + 1 WHERE `userId` = :userID;';
            $db->update($sqlfights, [':userID' => $attacker]);
        } else {
            return;
        }

        $db->update($sql, [
            ':honorpoints'  => $attackerHonorpointsChange,
            ':userID'       => $attackerUser['id'],
        ]);
    }

    /**
     * Calculate and update the change in honorpoints for multiple attackers and defenders(ACS)
     * Points get distributed proportional to the ships send(points)
     * Player needs to add more than 1% of the total points to get honorpoints
     *
     * honorable for attackers if:
     * sum of defender military points >= 50% of the sum of attacker military points OR defender is Bandit(Low 30%)
     *
     * honorable for defenders if:
     * sum of defender military points < 50% of the sum of attacker military points OR attacker is Bandit(Low 30%)
     *
     * @param array $attackers
     * @param array $defenders
     * @param int $attackerUnitsLost
     * @param int $defenderUnitsLost
     */
    public static function calculate_honorpoints_multi($attackers, $defenders, $attackerUnitsLost, $defenderUnitsLost)
    {
        $pricelist =& Singleton()->pricelist;
        $db = Database::get();

        $honorDataTop = PlayerUtil::get_honor_percent("low");
        $attackerHasBandit = false;
        $defenderHasBandit = false;
        $sumMilitaryPointsAttackers = 0;
        $sumMilitaryPointsDefenders = 0;

        $sql = 'SELECT u.id, `fleet_points`, `defs_points`, u.`honorpoints`
            FROM %%USERS%% u
            INNER JOIN %%STATPOINTS%% s ON s.id_owner = u.id
            WHERE u.`id` = :id AND s.stat_type = 1;';
        foreach ($attackers as $attacker) {
            $attackerData = $db->selectSingle($sql, [
                ':id' => $attacker['player']['id'],
            ]);
            $sumMilitaryPointsAttackers += $attackerData['fleet_points'] + $attackerData['defs_points'];
            if ($attackerData['honorpoints'] <= $honorDataTop['top30']['honorpoints'] && $attackerData['honorpoints'] <= -500) {
                $attackerHasBandit = true;
            }
        }

        foreach ($defenders as $defender) {
            $defenderData = $db->selectSingle($sql, [
                ':id' => $defender['player']['id'],
            ]);
            $sumMilitaryPointsDefenders += $defenderData['fleet_points'] + $defenderData['defs_points'];
            if ($defenderData['honorpoints'] <= $honorDataTop['top30']['honorpoints'] && $defenderData['honorpoints'] <= -500) {
                $defenderHasBandit = true;
            }
        }

        if($sumMilitaryPointsDefenders >= ($sumMilitaryPointsAttackers * 0.5) || $defenderHasBandit) {
            $attackerHonorpointsChange = floor(pow($defenderUnitsLost, 0.9) / 1000);
        } else {
            $attackerHonorpointsChange = 0 - floor(pow($defenderUnitsLost, 0.9) / 1000);
        }

        if($sumMilitaryPointsDefenders < ($sumMilitaryPointsDefenders * 0.5) || $attackerHasBandit) {
            $defenderHonorpointsChange = floor(pow($attackerUnitsLost, 0.9) / 1000);
        } else {
            $defenderHonorpointsChange = 0 - floor(pow($attackerUnitsLost, 0.9) / 1000);
        }

        $attackerFullpoints = 0;
        $defenderFullpoints = 0;
        foreach ($attackers as $FleetID => $player) {
            $playerData[$player['player']['id']] = [];
            foreach ($player['unit'] as $ShipID => $Amount) {
                $fullCost = $pricelist[$ShipID]['cost'][901] + $pricelist[$ShipID]['cost'][902] + $pricelist[$ShipID]['cost'][903];
                if(isset($playerData[$player['player']['id']]['fightpoints'])) {
                    $playerData[$player['player']['id']]['fightpoints'] += $Amount * $fullCost;
                } else {
                    $playerData[$player['player']['id']]['fightpoints'] = $Amount * $fullCost;
                }
                $attackerFullpoints += $Amount * $fullCost;
            }
            if($attackerHonorpointsChange > 0) {
                $sqlfights = 'UPDATE %%ADVANCED_STATS%% SET `honorable_fights` = `honorable_fights` + 1 WHERE `userId` = :userID;';
                $db->update($sqlfights, [':userID' => $player['player']['id']]);
            } else if($attackerHonorpointsChange < 0) {
                $sqlfights = 'UPDATE %%ADVANCED_STATS%% SET `dishonorable_fights` = `dishonorable_fights` + 1 WHERE `userId` = :userID;';
                $db->update($sqlfights, [':userID' => $player['player']['id']]);
            }
        }

        foreach ($playerData as $playerID => $data) {
            if($data['fightpoints'] / $attackerFullpoints <= 0.01) {
                continue;
            }
            $sql = 'UPDATE %%USERS%% SET `honorpoints` = `honorpoints` + :honorpoints WHERE `id` = :userID;';
            $db->update($sql, [
                ':honorpoints'  => $attackerHonorpointsChange * ($data['fightpoints'] / $attackerFullpoints),
                ':userID'       => $playerID,
            ]);
            if($defenderHonorpointsChange > 0) {
                $sqlfights = 'UPDATE %%ADVANCED_STATS%% SET `honorable_fights` = `honorable_fights` + 1 WHERE `userId` = :userID;';
                $db->update($sqlfights, [':userID' => $player['player']['id']]);
            } else if($defenderHonorpointsChange < 0) {
                $sqlfights = 'UPDATE %%ADVANCED_STATS%% SET `dishonorable_fights` = `dishonorable_fights` + 1 WHERE `userId` = :userID;';
                $db->update($sqlfights, [':userID' => $player['player']['id']]);
            }
        }

        $playerData = [];
        foreach ($defenders as $FleetID => $player) {
            $playerData[$player['player']['id']] = [];
            foreach ($player['unit'] as $ShipID => $Amount) {
                $fullCost = $pricelist[$ShipID]['cost'][901] + $pricelist[$ShipID]['cost'][902] + $pricelist[$ShipID]['cost'][903];
                if(isset($playerData[$player['player']['id']]['fightpoints'])) {
                    $playerData[$player['player']['id']]['fightpoints'] += $Amount * $fullCost;
                } else {
                    $playerData[$player['player']['id']]['fightpoints'] = $Amount * $fullCost;
                }
                $defenderFullpoints += $Amount * $fullCost;
            }

        }
        foreach ($playerData as $playerID => $data) {
            if(!isset($data['fightpoints'])){
                continue;
            }
            if($data['fightpoints'] / $defenderFullpoints <= 0.01) {
                continue;
            }
            $sql = 'UPDATE %%USERS%% SET `honorpoints` = `honorpoints` + :honorpoints WHERE `id` = :userID;';
            $db->update($sql, [
                ':honorpoints'  => $defenderHonorpointsChange * ($data['fightpoints'] / $defenderFullpoints),
                ':userID'       => $playerID,
            ]);
        }
    }

    /**
     * Compare the number of honorable and dishonorable fights of a player with the reference data
     * true if the player has more honorable fights or the same number of honorable fights and less dishonorable fights
     * @param array $userFights
     * @param array $referenceFights
     * @return bool
     */
    public static function compare_honor_fights($userFights, $referenceFights)
    {
        if ($userFights['honorable_fights'] > $referenceFights['honorable_fights']) {
            return true;
        } else if ($userFights['honorable_fights'] == $referenceFights['honorable_fights']) {
            if ($userFights['dishonorable_fights'] < $referenceFights['dishonorable_fights']) {
                return true;
            }
        }
        return false;
    }

    /**
     * Retrieve the honorpoints and the number of honorable and dishonorable fights of a player
     * @param int $userId
     * @return array $honordata
     */
    public static function get_honor_data($userId)
    {
        $db = Database::get();

        $sql = 'SELECT u.`honorpoints`, a.`honorable_fights`, a.`dishonorable_fights`
        FROM %%USERS%% u
        INNER JOIN %%ADVANCED_STATS%% a ON u.`id` = a.`userId`
        WHERE `id` = :userID;';
        $honordata = $db->selectSingle($sql, [
            ':userID'       => $userId,
        ]);

        return $honordata;
    }

    /**
     * Calculate good effect of honorpoints (decrease deut costs for attacks on players with high honorpoints)
     * can be 0.80(20%), 0.88(12%), 0.95(5%) or 1(0%) decrease
     * @param int $userId
     * @return int $factor
     */
    public static function calculate_honorpoints_boost($userId)
    {
        $factor = 1;
        $honordata = PlayerUtil::get_honor_data($userId);

        if($honordata['honorpoints'] >= 500 && isModuleAvailable(MODULE_HONORPOINTS)) {
            $honorDataTop = PlayerUtil::get_honor_percent("top");
            if ($honordata['honorpoints'] > $honorDataTop['top10']['honorpoints'] && $honordata['honorpoints'] >= 15000) {
                $factor = 0.80;
            } else if ($honordata['honorpoints'] == $honorDataTop['top10']['honorpoints'] && $honordata['honorpoints'] >= 2500) {
                $factor = PlayerUtil::compare_honor_fights($honordata, $honorDataTop['top10']) && $honordata['honorpoints'] >= 15000 ? 0.80 : 0.88;
            } else if ($honordata['honorpoints'] > $honorDataTop['top20']['honorpoints'] && $honordata['honorpoints'] >= 2500) {
                $factor = 0.88;
            } else if ($honordata['honorpoints'] == $honorDataTop['top20']['honorpoints']) {
                $factor = PlayerUtil::compare_honor_fights($honordata, $honorDataTop['top20']) && $honordata['honorpoints'] >= 2500 ? 0.88 : 0.95;
            } else if ($honordata['honorpoints'] > $honorDataTop['top30']['honorpoints']) {
                $factor = 0.95;
            } else if ($honordata['honorpoints'] == $honorDataTop['top30']['honorpoints']) {
                $factor = PlayerUtil::compare_honor_fights($honordata, $honorDataTop['top30']) ? 0.95 : 1;
            }
        }
        return $factor;
    }

    /**
     * Calculate bad effect of honorpoints (increase loot for attacks on players with low honorpoints)
     * can be 1(50%), 1.2(60%), 1.6(80%) or 2(100%) increase
     * @param int $userId
     * @return int $factor
     */
    public static function calculate_honorpoints_decrease($userId)
    {
        $factor = 1;
        $honordata = PlayerUtil::get_honor_data($userId);

        if($honordata['honorpoints'] <= -500 && isModuleAvailable(MODULE_HONORPOINTS)) {
            $honorDataTop = PlayerUtil::get_honor_percent("low");
            if ($honordata['honorpoints'] < $honorDataTop['top10']['honorpoints'] && $honordata['honorpoints'] <= -15000) {
                $factor = 2;
            } else if ($honordata['honorpoints'] == $honorDataTop['top10']['honorpoints'] && $honordata['honorpoints'] <= -2500) {
                $factor = PlayerUtil::compare_honor_fights($honordata, $honorDataTop['top10']) && $honordata['honorpoints'] > -15000 ? 1.6 : 2;
            } else if ($honordata['honorpoints'] < $honorDataTop['top20']['honorpoints'] && $honordata['honorpoints'] <= -2500) {
                $factor = 1.6;
            } else if ($honordata['honorpoints'] == $honorDataTop['top20']['honorpoints'] && $honordata['honorpoints'] <= -500) {
                $factor = PlayerUtil::compare_honor_fights($honordata, $honorDataTop['top20']) && $honordata['honorpoints'] > -2500 ? 1.2 : 1.6;
            } else if ($honordata['honorpoints'] < $honorDataTop['top30']['honorpoints'] && $honordata['honorpoints'] <= -500) {
                $factor = 1.2;
            } else if ($honordata['honorpoints'] == $honorDataTop['top30']['honorpoints']) {
                $factor = PlayerUtil::compare_honor_fights($honordata, $honorDataTop['top30']) && $honordata['honorpoints'] > -500 ? 1 : 1.2;
            }
        }
        return $factor;
    }

    /**
     * Calculate the border dataset for the top or low 10%, 20% and 30% of the universe
     *
     * @param string $kind("top" or "low")
     * @return array
     */
    public static function get_honor_percent($kind = "top")
    {
        $db = Database::get();
        $sql = 'SELECT count(*) as count FROM %%USERS%% WHERE universe = :universe;';
        $userAmount = $db->selectSingle($sql, [
            ':universe' => Universe::current(),
        ], 'count');

        $limTop10 = ceil(($userAmount -1) * (1 - 0.90));
        $limTop20 = ceil(($userAmount -1) * (1 - 0.80));
        $limTop30 = ceil(($userAmount -1) * (1 - 0.70));
        //top 10%   - 20% deut rabatt
        //top 20%   - 12% deut rabatt
        //top 30%   - 5% deut rabatt
        //mid 40%   - neutral (0% rabatt, 50% loot)
        //low 30%   - 60% loot
        //low 20%   - 80% loot
        //low 10%   - 100% loot
        if ($kind == "top") {
            $sql = 'SELECT u.`honorpoints`, a.`honorable_fights`, a.`dishonorable_fights`
            FROM %%USERS%% u
            INNER JOIN %%ADVANCED_STATS%% a ON u.`id` = a.`userId`
            WHERE `universe` = :universe
            ORDER BY u.`honorpoints` DESC, a.`honorable_fights` DESC , a.`dishonorable_fights` ASC ';
        } else {
            $sql = 'SELECT u.`honorpoints`, a.`honorable_fights`, a.`dishonorable_fights`
            FROM %%USERS%% u
            INNER JOIN %%ADVANCED_STATS%% a ON u.`id` = a.`userId`
            WHERE `universe` = :universe
            ORDER BY u.`honorpoints` ASC, a.`honorable_fights` ASC , a.`dishonorable_fights` DESC ';
        }


        $honorDataTop10 = $db->selectSingle($sql . 'LIMIT '.$limTop10.', 1;', [
            ':universe' => Universe::current(),
        ]);
        $honorDataTop20 = $db->selectSingle($sql. 'LIMIT '.$limTop20.', 1;', [
            ':universe' => Universe::current(),
        ]);
        $honorDataTop30 = $db->selectSingle($sql. 'LIMIT '.$limTop30.', 1;', [
            ':universe' => Universe::current(),
        ]);
        return [
            "top10" => $honorDataTop10,
            "top20" => $honorDataTop20,
            "top30" =>$honorDataTop30
        ];
    }

    /**
     * Compare two honor data arrays
     * check who has the hihger honorpoints
     * if tie, check who has more honorable fights
     * if tie, check who has less dishonorable fights
     * true if user is better than reference
     *
     * @param array $userHonorData
     * @param array $referenceHonorData
     * @return bool
     */
    public static function compare_honor_data($userHonorData, $referenceHonorData)
    {
        if ($userHonorData['honorpoints'] > $referenceHonorData['honorpoints']) {
            return true;
        } else if ($userHonorData['honorpoints'] == $referenceHonorData['honorpoints']) {
            if ($userHonorData['honorable_fights'] > $referenceHonorData['honorable_fights']) {
                return true;
            } else if ($userHonorData['honorable_fights'] == $referenceHonorData['honorable_fights']) {
                if ($userHonorData['dishonorable_fights'] < $referenceHonorData['dishonorable_fights']) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Injects the corresponding honor rank title into the statistic array
     * Top 1 with atleast 50.000 EP: God-Emperor
     * Top 10% with atleast 15.000 EP: Grand Emperor
     * Top 20% with atleast 2.500 EP: Emperor
     * Top 30% with atleast 500 EP: Starlord
     * Low 30% with atleast -500 EP: Bandit
     * Low 20% with atleast -2.500 EP: Banditlord
     * Low 10% with atleast -15.000 EP: King of the bandits
     * Low 1 with atleast -50.000 EP: Bandit Emperor
     *
     * @param array $statisticArray
     * @return array $statisticArray
     */
    public static function add_honorrank_title($statisticArray)
    {
        $topData = PlayerUtil::get_honor_percent("top");
        $lowData = PlayerUtil::get_honor_percent("low");

        foreach($statisticArray as $key => $statistic) {
            if($statistic['honor_rank'] == 1 && $statistic['honorpoints'] >= 50000) {
                $statisticArray[$key]['honortitle'] = "top1";
            } else if(PlayerUtil::compare_honor_data($statistic, $topData['top10']) && $statistic['honorpoints'] >= 15000) {
                $statisticArray[$key]['honortitle'] = "top10";
            } else if(PlayerUtil::compare_honor_data($statistic, $topData['top20']) && $statistic['honorpoints'] >= 2500) {
                $statisticArray[$key]['honortitle'] = "top20";
            } else if(PlayerUtil::compare_honor_data($statistic, $topData['top30']) && $statistic['honorpoints'] >= 500) {
                $statisticArray[$key]['honortitle'] = "top30";
            } else if (end($statisticArray)['id'] == $statistic['id'] && $statistic['honorpoints'] <= -50000) {
                $statisticArray[$key]['honortitle'] = "low1";
            } else if(PlayerUtil::compare_honor_data($lowData['top10'], $statistic) && $statistic['honorpoints'] <= -15000) {
                $statisticArray[$key]['honortitle'] = "low10";
            } else if(PlayerUtil::compare_honor_data($lowData['top20'], $statistic) && $statistic['honorpoints'] <= -2500) {
                $statisticArray[$key]['honortitle'] = "low20";
            } else if(PlayerUtil::compare_honor_data($lowData['top30'], $statistic) && $statistic['honorpoints'] <= -500) {
                $statisticArray[$key]['honortitle'] = "low30";
            } else {
                $statisticArray[$key]['honortitle'] = "neutral";
            }
        }

        return $statisticArray;
    }

    /**
     * Calculates, if enabled via config, the cost reduction factor based on the
     * percentage points difference to the top player of the universe. Points are
     * weighted: 1 x buildings + .8 x research + .6 x fleet
     *
     * - Calculation: factor = 10 * pow(0.9747, (userPoints/maxPoints * 100)) => (1 - 10)
     *                factor = (1-(1-0,5)/(10-1)*factor-1) => (1 - 0,5)
     * - Max factor: 0,5
     * - Min factor: 1
     * - Min percentage difference for boost: 10%
     * - Boost reduces build cost and therefore also build speed
     *
     * @param array $USER
     * @return float
     */
    public static function getCatchUpFactor(array $USER) : float
    {
        $factor = 1;
        $config = Config::get($USER['universe']);

        if ($config->catch_up && $USER['onlinetime'] > TIMESTAMP - INACTIVE) {
            $db = Database::get();

            $sql = 'SELECT (`build_points` * 1 + `tech_points` * 0.8 + `fleet_points` * 0.6) as points
                FROM %%STATPOINTS%%
                WHERE `id_owner` = :userID AND `stat_type` = 1;';
            $userPoints = $db->selectSingle($sql, [
                ':userID' => $USER['id'],
            ], 'points');
            $userPoints = max($userPoints, 1);

            $sql = 'SELECT max(`build_points` * 1 + `tech_points` * 0.8 + `fleet_points` * 0.6) AS max
                FROM %%STATPOINTS%%
                WHERE `universe` = :universe AND `stat_type` = 1;';
            $maxPoints = $db->selectSingle($sql, [
                ':universe' => $USER['universe'],
            ], 'max');
            $maxPoints = max($maxPoints, 1);

            $percentageOfMaxPoints = $userPoints / $maxPoints * 100;
            $maxBoost = 10;
            $reductionFactor = 0.9747;
            $factor = $maxBoost * pow($reductionFactor, $percentageOfMaxPoints);
            $factor = max($factor, 1);

            $maxFactor = 0.5;
            $factor = (1 - (1 - $maxFactor) / ($maxBoost-1) * ($factor-1));
        }

        return $factor;
    }
}
/*
    Enable to debug
*/
// try {
//     define('MODE', 'INSTALL');
//     define('ROOT_PATH', 'G:/xampp/htdocs/pr0game/');
//     define('TIMESTAMP', time());
//     require 'includes/constants.php';
//     require 'includes/classes/Database.class.php';
//     require 'includes/classes/Cache.class.php';
//     require 'includes/vars.php';
//     require 'includes/classes/Config.class.php';

//     PlayerUtil::randomHP(1);
// } catch (Exception $e) {
// }
