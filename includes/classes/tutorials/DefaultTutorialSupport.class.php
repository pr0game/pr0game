<?php

/**
 * pr0game powered by steemnova
 * tutorial
 * (c) 2024 Hyman
 */

class DefaultTutorialSupport extends AbstractTutorialSupport
{
    public function __construct(array $tutorialStep, array $USER)
    {
        parent::__construct($tutorialStep, $USER);
    }
}