<?php

/**
 * pr0game powered by steemnova
 * tutorial
 * (c) 2024 Hyman
 */

class MissionTutorialSupport extends AbstractTutorialSupport
{
    public function __construct(array $tutorialStep, array $USER)
    {
        parent::__construct($tutorialStep, $USER);
    }

    /**
     * List all enabeld and unskipped goals
     * 
     * Can be disabled by table enty
     *
     * @return array
     */
    public function listGoals() :array
    {
        $goals = [];
        foreach ($this->tutorialGoals as $goalRow) {
            $elementID = $goalRow['elementID'];

            if ($goalRow['enabled'] == 0 || $goalRow['skipped'] == 1) {
                continue;
            }

            $owned = 0;
            if ($goalRow['fullfilled'] == 1) {
                $owned = $goalRow['ammount'];
            }

            $goals[$elementID] = [
                'name'          => $this->getGoalName($elementID),
                'ammount'       => $goalRow['ammount'],
                'owned'         => $owned,
                'fullfilled'    => $goalRow['ammount'] <= $owned,
                'skippable'     => $goalRow['skippable'] == 1,
            ];
        }
        return $goals;
    }

    /**
     * Returns the goal name/description and link funktionality
     *
     * @param integer $elementID
     * @return string
     */
    protected function getGoalName(int $elementID) :string {
        $LNG =& Singleton()->LNG;
        $LNG->includeData(['TUTORIAL']);
        $reslist =& Singleton()->reslist;

        if (in_array($elementID, $reslist['fleet'])) {
            return parent::getGoalName($elementID);
        }
        return $LNG['tutorial_goal']['mission'][$elementID];
    }

    /**
     * Indicates whether all goals are fullfilled
     *
     * @return boolean
     */
    public function isFullfilled() :bool
    {
        $reslist =& Singleton()->reslist;

        $goals = $this->listGoals();
        $fullfilled = true;

        foreach ($goals as $elementID => $goalRow) {
            if (in_array($elementID, $reslist['fleet'])) {
                $fullfilled = $fullfilled && $goalRow['fullfilled'];
            }
            else if ($goalRow['fullfilled'] != 1) {
                $sql = "SELECT COUNT(`fleet_id`) as amount FROM %%LOG_FLEETS%% 
                    WHERE `fleet_mission` = :mission AND `fleet_end_time` <= :end_time AND `hasCanceled` = 0 AND `fleet_owner` = :userID;";
                $amount = $this->db->selectSingle($sql, [
                    ':mission' => $elementID,
                    ':end_time' => TIMESTAMP,
                    ':userID' => $this->user['id'],
                ], 'amount');

                if ($amount > 0) {
                    $this->fullfillGoal($elementID);
                    $fullfilled = $fullfilled && true;
                } else {
                    $fullfilled = false;
                }
            }
        }

        return $fullfilled;
    }

    /**
     * Fullfills the given goal for the current user
     *
     * @param integer $goal
     * @return void
     */
    public function fullfillGoal(int $goal) {
        foreach ($this->tutorialGoals as &$goalRow) {
            if ($goalRow['elementID'] == $goal) {
                $sql = "UPDATE %%USERS_TUTORIAL_GOAL%% SET `fullfilled` = 1 
                    WHERE `userID` = :userID AND `tutorialGoalID` = :tutorialGoalID";
                $this->db->update($sql, [
                    ':userID' => $this->user['id'],
                    ':tutorialGoalID' => $goalRow['id'],
                ]);
                $goalRow['fullfilled'] = 1;
                return;
            }
        }
    }
}