<?php

/**
 * pr0game powered by steemnova
 * tutorial
 * (c) 2024 Hyman
 */

class TutorialUtil
{
    public function __construct()
    {
        /**
         * @var Database
         */
        $db = Database::get();
        $USER =& Singleton()->USER;

        // Fehlende USER_TUTORIAL selektieren
        $sql = "SELECT t.`id` FROM %%TUTORIALS%% t
            LEFT JOIN %%TUTORIAL_UNIVERSE%% tu ON tu.`tutorialID` = t.`id` AND tu.`universe` = :universe AND tu.`enabled` = 0
            WHERE t.`enabled` = 1 AND tu.`id` IS NULL
              AND t.`id` NOT IN ( SELECT `tutorialID` AS id FROM %%USERS_TUTORIAL%% WHERE `userID` = :userID )";
        $missing = $db->select($sql, [
            ':userID' => $USER['id'],
            ':universe' => $USER['universe'],
        ]);

        $sql = "INSERT INTO %%USERS_TUTORIAL%% (`userID`, `tutorialID`, `claimed`) VALUES (:userID, :tutorialID, :claimed);";
        foreach ($missing as $tutorialRow) {
            $db->insert($sql, [
                ':userID' => $USER['id'],
                ':tutorialID' => $tutorialRow['id'],
                ':claimed' => (isModuleAvailable(MODULE_TUTORIAL_REWARDS) ? 0 : 1),
            ]);
        }

        // Fehlende USERS_TUTORIAL_GOAL selektieren
        $sql = "SELECT tg.`id` FROM %%TUTORIAL_GOALS%% tg
            JOIN %%TUTORIALS%% t ON t.id = tg.`tutorialID`
            LEFT JOIN %%TUTORIAL_UNIVERSE%% tu ON tu.`tutorialID` = t.`id` AND tu.`universe` = :universe AND tu.`enabled` = 0
            WHERE t.`enabled` = 1 AND tg.`enabled` = 1 AND tu.`id` IS NULL
              AND tg.`id` NOT IN ( SELECT `tutorialGoalID` AS id FROM %%USERS_TUTORIAL_GOAL%% WHERE `userID` = :userID )";
        $missing = $db->select($sql, [
            ':userID' => $USER['id'],
            ':universe' => $USER['universe'],
        ]);

        $sql = "INSERT INTO %%USERS_TUTORIAL_GOAL%% (`userID`, `tutorialGoalID`) VALUES (:userID, :tutorialGoalID);";
        foreach ($missing as $tutorialGoalRow) {
            $db->insert($sql, [
                ':userID' => $USER['id'],
                ':tutorialGoalID' => $tutorialGoalRow['id'],
            ]);
        }
    }

    /**
     * Returns details for the tutorial popup
     * 
     * Includes translated title and text, goals, rewards and states
     *
     * @return array
     */
    public function getTutorialStepDetails() :array
    {
        if (!isModuleAvailable(MODULE_TUTORIAL)) {
            return [];
        }

        $LNG =& Singleton()->LNG;
        $LNG->includeData(['TUTORIAL']);
        $USER =& Singleton()->USER;

        $tutorialStep = $this->getCurrentStep($USER['id'], $USER['universe']);
        if (empty($tutorialStep)) {
            return [];
        }

        $supportClass = $this->getSupportClass($tutorialStep, $USER);
        $translationKey = $tutorialStep['translation_key'];

        $tutorial = [
            'title' => $LNG['tutorial_title'][$translationKey],
            'text' => $LNG['tutorial_text'][$translationKey],
            'fullfilled' => $supportClass->isFullfilled(),
            'goals' => $supportClass->listGoals(),
            'rewards' => $supportClass->listRewards(),
            'last' => !$supportClass->hasNext(),
            'skippable' => $tutorialStep['skippable'],
        ];
        return $tutorial;
    }

    /**
     * Attempts to skip the current tutorial step, if able
     * 
     * Called by button/link in shared.tutorial.tpl via ShowTutorialPage.class.php
     *
     * @return void
     */
    public function skip() {
        /**
         * @var Database
         */
        $db = Database::get();
        $USER =& Singleton()->USER;
        
        $tutorialStep = $this->getCurrentStep($USER['id'], $USER['universe']);
        if (empty($tutorialStep) || $tutorialStep['skippable'] == 0) {
            return;
        }

        $sql = "UPDATE %%USERS_TUTORIAL%% SET `skipped` = 1 WHERE `userID` = :userID AND `tutorialID` = :tutorialID";
        $db->update($sql, [
            ':userID' => $USER['id'],
            ':tutorialID' => $tutorialStep['id'],
        ]);
    }

    /**
     * Attempts to skip the current tutorial step goal, if able
     * 
     * Called by button/link in shared.tutorial.tpl via ShowTutorialPage.class.php
     *
     * @param integer $goal
     * @return void
     */
    public function skipGoal(int $goal) {
        /**
         * @var Database
         */
        $db = Database::get();
        $USER =& Singleton()->USER;
        
        $tutorialStep = $this->getCurrentStep($USER['id'], $USER['universe']);
        if (empty($tutorialStep)) {
            return;
        }

        $supportClass = $this->getSupportClass($tutorialStep, $USER);
        if ($supportClass->isGoalSkippable($goal)) {
            $supportClass->skipGoal($goal);
        }
    }

    /**
     * Skips the entire tutorial
     * 
     * Called by button/link in shared.tutorial.tpl via ShowTutorialPage.class.php
     *
     * @return void
     */
    public function skipAll() {
        /**
         * @var Database
         */
        $db = Database::get();
        $USER =& Singleton()->USER;

        $sql = "UPDATE %%USERS_TUTORIAL%% SET `skipped` = 1 WHERE `userID` = :userID";
        $db->update($sql, [
            ':userID' => $USER['id'],
        ]);
    }


    /**
     * Claims all rewards for the tutorial step, if any, and tags it accordingly
     * 
     * Called by button/link in shared.tutorial.tpl via ShowTutorialPage.class.php
     *
     * @return void
     */
    public function claim() {
        $USER =& Singleton()->USER;
        $tutorialStep = $this->getCurrentStep($USER['id'], $USER['universe']);
        if (empty($tutorialStep)) {
            return;
        }

        $supportClass = $this->getSupportClass($tutorialStep, $USER);
        if ($supportClass->isFullfilled()) {
            $supportClass->rewardUser();
        }
    }

    /**
     * Attempts to tag the given goal as fullfilled
     * 
     * Called by button/link in shared.tutorial.tpl via ShowTutorialPage.class.php
     *
     * @param integer $goal
     * @return void
     */
    public function fullfillGoal(int $goal) {
        /**
         * @var Database
         */
        $db = Database::get();
        $USER =& Singleton()->USER;
        
        $tutorialStep = $this->getCurrentStep($USER['id'], $USER['universe']);
        if (empty($tutorialStep)) {
            return;
        }

        $supportClass = $this->getSupportClass($tutorialStep, $USER);
        $supportClass->fullfillGoal($goal);
    }

    /**
     * Creates and includes the AbstractTutorialSupport class for the current tutorial step
     *
     * @param array $tutorialStep
     * @param array $USER
     * @return AbstractTutorialSupport
     */
    private function getSupportClass(array $tutorialStep, array $USER) :AbstractTutorialSupport
    {
        require 'includes/classes/tutorials/AbstractTutorialSupport.class.php';
        $path = 'includes/classes/tutorials/' . $tutorialStep['support_class'] . '.class.php';
        require $path;
        /**
         * @var AbstractTutorialSupport
         */
        return new $tutorialStep['support_class']($tutorialStep, $USER);
    }

    /**
     * Returns the current tutorial step for the given user
     *
     * @param integer $userID
     * @param integer $universe
     * @return array
     */
    private function getCurrentStep(int $userID, int $universe) :array
    {
        /**
         * @var Database
         */
        $db = Database::get();

        $sql = "SELECT t.* FROM %%USERS_TUTORIAL%% ut
            JOIN %%TUTORIALS%% t ON t.`id` = ut.`tutorialID`
            LEFT JOIN %%TUTORIAL_UNIVERSE%% tu ON tu.`tutorialID` = t.`id` AND tu.`universe` = :universe AND tu.`enabled` = 0
            WHERE ut.`userID` = :userID AND ut.`completed` = 0 AND ut.`skipped` = 0 
              AND t.`enabled` = 1 AND tu.`id` IS NULL
            ORDER BY t.`order`;";
        $tutorialStep = $db->selectSingle($sql, [
            ':userID' => $userID,
            ':universe' => $universe,
        ]);
        return is_array($tutorialStep) ? $tutorialStep : [];
    }
}