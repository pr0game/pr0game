<?php

/**
 * pr0game powered by steemnova
 * tutorial
 * (c) 2024 Hyman
 */

abstract class AbstractTutorialSupport
{
    /**
     * @var Database
     */
    protected Database $db;
    protected array $tutorialStep;
    protected array $tutorialUniverse;
    protected array $tutorialGoals;
    protected array $tutorialRewards;
    protected array $user;
    protected array $planet;

    protected function __construct(array $tutorialStep, array $USER)
    {
        $this->db = Database::get();
        $this->user = $USER;
        $this->planet = getMainPlanet($USER);

        $this->tutorialStep = $tutorialStep;
        $tutorialID = $tutorialStep['id'];

        $sql = "SELECT * FROM %%TUTORIAL_UNIVERSE%% WHERE `tutorialID` = :tutorialID AND `universe` = :universe;";
        $result = $this->db->selectSingle($sql, [
            ':tutorialID' => $tutorialID,
            ':universe' => $this->user['universe'],
        ]);
        $this->tutorialUniverse = is_array($result) ? $result : [];

        $sql = "SELECT tg.*, ug.`fullfilled`, ug.`skipped` FROM %%TUTORIAL_GOALS%% tg
            JOIN %%USERS_TUTORIAL_GOAL%% ug ON ug.`tutorialGoalID` = tg.`id`
            WHERE tg.`tutorialID` = :tutorialID AND ug.`userID` = :userID
            ORDER BY tg.`elementID`;";
        $result = $this->db->select($sql, [
            ':tutorialID' => $tutorialID,
            ':userID' => $this->user['id'],
        ]);
        $this->tutorialGoals = is_array($result) ? $result : [];

        $sql = "SELECT tr.* FROM %%TUTORIAL_REWARDS%% tr
            JOIN %%USERS_TUTORIAL%% ut ON ut.`tutorialID` = tr.`tutorialID`
            WHERE tr.`tutorialID` = :tutorialID AND ut.`claimed` = 0
            ORDER BY tr.`elementID`;";
        $result = $this->db->select($sql, [
            ':tutorialID' => $tutorialID,
        ]);
        $this->tutorialRewards = is_array($result) ? $result : [];
    }

    /**
     * Indicates whether all goals are fullfilled
     *
     * @return boolean
     */
    public function isFullfilled() :bool
    {
        $goals = $this->listGoals();
        $fullfilled = true;

        foreach ($goals as $goalRow) {
            $fullfilled = $fullfilled && $goalRow['fullfilled'];
        }

        return $fullfilled;
    }

    /**
     * List all enabeld and unskipped goals
     * 
     * Can be disabled by table enty
     *
     * @return array
     */
    public function listGoals() :array
    {
        $resource =& Singleton()->resource;

        $goals = [];
        foreach ($this->tutorialGoals as $goalRow) {
            $elementID = $goalRow['elementID'];
            if ((array_key_exists($resource[$elementID], $this->user) || array_key_exists($resource[$elementID], $this->planet)) && 
                !BuildFunctions::isEnabled($elementID)) {
                continue;
            }

            if ($goalRow['enabled'] == 0 || $goalRow['skipped'] == 1) {
                continue;
            }

            $owned = 0;
            if (array_key_exists($resource[$elementID], $this->user)) {
                $owned = $this->user[$resource[$elementID]];
            } else if (array_key_exists($resource[$elementID], $this->planet)) {
                $owned = $this->planet[$resource[$elementID]];
            } else if ($goalRow['fullfilled'] == 1) {
                $owned = $goalRow['ammount'];
            }

            $goals[$elementID] = [
                'name'          => $this->getGoalName($elementID),
                'ammount'       => $goalRow['ammount'],
                'owned'         => $owned,
                'fullfilled'    => $goalRow['ammount'] <= $owned,
                'skippable'     => $goalRow['skippable'] == 1,
            ];
        }
        return $goals;
    }

    /**
     * Returns the goal name/description
     *
     * @param integer $elementID
     * @return string
     */
    protected function getGoalName(int $elementID) :string {
        $LNG =& Singleton()->LNG;
        return $LNG['tech'][$elementID];
    }

    /**
     * Indicates whether a goal can be skipped
     *
     * @param integer $goal
     * @return boolean
     */
    public function isGoalSkippable(int $goal) :bool
    {
        foreach ($this->listGoals() as $elementID => $goalRow) {
            if ($elementID == $goal && $goalRow['skippable'] == true) {
                return true;
            }
        }
        return false;
    }

    /**
     * Skips the given goal for the current user, if able
     *
     * @param integer $goal
     * @return void
     */
    public function skipGoal(int $goal)
    {
        foreach ($this->tutorialGoals as $goalRow) {
            if ($goalRow['elementID'] == $goal && $goalRow['skippable'] == true) {
                $sql = "UPDATE %%USERS_TUTORIAL_GOAL%% SET `skipped` = 1
                    WHERE `userID` = :userID AND `tutorialGoalID` = :tutorialGoalID";
                $this->db->update($sql, [
                    ':userID' => $this->user['id'],
                    ':tutorialGoalID' => $goalRow['id'],
                ]);
                return;
            }
        }
    }

    /**
     * Fullfills the given goal for the current user
     * 
     * Not implemented for default var goals
     *
     * @param integer $goal
     * @return void
     */
    public function fullfillGoal(int $goal) {
        // not implemented for default var goals
    }

    /**
     * Lists all enabled rewards
     * 
     * Can be disabled by module MODULE_TUTORIAL_REWARDS
     * or by table entry
     *
     * @return array
     */
    public function listRewards() :array
    {
        if (!isModuleAvailable(MODULE_TUTORIAL_REWARDS)) {
            return [];
        }

        $resource =& Singleton()->resource;
        $LNG =& Singleton()->LNG;

        $rewards = [];
        foreach ($this->tutorialRewards as $rewardRow) {
            $elementID = $rewardRow['elementID'];
            if ((array_key_exists($resource[$elementID], $this->user) || array_key_exists($resource[$elementID], $this->planet)) && 
                !BuildFunctions::isEnabled($elementID)) {
                continue;
            }
            
            if ($rewardRow['enabled'] == 0) {
                continue;
            }

            $rewards[$elementID] = [
                'name'      => $LNG['tech'][$elementID],
                'ammount'   => $rewardRow['ammount'],
            ];
        }
        return $rewards;
    }

    /**
     * Adds rewards to the user or the home planet based on vars elementID
     *
     * @return void
     */
    public function rewardUser()
    {
        if (!$this->isFullfilled()) {
            return;
        }
        $resource =& Singleton()->resource;

        $sql = "UPDATE %%USERS_TUTORIAL%% SET `completed` = 1, `claimed` = 1 WHERE `userID` = :userID AND `tutorialID` = :tutorialID";
        $this->db->update($sql, [
            ':userID' => $this->user['id'],
            ':tutorialID' => $this->tutorialStep['id'],
        ]);

        foreach ($this->listRewards() as $elementID => $rewardRow) {
            if (array_key_exists($resource[$elementID], $this->user)) {
                $sql = "UPDATE %%USERS%% SET " . $resource[$elementID] . " = " . $resource[$elementID] . " + :ammount
                    WHERE id = :userID;";
                $this->db->update($sql, [
                    ':ammount' => $rewardRow['ammount'],
                    ':userID' => $this->user['id'],
                ]);
            } else if (array_key_exists($resource[$elementID], $this->planet)) {
                $sql = "UPDATE %%PLANETS%% SET " . $resource[$elementID] . " = " . $resource[$elementID] . " + :ammount
                    WHERE id = :planetID;";
                $this->db->update($sql, [
                    ':ammount' => $rewardRow['ammount'],
                    ':planetID' => $this->planet['id'],
                ]);
            }
        }
    }

    /**
     * Returns the next tutorial step
     *
     * @return array
     */
    public function getNextStep() :array
    {
        $sql = "SELECT t.* FROM %%TUTORIALS%% t
            LEFT JOIN %%TUTORIAL_UNIVERSE%% tu ON tu.`tutorialID` = t.`id` AND tu.`universe` = :universe AND tu.`enabled` = 0
            WHERE t.`order` > :order AND t.`enabled` = 1 AND tu.`id` IS NULL
            ORDER BY t.`order`";
        $next = $this->db->selectSingle($sql, [
            ':order' => $this->tutorialStep['order'],
            ':universe' => $this->user['universe'],
        ]);
        return is_array($next) ? $next : [];
    }

    /**
     * Indicates whether there is a next tutorial step
     *
     * @return boolean
     */
    public function hasNext() :bool
    {
        return !empty($this->getNextStep());
    }

    /**
     * Indicates whether the tutorial step is enabled
     *
     * @return boolean
     */
    public function isEnabled() :bool
    {
        return $this->tutorialStep['enabled'] == 1
            && (!isset($this->tutorialUniverse) || $this->tutorialUniverse['enabled'] == 1);
    }

    /**
     * Indicates whether rewards are enabeld and are defined for this step
     *
     * @return boolean
     */
    public function hasRewards():bool
    {
        if (!isModuleAvailable(MODULE_TUTORIAL_REWARDS) || empty($this->tutorialRewards)) {
            return false;
        }

        foreach ($this->tutorialRewards as $rewardRow) {
            if ($rewardRow['enabled'] == 1) {
                return true;
            }
        }
        return false;
    }
}