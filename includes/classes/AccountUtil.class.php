<?php

/**
 * pr0game powered by steemnova
 * Accounts
 * (c) 2024 reflexrecon
 */

class AccountUtil
{
    public static function cryptPassword($password)
    {
        return password_hash($password, PASSWORD_BCRYPT, ['cost' => 13]);
    }

    public static function isNameValid($name)
    {
        if (UTF8_SUPPORT) {
            return preg_match('/^[0-9a-zA-ZäöüÄÖÜß _.-]*$/u', $name);
        } else {
            return preg_match('/^[A-z0-9_\-. ]*$/', $name);
        }
    }

    public static function isMailValid($address)
    {
        if (function_exists('filter_var')) {
            return filter_var($address, FILTER_VALIDATE_EMAIL) !== false;
        } else {
            return preg_match('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$', $address);
        }
    }

    /**
     * Create Account
     *
     * @param string $accountname
     * @param string $userPassword
     * @param string $userMail
     * @param string $userLanguage
     * @param int $authlevel
     * @param string $userIpAddress
     * @param string $timezone
     * @return int
     */
    public static function createAccount(
        $accountname,
        $userPassword,
        $userMail,
        $userLanguage = null,
        $authlevel = 0,
        $userIpAddress = null,
        $timezone = "Europe/Berlin"
    ) {
        $db = Database::get();

        $params = [
            ':accountname'       => $accountname,
            ':email'             => $userMail,
            ':email2'            => $userMail,
            ':authlevel'         => $authlevel,
            ':registerAddress'   => !empty($userIpAddress) ? $userIpAddress : Session::getClientIp(),
            ':registerTimestamp' => TIMESTAMP,
            ':password'          => $userPassword,
            ':language'          => $userLanguage,
            ':dpath'             => DEFAULT_THEME,
            ':timezone'          => $timezone,
        ];

        $sql = 'INSERT INTO %%ACCOUNTS%% SET
            `accountname`	    = :accountname,
            `email`			    = :email,
            `email_2`		    = :email2,
            `authlevel`		    = :authlevel,
            `ip_at_reg`		    = :registerAddress,
            `account_lastip`    = :registerAddress,
            `register_time`     = :registerTimestamp,
            `password`		    = :password,
            `dpath`			    = :dpath,
            `timezone`		    = :timezone,
            `lang`			    = :language;';

        $db->insert($sql, $params);

        $accountId = $db->lastInsertId();

        return $accountId;
    }

    /**
     * Delete Account
     *
     * @param int $accountId
     * @return bool
     */
    public static function deleteAccount($accountId)
    {
        if (ROOT_USER == $accountId) {
            // superuser can not be deleted.
            throw new Exception("Superuser #" . ROOT_USER . " can't be deleted!");
        }
        $playerUniverses = self::get_player_universes($accountId);
        foreach ($playerUniverses as $playerUniverse) {
            PlayerUtil::deletePlayer($playerUniverse['user_id']);
        }
        $db = Database::get();
        $sql = 'SELECT `id` FROM %%ACCOUNTS%% WHERE `id` = :accountId;';
        $userData = $db->selectSingle($sql, [
            ':accountId'   => $accountId
        ]);

        if (empty($userData)) {
            return false;
        }

        $sql = 'DELETE FROM %%ACCOUNTS%% WHERE `id` = :accountId;';
        $db->delete($sql, [
            ':accountId'   => $accountId
        ]);

        $sql = 'DELETE FROM %%ACCOUNTS_TO_USERS%% WHERE `account_id` = :accountId;';
        $db->delete($sql, [
            ':accountId'   => $accountId
        ]);

        $sql = 'DELETE FROM %%ACCOUNTS_BANNED%% WHERE `account_id` = :accountId;';
        $db->delete($sql, [
            ':accountId'   => $accountId
        ]);

        return true;
    }

    public static function getDeletionDate($dbdeaktjava, $config)
    {
        $db = Database::get();
        $sql = 'SELECT * FROM %%CRONJOBS%% WHERE `cronjobID` = 4;';
        $deletionJob = $db->selectSingle($sql);
        $deletionDate = 0;
        if(!empty($deletionJob) && $deletionJob['isActive'] == 1) {
            $earliestTime = $dbdeaktjava + ($config->del_user_manually * 86400);
            if($earliestTime < $deletionJob['nextTime']) {
                $deletionDate = $deletionJob['nextTime'];
            } else {
                $nextTime = TIMESTAMP + 60;
                $cronTabString = implode(' ', [$deletionJob['min'], $deletionJob['hours'], $deletionJob['dom'], $deletionJob['month'], $deletionJob['dow']]);
                $cron = new Cron\CronExpression($cronTabString);
                for($i = 0; $i < 10; $i++) {
                    $nextTime = $cron->getNextRunDate(new DateTime('@' . $nextTime))->getTimestamp();
                    if($nextTime > $earliestTime) {
                        $deletionDate = $nextTime;
                        break;
                    }
                }
            }
        }
        return $deletionDate;
    }

    /**
     * Get Universes in which the Account has a User
     *
     * @param int $accountId
     * @return array
     */
    public static function get_player_universes($accountId)
    {
        $db = Database::get();

        $sql = 'SELECT cu.uni, cu.uni_name, cu.`start`, cu.`end`, u.username, cu.uni_status, s.total_rank, u.id as user_id, u.authlevel
                FROM %%ACCOUNTS%% a 
                LEFT JOIN %%ACCOUNTS_TO_USERS%% au on au.account_id = a.id 
                LEFT JOIN %%USERS%% u on u.id = au.user_id 
                LEFT JOIN %%CONFIG_UNIVERSE%% cu on cu.uni = u.universe 
                left join %%STATPOINTS%% s on s.id_owner = u.id 
                where a.id = :id and s.stat_type = 1
                ORDER BY cu.uni ASC;';
        $playerUniverses = $db->select($sql, [
            ':id'       => $accountId,
        ]);
        return $playerUniverses;
    }

    /**
     * Get Universes in which the Account has no User
     *
     * @param int $accountId
     * @return array
     */
    public static function get_non_player_universes($accountId)
    {
        $db = Database::get();

        $sql = 'SELECT cu.uni, cu.uni_name, cu.`start`, cu.`end`, users.active, cu.uni_status
                FROM %%CONFIG_UNIVERSE%% cu
                LEFT JOIN  (SELECT universe, count(id) as active 
                    from %%USERS%% u 
                    where u.onlinetime > :inactive 
                    group by universe) users on users.universe = cu.uni
                WHERE NOT EXISTS (
                    SELECT 1
                    FROM %%USERS%% u
                    JOIN %%ACCOUNTS_TO_USERS%% au ON au.user_id = u.id
                    WHERE au.account_id = :id AND u.universe = cu.uni
                ) ORDER BY cu.uni ASC;';
        $nonPlayerUniverses = $db->select($sql, [
            ':id'       => $accountId,
            ':inactive' => TIMESTAMP - 86400,
        ]);
        return $nonPlayerUniverses;
    }

    /**
     * Get AuthLevel of Account
     *
     * @param int $accountId
     * @return int
     */
    public static function get_authlevel($accountId)
    {
        $db = Database::get();

        $sql = 'SELECT `authlevel` FROM %%ACCOUNTS%% WHERE `id` = :accountID;';
        $authlevel = $db->selectSingle($sql, [
            ':accountID'       => $accountId,
        ], 'authlevel');

        return $authlevel;
    }
    /**
     * Get data of Account
     *
     * @param int $accountId
     * @return array
     */
    public static function get_account($accountId)
    {
        $db = Database::get();

        $sql = 'SELECT * FROM %%ACCOUNTS%% WHERE `id` = :accountID;';
        $account = $db->selectSingle($sql, [
            ':accountID'       => $accountId,
        ]);

        return $account;
    }
}
/*
    Enable to debug
*/
// try {
//     define('MODE', 'INSTALL');
//     define('ROOT_PATH', 'G:/xampp/htdocs/pr0game/');
//     define('TIMESTAMP', time());
//     require 'includes/constants.php';
//     require 'includes/classes/Database.class.php';
//     require 'includes/classes/Cache.class.php';
//     require 'includes/vars.php';
//     require 'includes/classes/Config.class.php';

//     PlayerUtil::randomHP(1);
// } catch (Exception $e) {
// }
