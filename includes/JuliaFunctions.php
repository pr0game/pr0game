<?php

function isJuliaRunning($sim = false) : bool
{
    $port = $sim ? 8101 : 8100;
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, "http://127.0.0.1:$port/ping");
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($curl);
    curl_close($curl);

    return $result == 'alive';
}

function refreshJuliaShipData() : bool
{
    $refreshed = false;
    if (isJuliaRunning(false)) {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, "http://127.0.0.1:8100/updateships");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($curl);
        curl_close($curl);
        $refreshed = ($result == 'done');
    }
    if (isJuliaRunning(true)) {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, "http://127.0.0.1:8101/updateships");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($curl);
        curl_close($curl);
        $refreshed = ($result == 'done');
    }
    return $refreshed;
}

function simulateTestFleet()
{
    $attacker = [];
    $attacker['fleetDetail'] = [
        'fleet_start_galaxy' => 1,
        'fleet_start_system' => 33,
        'fleet_start_planet' => 7,
        'fleet_start_type' => 1,
        'fleet_end_galaxy' => 1,
        'fleet_end_system' => 33,
        'fleet_end_planet' => 7,
        'fleet_end_type' => 1,
        'fleet_resource_metal' => 0,
        'fleet_resource_crystal' => 0,
        'fleet_resource_deuterium' => 0,
    ];
    $attacker['player'] = [
        'id' => (1000 + 0 + 1),
        'username'  => 'Nr.' . (0 + 1),
        'military_tech' => 0,
        'shield_tech' => 0,
        'defence_tech' => 0,
    ];
    $attacker['unit'][SHIP_LIGHT_FIGHTER] = 1;
    $fleetAttack[] = $attacker;

    $defender = [];
    $defender['fleetDetail'] = [
        'fleet_start_galaxy' => 1,
        'fleet_start_system' => 33,
        'fleet_start_planet' => 7,
        'fleet_start_type' => 1,
        'fleet_end_galaxy' => 1,
        'fleet_end_system' => 33,
        'fleet_end_planet' => 7,
        'fleet_end_type' => 1,
        'fleet_resource_metal' => 0,
        'fleet_resource_crystal' => 0,
        'fleet_resource_deuterium' => 0,
    ];
    $defender['player'] = [
        'id' => (2000 + 1 + 1),
        'username'  => 'Nr.' . (1 + 1),
        'military_tech' => 0,
        'shield_tech' => 0,
        'defence_tech' => 0,
    ];
    $defender['unit'][SHIP_LIGHT_FIGHTER] = 1;
    $fleetDefend[] = $defender;

    try {
        require_once 'includes/classes/missions/functions/calculateAttack.php';
        if (isJuliaRunning(true)) {
            loadBattleEngine(true);
            calculateAttack($fleetAttack, $fleetDefend, Config::get()->fleet_debris_percentage, Config::get()->def_debris_percentage, true);
        }
        if (isJuliaRunning(false)) {
            loadBattleEngine(false);
            calculateAttack($fleetAttack, $fleetDefend, Config::get()->fleet_debris_percentage, Config::get()->def_debris_percentage, false);
        }
    } catch (\Throwable $th) {
        error_log($th);
    }
}

function exportJuliaShipData()
{
    $db = Database::get();

    $sql = "SELECT `elementID`, `attack`, `defend`, TRUNCATE((`cost901` + `cost902`) / 10, 0) AS armor, IF(`class` < 300, 1, 0) AS isShip, `class`
        FROM %%VARS%%
        WHERE `class` IN (200, 400) AND `universe` = :universe";
    $defaultElements = $db->select($sql, [
        ':universe' => 0,
    ]);

    $lines = [];
    foreach (Universe::availableUniverses() as $universe) {
        $sql = "SELECT `elementID`, `attack`, `defend`, TRUNCATE((`cost901` + `cost902`) / 10, 0) AS armor, IF(`class` = 200, 1, 0) AS isShip, `class`
            FROM %%VARS%%
            WHERE `class` IN (200, 201, 400, 401) AND `universe` = :universe";
        $uniElements = $db->select($sql, [
            ':universe' => $universe,
        ]);
        $uniElementArray = [];
        foreach ($uniElements as $element) {
            $uniElementArray[$element['elementID']] = $element;
        }

        foreach ($defaultElements as $defaultElement) {
            $line = '';
            if (!isset($uniElementArray[$defaultElement['elementID']])) {
                $line = $universe . ';' . $defaultElement['elementID'] . ';' . $defaultElement['attack'] . ';' . $defaultElement['defend'] . ';' . $defaultElement['armor'] . ';' . $defaultElement['isShip'] . ';';
                addRapidfire($defaultElement['elementID'], $universe, $line);
            } else {
                $uniElement = $uniElementArray[$defaultElement['elementID']];
                if ($uniElement['class'] == 200 || $uniElement['class'] == 400) {
                    $line = $universe . ';' . $uniElement['elementID'] . ';' . $uniElement['attack'] . ';' . $uniElement['defend'] . ';' . $uniElement['armor'] . ';' . $uniElement['isShip'] . ';';
                    unset($uniElementArray[$defaultElement['elementID']]);
                    addRapidfire($defaultElement['elementID'], $universe, $line);
                }
            }

            $lines[] = $line;
        }

        foreach ($uniElementArray as $element => $uniElement) {
            $line = $universe . ';' . $uniElement['elementID'] . ';' . $uniElement['attack'] . ';' . $uniElement['defend'] . ';' . $uniElement['armor'] . ';' . $uniElement['isShip'] . ';';
            addRapidfire($uniElement['elementID'], $universe, $line);
            $lines[] = $line;
        }
    }

    sort($lines);

    $firstLine = true;
    foreach ($lines as $line) {
        if ($firstLine) {
            $firstLine = false;
            file_put_contents('juliaBattleEngine/shipinfos.csv', $line . PHP_EOL);
        }
        file_put_contents('juliaBattleEngine/shipinfos.csv', $line . PHP_EOL, FILE_APPEND);
    }
}

function addRapidfire(string $element, int $universe, string &$line)
{
    $db = Database::get();

    $rapidfire = [];
    $sql = "SELECT `rapidfireID`, `shoots`
        FROM %%VARS_RAPIDFIRE%%
        WHERE `universe` = 0 AND `elementID` = :elementID";
    $defaultRapidfire = $db->select($sql, [
        ':elementID' => $element,
    ]);
    if (!empty($defaultRapidfire)) {
        foreach ($defaultRapidfire as $rfResult) {
            $rapidfire[$rfResult['rapidfireID']] = $rfResult['shoots'];
        }
    }

    $sql = "SELECT `rapidfireID`, `shoots`
        FROM %%VARS_RAPIDFIRE%%
        WHERE `universe` = :universe AND `elementID` = :elementID";
    $uniRapidfire = $db->select($sql, [
        ':elementID' => $element,
        ':universe' => $universe,
    ]);
    if (!empty($uniRapidfire)) {
        foreach ($uniRapidfire as $rfResult) {
            if ($rfResult['shoots'] < 2) {
                unset($rapidfire[$rfResult['rapidfireID']]);
            } else {
                $rapidfire[$rfResult['rapidfireID']] = $rfResult['shoots'];
            }
        }
    }

    if (!empty($rapidfire)) {
        foreach ($rapidfire as $rfKey => $rfValue) {
            $line .= $rfKey . ',' . $rfValue . '-';
        }
        $line = substr_replace($line, '', -1);
    }
}