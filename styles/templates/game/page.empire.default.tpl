{block name="title" prepend}{$LNG.lm_empire}{/block}
  {block name="script" append}
    <script>
      function toggle_rows(target) {
        const rows = document.querySelectorAll("." + target)
        if (rows[0].classList.contains("hidden")) {
          rows.forEach(r => r.classList.remove("hidden"))
          document.getElementById(target + "-toggle").innerText = "▲"
        } else {
          rows.forEach(r => r.classList.add("hidden"))
          document.getElementById(target + "-toggle").innerText = "▼"
        }
      }
    </script>
  {/block}
{block name="content"}
  <table>
    <tbody>
    <tr>
      <th colspan="{$colspan}">{$LNG.lv_imperium_title}</th>
    </tr>
    <tr>
      <td style="width:100px">{$LNG.lv_planet} ({$currentPlanetCount} / {$maxPlanetCount})</td>
      <td style="width:100px;font-size: 50px;">&Sigma;</td>
        {foreach $planetList.image as $image}
          <td style="width:100px">
            <a href="game.php?page=overview&amp;cp={$image.id}">
              {if $enablePlanetStyles eq 0}
                <img width="80" height="80" border="0" src="{$dpath}planeten/{$image.picture}.jpg">
              {else}
                {if $image.planet_style eq 0}
                  <img width="80" height="80" border="0" src="{$dpath}planeten/{$image.picture}.jpg">
                {/if}
                {if $image.planet_style eq 1}
                  <img width="80" height="80" border="0" src="{$dpath}planeten/varianten/v1_magic/{$image.picture}_v1_magic.webp">
                {/if}
                {if $image.planet_style eq 2}
                  <img width="80" height="80" border="0" src="{$dpath}planeten/varianten/v2_storm/{$image.picture}_v2_storm.webp">
                {/if}
                {if $image.planet_style eq 3}
                  <img width="80" height="80" border="0" src="{$dpath}planeten/varianten/v3_voronoi/{$image.picture}_v3_voronoi.webp">
                {/if}
              {/if}
            </a>
          </td>
        {/foreach}
    </tr>
    <tr>
      <td>{$LNG.lv_name}</td>
      <td>{$LNG.lv_total}</td>
        {foreach $planetList.name as $name}
          <td>{$name}</td>
        {/foreach}
    </tr>
    <tr>
      <td>{$LNG.lv_coords}</td>
      <td>-</td>
        {foreach $planetList.coords as $coords}
          <td><a href="game.php?page=galaxy&amp;galaxy={$coords.galaxy}&amp;system={$coords.system}">[{$coords.galaxy}:{$coords.system}:{$coords.planet}]</a></td>
        {/foreach}
    </tr>
    <tr>
      <td>{$LNG.lv_fields}</td>
      <td>-</td>
        {foreach $planetList.field as $field}
          <td>{$field.current} / {$field.max}</td>
        {/foreach}
    </tr>
    <tr>
      <th colspan="{$colspan}">
        <span onclick="toggle_rows('ressources')"><span id="ressources-toggle">▲</span> {$LNG.lv_resources}</span>
        <span style="float: right">
          {$LNG.lv_show}{$LNG.lv_prodPerHour} <input class="showProdPerHour" type="checkbox" {if $enableShowProdPerHour == "true"}checked="checked"{/if} oninput="$.cookie('empire_showProdPerHour', $('.showProdPerHour').prop('checked'));" />
          | {$LNG.lv_storageFilled} <input class="showStorageFilled" type="checkbox" {if $enableShowStorageFilled == "true"}checked="checked"{/if} oninput="$.cookie('empire_showStorageFilled', $('.showStorageFilled').prop('checked'));" />
        </span>
      </th>
    </tr>
    {foreach $planetList.resource as $elementID => $resourceArray}
      <tr class="ressources" data-info="r_{$elementID}">
        <td><a href='#' onclick='return Dialog.info({$elementID});' class='tooltip'
               data-tooltip-content="<table><tr><th>{$LNG.tech.{$elementID}}</th></tr><tr><table class='hoverinfo'><tr><td><img src='{$dpath}gebaeude/{$elementID}.{if $elementID >=600 && $elementID <= 699}jpg{else}gif{/if}'></td><td>{$LNG.shortDescription.$elementID}</td></tr></table></tr></table>">{$LNG.tech.$elementID}</a>
        </td>
        <td>
          {number_format(array_sum($resourceArray), 0, ",", ".")}
          {if in_array($elementID, array(901,902,903))}
            {if $enableShowProdPerHour == "true"}
              <br><span class="colorPositive">
                {number_format(array_sum($planetList.resourcePerHour[$elementID]), 0, ",", ".")} /h
              </span>
            {/if}
            {if $enableShowStorageFilled == "true"}
              <br><span {if min($planetList.resourceFull[$elementID])>23}class="colorPositive"
                  {elseif min($planetList.resourceFull[$elementID])<24 && min($planetList.resourceFull[$elementID])>6}class="colorNeutral"
                  {elseif min($planetList.resourceFull[$elementID])<7}class="colorNegative"
                {/if}>
                {number_format(min($planetList.resourceFull[$elementID]), 0, ",", ".")} h
              </span>
            {/if}
            {if array_sum($planetList.currentTransported[$elementID]) > 0}
              <br><span class="colorNeutral">✈ {number_format(array_sum($planetList.currentTransported[$elementID]), 0, ",", ".")}</span>
            {/if}
          {/if}
        </td>
        {foreach $resourceArray as $planetID => $resource}
          <td>
            {number_format($resource, 0, ",", ".")}
            {if in_array($elementID, array(901,902,903))}
              {if $planetList.planet_type[$planetID] == 1}
                {if $enableShowProdPerHour == "true"}
                  <br><span {if $planetList.resourceProductionPercent[$elementID][$planetID] < 10}
                      class="colorNeutral tooltip" data-tooltip-content="{$LNG['bd_actual_production']} {$planetList.resourceProductionPercent[$elementID][$planetID] * 10} %"
                    {else}
                      class="colorPositive"
                    {/if}>
                    {number_format($planetList.resourcePerHour[$elementID][$planetID], 0, ",", ".")} /h
                  </span>
                {/if}
                {if $enableShowStorageFilled == "true"}
                  <br><span {if $planetList.resourceFull[$elementID][$planetID]>23}class="colorPositive"
                      {elseif $planetList.resourceFull[$elementID][$planetID]<24 && $planetList.resourceFull[$elementID][$planetID]>6}class="colorNeutral"
                      {elseif $planetList.resourceFull[$elementID][$planetID]<7}class="colorNegative"{/if}>
                    {number_format($planetList.resourceFull[$elementID][$planetID], 0, ",", ".")} h
                  </span>
                {/if}
              {/if}
              {if $planetList.currentTransported[$elementID][$planetID] > 0}
                <br><span class="colorNeutral">✈ {number_format($planetList.currentTransported[$elementID][$planetID], 0, ",", ".")}</span>
              {/if}
            {/if}
          </td>
        {/foreach}
      </tr>
    {/foreach}
    <tr>
      <th colspan="{$colspan}">
        <span onclick="toggle_rows('buildings')"><span id="buildings-toggle">▲</span> {$LNG.lv_buildings}</span>
        <span style="float: right">
          {$LNG.lv_highlight}{$LNG.lv_buildable} <input class="highlightBuildable" type="checkbox" {if $enableHiglightBuildable == "true"}checked="checked"{/if} oninput="$.cookie('empire_highlightBuildable', $('.highlightBuildable').prop('checked'));" />
          | {$LNG.lv_unbuildable} <input class="highlightNonBuildable" type="checkbox" {if $enableHiglightNonBuildable == "true"}checked="checked"{/if} oninput="$.cookie('empire_highlightNonBuildable', $('.highlightNonBuildable').prop('checked'));" />
        </span>
      </th>
    </tr>
    {foreach $planetList.build as $elementID => $buildArray}
      <tr class="buildings" data-info="b_{$elementID}">
        <td><a href='#' onclick='return Dialog.info({$elementID})' class='tooltip'
               data-tooltip-content="<table><tr><th>{$LNG.tech.{$elementID}}</th></tr><tr><table class='hoverinfo'><tr><td><img src='{$dpath}gebaeude/{$elementID}.{if $elementID >=600 && $elementID <= 699}jpg{else}gif{/if}'></td><td>{$LNG.shortDescription.$elementID}</td></tr></table></tr></table>">{$LNG.tech.$elementID}</a>
        </td>
        <td>{number_format($avgBuildings.{$elementID}, 1, ",", ".")}</td>
        {foreach $buildArray as $planetID => $build}
          <td>
            <span {if $planetList.buildable.{$elementID}.{$planetID}}class="{if $enableHiglightBuildable == "true"}colorPositive{/if}"{else}class="{if $enableHiglightNonBuildable == "true"}colorNegative{/if}"{/if}>{number_format($build, 0, ",", ".")}</span>
            {if $planetList.currentBuilding[$planetID]==$elementID}
              <span class="colorNeutral"> ⬆ {$planetList.currentBuildinglvl[$planetID]}</span>
            {/if}
          </td>
        {/foreach}
      </tr>
    {/foreach}
    <tr>
      <th colspan="{$colspan}" onclick="toggle_rows('technology')"><span id="technology-toggle">▲</span> {$LNG.lv_technology}</th>
    </tr>
    {foreach $planetList.tech as $elementID => $techArray}
      <tr class="technology" data-info="t_{$elementID}">
        <td><a href='#' onclick='return Dialog.info({$elementID})' class='tooltip'
               data-tooltip-content="<table><tr><th>{$LNG.tech.{$elementID}}</th></tr><tr><table class='hoverinfo'><tr><td><img src='{$dpath}gebaeude/{$elementID}.{if $elementID >=600 && $elementID <= 699}jpg{else}gif{/if}'></td><td>{$LNG.shortDescription.$elementID}</td></tr></table></tr></table>">{$LNG.tech.$elementID}</a>
        </td>
        <td>
          {number_format(max(max($techArray), 0), 0, ",", ".")}
          {if $elementID== $researchq_id}<span class="colorNeutral"> ⬆ {$researchq_lvl}</span>{/if}
        </td>
        {foreach $techArray as $planetID => $tech}
          {if $planetList.tech.{$elementID}.{$planetID} >= 0}
            <td>
              <span {if $planetList.buildable.{$elementID}.{$planetID}}class="{if $enableHiglightBuildable == "true"}colorPositive{/if}"{else}class="{if $enableHiglightNonBuildable == "true"}colorNegative{/if}"{/if}>{number_format($tech, 0, ",", ".")}</span>
              {if $planetID==$researchq_planet && $elementID== $researchq_id}<span class="colorNeutral"> ⬆ {$researchq_lvl}</span>{/if}
            </td>
          {else}
            <td>-</td>
          {/if}
        {/foreach}
      </tr>
    {/foreach}
    <tr>
      <th colspan="{$colspan}" onclick="toggle_rows('ships')"><span id="ships-toggle">▲</span> {$LNG.lv_ships}</th>
    </tr>
    {foreach $planetList.fleet as $elementID => $fleetArray}
      <tr class="ships" data-info="f_{$elementID}">
        <td><a href='#' onclick='return Dialog.info({$elementID})' class='tooltip'
               data-tooltip-content="<table><tr><th>{$LNG.tech.{$elementID}}</th></tr><tr><table class='hoverinfo'><tr><td><img src='{$dpath}gebaeude/{$elementID}.{if $elementID >=600 && $elementID <= 699}jpg{else}gif{/if}'></td><td>{$LNG.shortDescription.$elementID}</td></tr></table></tr></table>">{$LNG.tech.$elementID}</a>
        </td>
        <td>
          {number_format(array_sum($fleetArray), 0, ",", ".")}
          {if array_sum($planetList.currentShipyard[$elementID])!=0}
            <span class="colorNeutral"> ⬆ {number_format(array_sum($planetList.currentShipyard[$elementID]), 0, ",", ".")}</span>
          {/if}
          {if array_sum($planetList.currentFlying[$elementID])!=0}
            <span class="colorNeutral"><br>✈ {number_format(array_sum($planetList.currentFlying[$elementID]), 0, ",", ".")}</span>
          {/if}
        </td>
        {foreach $fleetArray as $planetID => $fleet}
          <td>
            <span {if $planetList.buildable.{$elementID}.{$planetID}}class="{if $enableHiglightBuildable == "true"}colorPositive{/if}"{else}class="{if $enableHiglightNonBuildable == "true"}colorNegative{/if}"{/if}>{number_format($fleet, 0, ",", ".")}</span>
            {if $planetList.currentShipyard[$elementID][$planetID]!=0}
              <span class="colorNeutral"> ⬆ {number_format($planetList.currentShipyard[$elementID][$planetID], 0, ",", ".")}</span>
            {/if}
            {if $planetList.currentFlying[$elementID][$planetID]!=0}
              <span class="colorNeutral"><br>✈ {number_format($planetList.currentFlying[$elementID][$planetID], 0, ",", ".")}</span>
            {/if}
          </td>
        {/foreach}
      </tr>
    {/foreach}
    <tr>
      <th colspan="{$colspan}" onclick="toggle_rows('defenses')"><span id="defenses-toggle">▲</span> {$LNG.lv_defenses}</th>
    </tr>
    {foreach $planetList.defense as $elementID => $defenceArray}
      <tr class="defenses" data-info="d_{$elementID}">
        <td><a href='#' onclick='return Dialog.info({$elementID})' class='tooltip'
               data-tooltip-content="<table><tr><th>{$LNG.tech.{$elementID}}</th></tr><tr><table class='hoverinfo'><tr><td><img src='{$dpath}gebaeude/{$elementID}.{if $elementID >=600 && $elementID <= 699}jpg{else}gif{/if}'></td><td>{$LNG.shortDescription.$elementID}</td></tr></table></tr></table>">{$LNG.tech.$elementID}</a>
        </td>
        <td>
          {number_format(array_sum($defenceArray), 0, ",", ".")}
          {if array_sum($planetList.currentShipyard[$elementID])!=0}
            <span class="colorNeutral"> ⬆ {number_format(array_sum($planetList.currentShipyard[$elementID]), 0, ",", ".")}</span>
          {/if}
        </td>
        {foreach $defenceArray as $planetID => $defence}
          <td>
            <span {if $planetList.buildable.{$elementID}.{$planetID}}class="{if $enableHiglightBuildable == "true"}colorPositive{/if}"{else}class="{if $enableHiglightNonBuildable == "true"}colorNegative{/if}"{/if}>{number_format($defence, 0, ",", ".")}</span>
            {if $planetList.currentShipyard[$elementID][$planetID]!=0}
              <span class="colorNeutral"> ⬆ {number_format($planetList.currentShipyard[$elementID][$planetID], 0, ",", ".")}</span>
            {/if}
          </td>
        {/foreach}
      </tr>
    {/foreach}
    <tr>
      <th colspan="{$colspan}" onclick="toggle_rows('missile')"><span id="missile-toggle">▲</span> {$LNG.tech.500}</th>
    </tr>
    {foreach $planetList.missiles as $elementID => $missileArray}
      <tr class="missile" data-info="d_{$elementID}">
        <td><a href='#' onclick='return Dialog.info({$elementID})' class='tooltip'
               data-tooltip-content="<table><tr><th>{$LNG.tech.{$elementID}}</th></tr><tr><table class='hoverinfo'><tr><td><img src='{$dpath}gebaeude/{$elementID}.{if $elementID >=600 && $elementID <= 699}jpg{else}gif{/if}'></td><td>{$LNG.shortDescription.$elementID}</td></tr></table></tr></table>">{$LNG.tech.$elementID}</a>
        </td>
        <td>
          {number_format(array_sum($missileArray), 0, ",", ".")}
          {if array_sum($planetList.currentShipyard[$elementID])!=0}
            <span class="colorNeutral"> ⬆ {number_format(array_sum($planetList.currentShipyard[$elementID]), 0, ",", ".")}</span>
          {/if}
          {if array_sum($planetList.currentFlying[$elementID])!=0}
            <span class="colorNeutral"><br>✈ {number_format(array_sum($planetList.currentFlying[$elementID]), 0, ",", ".")}</span>
          {/if}
        </td>
        {foreach $missileArray as $planetID => $missile}
          <td>
            <span {if $planetList.buildable.{$elementID}.{$planetID}}class="{if $enableHiglightBuildable == "true"}colorPositive{/if}"{else}class="{if $enableHiglightNonBuildable == "true"}colorNegative{/if}"{/if}>{number_format($missile, 0, ",", ".")}</span>
            {if $planetList.currentShipyard[$elementID][$planetID]!=0}
              <span class="colorNeutral"> ⬆ {number_format($planetList.currentShipyard[$elementID][$planetID], 0, ",", ".")}</span>
            {/if}
            {if $planetList.currentFlying[$elementID][$planetID]!=0}
              <span class="colorNeutral"><br>✈ {number_format($planetList.currentFlying[$elementID][$planetID], 0, ",", ".")}</span>
            {/if}
          </td>
        {/foreach}
      </tr>
    {/foreach}
    </tbody>
  </table>
{/block}
