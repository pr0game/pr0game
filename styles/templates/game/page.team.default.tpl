{block name="title" prepend}Team{/block}
{nocache}
{block name="content"}
	<style>
		content {
			max-width: unset;
		}
	</style>
	<h1>{$LNG.team}</h1>
	{* <h3>{$LNG.teamAktiv}</h3> *}
	<h4>{$LNG.kontakt}</h4>
	<table>
		<tr>
			<th colspan="2">{$LNG.teamIngameName}</th>
			<th>{$LNG.teamDiscordName}</th>
			<th>{$LNG.teamRole}</th>
			<th>{$LNG.teamPlayer}</th>
			<th>{$LNG.teamCommunity}</th>
			<th>{$LNG.teamAdmin}</th>
			<th>{$LNG.teamDatabase}</th>
			<th>{$LNG.teamTickets}</th>
			<th>{$LNG.teamScripte}</th>
		</tr>
		<tr>
			{if $idSchmopfi != 0}
				<td><a href="#" onclick="return Dialog.PM({$idSchmopfi});"><img src="{$dpath}img/m.gif" border="0"
							title="{$LNG.write_message}"></a></td>
			{else}
				<td></td>
			{/if}
			<td>Schmopfi</td>
			<td>Schmopfi</td>
			<td>{$LNG.teamCommunity}</td>
			<td>{$LNG.teamYes}</td>
			<td>{$LNG.teamYes}</td>
			<td>{$LNG.teamNo}</td>
			<td>{$LNG.teamNo}</td>
			<td>{$LNG.teamNo}</td>
			<td>{$LNG.teamNo}</td>
		</tr>
		<tr>
			{if $idCornag != 0}
				<td><a href="#" onclick="return Dialog.PM({$idCornag});"><img src="{$dpath}img/m.gif" border="0"
							title="{$LNG.write_message}"></a></td>
			{else}
				<td></td>
			{/if}
			<td>Cornag</td>
			<td>Cornag</td>
			<td>{$LNG.teamCommunity}</td>
			<td>{$LNG.teamYes}</td>
			<td>{$LNG.teamYes}</td>
			<td>{$LNG.teamNo}</td>
			<td>{$LNG.teamNo}</td>
			<td>{$LNG.teamNo}</td>
			<td>{$LNG.teamNo}</td>
		</tr>
		<tr>
			{if $idQualle != 0}
				<td><a href="#" onclick="return Dialog.PM({$idQualle});"><img src="{$dpath}img/m.gif" border="0"
							title="{$LNG.write_message}"></a></td>
			{else}
				<td></td>
			{/if}
			<td>Qualle</td>
			<td>WirklichNichtMotzi</td>
			<td>{$LNG.teamCommunity}</td>
			<td>{$LNG.teamNo}</td>
			<td>{$LNG.teamYes}</td>
			<td>{$LNG.teamNo}</td>
			<td>{$LNG.teamNo}</td>
			<td>{$LNG.teamNo}</td>
			<td>{$LNG.teamNo}</td>
		</tr>
		<tr>
			{if $idReflexrecon != 0}
				<td><a href="#" onclick="return Dialog.PM({$idReflexrecon});"><img src="{$dpath}img/m.gif" border="0"
							title="{$LNG.write_message}"></a></td>
			{else}
				<td></td>
			{/if}
			<td>reflexrecon</td>
			<td>Reflexrecon</td>
			<td>{$LNG.teamDev}, {$LNG.teamEmergency}, {$LNG.teamMod}</td>
			<td>{$LNG.teamYes}</td>
			<td>{$LNG.teamNo}</td>
			<td>{$LNG.teamYes}</td>
			<td>{$LNG.teamYes}</td>
			<td>{$LNG.teamYes}</td>
			<td>{$LNG.teamNo}</td>
		</tr>
		<tr>
			{if $idAdman != 0}
				<td><a href="#" onclick="return Dialog.PM({$idAdman});"><img src="{$dpath}img/m.gif" border="0"
							title="{$LNG.write_message}"></a></td>
			{else}
				<td></td>
			{/if}
			<td>Adman</td>
			<td>Hyman/Adman</td>
			<td>{$LNG.teamDev}, {$LNG.teamEmergency}, {$LNG.teamMod}</td>
			<td>{$LNG.teamYes}</td>
			<td>{$LNG.teamNo}</td>
			<td>{$LNG.teamYes}</td>
			<td>{$LNG.teamYes}</td>
			<td>{$LNG.teamYes}</td>
			<td>{$LNG.teamYes}</td>
		</tr>
		<tr>
			{if $idTimoKa != 0}
				<td><a href="#" onclick="return Dialog.PM({$idTimoKa});"><img src="{$dpath}img/m.gif" border="0"
							title="{$LNG.write_message}"></a></td>
			{else}
				<td></td>
			{/if}
			<td>Timo_Ka</td>
			<td>Timo_Ka</td>
			<td>{$LNG.teamDev}, {$LNG.teamEmergency}, {$LNG.teamMod}</td>
			<td>{$LNG.teamYes}</td>
			<td>{$LNG.teamNo}</td>
			<td>{$LNG.teamYes}</td>
			<td>{$LNG.teamYes}</td>
			<td>{$LNG.teamYes}</td>
			<td>{$LNG.teamYes}</td>
		</tr>
		<tr>
			{if $idHackbrett != 0}
				<td><a href="#" onclick="return Dialog.PM({$idHackbrett});"><img src="{$dpath}img/m.gif" border="0"
							title="{$LNG.write_message}"></a></td>
			{else}
				<td></td>
			{/if}
			<td>Hackmin</td>
			<td>Hackbrett</td>
			<td>{$LNG.teamMod}</td>
			<td>{$LNG.teamYes}</td>
			<td>{$LNG.teamNo}</td>
			<td>{$LNG.teamYes}</td>
			<td>{$LNG.teamNo}</td>
			<td>{$LNG.teamYes}</td>
			<td>{$LNG.teamNo}</td>
		</tr>
		<tr>
			{if $idMasterspiel != 0}
				<td><a href="#" onclick="return Dialog.PM({$idMasterspiel});"><img src="{$dpath}img/m.gif" border="0"
							title="{$LNG.write_message}"></a></td>
			{else}
				<td></td>
			{/if}
			<td>Masterspiel</td>
			<td>Masterspiel</td>
			<td>{$LNG.teamMod}</td>
			<td>{$LNG.teamNo}</td>
			<td>{$LNG.teamNo}</td>
			<td>{$LNG.teamYes}</td>
			<td>{$LNG.teamNo}</td>
			<td>{$LNG.teamYes}</td>
			<td>{$LNG.teamNo}</td>
		</tr>
		<tr>
			{if $idPara != 0}
				<td><a href="#" onclick="return Dialog.PM({$idPara});"><img src="{$dpath}img/m.gif" border="0"
							title="{$LNG.write_message}"></a></td>
			{else}
				<td></td>
			{/if}
			<td>para Admin</td>
			<td>para bellum</td>
			<td>{$LNG.teamMod}</td>
			<td>{$LNG.teamYes}</td>
			<td>{$LNG.teamNo}</td>
			<td>{$LNG.teamYes}</td>
			<td>{$LNG.teamNo}</td>
			<td>{$LNG.teamYes}</td>
			<td>{$LNG.teamNo}</td>
		</tr>
		<tr>
			{if $idAtain != 0}
				<td><a href="#" onclick="return Dialog.PM({$idAtain});"><img src="{$dpath}img/m.gif" border="0"
							title="{$LNG.write_message}"></a></td>
			{else}
				<td></td>
			{/if}
			<td>Atain</td>
			<td>Atain</td>
			<td>{$LNG.teamDev}</td>
			<td>{$LNG.teamYes}</td>
			<td>{$LNG.teamNo}</td>
			<td>{$LNG.teamNo}</td>
			<td>{$LNG.teamNo}</td>
			<td>{$LNG.teamNo}</td>
			<td>{$LNG.teamYes}</td>
		</tr>
		<tr>
			{if $idMrgl != 0}
				<td><a href="#" onclick="return Dialog.PM({$idMrgl});"><img src="{$dpath}img/m.gif" border="0"
							title="{$LNG.write_message}"></a></td>
			{else}
				<td></td>
			{/if}
			<td>Captain Mrgl</td>
			<td>Captain Mrgl</td>
			<td>{$LNG.teamDev}</td>
			<td>{$LNG.teamYes}</td>
			<td>{$LNG.teamNo}</td>
			<td>{$LNG.teamNo}</td>
			<td>{$LNG.teamNo}</td>
			<td>{$LNG.teamNo}</td>
			<td>{$LNG.teamNo}</td>
		</tr>

	</table>

	<h3>{$LNG.teamAlumni}</h3>
	<p>{$LNG.teamAlumniLong}: ava, Axel auf dem Floß, DawnOfTheUwe, Eichhorn, Fionera, Klarname, Rosenreemann & Slippy</p>

	<h4>{$LNG.teamLegend}:</h4>
	<table>
		<tr>
			<td>
				{$LNG.teamMod}
			</td>
			<td>
				{$LNG.teamModLong}
			</td>
		</tr>
		<tr>
			<td>
				{$LNG.teamEmergency}
			</td>
			<td>
				{$LNG.teamEmergencyLong}
			</td>
		</tr>
		<tr>
			<td>
				{$LNG.teamCommunity}
			</td>
			<td>
				{$LNG.teamCommunityLong}
			</td>
		</tr>
		<tr>
			<td>
				{$LNG.teamDev}
			</td>
			<td>
				{$LNG.teamDevLong}
			</td>
		</tr>
		<tr>
			<td>
				{$LNG.teamAdmin}
			</td>
			<td>
				{$LNG.teamAdminLong}
			</td>
		</tr>
		<tr>
			<td>
				{$LNG.teamTickets}
			</td>
			<td>
				{$LNG.teamTicketsLong}
			</td>
		</tr>
		<tr>
			<td>
				{$LNG.teamScripte}
			</td>
			<td>
				{$LNG.teamScripteLong}
			</td>
		</tr>
	</table>
{/block}
{/nocache}