{if !empty($tutorial)}
    <div id="tutorial-header" class="tutorial-header">
        <span id="tutorial-toggle">▲</span> <b>Tutorial:</b> {$tutorial.title}
    </div>
    <div id="tutorial" class="tutorial">
        <div class="tutorial-text">
            {$tutorial.text}<br>
        </div>
        <div class="tutorial-goals">
            {if !empty($tutorial.goals)}
                <b>{$LNG.tutorial_goals_header}</b><br>
                <ul>
                    {foreach $tutorial.goals as $elementID => $goal}
                        <li>{$goal.name}&nbsp;:&nbsp;{$goal.owned}/{$goal.ammount}{if $goal.skippable} <a href="?page=tutorial&amp;mode=skip&amp;goal={$elementID}">{$LNG.tutorial_goals_ignore}</a>{/if}</li>
                    {/foreach}
                </ul>
            {/if}
            {if !empty($tutorial.rewards)}
                <b>{$LNG.tutorial_rewards_header}</b><br>
                <ul>
                    {foreach $tutorial.rewards as $elementID => $reward}
                        <li>{$reward.name}&nbsp;:&nbsp;{$reward.ammount}</li>
                    {/foreach}
                </ul>
            {/if}
        </div>
        <div class="tutorial-buttons">
            {if $tutorial.fullfilled && $tutorial.last}
                <a href="?page=tutorial&amp;mode=claim"><button>{$LNG.tutorial_buttons_finalize}</button></a>
            {else if $tutorial.fullfilled && !empty($tutorial.rewards)}
                <a href="?page=tutorial&amp;mode=claim"><button>{$LNG.tutorial_buttons_claim}</button></a>
            {else if $tutorial.fullfilled}
                <a href="?page=tutorial&amp;mode=claim"><button>{$LNG.tutorial_buttons_continue}</button></a>
            {else if $tutorial.skippable}
                <a href="?page=tutorial&amp;mode=skip"><button>{$LNG.tutorial_buttons_skip}</button></a>
                &nbsp;
                <a href="?page=tutorial&amp;mode=skipAll"><button onclick="return confirm('{$LNG.tutorial_buttons_skipAll_confirm}');">{$LNG.tutorial_buttons_skipAll}</button></a>
            {else}
                <a href="?page=tutorial&amp;mode=skipAll"><button onclick="return confirm('{$LNG.tutorial_buttons_skipAll_confirm}');">{$LNG.tutorial_buttons_skipAll}</button></a>
            {/if}
        </div>
    </div>
{/if}
{block name="script" append}
    <script>
        if (localStorage.getItem('tutorial') === null) {
            localStorage.setItem('tutorial', 'false'); // Default to visible
        }
        document.addEventListener("DOMContentLoaded", (event) => {
            const tutorialElement = document.getElementById('tutorial');
            const tutorialToggle = document.getElementById('tutorial-toggle');
            if (tutorialToggle && tutorialElement) {
                let tutorialHidden = localStorage.getItem('tutorial') === 'true';
                tutorialToggle.innerText = tutorialHidden ? "▼" : "▲";
                if (tutorialHidden) {
                    tutorialElement.classList.add("hidden");
                } else {
                    tutorialElement.classList.remove("hidden");
                }

                document.getElementById('tutorial-header').addEventListener("click", (event) => {
                    let newstate = localStorage.getItem('tutorial') === 'false';
                    tutorialToggle.innerText = newstate ? "▼" : "▲";
                    if (newstate) {
                        tutorialElement.classList.add("hidden");
                    } else {
                        tutorialElement.classList.remove("hidden");
                    }
                    localStorage.setItem('tutorial', newstate.toString());
                });
            }
        });
    </script>
{/block}