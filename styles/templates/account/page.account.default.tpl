
{block name="title" prepend}
{$LNG.Achievements}{/block}

{block name="content"}
	
<style>
    button:disabled ! {
        background-color: light-dark(rgba(239, 239, 239, 0.3), rgba(19, 1, 1, 0.3));
        color: light-dark(rgba(16, 16, 16, 0.3), rgba(255, 255, 255, 0.3));
        border-color: light-dark(rgba(118, 118, 118, 0.3), rgba(195, 195, 195, 0.3));
    }
    .UniverseHead {
        border: 1px solid black;
        text-align: center;
        padding: 8px 4px 8px 4px;
        margin-bottom: 10px;
    }
    .Universes {
        border: 1px solid black;
        display: grid;
        grid-template-columns: repeat(8, auto);
        margin-bottom: 10px;
        padding: 8px 4px 8px 4px;
    }
    .UniversesNew {
        border: 1px solid black;
        display: grid;
        grid-template-columns: repeat(5, auto);
        margin-bottom: 10px;
        padding: 8px 4px 8px 4px;
    }
    .Universe {
        display: contents;
    }
    .Uni{
        padding: 8px 4px;
        background: rgba(13, 16, 20, 0.95);
    }
</style>
<div>
    <div class="contentPage">
        <div class="contentBox">
            <div class="contentBodyOwnUniverses">
                <div class="UniverseHead">{$LNG["Your_universes"]}</div>
                <div class="Universes">
                    <div class="Universe" id="Head">
                        <div class="Uni Name"><strong> {$LNG['Universes']} </strong></div>
                        <div class="Uni Username"><strong>{$LNG["Playername"]}</strong></div>
                        <div class="Uni Rank"><strong>{$LNG["al_position"]}</strong></div>
                        <div class="Uni Age"><strong>{$LNG["Universe_start"]}</strong></div>
                        <div class="Uni End"><strong>{$LNG["Universe_end"]}</strong></div>
                        <div class="Uni Sitting"></div>
                        <div class="Uni SittingEnd"><strong>{$LNG["Sitting_end"]}</strong></div>
                        <div class="Uni Play"></div>
                    </div>
                    {foreach from=$playerUniverses item=universe}
                        <div class="Universe" id="{$universe["uni"]}">
                                <div class="Uni Name">{$universe["uni_name"]}</div>
                                <div class="Uni Username">{$universe["username"]}</div>
                                <div class="Uni Rank">{$universe["total_rank"]}</div>
                                <div class="Uni Age">{$universe["start"]}</div>
                                <div class="Uni End">{$universe["end"]}</div>
                                <div class="Uni Sitting">
                                    <button class="sitting" disabled style="background-color: rgba(19, 1, 1, 0.3); color: rgba(255, 255, 255, 0.3); border-color: rgba(195, 195, 195, 0.3);">{$LNG["Sitting_request"]}</button>
                                </div>
                                <div class="Uni SittingEnd">-</div>
                                <form action="account.php?page=account" method="post" class="Universe">
                                    <input type="hidden" name="mode" value="send">
                                    <input type="hidden" name="universe" value="{$universe["uni"]}">
                                    <input type="hidden" name="userID" value="{$universe["user_id"]}">
                                    {if $universe["uni_status"] == STATUS_OPEN || $universe["uni_status"] == STATUS_LOGIN_ONLY || $universe["authlevel"] == AUTH_ADM}
                                        <input type="hidden" name="action" value="login">
                                        <div class="Uni Play"><input value="{$LNG["Play"]}" type="submit"></div>
                                    {else}
                                        <input type="hidden" name="action" value="logout">
                                        <div class="Uni Play"><button>{$LNG["uni_status_regclosed_gameclosed"]}</button></div>
                                    {/if}
                                </form>
                        </div>
                    {/foreach}
                </div>
            </div>
            <div class="contentBodyNewUniverses">
                <div class="UniverseHead">{$LNG["Other_universes"]}</div>
                <div class="UniversesNew">
                    <div class="Universe" id="Head">
                        <div class="Uni Name"><strong> {$LNG['Universes']} </strong></div>
                        <div class="Uni Playercount"><strong>{$LNG["Active_players"]}</strong></div>
                        <div class="Uni Age"><strong>{$LNG["Universe_start"]}</strong></div>
                        <div class="Uni End"><strong>{$LNG["Universe_end"]}</strong></div>
                        <div class="Uni Register"></div>
                    </div>
                    {foreach from=$newUniverses item=universe}
                        <div class="Universe" id="{$universe["uni"]}">
                            <div class="Uni Name">{$universe["uni_name"]}</div>
                            <div class="Uni Playercount">{($universe["active"]=="") ? 0 : $universe["active"]}</div>
                            <div class="Uni Age">{$universe["start"]}</div>
                            <div class="Uni End">{$universe["end"]}</div>
                            {if $universe["uni_status"] == STATUS_OPEN || $universe["uni_status"] == STATUS_REG_ONLY || $authlevel == AUTH_ADM}
                                <div class="Uni Register"><a href="account.php?page=register&uni={$universe["uni"]}"><b>{$LNG["Register"]}</b></a></div>
                            {else}
                                <div class="Uni Register"><button>{$LNG["uni_status_regclosed_gameclosed"]}</button></div>
                            {/if}
                        </div>
                    {/foreach}
                </div>
                <div class="UniverseHead">
                    <div class="Uni Register"><a href="account.php?page=logout" style="border: 1px solid black;">{$LNG.lm_logout}</a></div>
                </div>
            </div>
        </div>
    </div>
</div>
{/block}
