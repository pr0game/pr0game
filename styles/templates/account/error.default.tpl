{block name="title" prepend}{$LNG.fcm_info}{/block}
{block name="content"}
<style>
    .UniverseHead {
        border: 1px solid black;
        text-align: center;
        padding: 8px 4px 8px 4px;
        margin-bottom: 10px;
    }
    .Universes {
        border: 1px solid black;
        display: block;
        margin-bottom: 10px;
        padding: 8px 4px 8px 4px;
    }
    .UniversesNew {
        margin-bottom: 10px;
        padding: 8px 4px 8px 4px;
    }
    .Universe {
        background: rgba(13, 16, 20, 0.95);
        display: block;
        border: 1px solid black;
		text-align: center;
    }
</style>
<div>
    <div class="contentPage">
        <div class="contentBox">
            <div class="contentBodySettings">
                <div class="UniverseHead">{$LNG.fcm_info}</div>
				<div class="Universe" id="Head">
					<p>{$message}</p>{if !empty($redirectButtons)}<p>{foreach $redirectButtons as $button}<a href="{$button.url}"><button>{$button.label}</button></a>{/foreach}</p>{/if}
				</div>
			</div>
		</div>
	</div>
</div>
{/block}