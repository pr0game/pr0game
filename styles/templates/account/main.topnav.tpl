<label class="hamburger" for="toggle-menu" class="toggle-menu"><i class="fas fa-bars"></i></label>

<div class="top">
   <div id="username">{$accountname}</div>
   
</div>
<div id="settings"><a href="{$toplink}"><b>{$topname}</b></a></div>
<style>
#username {
  /* border: 1px solid black; */
  border-bottom: 1px solid black;
  position:absolute;
  top: 72px;
  left: 50%;
  transform: translate(-50%, -50%);
  font-size: 40px;
}
#settings {
  position:absolute;
  top: 0px;
  right: 0px;
  font-size: 20px;
}
</style>
