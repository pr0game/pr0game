<style>
  .footer {
	bottom: 5;
	position: absolute;
	width:90%;
	display: inline;
	text-align: center;
	box-sizing: border-box;
  }
  .footer hr {
	box-sizing: inherit;
	margin-left: auto;
    margin-right: auto;
	align:center;
	width: 90%; 
	margin: 0 4% 0 5%;
  }
</style>

{if $use_google_analytics}
<script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', '{$google_analytics_key}']);
	_gaq.push(['_trackPageview']);

	(function() {
	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
</script>
{/if}
<div class="footer">
	<hr style = "width:100%;">
	<a href="index.php?page=dataPrivacy" target="dataPrivacy">{$LNG.lm_dataPrivacy}</a>
</div>
{if $debug == 1}
<script type="text/javascript">
	onerror = handleErr;
</script>
{/if}