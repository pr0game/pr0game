{*include file="main.header.tpl" bodyclass="full"*}
<link rel="stylesheet" type="text/css" href="{$dpath}formate.css">
<style>
	.infobox {
		border: 2px solid red;
		padding: 2px 5px;
		text-align: center;
	}
</style>
<div class="wrapper">

	<top>
		<div class="fixed">
		</div>
	</top>

	<logo>
		<div class="fixed">
			<a href="account.php?page=account"><img src="styles/resource/images/pr0game.gif" /></a>
		</div>
	</logo>
	
	<header>
		<div class="fixed">
			{include file="main.topnav.tpl"}
		</div>
	</header>
	
	<content>
        {if $delete}
            <div class="infobox">{$delete}</div>
        {/if}
		{block name="content"}{/block}
	</content>

	<footer>
		{include file="main.footer.tpl"}
	</footer>
</div>

</body>
</html>