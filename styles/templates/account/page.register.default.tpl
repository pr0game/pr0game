{block name="title" prepend}{$LNG.siteTitleRegister}{/block}
{block name="content"}
<style>
	.UniverseHead {
        border: 1px solid black;
        text-align: center;
        padding: 8px 4px 8px 4px;
        margin-bottom: 10px;
    }
    .Universes {
        border: 1px solid black;
        display: grid;
        grid-template-columns: repeat(8, auto);
        margin-bottom: 10px;
        padding: 8px 4px 8px 4px;
    }
    .UniversesNew {
        margin-bottom: 10px;
        padding: 8px 4px 8px 4px;
    }
    .Universe {
        background: rgba(13, 16, 20, 0.95);
        display: grid;
        border: 1px solid black;
        grid-template-columns: repeat(2, auto);
    }
</style>
<div>
    <div class="contentPage">
        <div class="contentBox">
            <div class="contentBodySettings">
                <div class="UniverseHead">{$LNG["Register"]}</div>
                <form action="account.php?page=register" method="post">
                    <div class="Universe" id="Head">
                        <input type="hidden" name="mode" value="send">
                        <input type="hidden" name="uni" value={$uniId}>
                        <div class="UniStatusLabel">{$LNG["Universe_status"]} </div>
                        <div class="UniStatus">{$status}</div>
                        <div class="username">{$LNG["Playername"]}: </div> 
                        <input name="username" maxlength="32" size="20" value="" type="text">
                        <div class="Lang">{$LNG['op_lang']}: </div> 
                        {html_options name=language options=$Selectors.lang selected=$userLang}
                        <div class="Dpath">{$LNG['op_skin_example']}: </div> 
                        {html_options options=$Selectors.Skins selected=$theme name="theme" id="theme"}
                        <div class="Timezone">{$LNG['op_timezone']}: </div> 
                        {html_options name=timezone options=$Selectors.timezones selected=$timezone}
                        <div class="colors">{$LNG.own_colors}: </div> 
                        {html_options name=colors options=$defaultUnis selected=$default}
                        <div class="missionPrios">{$LNG.fleetPrio}: </div> 
                        {html_options name=mission options=$defaultUnis selected=$default}
                        <div class="stbSettings">{$LNG['stb_title']}: </div> 
                        {html_options name=stb options=$defaultUnis selected=$default}
                        <div class="Register">
                            <input value="{$LNG['Register']}" type="submit">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
{/block}
{block name="script" append}
<link rel="stylesheet" type="text/css" href="styles/resource/css/login/register.css">
<script type="text/javascript" src="scripts/login/register.js"></script>
{/block}
