
{block name="title" prepend}
{$LNG.Achievements}{/block}

{block name="content"}
	
<style>
    .UniverseHead {
        border: 1px solid black;
        text-align: center;
        padding: 8px 4px 8px 4px;
        margin-bottom: 10px;
    }
    .Universes {
        border: 1px solid black;
        display: grid;
        grid-template-columns: repeat(8, auto);
        margin-bottom: 10px;
        padding: 8px 4px 8px 4px;
    }
    .UniversesNew {
        margin-bottom: 10px;
        padding: 8px 4px 8px 4px;
    }
    .Universe {
        background: rgba(13, 16, 20, 0.95);
        display: grid;
        border: 1px solid black;
        grid-template-columns: repeat(2, auto);
    }
    .Delete {
        background: rgba(13, 16, 20, 0.95);
        display: block;
        border: 1px solid black;
        margin: auto 0;
        position: absolute;
        left: 50%;
        -ms-transform: translateX(-50%);
        transform: translateX(-50%);
    }
</style>
<div>
    <div class="contentPage">
        <div class="contentBox">
            <div class="contentBodySettings">
                <div class="UniverseHead">{$LNG["Settings"]}</div>
                    <form action="account.php?page=accountSettings" method="post">
                        <div class="Universe" id="Head">
                            <input type="hidden" name="mode" value="send">
                            <div class="Password">{$LNG['op_old_pass']}: </div> <input name="password" size="20" type="password" class="autocomplete">
                            <div class="Password">{$LNG['op_new_pass']}: </div> <input name="newpassword" size="20" maxlength="40" type="password" class="autocomplete">
                            <div class="Password">{$LNG['op_repeat_new_pass']}: </div> <input name="newpassword2" size="20" maxlength="40" type="password" class="autocomplete">
                            <div class="Mail">{$LNG['op_email_adress']}: </div> <input name="email" maxlength="64" size="20" value="{*$email*}" type="email" maxlength="64">
                            <div class="Lang">{$LNG['op_lang']}: </div> {html_options name=language options=$Selectors.lang selected=$userLang}
                            <div class="Dpath">{$LNG['op_skin_example']}: </div> {html_options options=$Selectors.Skins selected=$theme name="theme" id="theme"}
                            <div class="Timezone">{$LNG['op_timezone']}: </div> {html_options name=timezone options=$Selectors.timezones selected=$timezone}
                            <div class="Save"><input value="{$LNG['op_save_changes']}" type="submit"></div>
                        </div>
                    </form>
                </div>
                <form action="account.php?page=accountSettings" method="post">
                    <input type="hidden" name="mode" value="toggleDelete">
                        <div class="Delete" >
						    
						    <input name="delete" type="submit" value="{if $delete}{$LNG.op_stop_dlte_account}{else}{$LNG.op_dlte_account}{/if}">
                        </div>
					</form>
            </div>
        </div>
    </div>
</div>
{/block}
