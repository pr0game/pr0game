{block name="title" prepend}{$LNG.siteTitleRegister}{/block}
{block name="content"}
<div id="registerFormWrapper">
<form id="registerForm" method="post" action="index.php?page=register" data-action="index.php?page=register">
<input type="hidden" value="send" name="mode">
<input type="hidden" value="{$referralData.id}" name="referralID">
	<div class="rowForm">
		<label for="email">{$LNG.registerEmail}</label>
		<input type="email" class="input" name="email" id="email" maxlength="64">
		{if !empty($error.email)}<span class="error errorEmail"></span>{/if}
		<span class="inputDesc">{$LNG.registerEmailDesc}</span>
	</div>
	<div class="rowForm">
		<label for="emailReplay">{$LNG.registerEmailReplay}</label>
		<input type="email" class="input" name="emailReplay" id="emailReplay" maxlength="64">
		{if !empty($error.emailReplay)}<span class="error errorEmailReplay"></span>{/if}
		<span class="inputDesc">{$LNG.registerEmailReplayDesc}</span>
	</div>
	<div class="rowForm">
		<label for="username">{$LNG.registerUsername}</label>
		<input type="text" class="input" name="username" id="username" maxlength="32">
		{if !empty($error.username)}<span class="error errorUsername"></span>{/if}
		<span class="inputDesc">{$LNG.registerUsernameDesc}</span>
	</div>
	<div class="rowForm">
		<label for="password">{$LNG.registerPassword}</label>
		<input type="password" class="input" name="password" id="password">
		{if !empty($error.password)}<span class="error errorPassword"></span>{/if}
		<span class="inputDesc">{$registerPasswordDesc}</span>
	</div>
	<div class="rowForm">
		<label for="passwordReplay">{$LNG.registerPasswordReplay}</label>
		<input type="password" class="input" name="passwordReplay" id="passwordReplay">
		{if !empty($error.passwordReplay)}<span class="error errorPasswordReplay"></span>{/if}
		<span class="inputDesc">{$LNG.registerPasswordReplayDesc}</span>
	</div>
	{if count($languages) > 1}
		<div class="rowForm">
			<label for="language">{$LNG.registerLanguage}</label>
			<select name="lang" id="language">{html_options options=$languages selected=$lang}</select>
			{if !empty($error.language)}<span class="error errorLanguage"></span>{/if}
			<div class="clear"></div>
		</div>
	{/if}
	{if !empty($referralData.name)}
		<div class="rowForm">
			<label for="language">{$LNG.registerReferral}</label>
			<span class="text">{$referralData.name}</span>
			{if !empty($error.language)}<span class="error errorLanguage"></span>{/if}
			<div class="clear"></div>
		</div>
	{/if}
	{if count($timezones) > 1}
		<div class="rowForm">
			<label for="timezone">{$LNG.registerTimezone}</label>
			<select name="time" id="timezone">{html_options options=$timezones}</select>
			<div class="clear"></div>
		</div>
	{/if}
	<div class="rowForm">
		<label for="rules">{$LNG.registerRules}</label>
		<input type="checkbox" name="rules" id="rules" value="1">
		{if !empty($error.rules)}<span class="error errorRules"></span>{/if}
		<span class="inputDesc">{$registerRulesDesc}</span>
	</div>
	<div class="rowForm">
		<label for="rules">{$LNG.siteTitleDataPrivacy}</label>
		<input type="checkbox" name="privacy" id="privacy" value="1">
		{if !empty($error.dataPrivacy)}<span class="error errordataPrivacy"></span>{/if}
		<span class="inputDesc">{$registerDataPrivacyDesc}</span>
	</div>
	<div class="rowForm">
		<input type="submit" class="submitButton" value="{$LNG.buttonRegister}">
	</div>
</form>
<script type="text/javascript">
	const tz = Intl.DateTimeFormat().resolvedOptions().timeZone;
	document.getElementById('timezone').value = tz;
</script>
{/block}
{block name="script" append}
<link rel="stylesheet" type="text/css" href="styles/resource/css/login/register.css">
<script type="text/javascript" src="scripts/login/register.js"></script>
{/block}
