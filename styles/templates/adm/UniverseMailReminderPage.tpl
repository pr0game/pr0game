{include file="overall_header.tpl"}
<table class="table569">
    {if $error}
        <tr><td>{$error}</td></tr>
    {else}
        <form name="message" id="message" action="admin.php?page=universeMailReminder&action=send" method="post">
        <tr>
            <td>{$LNG.mr_timestamp}</td>
            <td><input id="opentime" type="text" name="opentime" value="{$opentime}" oninput="updateDiv()"></td>
            <td id="readableTime">{$readableTime}</td>
        </tr>
        <tr>
            <td colspan="3"><input type="submit" value="{$LNG.mr_send}"></td>
        </tr>
        </form>
    {/if}
</table>
{include file="overall_footer.tpl"}

<script>
    function updateDiv() {
        // Get the input value
        const inputValue = document.getElementById("opentime").value;
        const timestamp = parseInt(inputValue) * 1000;

        // Check if the timestamp is valid
        if (!isNaN(timestamp)) {
            // Create a Date object from the timestamp
            const date = new Date(timestamp);

            // Convert it to a local time string
            const localTimeString = date.toLocaleString();

            // Update the div with the local time string
            document.getElementById("readableTime").innerText = localTimeString;
        } else {
            // If input is not a valid number, show an error message
            document.getElementById("readableTime").innerText = "Invalid timestamp!";
        }
        // Set the div's content to the input value
        document.getElementById("readableTime").innerText = inputValue;
    }
</script>