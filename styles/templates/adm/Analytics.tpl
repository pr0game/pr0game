{include file="overall_header.tpl"}
<table width="100%" border="0px">
	<td style="border:0px;width:50%" class="transparent">
		<form action="" method="POST" name="users">
			<table align="center" width="90%">
				<tr>
					<th>{$LNG.an_user_list}</th>
				</tr>
				<tr>
					<td>
						<select name="user_name" style="width:70%; height:240px;" size="20">
							{$UserSelect.List}
						</select>
						<br>
						<a href="?page=analytics">{$LNG.bo_order_username}</a> &nbsp; <a href="?page=analytics&amp;order=id">{$LNG.bo_order_id}</a>
						<script TYPE="text/javascript">
							var UserList = new filterlist(document.getElementsByName('user_name')[0]);
						</script>
						
						<br><br>
						<a href="javascript:UserList.set('^A')" title="{$bo_select_title} A">A</A>
						<a href="javascript:UserList.set('^B')" title="{$bo_select_title} B">B</A>
						<a href="javascript:UserList.set('^C')" title="{$bo_select_title} C">C</A>
						<a href="javascript:UserList.set('^D')" title="{$bo_select_title} D">D</A>

						<a href="javascript:UserList.set('^E')" title="{$bo_select_title} E">E</A>
						<a href="javascript:UserList.set('^F')" title="{$bo_select_title} F">F</A>
						<a href="javascript:UserList.set('^G')" title="{$bo_select_title} G">G</A>
						<a href="javascript:UserList.set('^H')" title="{$bo_select_title} H">H</A>
						<a href="javascript:UserList.set('^I')" title="{$bo_select_title} I">I</A>
						<a href="javascript:UserList.set('^J')" title="{$bo_select_title} J">J</A>
						<a href="javascript:UserList.set('^K')" title="{$bo_select_title} K">K</A>
						<a href="javascript:UserList.set('^L')" title="{$bo_select_title} L">L</A>
						<a href="javascript:UserList.set('^M')" title="{$bo_select_title} M">M</A>

						<a href="javascript:UserList.set('^N')" title="{$bo_select_title} N">N</A>
						<a href="javascript:UserList.set('^O')" title="{$bo_select_title} O">O</A>
						<a href="javascript:UserList.set('^P')" title="{$bo_select_title} P">P</A>
						<a href="javascript:UserList.set('^Q')" title="{$bo_select_title} Q">Q</A>
						<a href="javascript:UserList.set('^R')" title="{$bo_select_title} R">R</A>
						<a href="javascript:UserList.set('^S')" title="{$bo_select_title} S">S</A>
						<a href="javascript:UserList.set('^T')" title="{$bo_select_title} T">T</A>
						<a href="javascript:UserList.set('^U')" title="{$bo_select_title} U">U</A>
						<a href="javascript:UserList.set('^V')" title="{$bo_select_title} V">V</A>

						<a href="javascript:UserList.set('^W')" title="{$bo_select_title} W">W</A>
						<a href="javascript:UserList.set('^X')" title="{$bo_select_title} X">X</A>
						<a href="javascript:UserList.set('^Y')" title="{$bo_select_title} Y">Y</A>
						<a href="javascript:UserList.set('^Z')" title="{$bo_select_title} Z">Z</A>
						<br>
						<input name="regexp" onKeyUp="UserList.set(this.value)">
						<input type="button" onClick="UserList.set(this.form.regexp.value)" value="{$LNG.button_filter}">
						<input type="button" onClick="UserList.reset();this.form.regexp.value=''" value="{$LNG.button_deselect}">
					</td>
				</tr>
				<tr>
					<td>
						<input type="submit" name="userListSubmit" value="{$LNG.an_btn_enable}">&nbsp;
						<input type="button" onClick="UserList.reset();this.form.regexp.value=''" value="{$LNG.button_deselect}">
					</td>
				</tr>
				<tr>
					<td align="left">
						{$LNG.bo_total_users}<span class="colorPositive">{$usercount}</span>
					</td>
				</tr>
			</table>
		</form>
	</td>
	<td style="border:0px;width:50%;" class="transparent">
		<form action="" method="POST" name="userban">
			<table align="center" width="90%">
				<tr>
					<th>{$LNG.an_analyzed_list}</th>
				</tr>
				<tr>
					<td>
						<select name="analyse_name" style="width:70%; height:240px;" size="20">
							{$UserSelect.ListBan}
						</select>
						<br>
						<a href="?page=analytics">{$LNG.bo_order_username}</a> &nbsp; <a href="?page=analytics&amp;order2=id">{$LNG.bo_order_id}</a>
						<script TYPE="text/javascript">
							var analysedUsers = new filterlist(document.getElementsByName('analyse_name')[0]);
						</script>
						
						<br><br>
						<a href="javascript:analysedUsers.set('^A')" title="{$bo_select_title} A">A</A>
						<a href="javascript:analysedUsers.set('^B')" title="{$bo_select_title} B">B</A>
						<a href="javascript:analysedUsers.set('^C')" title="{$bo_select_title} C">C</A>
						<a href="javascript:analysedUsers.set('^D')" title="{$bo_select_title} D">D</A>

						<a href="javascript:analysedUsers.set('^E')" title="{$bo_select_title} E">E</A>
						<a href="javascript:analysedUsers.set('^F')" title="{$bo_select_title} F">F</A>
						<a href="javascript:analysedUsers.set('^G')" title="{$bo_select_title} G">G</A>
						<a href="javascript:analysedUsers.set('^H')" title="{$bo_select_title} H">H</A>
						<a href="javascript:analysedUsers.set('^I')" title="{$bo_select_title} I">I</A>
						<a href="javascript:analysedUsers.set('^J')" title="{$bo_select_title} J">J</A>
						<a href="javascript:analysedUsers.set('^K')" title="{$bo_select_title} K">K</A>
						<a href="javascript:analysedUsers.set('^L')" title="{$bo_select_title} L">L</A>
						<a href="javascript:analysedUsers.set('^M')" title="{$bo_select_title} M">M</A>

						<a href="javascript:analysedUsers.set('^N')" title="{$bo_select_title} N">N</A>
						<a href="javascript:analysedUsers.set('^O')" title="{$bo_select_title} O">O</A>
						<a href="javascript:analysedUsers.set('^P')" title="{$bo_select_title} P">P</A>
						<a href="javascript:analysedUsers.set('^Q')" title="{$bo_select_title} Q">Q</A>
						<a href="javascript:analysedUsers.set('^R')" title="{$bo_select_title} R">R</A>
						<a href="javascript:analysedUsers.set('^S')" title="{$bo_select_title} S">S</A>
						<a href="javascript:analysedUsers.set('^T')" title="{$bo_select_title} T">T</A>
						<a href="javascript:analysedUsers.set('^U')" title="{$bo_select_title} U">U</A>
						<a href="javascript:analysedUsers.set('^V')" title="{$bo_select_title} V">V</A>

						<a href="javascript:analysedUsers.set('^W')" title="{$bo_select_title} W">W</A>
						<a href="javascript:analysedUsers.set('^X')" title="{$bo_select_title} X">X</A>
						<a href="javascript:analysedUsers.set('^Y')" title="{$bo_select_title} Y">Y</A>
						<a href="javascript:analysedUsers.set('^Z')" title="{$bo_select_title} Z">Z</A>

						<br>
						<input name="regexp" onKeyUp="analysedUsers.set(this.value)">
						<input type="button" onClick="analysedUsers.set(this.form.regexp.value)" value="{$LNG.button_filter}">
						<input type="button" onClick="analysedUsers.set(this.form.regexp.value)" value="{$LNG.button_deselect}">
					</td>
				</tr>
				<tr>
					<td>
						<input type="submit" name="analysedUsersSubmit" value="{$LNG.an_btn_disable}">&nbsp;
						<input type="button" onClick="analysedUsers.reset();this.form.regexp.value=''" value="{$LNG.button_deselect}">
					</td>
				</tr>
				<tr>
					<td align="left">
						{$LNG.an_total_analyzed}<span class="colorPositive">{$bancount}</span>
					</td>
				</tr>
			</table>
		</form>
	</td>
</table>

<table style="width:760px">
	<tr>
		<th>{$LNG.input_id_user}</th>
        <th>{$LNG.ac_name}</th>
        <th>{$LNG.ac_alliance}</th>
		<th>{$LNG.an_timestamp}</th>
		<th>URL</th>
	</tr>
	{foreach $analytics as $Users}
	<tr>
		<td class="left" style="padding:3px;">{$Users.userID}</td>
		<td class="left" style="padding:3px;"><a href="admin.php?page=accountdata&id_u={$Users.userID}">{$Users.username}</a></td>
		<td class="left" style="padding:3px;">{$Users.ally_name}</td>
		<td class="left" style="padding:3px;">{$Users.onlinetime}</td>
		<td class="left" style="padding:3px;">{$Users.url}</td>
		</tr>{if !$Users@last}<tr>{/if}
	{/foreach}
</table>
{include file="overall_footer.tpl"}