{include file="overall_header.tpl"}
<center>
<form action="" method="post">
    <input type="hidden" name="mode" value="send">
    <table width="70%" cellpadding="2" cellspacing="2">
        <tr>
            <th colspan="8">Planeteneigenschaften</th>
        </tr>
        {foreach $planet_data as $Element}
            <tr>
                <td>Planet Position: {$Element.planet_position}</td>
                <td>Temperature Min: <input name="temp_min_{$Element.planet_position}" style="width: 50px" value="{$Element.temp_min}" type="number"></td>
                <td>Temperature Max: <input name="temp_max_{$Element.planet_position}" style="width: 50px" value="{$Element.temp_max}" type="number"></td>
                <td>Fields Min: <input name="fields_min_{$Element.planet_position}" style="width: 50px" value="{$Element.fields_min}" type="number"></td>
                <td>Fields Max: <input name="fields_max_{$Element.planet_position}" style="width: 50px" value="{$Element.fields_max}" type="number"></td>
                <td>Metal Bonus Percent: <input name="metalBonusPercent_{$Element.planet_position}" style="width: 50px" value="{$Element.metalBonusPercent}" type="number"></td>
                <td>Crystal Bonus Percent: <input name="crystalBonusPercent_{$Element.planet_position}" style="width: 50px" value="{$Element.crystalBonusPercent}" type="number"></td>
                <td>Deuterium Bonus Percent: <input name="deuteriumBonusPercent_{$Element.planet_position}" style="width: 50px" value="{$Element.deuteriumBonusPercent}" type="number"></td>
            </tr>
        {/foreach}
        <tr>
            <td colspan="8"><input value="{$LNG.se_save_parameters}" type="submit"></td>
        </tr>
    </table>
</form>
{include file="overall_footer.tpl"}